<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
include_once "route_slugs.php";

$route["search"] = 'main_controller/search';
//admin route
$r_admin = $custom_slug_array["admin"];
/*
//view/cdti/default_page
//เกี่ยวกับสถาบัน


$route["strategic_plan"] = 'main_controller/strategic_plan_page';
$route["annual_budget_plan"] = 'main_controller/annual_budget_plan_page';
$route["transparency"] = 'main_controller/transparency_page';

//นักเรียนนักศึกษา
$route["manual"] = 'main_controller/manual_page';
$route["service_statistic"] = 'main_controller/service_statistic_page';

//บุคลากร
$route["HR"] = 'main_controller/hr_page';
$route["work_manual"] = 'main_controller/work_manual_page';
*/
//english
//$route["English"] = 'en/home_controller/index';
$route["English"] = 'en/cdti_at_present_controller/index';
$route["English/History"] = 'en/cdti_at_present_controller/history';
$route["English/Cdti_at_present"] = 'en/cdti_at_present_controller/index';
$route["English/Business_admin"] = 'en/course_controller/business_administration';
$route["English/Industry_tech"] = 'en/course_controller/techology';
$route["English/Digital_tech"] = 'en/course_controller/digital_tech';
//do not working  yet

$route["English/high-vocational"] = 'en/course_controller/high_vocational';
$route["English/vocational"] = 'en/course_controller/vocational';

//thai

//visitor
$route["visitor/add"] = 'visitor_controller/add_visitor';
$route["visitor/get"] = 'visitor_controller/get_visitor';
$route["visitor/get_today"] = 'visitor_controller/get_today';
$route["visitor/get_yesterday"] = 'visitor_controller/get_yesterday';
$route["visitor/get_last_thirty_entries"] = 'visitor_controller/get_last_thirty_entries';
$route["visitor/get_total"] = 'visitor_controller/get_total';
$route["visitor/get_test"] = 'visitor_controller/get_test';

//contact-us
$route['contact-us'] = 'main_controller/page_contact_us';
//$route['contact-us'] = 'main_controller/page_contact_us';

// $route['default_controller'] = 'home_controller';
$route['testfn'] = 'home_controller/testfn';
// $route['testfn'] = 'home_controller';
$route['testss'] = 'test_controller/index';
$route["testnews"] = 'test_controller/updatenews';
$route["testtea"] = 'test_controller/updateteacher';

$route['get_all_tags'] = 'main_controller/get_all_tags';
// $route['default_controller'] = 'econ_controller/index';
$route['default_controller'] = 'main_controller/index';
// $route['404_override'] = 'econ_controller/error_404';
$route['404_override'] = 'main_controller/error_404';
$route['translate_uri_dashes'] = FALSE;
$route['index'] = 'home_controller/index';
// $route['error-404'] = 'econ_controller/error_404';
$route['error-404'] = 'main_controller/error_404';

$route['posts'] = 'main_controller/posts';
$route['featured-posts'] = 'main_controller/featured_posts';

// $route['gallery'] = 'home_controller/gallery';
// $route['login'] = 'home_controller/gallery';
$route['login-member'] = 'login_controller/member';
$route['contact'] = 'home_controller/contact';
$route['teams'] = 'main_controller/teams';
// $route['isp'] = 'main_controller/isp';
// $route['isp/(:num)'] = 'main_controller/isp/$1';
$route['isp-detail/(:any)'] = 'main_controller/isp_detail/$1';
$route['fac/(:any)/(:any)'] = 'main_controller/fac/$1/$2';
$route['fac-reward/(:any)/(:any)'] = 'main_controller/fac_reward/$1/$2';
$route['fac-gellery/(:any)/(:any)'] = 'main_controller/fac_gallery/$1/$2';

$route['general/(:any)/(:any)'] = 'main_controller/general/$1/$2';
$route['school/(:any)/(:any)'] = 'main_controller/school/$1/$2';
$route['office/(:any)/(:any)'] = 'main_controller/office/$1/$2';

$route['personnel-administration'] = 'main_controller/personnel_administration';
$route['personnel-student-affairs'] = 'main_controller/student_affairs';
$route['personnel-professional-academic'] = 'main_controller/professional_academic';

// $route['category/(:any)'] = 'home_controller/category/$1';
// $route['category/(:any)'] = 'econ_controller/category/$1';
$route['category/(:any)'] = 'main_controller/category/$1';
// $route['events'] = 'econ_controller/events';
$route['events'] = 'main_controller/events';
// $route['contactus'] = 'econ_controller/contactus';
$route['contactus'] = 'main_controller/contactus';
// $route['event/(:any)'] = 'econ_controller/event/$1';
$route['event/(:any)'] = 'main_controller/event/$1';
$route['tag/(:any)'] = 'main_controller/tag/$1';
$route['news/(:any)'] = 'main_controller/news/$1';
$route['reading-list'] = 'home_controller/reading_list';
$route['search'] = 'main_controller/search';
$route['download-audio']['post'] = 'home_controller/download_audio';

//rss routes
$route['rss-feeds'] = 'home_controller/rss_feeds';
$route['rss/latest-posts'] = 'home_controller/rss_lamain_posts';
$route['rss/category/(:any)'] = 'home_controller/rss_by_category/$1';

//auth routes
$route['register'] = 'auth_controller/register';
$route['change-password'] = 'auth_controller/change_password';
$route['reset-password'] = 'auth_controller/reset_password';

//profile routes
$route['profile/(:any)'] = 'profile_controller/profile/$1';
$route['settings'] = 'profile_controller/update_profile';
$route['settings/social-accounts'] = 'profile_controller/social_accounts';
$route['settings/change-password'] = 'profile_controller/change_password';
$route['confirm'] = 'auth_controller/confirm';
$route['logout'] = 'auth_controller/logout';
$route["unsubscribe"] = 'home_controller/unsubscribe';

$route['tag/(:any)'] = 'main_controller/tag/$1';



// $route["test"] = 'main_controller/index';
// $route["custom"] = 'main_controller/custom';
// $route["faqs"] = 'CustomPage_controller/faqs';
// $route["econ"] = 'econ_controller/index';
// $route["information"] = 'main_controller/information';
// $route["register"] = 'register_controller/index';
// $route["register_isp"] = 'register_controller/register_isp';

//คณะและสาขา course,faculty,branch Course_controller.php
$route['bachelor/branch/(:any)'] = 'course_controller/branch/$1/';
$route['branch/(:any)'] = 'course_controller/branch/$1/';
$route['bachelor/(:any)/about'] = 'course_controller/faculty/$1/bachelor/about';
$route['bachelor/(:any)/branch'] = 'course_controller/faculty/$1/bachelor/branch';


//เอกสารอัพโหลด
$route['document/(:any)'] = 'main_controller/document/$1';

//event_calendar
$route['event_calendar'] = 'main_controller/event_calendar';
$route['event_calendar_date/(:any)'] = 'main_controller/event_calendar_date/$1';

//เอกสารเผยแพร่
$route['public/(:any)'] = 'main_controller/public/$1';
/*
*-------------------------------------------------------------------------------------------------
* ADMIN ROUTES
*-------------------------------------------------------------------------------------------------
*/

$route[$r_admin] = 'admin_controller/index';

// $route[$r_admin . "/"] = 'login_controller/index';
//oit
$route[$r_admin . "/oit"] = 'admin_controller/oit';
$route[$r_admin . "/monthly-view/(:num)"] = 'admin_controller/monthly_view_with_count/$1';
//$route[$r_admin . "/testOit"] = 'admin_controller/testFilePdf';
$route[$r_admin . "/delete_oit"] = 'admin_controller/delete_oit';
//$route[$r_admin . "/update_oit_put"] = 'admin_controller/update_oit_put';
//$route[$r_admin . "/1"] = 'admin_controller/oit';

$route[$r_admin . "/login"] = 'login_controller/index';
$route[$r_admin . "/test"] = 'admin_controller/test';

$route[$r_admin . "/navigation"] = 'admin_controller/navigation';
$route[$r_admin . "/navigation-econ"] = 'admin_controller/navigation_econ';
$route[$r_admin . "/update-menu-link/(:num)"] = 'admin_controller/update_menu_link/$1';
$route[$r_admin . "/update-menu-link-econ/(:num)"] = 'admin_controller/update_menu_link_econ/$1';
$route[$r_admin . "/preferences"] = 'admin_controller/preferences';
//page
$route[$r_admin . "/add-page"] = 'page_controller/add_page';
$route[$r_admin . "/update-page/(:num)"] = 'page_controller/update_page/$1';
$route[$r_admin . "/update-page/(:num)/(:any)"] = 'page_controller/update_page_tab/$1/$2';
$route[$r_admin . "/pages"] = 'page_controller/pages';
//page-fac
$route[$r_admin . "/add-page-fac"] = 'page_controller/add_page_fac';
$route[$r_admin . "/update-page_fac/(:num)"] = 'page_controller/update_page/$1';
$route[$r_admin . "/pages-fac"] = 'page_controller/pages_fac';
//post
$route[$r_admin . "/add-post"] = 'post_controller/add_post';
$route[$r_admin . "/add-post-event"] = 'post_events_controller/add_post';
$route[$r_admin . "/add-video"] = 'post_controller/add_video';
$route[$r_admin . "/add-audio"] = 'post_controller/add_audio';
$route[$r_admin . "/posts"] = 'post_controller/posts';
$route[$r_admin . "/posts-econ"] = 'post_controller/posts_econ';
$route[$r_admin . "/post-events"] = 'post_events_controller/posts';
$route[$r_admin . "/post-events-econ"] = 'post_events_controller/posts_econ';
$route[$r_admin . "/slider-posts"] = 'post_controller/slider_posts';
$route[$r_admin . "/featured-posts"] = 'post_controller/featured_posts';
$route[$r_admin . "/breaking-news"] = 'post_controller/breaking_news';
$route[$r_admin . "/recommended-posts"] = 'post_controller/recommended_posts';
$route[$r_admin . "/pending-posts"] = 'post_controller/pending_posts';
$route[$r_admin . "/scheduled-posts"] = 'post_controller/scheduled_posts';
$route[$r_admin . "/drafts"] = 'post_controller/drafts';
$route[$r_admin . "/update-post/(:num)"] = 'post_controller/update_post/$1';
$route[$r_admin . "/update-post-event/(:num)"] = 'post_events_controller/update_post/$1';
//manul
$route[$r_admin . "/manual"] = 'manual_controller/posts';
$route[$r_admin . "/add-manual"] = 'manual_controller/add_post';
$route[$r_admin . "/update-manual/(:num)"] = 'manual_controller/update_post/$1';
//petition_forms
$route[$r_admin . "/petition_form"] = 'petition_form_controller/posts';
$route[$r_admin . "/add-petition_form"] = 'petition_form_controller/add_post';
$route[$r_admin . "/update-petition_form/(:num)"] = 'petition_form_controller/update_post/$1';
//personnel
$route[$r_admin . "/administration"] = 'personnel_administration_controller/update_post/1177'; //ฝ่ายบริหาร
$route[$r_admin . "/student-affairs"] = 'personnel_student_affairs/update_post/1178'; //กิจการนักเรียน
$route[$r_admin . "/professional-academic"] = 'personnel_professional_academic/update_post/1179'; //วิชาการวิชาชีพ
//$route[$r_admin . "/administration"] = 'personnel_administration_controller/add_post'; //ฝ่ายบริหาร
//$route[$r_admin . "/student-affairs"] = 'personnel_student_affairs/add_post'; //กิจการนักเรียน
//$route[$r_admin . "/professional-academic"] = 'personnel_professional_academic/add_post'; //วิชาการวิชาชีพ
//rss
$route[$r_admin . "/import-feed"] = 'rss_controller/import_feed';
$route[$r_admin . "/feeds"] = 'rss_controller/feeds';
$route[$r_admin . "/update-feed/(:num)"] = 'rss_controller/update_feed/$1';
$route["cron/update-feeds"] = 'cron_controller/check_feed_posts';
$route["cron/update-sitemap"] = 'cron_controller/update_sitemap';
//category

$route[$r_admin . "/categories"] = 'category_controller/categories';
$route[$r_admin . "/categories"] = 'category_controller/categories';
$route["categories-data"] = 'category_controller/categories_data';

$route[$r_admin . "/subcategories"] = 'category_controller/subcategories';
$route[$r_admin . "/update-category/(:num)"] = 'category_controller/update_category/$1';
$route[$r_admin . "/update-subcategory/(:num)"] = 'category_controller/update_subcategory/$1';
//widget
$route[$r_admin . "/add-widget"] = 'widget_controller/add_widget';
$route[$r_admin . "/widgets"] = 'widget_controller/widgets';
$route[$r_admin . "/update-widget/(:num)"] = 'widget_controller/update_widget/$1';
//poll
$route[$r_admin . "/add-poll"] = 'poll_controller/add_poll';
$route[$r_admin . "/polls"] = 'poll_controller/polls';
$route[$r_admin . "/update-poll/(:num)"] = 'poll_controller/update_poll/$1';
//gallery
$route[$r_admin . "/gallery-categories"] = 'category_controller/gallery_categories';
$route[$r_admin . "/update-gallery-category/(:num)"] = 'category_controller/update_gallery_category/$1';

$route[$r_admin . "/gallery-main-categories"] = 'category_controller/gallery_main_categories';
$route[$r_admin . "/update-gallery-main-category/(:num)"] = 'category_controller/update_gallery_maincategory/$1';

$route[$r_admin . "/gallery-images"] = 'gallery_controller/images';
$route[$r_admin . "/update-gallery-image/(:num)"] = 'gallery_controller/update_gallery_image/$1';
//contact
$route[$r_admin . "/contact-messages"] = 'admin_controller/contact_messages';
//newsletter
$route[$r_admin . "/send-email-subscribers"] = 'admin_controller/send_email_subscribers';
$route[$r_admin . "/subscribers"] = 'admin_controller/subscribers';
//comments
$route[$r_admin . "/comments"] = 'admin_controller/comments';
//ad_spaces
$route[$r_admin . "/ad-spaces"] = 'admin_controller/ad_spaces';
$route[$r_admin . "/font-options"] = 'admin_controller/font_options';
$route[$r_admin . "/seo-tools"] = 'admin_controller/seo_tools';
$route[$r_admin . "/social-login-configuration"] = 'admin_controller/social_login_configuration';
$route[$r_admin . "/cache-system"] = 'admin_controller/cache_system';
$route[$r_admin . "/preferences"] = 'admin_controller/preferences';

$route[$r_admin . "/links-settings"] = 'gallery_controller/links';
$route[$r_admin . "/update-links-settings/(:num)"] = 'gallery_controller/update_links_settings/$1';

$route[$r_admin . "/teams-settings"] = 'gallery_controller/teams';
$route[$r_admin . "/update-teams-settings/(:num)"] = 'gallery_controller/update_teams_settings/$1';

$route[$r_admin . "/howto-settings"] = 'gallery_controller/howtos';
$route[$r_admin . "/update-howtos-settings/(:num)"] = 'gallery_controller/update_howtos_settings/$1';

$route[$r_admin . "/banners-settings"] = 'gallery_controller/banners';
$route[$r_admin . "/update-banners-settings/(:num)"] = 'gallery_controller/update_banners_settings/$1';

$route[$r_admin . "/landingpages-settings"] = 'gallery_controller/landingpages';
$route[$r_admin . "/update-landingpages-settings/(:num)"] = 'gallery_controller/update_landingpages_settings/$1';
//language
$route[$r_admin . "/language-settings"] = 'language_controller/languages';
$route[$r_admin . "/update-language/(:num)"] = 'language_controller/update_language/$1';
$route[$r_admin . "/update-phrases/(:num)"] = 'language_controller/update_phrases/$1';
$route[$r_admin . "/visual-settings"] = 'admin_controller/visual_settings';
$route[$r_admin . "/email-settings"] = 'admin_controller/email_settings';
$route[$r_admin . "/settings"] = 'admin_controller/settings';
$route[$r_admin . "/settings-econ"] = 'admin_controller/settings_econ';
//users
$route[$r_admin . "/users"] = 'admin_controller/users';
$route[$r_admin . '/administrators'] = 'admin_controller/administrators';
$route[$r_admin . '/add-user'] = 'admin_controller/add_user';
$route[$r_admin . '/edit-user/(:num)'] = 'admin_controller/edit_user/$1';
$route[$r_admin . '/roles-permissions'] = 'admin_controller/roles_permissions';
$route[$r_admin . '/edit-role/(:num)'] = 'admin_controller/edit_role/$1';
$route[$r_admin . "/email-preview"] = 'admin_controller/email_preview';


$route[$r_admin . "/open-isp"] = 'open_controller/isp';
$route[$r_admin . "/open-sme"] = 'open_controller/sme';
$route[$r_admin . "/open-project"] = 'open_controller/project';

$route[$r_admin . "/thematic-member"] = 'thematic_controller/member';
$route[$r_admin . "/thematic-project"] = 'thematic_controller/project';


$route[$r_admin . "/faq-settings"] = 'faq_controller/faq_setting';
$route[$r_admin . "/update-faq/(:num)"] = 'faq_controller/update_faq/$1';


$route[$r_admin . "/participants-settings"] = 'participants_controller/participants_setting';
$route[$r_admin . "/update-participants/(:num)"] = 'participants_controller/update_participants/$1';

$route[$r_admin . "/announce-settings"] = 'announce_controller/announce_setting';
$route[$r_admin . "/update-announce/(:num)"] = 'announce_controller/update_announce/$1';

$route[$r_admin . "/industry-settings"] = 'industry_controller/industry_setting';
$route[$r_admin . "/update-industry/(:num)"] = 'industry_controller/update_industry/$1';
$route[$r_admin . "/subindustry-settings"] = 'industry_controller/subcategories';
$route[$r_admin . "/update-subindustry/(:num)"] = 'industry_controller/update_subindustry/$1';

$route[$r_admin . "/ebooks"] = 'emagazine_controller/ebooks_setting';

//emagazine
$route[$r_admin . "/emagazine"] = 'emagazine_controller/posts';
$route[$r_admin . "/add-emagazine"] = 'emagazine_controller/add_post';
$route[$r_admin . "/update-emagazine/(:num)"] = 'emagazine_controller/update_post/$1';
$route[$r_admin . "/del-emagazine/(:any)"] = 'emagazine_controller/delete_directory/$1';

// //petition_forms
// $route[$r_admin . "/petition_form"] = 'petition_form_controller/posts';
// $route[$r_admin . "/add-petition_form"] = 'petition_form_controller/add_post';
// $route[$r_admin . "/update-petition_form/(:num)"] = 'petition_form_controller/update_post/$1';

/*
*-------------------------------------------------------------------------------------------------
* DYNAMIC ROUTES
*-------------------------------------------------------------------------------------------------
*/

require_once(BASEPATH . 'database/DB.php');
$db =& DB();
$general_settings = $db->get('general_settings')->row();
$languages = $db->get('languages')->result();
foreach ($languages as $language) {
    if ($language->status == 1 && $general_settings->site_lang != $language->id) {
        $key = $language->short_form;
        $route[$key] = "$key/eng_controller/index";
        $route[$key . '/posts'] = 'main_controller/posts';
        $route[$key . '/gallery'] = 'main_controller/gallery';

        $route[$key . '/contact'] = 'main_controller/contact';
        $route[$key . '/teams']   = 'main_controller/teams';
        $route[$key . '/category/(:any)'] = 'main_controller/category/$1';

        $route[$key . '/tag/(:any)'] = 'main_controller/tag/$1';
        $route[$key . '/reading-list'] = 'main_controller/reading_list';
        $route[$key . '/search'] = 'main_controller/search';
        $route[$key . '/rss-feeds'] = 'main_controller/rss_feeds';
        $route[$key . '/rss/latest-posts'] = 'main_controller/rss_lamain_posts';
        $route[$key . '/rss/category/(:any)'] = 'main_controller/rss_by_category/$1';
        $route[$key . '/register'] = 'auth_controller/register';
        $route[$key . '/change-password'] = 'auth_controller/change_password';
        $route[$key . '/reset-password'] = 'auth_controller/reset_password';
        $route[$key . '/profile/(:any)'] = 'profile_controller/profile/$1';
        $route[$key . '/settings'] = 'profile_controller/update_profile';
        $route[$key . '/settings/social-accounts'] = 'profile_controller/social_accounts';
        $route[$key . '/settings/change-password'] = 'profile_controller/change_password';
        $route[$key . '/confirm'] = 'auth_controller/confirm';
        $route[$key . '/logout'] = 'auth_controller/logout';
        $route[$key . '/unsubscribe'] = 'main_controller/unsubscribe';
        $route[$key . '/fac/(:any)/(:any)'] = 'main_controller/fac/$1/$2';
        $route[$key . '/(:any)'] = 'main_controller/any/$1';
    }
}

$route['(:any)'] = 'main_controller/any/$1';
$route['post_cache/(:any)'] = 'main_controller/post_cache/$1';
