
<?php $this->load->view('cdti/layout/category_topbar',['category' => isset($category)?$category:'','subcategory' => isset($subcategory)?$subcategory:'','title' => (isset($category->name))?html_escape($category->name):$title])?>
 
<div class="bg-light">
	<section id="news-heighlight" class="mini-spacer feature7 news">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-8">
              <div class="section-header p-t-20 p-b-20 m-b-20">
                  <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i>  <?php echo html_escape($title); ?></h3>
                  <!-- <a href="" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a> -->
              </div>
                <div class="row mb-4">
                   <?php foreach ($posts as $key => $post): ?>
              			    <div class="col-12 col-sm-6 col-lg-6  wrap-feature7-box  wrap-feature7-box-small">
                              <?php $this->load->view('cdti/include/post/post_small',['data' => $post, 'height' => 215]);?>
              			    </div>
                  	<?php endforeach; ?>
        			</div>
        			<div class="row mb-4">
        				<div class="col-md-10 mx-auto text-center">
                    <?php echo $this->pagination->create_links(); ?>
        				</div>
        			</div>
        	</div>
          <div class="col-md-12 col-lg-4">
  					  <div class="sidebar">
                  <?php $this->load->view("cdti/include/post/sidebar_featured_post",['data' => $featured_posts]);?>
              </div>
  				</div>
        </div>
      </div>
  </section>
</div>
