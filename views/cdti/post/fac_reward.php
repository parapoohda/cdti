<link href="<?php echo base_url(); ?>cdti_assets/node_modules/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
 <div class="bg-white">
	<section id="news-detail" class="news mini-spacer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8">
		      <div class="section-header p-t-20 p-b-20 m-b-20">
            <h3 class="mr-auto mb-2"><?php echo html_escape($post->title); ?></h3>
            <p class="text-muted mb-0 d-inline"><span><i class="fas fa-stopwatch"></i> เผยแพร่เมื่อ : <?php echo helper_date_format($post->created_at); ?></span> <span><i class="fas fa-book-open"></i> จำนวนผู้ชม : <?php echo $post->hit; ?> คน</span></p>
            <?php $this->load->view('cdti/include/post/_post_share_box')?>
          </div>
          <div class="news-detail">
          	<div class="row mb-4">
          		<div class="col-sm-12">
                  <?php if ($post->post_type == "video"): ?>
                      <?php $this->load->view('cdti/include/post/_post_details_video', ['post' => $post]); ?>
                  <?php elseif ($post->post_type == "audio"): ?>
                      <?php $this->load->view('cdti/include/post/_post_details_audio', ['post' => $post]); ?>
                  <?php else: ?>
                      <?php $this->load->view('cdti/include/post/_post_details', ['post' => $post]); ?>
                  <?php endif; ?>
          		</div>
          	</div>
          	<div class="row">
          		<div class="col-sm-12">
          		       <?php echo $post->content; ?>
          		</div>
          	</div>
          </div>
          <div class="news-gallery portfolio4 mb-4 ">
              <?php if ($post_image_count > 0) : ?>
            	<div class="section-header filterby p-t-20 p-b-20 m-b-20">
	                <h3 class="mr-auto mb-2 filter-items active" data-names="*">
	                	<a href="javascript:void(0);"  class="filter-link active">อัลบั้ม</a>
	                </h3>
	            </div>
	            <div id="" class="mb-4">
	               	<div class="row gallery-box popup-gallery portfolio-box4">
                   	<?php foreach ($post_images as $image): ?>
                         <div class="col-lg-4 col-md-6 wrap-feature2-box filter mb-1">
                             	<a href="<?php echo base_url() . html_escape($image->image_default); ?>" title="<?php echo html_escape($post->title); ?>">
                               <img src="<?php echo base_url() . html_escape($image->image_default); ?>" class="img-fluid" alt="<?php echo html_escape($post->title); ?>"/>
                             	</a>
                         </div>
                     	<?php endforeach; ?>
	               	</div>
	            </div>
              <?php endif; ?>

             <?php $this->load->view('cdti/include/list_post_tag',['categories' => $post_tags]);?>
          </div>
				</div>
				<div class="col-md-12 col-lg-4">
					<div class="sidebar">
                		<?//php $this->load->view("cdti/include/post/sidebar_featured_post",['data' => $featured_posts]);?>
                		<?php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
           </div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url()?>cdti_assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo base_url()?>cdti_assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/magnific-popup/dist/jquery.magnific-popup.js"></script>
<script>
   window.onscroll = function() {clickloadmore()};

  function clickloadmore() {
    if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
      $('#load-more').trigger("click");
    }
  }
</script>
<script>
 $(document).ready(function(){
  // filter items on button click
  $('.portfolio4 .filterby').on('click', 'a', function() {

      var filterValue = $(this).attr('data-filter');

      $(".filterby .active").removeClass("active");
      $(".filterby").find("li[data-names=\"" + filterValue + "\"]").addClass("active");

      $grid.isotope({ filter: filterValue })

  });
  // init Isotope
  var $grid = $('.portfolio-box4').isotope({
      itemSelector: '.filter',
      percentPosition: true,
      masonry: {
          columnWidth: '.filter',
      }
  });
  //****************************
    // Isotope Load more button
    //****************************
    var initShow = 6; //number of images loaded on init & onclick load more button
    var counter = initShow; //counter for load more button
    var iso = $grid.data('isotope'); // get Isotope instance

    loadMore(initShow); //execute function onload

    function loadMore(toShow) {
      $grid.find(".hidden").removeClass("hidden");

      var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
        return item.element;
      });
      $(hiddenElems).addClass('hidden');
      $grid.isotope('layout');

      //when no more to load, hide show more button
      if (hiddenElems.length == 0) {
        $("#load-more").hide();
      }
      else {
        $("#load-more").hide();
      };
      // append load more button
      $grid.after('<div class="text-center"><a id="load-more" class="btn btn-info btn-md btn-arrow m-t-20" href="javascript:void(0)" style="opacity:0;"> <span>โหลดเพิ่ม <i class="ti-arrow-right"></i></span></a></div>');

      //when load more button clicked
      $(".portfolio4 #load-more").click(function() {
        if ($('#filters').data('clicked')) {

          counter = initShow;
          j$('#filters').data('clicked', false);
        } else {
          counter = counter;
        };

        counter = counter + initShow;

        loadMore(counter);
      });

    }


});

$('.popup-gallery').magnificPopup({
  delegate: 'a',
  type: 'image',
  tLoading: 'Loading image #%curr%...',
  mainClass: 'mfp-img-mobile',
  gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  },
  image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      titleSrc: function(item) {
          return item.el.attr('title');
      }
  }
});

</script>
