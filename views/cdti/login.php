<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en" class="bg-light">
<head>
    <!--test3-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo html_escape($title); ?> - <?php echo html_escape($settings->site_title); ?></title>
    <meta name="description" content="<?php echo html_escape($description); ?>"/>
    <meta name="keywords" content="<?php echo html_escape($keywords); ?>"/>
    <meta name="author" content="NIA"/>
    <meta property="og:locale" content="th_TH"/>
    <meta property="og:site_name" content="<?php echo $settings->application_name; ?>"/>
    
    <meta property="og:image:alt" content="http://www.cdti.ac.th/uploads/logo/logo_5cee057a0dd37.png"/>
    <meta property="og:image:secure_url" content="http://www.cdti.ac.th/uploads/logo/logo_5cee057a0dd37.png"/>
    <meta property="og:image" content="http://www.cdti.ac.th/uploads/logo/logo_5cee057a0dd37.png"/>
    
    <meta property="og:url" content="https://www.cdti.ac.th"/>
    <?php if (isset($post_type)): ?>
    <meta property="og:type" content="<?php echo $og_type; ?>"/>
    <meta property="og:title" content="<?php echo html_escape($og_title); ?>"/>
    <meta property="og:description" content="<?php echo $og_description; ?>"/>
    
    
    <meta property="article:author" content="<?php echo $og_author; ?>"/>
    <meta property="fb:app_id" content="<?php echo $this->general_settings->facebook_app_id; ?>"/>
<?php foreach ($og_tags as $tag): ?>
    <meta property="article:tag" content="<?php echo $tag->tag; ?>"/>
<?php endforeach; ?>
    <meta property="article:published_time" content="<?php echo $og_published_time; ?>"/>
    <meta property="article:modified_time" content="<?php echo $og_modified_time; ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@<?php echo html_escape($settings->application_name); ?>"/>
    <meta name="twitter:creator" content="@<?php echo html_escape($og_creator); ?>"/>
    <meta name="twitter:title" content="<?php echo html_escape($post->title); ?>"/>
    <meta name="twitter:description" content="<?php echo html_escape($post->summary); ?>"/>
    <meta name="twitter:image" content="<?php echo $og_image; ?>"/>
<?php else: ?>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo html_escape($title); ?> - <?php echo html_escape($settings->site_title); ?>"/>
    <meta property="og:description" content="<?php echo html_escape($description); ?>"/>

    <meta property="fb:app_id" content="<?php echo $this->general_settings->facebook_app_id; ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@<?php echo html_escape($settings->application_name); ?>"/>
    <meta name="twitter:title" content="<?php echo html_escape($title); ?> - <?php echo html_escape($settings->site_title); ?>"/>
    <meta name="twitter:description" content="<?php echo html_escape($description); ?>"/>
<?php endif; ?>
    <meta name="google-signin-client_id" content="<?php echo $general_settings->google_client_id ?>">
    <link rel="canonical" href="<?php echo current_url(); ?>"/>
    <?php if ($general_settings->multilingual_system == 1):
    foreach ($languages as $language):
    if ($language->id == $site_lang->id):?>
        <link rel="alternate" href="<?php echo base_url(); ?>" hreflang="<?php echo $language->language_code ?>"/>
    <?php else: ?>
        <link rel="alternate" href="<?php echo base_url() . $language->short_form . "/"; ?>" hreflang="<?php echo $language->language_code ?>"/>
    <?php endif; endforeach; endif; ?>
    <link rel="shortcut icon" type="image/png" href="<?php echo get_favicon($vsettings); ?>"/>
    <?php echo $primary_font_url; ?>
    <?php echo $secondary_font_url; ?>
    <?php echo $tertiary_font_url; ?>
    <?php if (isset($post_type) && $post_type == "audio"): ?>
        <link href="<?php echo base_url(); ?>assets/vendor/audio-player/css/amplitude.min.css" rel="stylesheet"/>
    <?php endif; ?>
    <?php if (isset($post_type) && $post_type == "video"): ?>
        <link href="<?php echo base_url(); ?>assets/vendor/video-player/video-js.min.css" rel="stylesheet"/>
    <?php endif; ?>
    <?php if ($vsettings->site_color == '') : ?>
        <link href="<?php echo base_url(); ?>assets/css/colors/default.min.css" rel="stylesheet"/>
    <?php else : ?>
        <link href="<?php echo base_url(); ?>assets/css/colors/<?php echo html_escape($vsettings->site_color); ?>.css" rel="stylesheet"/>
    <?php endif; ?>
    <?php echo $general_settings->google_analytics; ?>
    <?php echo $general_settings->head_code; ?>
    <?php if ($selected_lang->text_direction == "rtl"): ?>
        <script>var rtl = true;</script>
    <?php else: ?>
        <script>var rtl = false;</script>
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/font-awesome/css/font-awesome.min.css">

    <!-- link old -->
    <link href="<?php echo base_url() ?>front-assets/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>front-assets/node_modules/aos/dist/aos.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>front-assets/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>front-assets/node_modules/owl.carousel/dist/assets/owl.theme.green.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url() ?>front-assets/css/demo.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>front-assets/css/testimonial1-10.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>front-assets/node_modules/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>front-assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>front-assets/css/yourstyle.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?Php echo base_url() ?>app-assets/vendors/css/extensions/sweetalert.css">
    <link href="<?php echo base_url() ?>front-assets/css/dropzone.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>front-assets/css/basic.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>front-assets/css/customs.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>front-assets/icons/themify-icons/themify-icons.css" rel="stylesheet" type="text/css">
 </head>

    <body class="">
     <div class="preloader">
         <div class="loader">
             <div class="loader__figure"></div>
             <p class="loader__label">Admin Console</p>
         </div>
     </div>
     <div id="main-wrapper" class="bg-light">
         <div class="page-wrapper">
             <div class="container-fluid">
               <div class="spacer form5" style="">
                    <div class="container">
                        <!-- Row -->
                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-md-7 text-center both-space">
                                <div class="card aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                                    <div class="card-body">
                                        <div class="text-box">
                                            <h2 class="title font-light"><span class="font-stylish">Admin</span> <br>Console</h2>
                                            <div id="result-login"></div>
                                            <form id="form-login" class="m-t-30">
                                                      <div class="form-group row">
                                                          <div class="col-12">
                                                              <input class="form-control"
                                                                    type="text"
                                                                    placeholder="Email Address"
                                                                    name="email" id="email"
                                                                    required
                                                                    data-validation-required-message="Email Address"
                                                                    >
                                                                <p class="help-block text-danger"></p>
                                                          </div>
                                                      </div>

                                                      <div class="form-group row">
                                                          <div class="col-12">
                                                              <input class="form-control"
                                                                    type="password"
                                                                    placeholder="Password"
                                                                    name="password" id="password"
                                                                    required
                                                                    data-validation-required-message="Password"
                                                                    >
                                                                <p class="help-block text-danger"></p>
                                                          </div>
                                                      </div>

                                                      <div class="form-group row">
                                                          <div class="col-12">
                                                              <button type="submit" style="padding: 11px 25px;" class="btn btn-md btn-block btn-danger-gradiant btn-arrow"><span> เข้าสู่ระบบ <i class="ti-arrow-right"></i></span></button>
                                                          </div>
                                                      </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
    </div>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url() ?>front-assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url() ?>front-assets/node_modules/popper/dist/popper.min.js"></script>
    <script src="<?php echo base_url() ?>front-assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- This is for the animation -->
    <script src="<?php echo base_url() ?>front-assets/node_modules/aos/dist/aos.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins.js"></script>
    <?php if (!isset($_COOKIE["vr_cookies"]) && $settings->cookies_warning): ?>
        <div class="cookies-warning">
            <div class="text"><?php echo $this->settings->cookies_warning_text; ?></div>
            <a href="javascript:void(0)" onclick="hide_cookies_warning();" class="icon-cl"> <i class="icon-close"></i></a>
        </div>
    <?php endif; ?>
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var fb_app_id = '<?php echo $this->general_settings->facebook_app_id; ?>';
        var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
        var csfr_cookie_name = '<?php echo $this->config->item('csrf_cookie_name'); ?>';
    </script>

    <script src="<?php echo base_url() ?>front-assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>front-assets/node_modules/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url() ?>front-assets/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>front-assets/js/testimonial.js"></script>

    <?php if(isset($load_js)): foreach($load_js as $key =>  $js): ?>
    <script src="<?php echo base_url() ?>app-assets/vendors/js/<?php echo $js; ?>" type="text/javascript"></script>
    <?php endforeach; endif; ?>
    <script src="<?php echo base_url() ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var a;
            $("#form-login").submit(function (d) {
                d.preventDefault();
                if (a) {
                    a.abort()
                }
                var b = $(this);
                var c = b.find("input, select, button, textarea");
                var e = b.serializeArray();
                e.push({name: csfr_token_name, value: $.cookie(csfr_cookie_name)});
                a = $.ajax({url: base_url + "auth_controller/login_post", type: "post", data: e,});
                a.done(function (f) {
                    c.prop("disabled", false);
                    if (f == "success") {
                        location.reload()
                    } else if (f.indexOf("<div class=\"error-message\">") >= 0) {
                        document.getElementById("result-login").innerHTML = f
                    }
                })
            })
        });
    </script>
    </body>

</html>
