<div class="bg-primary">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <p class="text-white m-t-20">
         E-magazine
        </p>
      </div>
    </div>
  </div>
</div>
<div class="bg-light">
	<section id="news-heighlight" class="mini-spacer feature7 news">
        <div class="container">
            <div class="section-header p-t-20 p-b-20 m-b-20">
                <h3 class="mr-auto d-inline"><i class="fas fa-tags"></i>  E-magazine </h3>
            </div>
              <div class="row mb-4">
                 <?php foreach ($posts as $key => $post): ?>
            			    <div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
                            <?php $this->load->view('cdti/include/emagazine/post_full',['data' => $post , 'height' => 350]);?>
            			    </div>
                	<?php endforeach; ?>
      			</div>
			<div class="row mb-4">
				<div class="col-md-10 mx-auto text-center">
            <?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
        </div>

    </section>
</div>
