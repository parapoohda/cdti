
 <?php $this->load->view('cdti/include/banner',['banners' => $banners])?>

 <div class="bg-white">
     <section id="dreegree">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-lg-3 col-sm-6 p-0">
                     <div class="card card-class card-vocation card-shadow aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                         <a href="#" class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/1.png" alt="wrappixel kit"></a>
                         <div class="card-overlay text-center">
                             <h5 class="font-medium m-b-0">ระดับวิชาชีพ (ปวช.)</h5>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-sm-6 p-0">
                     <div class="card card-class card-vocation-heigh card-shadow aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                         <a href="#" class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/2.png" alt="wrappixel kit"></a>
                         <div class="card-overlay text-center">
                             <h5 class="font-medium m-b-0">ระดับวิชาชีพ (ปวส.)</h5>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-sm-6 p-0">
                     <div class="card card-class card-bachelor card-shadow aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                         <a href="#" class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/3.png" alt="wrappixel kit"></a>
                         <div class="card-overlay text-center">
                             <h5 class="font-medium m-b-0">ระดับปริญญาตรี</h5>
                         </div>
                     </div>
                 </div>

                 <div class="col-lg-3 col-sm-6 p-0">
                      <div class="card card-class card-register card-shadow aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                         <a href="<?php echo (date("Y-m-d H:i:s")  <= $vsettings->regis_date)?'##':'#'?>" class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/register.png" alt="wrappixel kit"></a>
                         <div class="card-overlay text-center">
                             <h5 class="font-medium m-b-0"><?php echo (date("Y-m-d H:i:s")  <= $vsettings->regis_date)?trans('enroll'):trans('enroll_close')?></h5>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <!-- พระราโชวาช start -->
        <?php
          if(isset($royal_speech) and $royal_speech->visibility == 1) {
              $this->load->view($royal_speech->link,['data' => $royal_speech]);
          }
        ?>
    <!-- พระราโชวาช start -->


      <section id="aboutus" class="" style="">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-10 col-lg-6 bg-custom-blue-light spacer">
                      <div class="row">
                          <div class="col-sm-12 col-md-10 col-lg-8 ml-auto">
                              <div class="about-content spacer" >
                                  <h3 class="about-title mb-3">
                                      CDTI สถาบันเทคโนโลยีจิตรลดา
                                  </h3>
                                  <hr class="mb-3">
                                  <div class="about-detail">
                                      <p class="text-white m-b-60">
                                          “สถาบันที่จัดการศึกษาตามแนวพระราชดำริ ด้านเทคโนโลยีบนฐานวิทยาศาสตร์ ในรูปแบบ เรียนคู่งาน งานคู่เรียน โดยเน้น ช่างอุตสาหกรรม ธุรกิจอาหาร และศิลปะ ประยุกต์ ทั้งในระดับวิชาชีพและปริญญา เพื่อสร้างคนดี มีจิตอาสา มีทักษะอาชีพ ใฝ่รู้ สู้งาน  สร้างสรรค์นวัตกรรม สื่อสารเป็น”
                                      </p>
                                      <a href="" class="btn btn-custom-primary">เกี่ยวกับเรา</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
</div>



<div class="bg-white">
    <section id="news-heighlight" class="mini-spacer feature7 news">
        <?php $this->load->view('cdti/include/post/main_featured_post',['data' => $featured_posts]);?>
    </section>
<div>


<section id="news" class="mini-spacer feature7 news">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-9">

                <?php $this->load->view('cdti/include/post/main_post',['data' => $latest_posts]);?>

                <?php $this->load->view('cdti/include/post/channel_post',['data' => $channel_posts]);?>
            </div>
            <div class="col-lg-4 col-md-3">
                <?php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
            </div>
        </div>
    </div>
</section>

<div class="bg-white">
          <section id="gallery" class="spacer gallery">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-sm-12 col-12 text-center">
                          <div class="section-title m-b-40">
                              <h3 class="color-primary">บรรยากาศครอบครัว CDTI</h3>
                          </div>
                          <div class="row">
                              <div class="col-12 col-sm-12">
                                  <div id="gallery-slide" class="owl-carousel owl-theme">
                                      <?php if(isset($main_category1_cover) and count($main_category1_cover) > 0):?>
                                            <?php foreach ($main_category1_cover as $key => $value): ?>
                                                  <div class="item">
                                                      <div class="card card-shadow card-gallery" data-aos="flip-left" data-aos-duration="1200">
                                                          <a href="#" class="img-ho"> <img src="<?php echo base_url().$value->path_small?>" alt="" class="img-responsive"></a>
                                                          <div class="card-overlay">
                                                              <h5 class="font-medium m-b-0"><?php echo $value->category_name?></h5>
                                                              <p class="m-b-0 font-14"><?php echo $value->main_category_name?></p>
                                                              <a href="" class="btn btn-custom-primary m-t-20"><?php echo trans('view_more')?></a>
                                                          </div>
                                                      </div>
                                                  </div>
                                            <?php endforeach;?>
                                      <?php endif;?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>




      <section id="parter" class="spacer partner">
          <div class="container">
              <div class="row">
                  <div class="col-12 col-sm-12 text-center">
                      <div class="section-title m-b-40">
                          <h3 class="color-primary"><?php echo trans('home_links')?></h3>
                      </div>
                      <div class="row">
                          <div class="col-sm-12">
                              <div id="partner-slider"  class="owl-carousel owl-theme">
                                  <?php if(isset($links)):?>
                                    <?php foreach ($links as $key => $item): ?>
                                            <div class="item">
                                                <?php if($item->description != ""):?>
                                                    <a href="<?php echo $item->description;?>" target="_blank" >
                                                      <img src="<?php echo base_url() . html_escape($item->path_small); ?>" alt="<?php echo html_escape($item->title); ?>" >
                                                    </a>
                                                <?php else:?>
                                                      <img src="<?php echo base_url() . html_escape($item->path_small); ?>" alt="<?php echo html_escape($item->title); ?>" >
                                                <?php endif;?>
                                          </div>
                                    <?php endforeach;?>
                                  <?php endif;?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>


</div>
