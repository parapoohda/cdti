<?php include('views/layouts/header_inner.php'); ?>
<div class="bg-primary">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<p class="text-white m-t-20">
					ข่าวประชาสัมพันธ์
				</p>
			</div>
			<div class="col-4 ml-auto text-right">
				<ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
			</div>
		</div>
	</div>
</div>
<div class="bg-light">
	<section id="news-detail" class="news mini-spacer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8">
					<div class="section-header p-t-20 p-b-20 m-b-20">
		                <h3 class="mr-auto mb-2">กิจกรรมปฐมนิเทศสถานบันเทคโนโลยีจิตรลดาประจำปีการศึกษา 2562 บรรยากาศต้อนรับนักศึกษาใหม่</h3>
		                <p class="text-muted mb-0 d-inline"><span><i class="fas fa-stopwatch"></i> เผยแพร่เมื่อ : จันทร์ 14 มกราคม 2562</span> <span><i class="fas fa-book-open"></i> จำนวนผู้ชม : 252 คน</span></p>
		                <ul class="list-inline ml-auto social-list d-inline">
		                	<li>
		                		<a href="" class="social-icon social-icon-facebook">
		                			<i class="fab fa-facebook-f"></i>
		                		</a>
		                	</li>
		                	<li>
		                		<a href="" class="social-icon social-icon-line">
		                			<i class="fab fa-line"></i>
		                		</a>
		                	</li>
		                	<li>
		                		<a href="" class="social-icon social-icon-twitter">
		                			<i class="fab fa-twitter"></i>
		                		</a>
		                	</li>
		                </ul>
		            </div>
		            <div class="news-detail">
		            	<div class="row mb-4">
		            		<div class="col-sm-12">
		            			<img src="https://cdti.sspengine.com/uploads/images/image_380x240_5c5bf4dc3f627.jpg" alt="" class="img-responsive">
		            		</div>
		            	</div>
		            	<div class="row">
		            		<div class="col-sm-12">
		            			<p class="text-primary">ปฐมนิเทศนักศึกษา 2562 กจิกรรมปฐมนิเทศสถาบันจิตรลดาประจำปีการศึกษา 2562 บรรยากาศการต้อนรับนักศึกษาใหม่ </p>
		            			<p class="text-muted">
		            				เมื่อวันที่ 25 มิถุนายน 2561 (เวลา 08.30 น.) สถาบันเทคโนโลยีจิตรลดา จัดพิธีปฐมนิเทศนักศึกษาใหม่ ประจำปีการศึกษา 2561 ณ หอประชุมมหาวิทยาลัย
		            			</p>
		            			<p>
		            				“ในนามมหาวิทยาลัยเทคโนโลยีราชมงคลล้านนา ขอต้อนรับนักศึกษาใหม่ ปีการศึกษา 2561 ทุกคน เข้าสู่รั้วมหาวิทยาลัยแห่งนี้ ที่ ด้วยความยินดียิ่ง และในวันนี้ถือเป็นวันสำคัญ วันที่นักศึกษาใหม่ทุกคนได้ก้าวผ่าน จากการเป็น “นักเรียน” มาเป็น “นักศึกษา” ถือเป็นอีกก้าวหนึ่งก่อนที่จะออกไปสู่สังคมแน่นอนว่านักศึกษาทุกคนต้องพบกับการเปลี่ยนแปลงครั้งสำคัญครั้งหนึ่งในชีวิต เพราะการศึกษาในระบบอุดมศึกษานั้นต่างกับระบบการศึกษาที่ผ่านมา เพราะระดับอุดมศึกษานั้นได้ให้ความเป็นอิสระมากขึ้นในการเลือกวิชาเรียน เพื่อที่จะได้รับองค์ความรู้ใหม่ๆ รวมถึงการเปิดโลกทัศน์ในยุคสารสนเทศ ซึ่งการรับข่าวสารรวมถึงการเรียนรู้ในสมัยนี้ นักศึกษาต้องรู้จักคิดวิเคราะห์ สังเคราะห์ให้เป็นนั้นเป็นเรื่องสำคัญ ดังนั้นนักศึกษาควรใช้ช่วงเวลาของการเป็นนักศึกษาและโอกาสที่ได้รับในรั้วมหาวิทยาลัยแห่งนี้อย่างคุ้มค่าให้เป็นช่วงเวลาของการพัฒนาความคิด ตลอดจนการเพิ่มทักษะทางวิชาการ วิชาชีพ ที่ได้รับจากการถ่ายทอดของคณาจารย์ที่มีความมุ่งมั่น ตั้งใจ ที่จะประสิทธิประสาทวิชาองค์ความรู้ต่างๆ ให้อย่างเต็มที่ ซึ่งมหาวิทยาลัยของเรานั้นมีความพร้อม และมีมาตรฐาน ในการจัดการเรียนการสอนเป็นที่ยอมรับมาโดยตลอด มุ่งผลิตบัณฑิตให้พร้อมออกไปสู่สังคม ที่มีการเปลี่ยนแปลงของเศรษฐกิจ สังคม รวมถึงความก้าวหน้าทางด้านวิทยาศาสตร์ และเทคโนโลยี ซึ่งทางมหาวิทยาลัยมีความมุ่งหวังสร้างนักศึกษา ให้มีคุณสมบัติที่พึงประสงค์พร้อมปฏิบัติงานได้จริง มีความเป็นมืออาชีพทางด้านการทำงาน เพื่อรับใช้สังคมและประเทศชาติ ท้ายนี้ขอเป็นกำลังใจให้นักศึกษาใหม่และนักศึกษาที่ได้รับทุนการศึกษาทุกคน พึงระลึกอยู่เสมอว่าเราเป็นนักศึกษา ควรมีความรับผิดชอบ คือทำหน้าที่ของตนให้ดีที่สุด ตั้งใจพากเพียรศึกษา ประพฤติปฏิบัติตามกฎระเบียบของมหาวิทยาลัย และรู้จักแบ่งเวลาในการทำกิจกรรมต่างๆ อย่างเหมาะสม เตรียมพร้อมสู่การเป็นบัณฑิตที่ดี และสมบูรณ์แบบในอนาคตต่อไป”
		            			</p>
		            			<img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d19a284800.jpg" alt="" class="img-responsive mb-4">
		            			<p>
		            				จากนั้นเป็นการแนะนำคณะผู้บริหารและหน่วยงานต่างๆ ของมหาวิทยาลัยฯ เพื่อให้นักศึกษาใหม่ได้ทราบและติดต่อหน่วยงานต่างๆ ได้อย่างถูกต้องมอบทุนการศึกษาสำหรับนักศึกษาที่มีผลการเรียนระดับดีเยี่ยม (เกรดเฉลี่ยน 2.75 ขึ้นไป จำนวน 61 คน) และในเวลา 11.00 น. นายวรวุฒิ  เพ็รชดี นายกสโมสรนักศึกษา ประจำปีการศึกษา 2561 พร้อมด้วยประธานนักศึกษา นายกสโมทั้ง 3 คณะ นำกล่าวปฏิญาณตนเป็นอันเสร็จพิธีปฐมนิเทศประจำปีการศึกษา 2561
		            			</p>
		            			<p>สำหรับปีการศึกษา 2561 มีผู้ผ่านเข้าเกณฑ์เข้าศึกษาต่อในมหาวิทยาลัยในระดับประกาศนียบัตรวิชาชีพ (ปวช.) ประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.) และปริญญาตรีใน 3 คณะ 1 วิทยาลัย (คณะบริหารธุกริจและศิลปศาสตร์, คณะวิศวกรรมศาสตร์, คณะศิลปกรรมและสถาปัตยกรรมศาสตร์, วิทยาลัยเทคโนโลยีและสหวิทยาการ) รวมทั้งสิ้น 2,700 คน</p>
		            			
		            		</div>
		            	</div>
		            </div>
		            <div class="news-gallery mb-4">
		            	<div class="section-header p-t-20 p-b-20 m-b-20">
			                <h3 class="mr-auto mb-2">อัลบั้ม</h3>
			            </div>
			            <div id="" class="gallery-box portfolio4 mb-4">
			               <div class="row portfolio-box4 popup-gallery">
			               	 	<div class="col-lg-4 col-md-6 filter ">
	                                <a href="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg">
	                                    <img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg" alt="wrpkit" class="img-fluid card-img-top">
	                                </a>
                            	</div>
                            	<div class="col-lg-4 col-md-6 filter  ">
	                                 <a href="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg">
	                                    <img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg" alt="wrpkit" class="img-fluid card-img-top">
	                                </a>
                            	</div>
                            	<div class="col-lg-4 col-md-6 filter  ">
	                                 <a href="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg">
	                                    <img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg" alt="wrpkit" class="img-fluid card-img-top">
	                                </a>
                            	</div>
                            	<div class="col-lg-4 col-md-6 filter  ">
	                                 <a href="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg">
	                                    <img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg" alt="wrpkit" class="img-fluid card-img-top">
	                                </a>
                            	</div>
                            	<div class="col-lg-4 col-md-6 filter  ">
	                                <a href="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg">
	                                    <img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg" alt="wrpkit" class="img-fluid card-img-top">
	                                </a>
                            	</div>
                            	<div class="col-lg-4 col-md-6 filter  ">
	                                 <a href="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg">
	                                    <img src="https://cdti.sspengine.com/uploads/images/image_750x_5c5d17f4de036.jpg" alt="wrpkit" class="img-fluid card-img-top">
	                                </a>
                            	</div>
			               </div>
			            </div>
		                <div class="tag">
		                    <ul class="list-tags list-inline">
		                        <li>
		                            <p>ข่าวการศึกษา : </p>
		                        </li>
		                        <li>
		                            <a href="">ข่าวกิจกรรม</a>
		                        </li>
		                        <li>
		                            <a href="">ข่าวประชาสัมพันธ์</a>
		                        </li>
		                        <li>
		                            <a href="">ข่าวน่ารู้</a>
		                        </li>
		                        <li>
		                            <a href="">สมัครเรียน</a>
		                        </li>
		                        <li>
		                            <a href="">ภาพกจิกรรมการเรียน</a>
		                        </li>
		                         <li>
		                            <a href="">ภาพกจิกรรมนักศึกษา</a>
		                        </li>
		                    </ul>
		                </div>
		            </div>
		            <div class="news-release feature7">
		            	<div class="section-header p-t-20 p-b-20 m-b-20">
			                <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> ข่าวที่เกี่ยวข้อง</h3>
                			<a href="" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
			            </div>
			            <div class="row">
			            	<div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
                                <div class="row">
                                    <div class="col-md-12 col-6">
                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
                                    </div>
                                    <div class="col-md-12 col-6">
                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
                                <div class="row">
                                    <div class="col-md-12 col-6">
                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
                                    </div>
                                    <div class="col-md-12 col-6">
                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
                                <div class="row">
                                    <div class="col-md-12 col-6">
                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
                                    </div>
                                    <div class="col-md-12 col-6">
                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
                                <div class="row">
                                    <div class="col-md-12 col-6">
                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
                                    </div>
                                    <div class="col-md-12 col-6">
                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
                                    </div>
                                </div>
                            </div>
			            </div>
		            </div>
				</div>
				<div class="col-md-12 col-lg-4">
					 <div class="sidebar">
                        <div class="sidebar-title">
                            <h3><i class="far fa-newspaper"></i> ข่าวแนะนำ</h3>
                        </div>
                      	<div class="feature7">
                      		<div class="row">
                      			<div class="col-md-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-large">
			                        <div class="" data-aos="flip-left" data-aos-duration="1200">
			                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			                                <ol class="carousel-indicators">
			                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			                                </ol>
			                                <div class="carousel-inner">
			                                    <div class="carousel-item active">
			                                      <img src="<?=$base_url?>/assets/images/news/larrge_thumb.png" class="d-block w-100" alt="...">
			                                    </div>
			                                    <div class="carousel-item">
			                                      <img src="<?=$base_url?>/assets/images/news/larrge_thumb.png" class="d-block w-100" alt="...">
			                                    </div>
			                                    <div class="carousel-item">
			                                      <img src="<?=$base_url?>/assets/images/news/larrge_thumb.png" class="d-block w-100" alt="...">
			                                    </div>
			                                </div>
			                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			                                    <span class="sr-only">Previous</span>
			                                </a>
			                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			                                    <span class="sr-only">Next</span>
			                                </a>
			                            </div>

			                            <div class="m-t-10">
			                                <p class="text-dark">งานอุ่นไอรักคลายความหนาวสายน้ำแห่งรัตนโกสินพาเที่ยวชมงานอุ่นไอรักคลายความหนาว 2562</p>
			                                <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
			                            </div>
			                        </div>
			                    </div>
                      		</div>
                      		<div class="row">
                      			<div class="col-12 col-sm-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-small">
	                                <div class="row">
	                                    <div class="col-md-6 col-6">
	                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
	                                    </div>
	                                    <div class="col-md-6 col-6">
	                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
	                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-12 col-sm-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-small">
	                                <div class="row">
	                                    <div class="col-md-6 col-6">
	                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
	                                    </div>
	                                    <div class="col-md-6 col-6">
	                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
	                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-12 col-sm-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-small">
	                                <div class="row">
	                                    <div class="col-md-6 col-6">
	                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
	                                    </div>
	                                    <div class="col-md-6 col-6">
	                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
	                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-12 col-sm-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-small">
	                                <div class="row">
	                                    <div class="col-md-6 col-6">
	                                        <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
	                                    </div>
	                                    <div class="col-md-6 col-6">
	                                        <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
	                                        <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
	                                    </div>
	                                </div>
	                            </div>
                      		</div>
                      	</div>
                      	<div class="sidebar-title">
                            <h3><i class="far fa-calendar-alt"></i> ปฎิทินการศึกษา</h3>
                        </div>
                        <div class="carlendar  text-center">
                            <p class="text-muted date-now">14 มกราคม 2562</p>
                            <div class="jsCalendar"  data-language="th"></div>
                        </div>
                        <div class="timeline-box t">
                            <ul class="timeline">
                                <li class="timeline-item current">
                                    <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                    <div class="timeline-content">
                                        <p class="timeline-title mb-0">
                                            กิจกรรมอุ่นไอรัก
                                        </p>
                                        <p class="text-muted">
                                            <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
                                            <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> <br/>
                                            <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
                                        </p>
                                    </div>
                                </li>
                                <li class="timeline-item timeline-orange">
                                    <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                    <div class="timeline-content">
                                        <p class="timeline-title mb-0">
                                            กิจกรรมอุ่นไอรัก
                                        </p>
                                        <p class="text-muted">
                                            <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
                                            <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> <br/>
                                            <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
                                        </p>
                                    </div>
                                </li>
                                <li class="timeline-item timeline-danger">
                                    <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                    <div class="timeline-content">
                                        <p class="timeline-title mb-0">
                                            กิจกรรมอุ่นไอรัก
                                        </p>
                                        <p class="text-muted">
                                            <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
                                            <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> <br/>
                                            <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a href="" class="btn btn-custom-primary mx-auto">ดูทั้งหมด</a>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include('views/layouts/footer.php'); ?>