<div class="bg-primary header1 po-relative">
    <div class="container">
        <nav class="navbar navbar-expand-lg h1-nav">
          <a class="navbar-brand" href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#campusnav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="ti-menu"></span>
          </button>

          <div class="collapse navbar-collapse" id="campusnav">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            
              	<li class="nav-item <?php echo ($route == "")?'active':''?>"><a class="nav-link" href="<?php echo general_link($page,'')?>"><?php echo ($page->category_id == 10)?"รู้จักคณะ":"รู้จักสำนัก" ?></a></li>
            	<li class="nav-item <?php echo ($route == $fac_link[4])?'active':''?>"><a class="nav-link" href="<?php echo general_link($page,$fac_link[4])?>">บุคลากร</a></li>
            	<li class="nav-item <?php echo ($route == $fac_link[7])?'active':''?>"><a class="nav-link" href="<?php echo general_link($page,$fac_link[7])?>">ติดต่อสำนัก</a></li>
            </ul>
          </div>
        </nav>
    </div>
</div>
<script>
$(window).scroll(function() {
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 20) {
        //clearHeader, not clearheader - caps H
        $(".header1").addClass("fixed");
    }else{
      $(".header1").removeClass("fixed");
    }
}); //missing );
</script>
