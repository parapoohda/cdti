<!DOCTYPE html>
<html lang="en">

<?php $base_url = 'https://cdti.sspengine.com/cdti_assets/' ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>CDTI - HOMNE PAGE</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $base_url; ?>assets/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- This is for the animation CSS -->
    <link href="<?php echo $base_url; ?>assets/node_modules/aos/dist/aos.css" rel="stylesheet">
    <!-- This page CSS -->
    <link href="<?php echo $base_url; ?>assets/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css" rel="stylesheet" media="all">
    <link href="<?php echo $base_url; ?>assets/node_modules/prism/prism.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/node_modules/owl.carousel/dist/assets/owl.theme.green.css" rel="stylesheet">

    <link href="<?php echo $base_url; ?>assets/node_modules/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <!-- This page CSS -->

    <!-- Custom CSS -->
    <link href="<?php echo $base_url; ?>assets/css/slider.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/css/customs.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>assets/css/ssp_style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

    <script src="<?php echo $base_url; ?>assets/node_modules/jquery/dist/jquery.js"></script>
</head>

<body class="innerpage">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">CDTI</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <div class="topbar">
            <div class="header11 ">
                <div class="container">
                    <!-- Header 1 code -->
                    <nav class="navbar navbar-expand-lg h11-nav">
                        <a class="navbar-brand" href="#"><img src="<?=$base_url?>assets/images/logo_l.png" alt="สถาบันเทคโนโลยีจิตรลดา" /></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header11" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                        <div class="collapse navbar-collapse hover-dropdown flex-column" id="header11">
                            <div class="ml-auto h11-topbar b-d-1">
                                <ul class="list-inline ">
                                    <li><a><i class="icon-Phone-2"></i> 215 123 4567</a></li>
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fab fa-line"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li class="b-l-1 pl-2 select-lang">

                                        <a href="" class="d-inline">
                                            <img src="<?=$base_url ?>assets/images/th.png" alt="">
                                        </a>
                                        <a href="" class="d-inline">
                                            <img src="<?=$base_url ?>assets/images/en.png" alt="">
                                        </a>
                                    </li>
                                    <li><a class="btn btn-custom-primary btn-register-nav">สมัครเรียน</a></li>
                                </ul>
                            </div>
                            <ul class="navbar-nav ml-auto font-13">
                                <li class="nav-item active"><a class="nav-link" href="#">หน้าแรก</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">สมัครเรียน</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">เกี่ยวกับสถาบัน</a></li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">คณะและหลักสูตร <i class="fa fa-angle-down m-l-5"></i>
                                    </a>
                                    <ul class="b-none dropdown-menu animated fadeInUp">
                                        <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown">ระดับปริญญาตรี <i class="fa fa-angle-right ml-auto"></i></a>
                                            <ul class="dropdown-menu b-none menu-right">
                                                <li><a class="dropdown-item" href="#">คณะบริหารธุรกิจ </a></li>
                                                <li><a class="dropdown-item" href="#">คณะเทคโนโลยีอุตสาหกรรม </a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown">ประกาศนียบัตรวิชาชีพ (ปวส.) <i class="fa fa-angle-right ml-auto"></i></a>
                                            <ul class="dropdown-menu b-none menu-right">
                                                <li><a class="dropdown-item" href="#">คณะบริหารธุรกิจ </a></li>
                                                <li><a class="dropdown-item" href="#">คณะเทคโนโลยีอุตสาหกรรม </a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown">ประกาศนียบัตรวิชาชีพ (ปวช.) <i class="fa fa-angle-right ml-auto"></i></a>
                                            <ul class="dropdown-menu b-none menu-right">
                                                <li><a class="dropdown-item" href="#">คณะบริหารธุรกิจ </a></li>
                                                <li><a class="dropdown-item" href="#">คณะเทคโนโลยีอุตสาหกรรม </a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#">ศิษย์เก่า</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">นักศึกษา</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">บุคคลทั่วไป</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Slider  -->
                <!-- ============================================================== -->
