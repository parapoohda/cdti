<div class="bg-primary header1 po-relative">
    <div class="container">
        <nav class="navbar navbar-expand-lg h1-nav"> 
          <a class="navbar-brand" href="#">ข่าวนักศึกษา >> <?php echo $title?></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#campusnav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="ti-menu"></span>
          </button>
          <div class="collapse navbar-collapse" id="campusnav">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            	<li class="nav-item "><a class="nav-link" href="<?php echo lang_base_url(); ?>"><i class="fa fa-home"></i> </a></li>
              <li class="nav-item <? echo ($category->id == 10)?'active':'' ?>"><a class="nav-link" href="<?php echo lang_base_url(); ?>news/<?php echo html_escape('ระดับปริญญาตรี'); ?>">ระดับปริญญาตรี </a></li>
              <li class="nav-item <? echo ($category->id == 9)?'active':'' ?>"><a class="nav-link" href="<?php echo lang_base_url(); ?>news/<?php echo html_escape('ระดับวิชาชีพ-ปวส.'); ?>"> ระดับวิชาชีพ (ปวส.) </a></li>
              <li class="nav-item <? echo ($category->id == 6)?'active':'' ?>"><a class="nav-link" href="<?php echo lang_base_url(); ?>news/<?php echo html_escape('ระดับวิชาชีพ-ปวช.'); ?>"> ระดับวิชาชีพ (ปวช.) </a></li>
            </ul>
          </div>
        </nav>
    </div>
</div>
<script>
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 20) {
        $(".header1").addClass("fixed");
    }else{
      $(".header1").removeClass("fixed");
    }
});
</script>
