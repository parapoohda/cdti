<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo html_escape($title); ?> - <?php echo html_escape($settings->site_title); ?></title>
    <meta name="description" content="<?php echo html_escape($description); ?>"/>
    <meta name="keywords" content="<?php echo html_escape($keywords); ?>"/>
    <meta name="author" content=""/>
    <meta property="og:locale" content="th_TH"/>
    <meta property="og:site_name" content="<?php echo $settings->application_name; ?>"/>
    
    <meta property="og:image:alt" content="http://www.cdti.ac.th/uploads/logo/logo_5cee057a0dd37.png"/>
    <meta property="og:image:secure_url" content="http://www.cdti.ac.th/uploads/logo/logo_5cee057a0dd37.png"/>
    <meta property="og:image" content="http://www.cdti.ac.th/uploads/logo/logo_5cee057a0dd37.png"/>
    <meta property="og:url" content="https://www.cdti.ac.th/">
    <!--https://tagmanager.google.com/-->
    <!--https://support.google.com/tagmanager/answer/6103696-->
    <!--https://support.google.com/tagmanager/answer/6102821?hl=en&utm_id=ad-->
    <!---->
<!-- Google Tag Manager -->
<!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PF2L5VB');</script>
--><!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PF2L5VB');</script>
--><!-- End Google Tag Manager -->
<!--try mamomoto-->
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://cdti.matomo.cloud/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src='//cdn.matomo.cloud/cdti.matomo.cloud/matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
<!--end mamomoto-->
    <?php if (isset($post_type)): ?>
    <meta property="og:type" content="<?php echo $og_type; ?>"/>
    <meta property="og:title" content="<?php echo html_escape($og_title); ?>"/>
    <meta property="og:description" content="<?php echo $og_description; ?>"/>
    
    
    <meta property="article:author" content="<?php echo $og_author; ?>"/>
    <meta property="fb:app_id" content="<?php echo $this->general_settings->facebook_app_id; ?>"/>
<?php foreach ($og_tags as $tag): ?>
    <meta property="article:tag" content="<?php echo $tag->tag; ?>"/>
<?php endforeach; ?>
    <meta property="article:published_time" content="<?php echo $og_published_time; ?>"/>
    <meta property="article:modified_time" content="<?php echo $og_modified_time; ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@<?php echo html_escape($settings->application_name); ?>"/>
    <meta name="twitter:creator" content="@<?php echo html_escape($og_creator); ?>"/>
    <meta name="twitter:title" content="<?php echo html_escape($post->title); ?>"/>
    <meta name="twitter:description" content="<?php echo html_escape($post->summary); ?>"/>
    <meta name="twitter:image" content="<?php echo $og_image; ?>"/>
<?php else: ?>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo html_escape($title); ?> - <?php echo html_escape($settings->site_title); ?>"/>
    <meta property="og:description" content="<?php echo html_escape($description); ?>"/>
    <meta property="fb:app_id" content="<?php echo $this->general_settings->facebook_app_id; ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@<?php echo html_escape($settings->application_name); ?>"/>
    <meta name="twitter:title" content="<?php echo html_escape($title); ?> - <?php echo html_escape($settings->site_title); ?>"/>
    <meta name="twitter:description" content="<?php echo html_escape($description); ?>"/>
<?php endif; ?>
    <meta name="google-signin-client_id" content="<?php echo $general_settings->google_client_id ?>">
    <link rel="canonical" href="<?php echo current_url(); ?>"/>
    <?php if ($general_settings->multilingual_system == 1):
    foreach ($languages as $language):
    if ($language->id == $site_lang->id):?>
        <link rel="alternate" href="<?php echo base_url(); ?>" hreflang="<?php echo $language->language_code ?>"/>
    <?php else: ?>
        <link rel="alternate" href="<?php echo base_url() . $language->short_form . "/"; ?>" hreflang="<?php echo $language->language_code ?>"/>
    <?php endif; endforeach; endif; ?>
    <link rel="shortcut icon" type="image/png" href="<?php echo get_favicon($vsettings); ?>"/>
    <?php echo $primary_font_url; ?>
    <?php echo $secondary_font_url; ?>
    <?php echo $tertiary_font_url; ?>
    <?php if (isset($post_type) && $post_type == "audio"): ?>
        <link href="<?php echo base_url(); ?>assets/vendor/audio-player/css/amplitude.min.css" rel="stylesheet"/>
    <?php endif; ?>
    <?php if (isset($post_type) && $post_type == "video"): ?>
        <link href="<?php echo base_url(); ?>assets/vendor/video-player/video-js.min.css" rel="stylesheet"/>
    <?php endif; ?>

    <?php if ($vsettings->site_color == '') : ?>
        <!-- <link href="<?php echo base_url(); ?>assets/css/colors/default.min.css" rel="stylesheet"/> -->
    <?php else : ?>
        <!-- <link href="<?php echo base_url(); ?>assets/css/colors/<?php echo html_escape($vsettings->site_color); ?>.css" rel="stylesheet"/> -->
    <?php endif; ?>

    <?php echo $general_settings->google_analytics; ?>
    <?php echo $general_settings->head_code; ?>
    <?php if ($selected_lang->text_direction == "rtl"): ?>
        <script>var rtl = true;</script>
    <?php else: ?>
        <script>var rtl = false;</script>
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/font-awesome/css/font-awesome.min.css">



      <!--<link href="<?php echo base_url(); ?>cdti_assets/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <!-- This is for the animation CSS -->
      <link href="<?php echo base_url(); ?>cdti_assets/node_modules/aos/dist/aos.css" rel="stylesheet">
      <!-- This page CSS -->
      <link href="<?php echo base_url(); ?>cdti_assets/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css" rel="stylesheet" media="all">
      <link href="<?php echo base_url(); ?>cdti_assets/node_modules/prism/prism.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/node_modules/owl.carousel/dist/assets/owl.theme.green.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/plugin/slick/slick.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/plugin/slick/slick-theme.css" rel="stylesheet">
      <!-- This page CSS -->
      <!-- Custom CSS -->
      <link href="<?php echo base_url(); ?>cdti_assets/css/slider.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/css/customs.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>cdti_assets/css/ssp_style.css" rel="stylesheet">


      <?php if(isset($css_theme)):?>
        <link href="<?php echo $css_theme ?>" rel="stylesheet">
      <?php endif;?>

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

      <script src="<?php echo base_url(); ?>cdti_assets/node_modules/jquery/dist/jquery.js"></script>
      <style>
        .d-inlinex{
            display: inline!important;
            opacity: 0.2;

        }
        .select-lang .selected{
            opacity: 1 !important;
        }

        /* html body .text-primary {
            color: #b51515 !important;
        }
        .bg-primary {
              background: #b51515 !important;
          }
        .color-primary{
            color: #b51515 !important;
        }
        .bg-custom-blue-light {
              background: rgba(130, 1, 1, 0.85) !important;
          }
        .color-2{
            color: #b51515 !important;
        }
        .select-lang .selected{
            opacity: 1 !important;
        }
        .btn.btn-custom-primary {
          background: #f8da16;
          color: #b41717;
          -webkit-transition: all .25s linear;
          transition: all .25s linear;
        } */


        .image-container {
              position: relative;
              border-radius: 5px;
              width: 100%;
              display: inline-block;
              float: left;
              overflow: hidden;
          }

        .image-container img {
            /* height: 110%; */
            width: 100%;
            position: absolute;
            /*left: 50%;
            top: 50%;*/
            -webkit-transform: translateY(0%) translateX(0%);
        }
      </style>
 </head>

<body class="<?php echo ($title != 'หน้าแรก')?'innerpage':'home'?>">

   <?php $this->load->view('cdti/include/preloader',true)?>

   <div id="main-wrapper">

       <div class="topbar">
         <?php  $this->load->view('cdti/layout/navbar'); ?>
       </div>

       <div class="page-wrapper hidden-md-down visible-lg-up" style="padding-top: 90px;">
       </div>
       
       <div class="page-wrapper visible-md-down hidden-lg-up" style="padding-top: 50px;">
       </div>
       <div class="page-wrapper" >
       </div>
           <div class="container-fluid">
