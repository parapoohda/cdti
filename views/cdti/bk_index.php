
 <?php $this->load->view('cdti/include/banner',['banners' => $banners])?>

 <div class="bg-white">
     <section id="dreegree">
         <div class="container-fluid">
             <div class="row">
              <div class="col-lg-3 col-sm-6 p-0">
                  <div class="card card-class card-vocation card-shadow aos-init aos-anim" >
                      <div class="img-ho">
                          <img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/1.png" alt="ระดับวิชาชีพ (ปวช.)">
                          <div class="card-overlay text-center">
                              <div class="card-overlay-header">
                                  <h5 class="font-medium m-b-0">ระดับวิชาชีพ (ปวช.)</h5>
                              </div>
                              <div class="card-overlay-body">
                                 <div class="row">
                                     <div class="col-10 col-sm-10 mx-auto">
                                          <p class="card-class-title">คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.)</p>
                                          <ul class="list-class mb-2">
                                              <li> <p>สาขาวิชาการจัดการธุรกิจอาหาร</p></li>
                                          </ul>
                                          <p class="card-class-title">คณะเทคโนโลยีอุตสาหกรรม เทคโนโลยีบัณฑิต (ทล.บ.)</p>
                                          <ul class="list-class mb-2">
                                              <li> <p>สาขาเทคโนโลยีอุตสาหกรรม (ต่อเนื่อง)</p></li>
                                              <li> <p>สาขาเทคโนโลยีไฟฟ้าอิเล็กทรอนิกส์</p></li>
                                          </ul>
                                     </div>
                                 </div>
                                  <a href="" class="btn btn-default btn-custom mx-auto d-block">ดูรายละเอียด</a >
                              </div>
                          </div>
                      </div >
                  </div>
              </div>
              <div class="col-lg-3 col-sm-6 p-0">
                  <div class="card card-class card-vocation-heigh card-shadow " >
                      <div class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/2.png" alt="ระดับวิชาชีพ (ปวส.)">
                          <div class="card-overlay text-center">
                              <div class="card-overlay-header">
                                  <h5 class="font-medium m-b-0">ระดับวิชาชีพ (ปวส.)</h5>
                              </div>
                              <div class="card-overlay-body">
                                 <div class="row">
                                     <div class="col-10 col-sm-10 mx-auto">
                                          <p class="card-class-title">คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.)</p>
                                          <ul class="list-class mb-2">
                                              <li> <p>สาขาวิชาการจัดการธุรกิจอาหาร</p></li>
                                          </ul>
                                          <p class="card-class-title">คณะเทคโนโลยีอุตสาหกรรม เทคโนโลยีบัณฑิต (ทล.บ.)</p>
                                          <ul class="list-class mb-2">
                                              <li> <p>สาขาเทคโนโลยีอุตสาหกรรม (ต่อเนื่อง)</p></li>
                                              <li> <p>สาขาเทคโนโลยีไฟฟ้าอิเล็กทรอนิกส์</p></li>
                                          </ul>
                                     </div>
                                 </div>
                                  <a href="" class="btn btn-default btn-custom mx-auto d-block">ดูรายละเอียด</a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-sm-6 p-0">
                  <div class="card card-class card-bachelor card-shadow" >
                      <div class="img-ho">
                          <img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/3.png" alt="ระดับปริญญาตรี">
                          <div class="card-overlay text-center">
                              <div class="card-overlay-header">
                                  <h5 class="font-medium m-b-0">ระดับปริญญาตรี</h5>
                              </div>
                              <div class="card-overlay-body">
                                 <div class="row">
                                     <div class="col-10 col-sm-10 mx-auto">
                                          <p class="card-class-title">คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.)</p>
                                          <ul class="list-class mb-2">
                                              <li> <p>สาขาวิชาการจัดการธุรกิจอาหาร</p></li>
                                          </ul>
                                          <p class="card-class-title">คณะเทคโนโลยีอุตสาหกรรม เทคโนโลยีบัณฑิต (ทล.บ.)</p>
                                          <ul class="list-class mb-2">
                                              <li> <p>สาขาเทคโนโลยีอุตสาหกรรม (ต่อเนื่อง)</p></li>
                                              <li> <p>สาขาเทคโนโลยีไฟฟ้าอิเล็กทรอนิกส์</p></li>
                                          </ul>
                                     </div>
                                 </div>
                                  <a href="" class="btn btn-default btn-custom mx-auto d-block">ดูรายละเอียด</a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-sm-6 p-0">
                <div class="card card-class card-register card-shadow " >
                  <div class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/register.png" alt="สมัครเรียน">
                    <div class="card-overlay text-center">
                      <div class="card-overlay-header">
                          <h5 class="font-medium m-b-0"><?php echo (date("Y-m-d H:i:s")  <= $vsettings->regis_date)?trans('enroll'):trans('enroll_close')?></h5>
                      </div>
                      <div class="card-overlay-body">
                         <!-- <div class="row">
                             <div class="col-10 col-sm-10 mx-auto">
                                  <p class="card-class-title">คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.)</p>
                                  <ul class="list-class mb-2">
                                      <li> <p>สาขาวิชาการจัดการธุรกิจอาหาร</p></li>
                                  </ul>
                                  <p class="card-class-title">คณะเทคโนโลยีอุตสาหกรรม เทคโนโลยีบัณฑิต (ทล.บ.)</p>
                                  <ul class="list-class mb-2">
                                      <li> <p>สาขาเทคโนโลยีอุตสาหกรรม (ต่อเนื่อง)</p></li>
                                      <li> <p>สาขาเทคโนโลยีไฟฟ้าอิเล็กทรอนิกส์</p></li>
                                  </ul>
                             </div>
                         </div> -->
                          <a href="<?php echo (date("Y-m-d H:i:s")  <= $vsettings->regis_date)?'##':'#'?>" class="btn btn-default btn-custom ml-auto mr-auto m-t-40 d-block">ดูรายละเอียด</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- <div class="col-lg-3 col-sm-6 p-0">
                  <div class="card card-class card-register card-shadow aos-init aos-animate" data-aos="flip-left" data-aos-duration="1200">
                     <a href="<?php echo (date("Y-m-d H:i:s")  <= $vsettings->regis_date)?'##':'#'?>" class="img-ho"><img class="card-img-top" src="<?php echo base_url(); ?>cdti_assets//images/class_banner/register.png" alt="wrappixel kit"></a>
                     <div class="card-overlay text-center">
                         <h5 class="font-medium m-b-0"><?php echo (date("Y-m-d H:i:s")  <= $vsettings->regis_date)?trans('enroll'):trans('enroll_close')?></h5>
                     </div>
                 </div>
              </div> -->
            </div>
         </div>
     </section>

     <!-- พระราโชวาช start -->
        <?php
          if(isset($royal_speech) and $royal_speech->visibility == 1) {
              // $this->load->view($royal_speech->link,['data' => $royal_speech]);
          }
        ?>
    <!-- พระราโชวาช start -->
    <section id="aboutus" class="" style="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 col-lg-6 bg-custom-blue-light spacer">
                    <div class="row">
                        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto">
                            <div class="about-content spacer" >
                                <h3 class="about-title mb-3">
                                    CDTI สถาบันเทคโนโลยีจิตรลดา
                                </h3>
                                <hr class="mb-3">
                                <div class="about-detail">
                                    <p class="text-white m-b-60">
                                        “สถาบันที่จัดการศึกษาตามแนวพระราชดำริ ด้านเทคโนโลยีบนฐานวิทยาศาสตร์ ในรูปแบบ เรียนคู่งาน งานคู่เรียน โดยเน้น ช่างอุตสาหกรรม ธุรกิจอาหาร และศิลปะ ประยุกต์ ทั้งในระดับวิชาชีพและปริญญา เพื่อสร้างคนดี มีจิตอาสา มีทักษะอาชีพ ใฝ่รู้ สู้งาน  สร้างสรรค์นวัตกรรม สื่อสารเป็น”
                                    </p>
                                    <a href="" class="btn btn-custom-primary">เกี่ยวกับเรา</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



<div class="bg-white">
    <section id="news-heighlight" class="mini-spacer feature7 news">
        <?php $this->load->view('cdti/include/post/main_featured_post',['data' => $featured_posts]);?>
    </section>
<div>


<section id="news" class="mini-spacer feature7 news">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-9">

                <?php $this->load->view('cdti/include/post/main_post',['data' => $latest_posts]);?>

                <?php $this->load->view('cdti/include/post/channel_post',['data' => $channel_posts]);?>
            </div>
            <div class="col-lg-4 col-md-3">
                <div class="sidebar">
                  <?php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="bg-white">
      	<?php $this->load->view("cdti/include/gallery_cdti",["main_category1_cover" => $main_category1_cover])?>
      	<?php $this->load->view("cdti/include/partner",["link" => $links])?>
</div>
