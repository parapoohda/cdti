<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> รายงานผลการดำเนินงานของคณะกรรมการตรวจสอบ</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container mb-5">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="active" 
                        data-parent="#accordion">รายงานผลการดำเนินงานของคณะกรรมการตรวจสอบ</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/reportexamine/รายงานผลการดำเนินงานของคณะกรรมการตรวจสอ.pdf">-  รายงานผลการดำเนินงานของคณะกรรมการตรวจสอบสถาบันเทคโนโลยีจิตรลดา ประจำปีงบประมาณ 2564</a><br>
                        </p>
                        
                </div>
            </div>
        </div>
      </section>
      
    
<!--
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-10" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">10.คำสั่งใหม่สภาสถาบัน</a>
                </h4>
            </div>
            <div id="panelBody-10" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="">เอกสาร 10</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-11" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">อื่นๆ</a>
                </h4>
            </div>
            <div id="panelBody-11" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="">เอกสาร อื่นๆ</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>
      -->
    </div>
</div>

</style>
<!--<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt="">สืบค้นเอกสาร</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container" style="margin-bottom : 3em;">
    <div id="accordion" class="panel-group">
      <div class="row">
  			<div class="col-sm-12">
        <iframe class="WebPublish_iframe" src="https://edoc.cdti.ac.th/docweb/v2/publishDoc.aspx" frameborder="0" width="100%" height="600px"></iframe>
        </div>
    </div>
  </div>
</div>
-->