
<section class="campus-box ">
	<div class="container">
		<div class="section-header campus-header mb-4">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 text-primary"><i class="far fa-newspaper"></i> ระดับปริญญาตรี
                </h3>
				</div>
				<div class="col-sm-6 text-right">
					<ol class="breadcrumb ">
              <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
              <li class="breadcrumb-item active">ระดับปริญญาตรี</li>
          </ol>
				</div>
			</div>
		</div>
		<div class="campus-content">
			
			<div class="row m-b-40">
				<div class="col-sm-2 col-lg-12">
					
					<div class="campus-paragraph ">
                    <div>
<h3 class="font-light color-primary">ขั้นตอนการสมัครเรียนด้วยระบบลงทะเบียนออนไลน์</h3>

<hr />
<p><img src="<?=base_url()?>uploads/register/register_step.jpg" alt=""></p>

<h3 class="font-light color-primary">สมัครผ่านเว็บไซต์</h3>

<hr />

<p>
                                            1. ศึกษาข้อมูลหลักสูตรและการรับสมัคร </p>
                                            <p>2. สมัครออนไลน์ https://reg.cdti.ac.th/registrar/apphome.asp <a href="https://reg.cdti.ac.th/registrar/apphome.asp">คลิ้กที่นี่</a>
                                            </p>
                                            <p>3. ชำระค่าธรรมเนียมการสมัคร 300 บาท ผ่านทางธนาคาร</p>
                                            <p>4. ตรวจสอบสถานภาพการสมัคร</p>
                                            <p>5. พิมพ์หลักฐานการสมัคร เพื่อนำส่งวันสอบสัมภาษณ์</p>
                                            <p>- ใบสมัคร 1 ฉบับ พร้อมติดรูปถ่าย (พิมพ์จากระบบรับสมัครนักศึกษา)</p>
                                            <p>- สำเนาบัตรประจำตัวประชาชน 1 ฉบับ</p>
											<p>- สำเนาแสดงคุณวุฒิการศึกษา (ปพ.1) 1 ฉบับ</p>
											<p class="m-b-40"> 6. เข้ามาสอบสัมภาษณ์ที่สถาบันฯ  วันจันทร์ - วันศุกร์ เวลา 8.30 น. - 12.00 น. / 13.00 น. - 16.00 น.  </p>

											<h3 class="font-light color-primary">สมัครด้วยตนเอง</h3>
											<hr />
											<p  class="m-b-40">สมัครด้วยตนเองที่สถาบันฯ วันจันทร์ - วันศุกร์ เวลา 8.30 น. - 16.30 น.  </p>

											<h3 class="font-light color-primary">ติดต่อสอบถาม </h3>
											<hr />
											<p> งานทะเบียนและวัดผล 02-280-0551 ต่อ 3296</p>


</div>

					<?php //echo html_entity_decode($page->page_content) ?>
					</div>
				</div>
				
			</div>
			<div class="feature7">
			</div>
		</div>

	</div>
</section>


