<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> ระดับปริญญาตรี (ป.ตรี)</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>`
</section>
<section class="mini-spacer">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<p class="text-dark text-indent mb-2">หลักสูตรนี้ถูกใช้เป็นกรอบในการพัฒนานักศึกษา ให้มีคุณลักษณะของการเป็นผู้ประกอบการที่มีความรู้ด้านบริหารธุรกิจและศาสตร์ที่เกี่ยวข้อง เป็นบัณฑิตที่มีคุณธรรม จริยธรรมทางธุรกิจ รวมถึงสามารถดำเนินธุรกิจหรือปฏิบัติงานภายใต้สภาวการณ์ทางเศรษฐกิจและสังคม ที่เปลี่ยนแปลงอย่างรวดเร็วจากความได้เปรียบของประเทศในด้านทำเลที่ตั้งและภูมิอากาศ</p>
				
				<div class="table-responsive m-t-20">
                    <?php
                    $pages_fac_bachelor= $this->page_model->get_all_pages_fac(true,10);
                    $pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,9);
                    // 
                    ?>
					<table class="table table-bordered table-branch">
						<thead>
							<tr class="bg-primary text-white">
								<th class="text-center">คณะ/สาขาวิชา</th>
								<th class="text-center">กองทุนกู้ยืมฯ</th>
								<th class="text-center">ทุนการศึกษา</th>
								<th class="text-center">วุฒิการศึกษาที่ใช้ในการสมัคร</th>
							</tr>
						</thead>
						<tbody>
                        <?php
                            
                            foreach ($pages_fac_bachelor as  $value) {
                                ?>
                            <tr style="background-color: aliceblue;">
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><h5><?php echo $value->title;?></h5></a></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>
                            <?php
                            // dd($value);exit;
                                $page_branch = $this->page_model->get_branch_page($value->main_category_id,$this->selected_lang->id);
                                foreach ($page_branch as  $branch_value) {
                                ?>
							<tr>
								<td class="text-left"><a  href="<?php echo base_url()."branch/".$branch_value->slug;?>"><?php echo $branch_value->title;?></a></td>
								<td class="text-center"><a href="">กรอ./กยศ.*</a></td>
								<td class="text-center"><a href=""><i class="far fa-check-circle"></i></a></td>
								<td class="text-center">
								<?php 
								if($branch_value->id == 63){
									echo "ระดับ ปวส.";
								}else{
									echo "ระดับมัธยมศึกษาปีที่ 6 หรือเทียบเท่า";
								}
								?>	
								</td>
                            </tr>
                            <?php
                                }
                            }
                            ?>
                            
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/7.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/11.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/10.png" alt="" class="img-fluid img-responsive">
					</div>
				</div>
            </div>
            <!-- <div class="hidden-sm-down col-md-4">
				<img src="<?=base_url();?>assets/images/13.png" alt="" class="img-fluid img-responsive">
			</div> -->
		</div>
	</div>
</section>