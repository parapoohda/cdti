<section class="campus-box campus-box-calendar p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-header campus-header">
                    <div class="row">
                        <div class="col-sm-8 align-middle">
                            <h3 class="campus-title mt-4 text-primary"><img
                                    src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> <?=$page->title?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`
</section>
<section class="mini-spacer">
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <div class="table-responsive m-t-20">
                    <?php
                    $pages_fac_bachelor= $this->page_model->get_all_pages_fac(true,10);
                    $pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,9);
                    //
                    ?>
                    <table class="table table-bordered table-branch">
                        <thead>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
                                <th class="text-center" rowspan="3">หลักสูตร 4 ปี</th>
                                <th class="text-center" colspan="8">จำนวนค่าเทอมที่เรียกเก็บในแต่ละภาคการศึกษา</th>
                            </tr>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
                                <th class="text-center" colspan="2">ชั้นปีที่ 1</th>
                                <th class="text-center" colspan="2">ชั้นปีที่ 2</th>
                                <th class="text-center" colspan="2">ชั้นปีที่ 3</th>
                                <th class="text-center" colspan="2">ชั้นปีที่ 4</th>
                            </tr>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
                                <th class="text-center">เทอมที่ 1</th>
                                <th class="text-center">เทอมที่ 2</th>
                                <th class="text-center">เทอมที่ 1</th>
                                <th class="text-center">เทอมที่ 2</th>
                                <th class="text-center">เทอมที่ 1</th>
                                <th class="text-center">เทอมที่ 2</th>
                                <th class="text-center">เทอมที่ 1</th>
                                <th class="text-center">เทอมที่ 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($pages_fac_bachelor as  $value) {
                                ?>
                            <tr style="background-color: aliceblue;">
                                <td class="text-left" colspan="9"><a
                                        href="<?php echo base_url().$value->slug;?>"><h5><?php echo $value->title;?></h5></a>
                                </td>

                            </tr>
                            <?php
                            //dd($value);exit;
                                $page_branch = $this->page_model->get_branch_page($value->main_category_id,$this->selected_lang->id);
                                if($value->id == 53){
                                    $price = "29,500";
                                }else{
                                    $price = "31,500";
                                }
                                foreach ($page_branch as  $branch_value) {
                                ?>
                            <tr>
                                <td class="text-left"><a
                                        href="<?php echo base_url()."branch/".$branch_value->slug;?>"><?php echo $branch_value->title;?></a>
                                </td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>

                                <?php
                                if($branch_value->id == 63){
                                    ?>
                                <td class="text-center"><a href="">-</a></td>
                                <td class="text-center"><a href="">-</a></td>
                                <td class="text-center"><a href="">-</a></td>
                                <td class="text-center"><a href="">-</a></td>
                                <?php
                                }else{
                                    ?>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <?php
                                }
                                ?>

                            </tr>
                            <?php
                                }
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <img src="<?=base_url(); ?>assets/images/7.png" alt="" class="img-fluid img-responsive">
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <img src="<?=base_url(); ?>assets/images/11.png" alt="" class="img-fluid img-responsive">
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <img src="<?=base_url(); ?>assets/images/10.png" alt="" class="img-fluid img-responsive">
                    </div>
                </div>
            </div>
            <!-- <div class="hidden-sm-down col-md-4">
				<img src="<?=base_url();?>assets/images/13.png" alt="" class="img-fluid img-responsive">
			</div> -->
        </div>
    </div>
</section>
