<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> ระดับ ปวช. / ปวส.</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>`
</section>
<section class="mini-spacer">
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<p class="text-dark text-indent mb-2"></p>
				<!-- <a href="" class="read-more">ดูเพิ่มเติม >></a> -->
				<div class="table-responsive ">
                    <?php

					$pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,9);
                    $pages_fac_vocational= $this->page_model->get_all_pages_fac(true,6);
                    ?>
					<table class="table table-bordered table-branch">
						<thead>
							<tr class="bg-primary text-white">
								<th class="text-center">ประเภทวิชา</th>
								<th class="text-center">กองทุนกู้ยืมฯ</th>
								<th class="text-center">ทุนการศึกษา</th>
								<th class="text-center">วุฒิการศึกษาที่ใช้ในการสมัคร</th>
							</tr>
						</thead>
						<tbody>
							<tr style="background-color: aliceblue;">
								<td class="text-left"><a  href="#"><h5>ระดับประกาศนียบัตรวิชาชีพ (ปวช.)</h5></a></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>
                        <?php

                            foreach ($pages_fac_vocational as  $value) {
                                ?>
                            <tr>
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></td>
								<td class="text-center"><a href="">กรอ./กยศ.*</a></td>
								<td class="text-center"><a href=""><i class="far fa-check-circle"></i></a></td>
								<td class="text-center"><a href="">ระดับมัธยมศึกษาปีที่ 3 หรือเทียบเท่า</a></td>
                            </tr>

                            <?php

                            }
                            ?>

							<tr style="background-color: aliceblue;">
								<td class="text-left"><a  href="#"><h5>ระดับประกาศนียบัตรวิชาชีพ (ปวส.)</h5></a></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>
                        <?php

                            foreach ($pages_fac_high_vocational as  $value) {
                                ?>
                            <tr>
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></td>
								<td class="text-center"><a href="">กรอ./กยศ.*</a></td>
								<td class="text-center"><a href=""><i class="far fa-check-circle"></i></a></td>
								<td class="text-center"><a href="">ระดับปวช. โรงเรียนจิตรลดาวิชาชีพ</a></td>
                            </tr>

                            <?php

                            }
							?>

						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/7.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/11.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/10.png" alt="" class="img-fluid img-responsive">
					</div>
				</div>
            </div>
            <!-- <div class="hidden-sm-down col-md-4">
				<img src="<?=base_url();?>assets/images/13.png" alt="" class="img-fluid img-responsive">
			</div> -->
		</div>
	</div>
</section>
