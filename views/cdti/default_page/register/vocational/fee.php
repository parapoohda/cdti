<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> <?=$page->title?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>`
</section>
<section class="mini-spacer">
	<div class="container">
		<div class="row">
        <?php
        $pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,9);
        $pages_fac_vocational= $this->page_model->get_all_pages_fac(true,6);
        //
        ?>
			<div class="col-md-12">
                <h3 class="font-light color-primary">ระดับวิชาชีพ ปวช.</h3>
                <hr>
				<div class="table-responsive m-t-20">

					<table class="table table-bordered table-branch">
						<thead>
							<tr class="text-bluemain" style="background-color: #48a6ec82;">
								<th class="text-center" rowspan="3">หลักสูตร 3 ปี</th>
								<th class="text-center" colspan="6">จำนวนค่าเทอมที่เรียกเก็บในแต่ละภาคการศึกษา</th>
                            </tr>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
								<th class="text-center" colspan="2">ชั้นปีที่ 1</th>
                                <th class="text-center" colspan="2">ชั้นปีที่ 2</th>
                                <th class="text-center" colspan="2">ชั้นปีที่ 3</th>
                            </tr>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
                                <th class="text-center" >เทอมที่ 1</th>
                                <th class="text-center" >เทอมที่ 2</th>
                                <th class="text-center" >เทอมที่ 1</th>
                                <th class="text-center" >เทอมที่ 2</th>
                                <th class="text-center" >เทอมที่ 1</th>
                                <th class="text-center" >เทอมที่ 2</th>
							</tr>
						</thead>
						<tbody>
                        <?php
                            foreach ($pages_fac_vocational as  $value) {
                                $price = '19,500';
                                if($value->id == 89) // ประเภทวิชาเกษตรกรรม (ปวช.)
                                {
                                    $price = '5,000';
                                }
                                // if($value->id == 53){
                                //     $price = "19,500-";
                                // }else{
                                //     $price = "19,500-";
                                // }

                                ?>

                            <?php
                            //dd($value);exit;
                                // $page_branch = $this->page_model->get_branch_page($value->main_category_id,$this->selected_lang->id);
                                // if($value->id == 53){
                                //     $price = "19,500-";
                                // }else{
                                //     $price = "19,500-";
                                // }
                                // foreach ($page_branch as  $branch_value) {
                                ?>
							<tr>
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                            </tr>
                            <?php
                                // }
                            }
                            ?>

						</tbody>
					</table>
                </div>

                <h3 class="font-light color-primary">ระดับวิชาชีพ ปวส.</h3>
                <hr>
				<div class="table-responsive m-t-20">

					<table class="table table-bordered table-branch">
						<thead>
							<tr class="text-bluemain" style="background-color: #48a6ec82;">
								<th class="text-center" rowspan="3">หลักสูตร 2 ปี</th>
								<th class="text-center" colspan="6">จำนวนค่าเทอมที่เรียกเก็บในแต่ละภาคการศึกษา</th>
                            </tr>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
								<th class="text-center" colspan="2">ชั้นปีที่ 1</th>
                                <th class="text-center" colspan="2">ชั้นปีที่ 2</th>
                            </tr>
                            <tr class="text-bluemain" style="background-color: #48a6ec82;">
                                <th class="text-center" >เทอมที่ 1</th>
                                <th class="text-center" >เทอมที่ 2</th>
                                <th class="text-center" >เทอมที่ 1</th>
                                <th class="text-center" >เทอมที่ 2</th>
							</tr>
						</thead>
						<tbody>
                        <?php
                            foreach ($pages_fac_high_vocational as  $value) {
                                if($value->id == 91||$value->id ==82/*ประเภทวิชาบริหารธุรกิจ (ปวส.)*/){
                                    $price = "20,500-";
                                }else{
                                    $price = "22,600-";
                                }
                                ?>
							<tr>
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                                <td class="text-center"><a href=""><?php echo $price;?></a></td>
                            </tr>
                            <?php
                            }
                            ?>

						</tbody>
					</table>
                </div>

				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/7.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/11.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/10.png" alt="" class="img-fluid img-responsive">
					</div>
				</div>
            </div>
            <!-- <div class="hidden-sm-down col-md-4">
				<img src="<?=base_url();?>assets/images/13.png" alt="" class="img-fluid img-responsive">
			</div> -->
		</div>
	</div>
</section>
