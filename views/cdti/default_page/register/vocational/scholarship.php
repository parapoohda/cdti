<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> <?=$page->title?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="bursary mini-spacer pb-0">
	 
	<section class="bursary-table">
		<div class="bg-white">
			<div class="container">
				<div class="row  justify-content-end bursary-header-row">
					<div class="col-md-3 text-center">
						<p class="bursary-table-title color-primary text-center">ชื่อโครงการทุนการศึกษา</p>
					</div>
					<div class="col-md-3 text-center">
						<p class="bursary-table-title color-primary text-center">คณะและสาขาวิชาที่ให้ทุน</p>
					</div>
					<div class="col-md-3 text-center">
						<p class="bursary-table-title color-primary text-center">ทุนการศึกษาที่ได้รับ</p>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-ligt-gray p-t-0 p-b-20">
			<div class="container">
				 
				<div class="bursary-row">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
							<img src="uploads/scholarship/thumb/Banner-CDTI-1.jpg" alt="" class="img-responsive ">
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray mb-0 "><strong>ทุนช่วยเหลือการศึกษา</strong></p>
							<p class="color-darkgray mb-0"><span class="color-primary">นักศึกษาของสถาบัน </span></p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
                            นักศึกษาของสถาบัน
							</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
                            <strong>ก. ทุนช่วยเหลือการเล่าเรียนและค่าใช้จ่ายรายเดือน</strong><br> วงเงินไม่เกิน ๑๒๐,๐๐๐ บาท ต่อทุน/คน/ปีการศึกษา <br>
                            <strong>ข. ทุนช่วยเหลือค่าเล่าเรียน</strong><br> วงเงินไม่เกิน ๖๐,๐๐๐ บาท ต่อทุน/คน/ปีการศึกษา<br>
                            <strong>ค. ทุนช่วยเหลือค่าใช้จ่ายรายเดือน</strong><br> วงเงินไม่เกิน ๖๐,๐๐๐ บาท ต่อทุน/คน/ปีการศึกษา
                                
								

							</p>
						</div>
					</div>
				</div>
				 
				<div class="bursary-row">
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-3">
							<img src="uploads/scholarship/thumb/Banner-CDTI-2.jpg" alt="" class="img-responsive ">
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray mb-0 "><strong>ทุนเรียนดี</strong></p>
							<p class="color-darkgray mb-0"><span class="color-primary">ทุนการศึกษาปริญญาตรี </span> 12 ม.ค. 62</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								ระดับปริญญาตรี คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.) สาขาวิชาการจัดการธุรกิจอาหาร
							</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								รับทุนส่วนลด 8,000 บาท ต่อคน (ลดให้ในค่าเทอม) ภาคการศึกษาที่ 1 ปีการศึกษา 2562  

							</p>
						</div>
					</div>
				</div>
				 
				<div class="bursary-row">
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-3">
							<img src="uploads/scholarship/thumb/Banner-CDTI-3.jpg" alt="" class="img-responsive ">
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray mb-0 "><strong>ทุนเรียนดีเกรด 3.50</strong></p>
							<p class="color-darkgray mb-0"><span class="color-primary">ทุนการศึกษาปริญญาตรี </span> 12 ม.ค. 62</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								ระดับปริญญาตรี คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.) สาขาวิชาการจัดการธุรกิจอาหาร
							</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								รับทุนส่วนลด 8,000 บาท ต่อคน (ลดให้ในค่าเทอม) ภาคการศึกษาที่ 1 ปีการศึกษา 2562  

							</p>
						</div>
					</div>
				</div>
				 
				<div class="bursary-row">
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-3">
							<img src="uploads/scholarship/thumb/Banner-CDTI-4.jpg" alt="" class="img-responsive ">
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray mb-0 "><strong>ทุนเรียนดีเกรด 3.50</strong></p>
							<p class="color-darkgray mb-0"><span class="color-primary">ทุนการศึกษาปริญญาตรี </span> 12 ม.ค. 62</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								ระดับปริญญาตรี คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.) สาขาวิชาการจัดการธุรกิจอาหาร
							</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								รับทุนส่วนลด 8,000 บาท ต่อคน (ลดให้ในค่าเทอม) ภาคการศึกษาที่ 1 ปีการศึกษา 2562  

							</p>
						</div>
					</div>
				</div>
				 
				<div class="bursary-row">
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-3">
							<img src="uploads/scholarship/thumb/Banner-CDTI-5.jpg" alt="" class="img-responsive ">
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray mb-0 "><strong>ทุนเรียนดีเกรด 3.50</strong></p>
							<p class="color-darkgray mb-0"><span class="color-primary">ทุนการศึกษาปริญญาตรี </span> 12 ม.ค. 62</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								ระดับปริญญาตรี คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.) สาขาวิชาการจัดการธุรกิจอาหาร
							</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								รับทุนส่วนลด 8,000 บาท ต่อคน (ลดให้ในค่าเทอม) ภาคการศึกษาที่ 1 ปีการศึกษา 2562  

							</p>
						</div>
					</div>
				</div>
				 
				<div class="bursary-row">
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-3">
							<img src="uploads/scholarship/thumb/Banner-CDTI-6.jpg" alt="" class="img-responsive ">
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray mb-0 "><strong>ทุนเรียนดีเกรด 3.50</strong></p>
							<p class="color-darkgray mb-0"><span class="color-primary">ทุนการศึกษาปริญญาตรี </span> 12 ม.ค. 62</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								ระดับปริญญาตรี คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต (บธ.บ.) สาขาวิชาการจัดการธุรกิจอาหาร
							</p>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<p class="color-darkgray">
								รับทุนส่วนลด 8,000 บาท ต่อคน (ลดให้ในค่าเทอม) ภาคการศึกษาที่ 1 ปีการศึกษา 2562  

							</p>
						</div>
					</div>
				</div>
	
			</div>
		</div>
	</section>
</div>