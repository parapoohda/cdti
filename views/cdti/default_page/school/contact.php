
<section class="campus-box  mini-spacer p-b-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url(); ?>cdti_assets/images/studying.png" alt=""> ติดต่อคณะ</h3>
						</div>
						<div class="col-sm-8 text-right">
							<ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
                  <li class="breadcrumb-item active">ติดต่อคณะ</li>
              </ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="mini-spacer pb-5">
	<div class="container">
			<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-8">
						<img src="<?php echo base_url(); ?>cdti_assets/images/logo_l.png" alt="" class="img-responsive img-fluid mb-4">
					</div>
				</div>
				<h4 class="text-primary">สถาบันเทคโนโลยีจิตรลดา</h4>
				<hr class="hr-primary">
				<div class="row">
					<div class="col-2">
						<p class="text-primary">ที่อยู่:</p>
					</div>
					<div class="col-10">
						<p class="text-dark"> อาคาร 611/612 สำนักพระราชวัง สนามเสือป่า ถนนศรีอยุธยา เขตดุสิต กรุงเทพฯ 10300</p>
					</div>
				</div>
				<div class="row">
					<div class="col-2">
						<p class="text-primary">โทรศัพท์: </p>
					</div>
					<div class="col-10">
						<p class="text-dark"> 02-282-6808, 02-282-6782, 02-282-6786 </p>
						
					</div>
				</div>
				<div class="row">
					<div class="col-2">
						<p class="text-primary">โทรสาร: </p>
					</div>
					<div class="col-10">
						<p class="text-dark"> 02-282-1396</p>
						
					</div>
				</div>
				<div class="row">
					<div class="col-2">
						<p class="text-primary">อีเมล์: </p>
					</div>
					<div class="col-10">
						<p class="text-dark"> info@cdti.ac.th </p>
					</div>
				</div>
				<div class="row mb-4">
					<div class="col-2">
						<p class="text-primary">เฟซบุ๊ก: </p>
					</div>
					<div class="col-10">
						<a href="https://www.facebook.com/AFT.CDV/"> <p class="text-dark"> เพจ องค์การนักวิชาชีพในอนาคตแห่งประเทศไทย โรงเรียนจิตรลดาวิชาชีพ </p> </a>
					</div>
				</div>
				<!-- <h3 class="text-primary">ติดตามเราได้ที่</h3>

				<p class="text-primary"><i class="fab fa-facebook"></i>  Facebook : <a href="" class="text-dark">www.facebook.com/Business.Chitralada</a></p> -->
			</div>
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7750.382127325458!2d100.51340137036131!3d13.76734866549204!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc4710800dc05baa6!2sChitralada%20Vocational%20School!5e0!3m2!1sen!2sth!4v1580215096594!5m2!1sen!2sth" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
			
		
	</div>
</section>
