<section class="mini-spacer">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<p class="text-dark text-indent mb-2"></p>
				<!-- <a href="" class="read-more">ดูเพิ่มเติม >></a> -->
				<div class="table-responsive ">
                    <?php
					
					$pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,9);
                    $pages_fac_vocational= $this->page_model->get_all_pages_fac(true,6);
                    ?>
					<table class="table table-bordered table-branch">
						<thead>
							<tr class="bg-primary text-white">
								<th class="text-center">สาขาวิชา</th>
								
							</tr>
						</thead>
						<tbody>
							<tr style="background-color: aliceblue;">
								<td class="text-left"><h5>ระดับประกาศนียบัตรวิชาชีพ (ปวช.)</h5></td>
								
							</tr>
                        <?php
                            
                            foreach ($pages_fac_vocational as  $value) {
                                ?>
                            <tr>
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></td>
								
                            </tr>
                            
                            <?php
                                
                            }
                            ?>
							
							<tr style="background-color: aliceblue;">
								<td class="text-left"><h5>ระดับประกาศนียบัตรวิชาชีพ (ปวส.)</h5></td>
								
							</tr>
                        <?php
                            
                            foreach ($pages_fac_high_vocational as  $value) {
                                ?>
                            <tr>
								<td class="text-left"><a  href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></td>
								
                            </tr>
                            
                            <?php
                                
                            }
							?>
							
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/7.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/11.png" alt="" class="img-fluid img-responsive">
					</div>
					<div class="col-sm-4 col-xs-12">
						<img src="<?=base_url(); ?>assets/images/10.png" alt="" class="img-fluid img-responsive">
					</div>
				</div>
            </div>
            <!-- <div class="hidden-sm-down col-md-4">
				<img src="<?=base_url();?>assets/images/13.png" alt="" class="img-fluid img-responsive">
			</div> -->
		</div>
	</div>
</section>