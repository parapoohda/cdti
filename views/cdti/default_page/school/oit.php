<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> การเปิดเผยข้อมูลสาธารณะ (OIT)</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container">

    <div id="accordion" class="panel-group">
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyOne" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">ข้อมูลพื้นฐาน</a>
                </h4>
            </div>
            <div id="panelBodyOne" class="panel-collapse collapse in borderone show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="//www.cdti.ac.th/โครงสร้างสถาบัน">1. โครงสร้างองค์กร</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/ผู้บริหาร">2. ข้อมูลผู้บริหาร</a><br>
                        <a target="_blank" href="">3. อำนาจหน้าที่</a><br>
                        <a target="_blank" href="<?=base_url()?>/uploads/files/04_เล่มเเผนยุทธศาตร์สถาบันเทคโนโลยีจิตรลดา_2562_2564.pdf">4. แผนยุทธศาสตร์หรือแผนพัฒนาหน่วยงาน</a><br>
                        <a target="_blank" href="">5. ข้อมูลติดต่อหน่วยงาน</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/พรบ">6. กฎหมายที่เกี่ยวข้อง</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/featured-posts">7. ข่าวประชาสัมพันธ์ทั่วไป</a><br>
                        <a target="_blank" href="">8. Q&A</a><br>
                        <a target="_blank" href="//www.facebook.com/ChitraladaTechnologyInstitute">9. Social Network</a><br>
                    </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">แผนการดำเนินงาน</a>
                </h4>
            </div>
            <div id="panelBodyTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                    <a target="_blank" href="">10. แผนดำเนินงานประจำปี</a><br>
                    <a target="_blank" href="">11. รายงานการกำกับติดตาม การดำเนินงาน รอบ 6 เดือน</a><br>
                    <a target="_blank" href="">12. รายงานผลการดำเนินงานประจำปี</a><br>
                    </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-3" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การปฏิบัติงาน</a>
                </h4>
            </div>
            <div id="panelBody-3" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                      <a target="_blank" href="">13. มาตรฐานการปฏิบัติงาน</a>
                      </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-4" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การให้บริการงาน</a>
                </h4>
            </div>
            <div id="panelBodyTwo-4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                    <a target="_blank" href="">14. มาตรฐานการให้บริการ</a><br>
                    <a target="_blank" href="">15. ข้อมูลเชิงสถิติของการให้บริการ</a><br>
                    <a target="_blank" href="">16. รายงานผลการสำรวจความพึงพอใจในการให้บริการ</a><br>
                    <a target="_blank" href="">17. E-Service</a><br>
                    </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-5" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">แผนการใช้จ่ายงบประมาณประจำปี</a>
                </h4>
            </div>
            <div id="panelBodyTwo-5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                    <a target="_blank" href="">18. แผนการใช้จ่ายงบประมาณประจำปี</a><br>
                    <a target="_blank" href="">19. รายงานการกำกับติดตาม การใช้จ่ายงบประมาณ รอบ 6 เดือน</a><br>
                    <a target="_blank" href="">20. รายงานผลการใช้จ่ายงบประมาณ ประจำปี</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-6" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การจัดซื้อจัดจ้างหรือการจัดหาพัสดุ</a>
                </h4>
            </div>
            <div id="panelBodyTwo-6" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                      <a target="_blank" href="">21. แผนการจัดซื้อจัดจ้างหรือแผนการจัดหาพัสดุ</a><br>
                      <a target="_blank" href="">22. ประกาศต่าง ๆ เกี่ยวกับการจัดซื้อจัดจ้างหรือการจัดหาพัสดุ</a><br>
                      <a target="_blank" href="">23. สรุปผลการจัดซื้อจัดจ้างหรือการจัดหาพัสดุรายเดือน</a><br>
                      <a target="_blank" href="">24. รายงานผลการจัดซื้อจัดจ้างหรือการจัดหาพัสดุประจำปี</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-7" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การบริหารและพัฒนาทรัพยากรบุคคล</a>
                </h4>
            </div>
            <div id="panelBodyTwo-7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                      <a target="_blank" href="">25. นโยบายการบริหารทรัพยากรบุคคล</a><br>
                      <a target="_blank" href="">26. การดำเนินการตามนโยบายการบริหารทรัพยากรบุคคล</a><br>
                      <a target="_blank" href="">27. หลักเกณฑ์การบริหารและพัฒนาทรัพยากรบุคคล</a><br>
                      <a target="_blank" href="">28. รายงานผลการบริหารและพัฒนาทรัพยากรบุคคลประจำปี</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-8" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การจัดการเรื่องร้องเรียนการทุจริตและการเปิดโอกาสให้เกิดการมีส่วนร่วม</a>
                </h4>
            </div>
            <div id="panelBodyTwo-8" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                      <a target="_blank" href="">29. แนวปฏิบัติการจัดการเรื่องร้องเรียนการทุจริต</a><br>
                      <a target="_blank" href="">30. ช่องทางแจ้งเรื่องร้องเรียนการทุจริต</a><br>
                      <a target="_blank" href="">31. ข้อมูลเชิงสถิติเรื่องร้องเรียนการทุจริตประจำปี</a><br>
                      <a target="_blank" href="">32. ช่องทางการรับฟังความคิดเห็น</a><br>
                      <a target="_blank" href="">33. การเปิดโอกาสให้เกิดการมีส่วนร่วม</a>
                      </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-9" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การดำเนินการเพื่อป้องกันการทุจริต</a>
                </h4>
            </div>
            <div id="panelBodyTwo-9" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>/uploads/files/ประกาศ เรื่อง เจตจำนงสุจริตในการบริหารงานของสถาบันเทคโนโลยีจิตรดา.pdf">34. เจตจำนงของผู้บริหารสูงสุด  <b>(เอกสาร)</b></a>
                    &nbsp;,&nbsp;
                    <a target="_blank" href="<?=base_url()?>/uploads/files/Announcement on Integrity and Transparency Intention of Chitralada Technology Institute.pdf">Announcement on Integrity and Transparency Intention  <b>(Document)</b></a>
                    <br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/คำสั่ง เรื่อง แต่งตั้งคณะทำงานเพื่อเข้ารับการประเมิณคุณธรรม2563.pdf">35. การมีส่วนร่วมของผู้บริหาร  <b>(เอกสาร)</b></a><br>
                    <a target="_blank" href="">36. การประเมินความเสี่ยงการทุจริตประจำปี</a><br>
                    <a target="_blank" href="">37. การดำเนินงานเพื่อจัดการความเสี่ยงการทุจริต</a><br>
                    <a target="_blank" href="">38. การเสริมสร้างวัฒนธรรมองค์กร</a><br>
                    <a target="_blank" href="">39. แผนปฏิบัติการป้องกันการทุจริตประจำปี</a><br>
                    <a target="_blank" href="">40. รายงานการกำกับติดตามการดำเนินการป้องกันการทุจริต</a><br>
                    <a target="_blank" href="">41. รายงานผลการดำเนินการป้องกันการทุจริต</a>
                  </p>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-10" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">มาตรการภายในเพื่อส่งเสริมความโปร่งใสและป้องกันการทุจริต</a>
                </h4>
            </div>
            <div id="panelBodyTwo-10" class="panel-collapse collapse" >
                <div class="panel-body">
                    <p class="pt10-pl30">
                      <a target="_blank" href="">42. มาตรการเผยแพร่ข้อมูลต่อสาธารณะ</a><br>
                      <a target="_blank" href="">43. มาตรการให้ผู้มีส่วนได้ส่วนเสียมีส่วนร่วม</a><br>
                      <a target="_blank" href="">44. มาตรการส่งเสริมความโปร่งใสในการจัดซื้อจัดจ้าง</a><br>
                      <a target="_blank" href="">45. มาตรการจัดการเรื่องร้องเรียนการทุจริต</a><br>
                      <a target="_blank" href="">46. มาตรการป้องกันการรับสินบน</a><br>
                      <a target="_blank" href="">47. มาตรการป้องกันการขัดกันระหว่างผลประโยชน์ส่วนตนกับผลประโยชน์ส่วนรวม</a><br>
                      <a target="_blank" href="">48. มาตรการตรวจสอบการใช้ดุลพินิจ</a>
                    </p>
                </div>
            </div>
        </div>


    </div>
</div>
