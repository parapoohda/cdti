<section class="mini-spacer campus-box mt-5">
	<div class="container">
		<div class="section-header campus-header m-b-60">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title color-primary mt-4"><img src="<?php echo base_url();?>cdti_assets/images/seminar.png" alt=""> สภาสถาบันฯ</h3>
				</div>
			</div>

		</div>
		<nav class="nav nav-tabs navbar-team">
		  <a class="nav-link" href="<?php echo base_url('สภาสถาบัน')?>">สภาสถาบันฯ</a>
		  <a class="nav-link active" href="<?php echo base_url('โครงสร้างและผู้บริหาร')?>">โครงสร้างและผู้บริหาร</a>
		</nav>
	</div>
</section>

<?php if($teams_type3): $i = 0;?>

<section id="teambanner" class="teamprojects spacer pb-0">
	<div class="container">
			<?php foreach ($teams_type3 as $key => $value):
						if($key == 0 and $i== 0):
			?>
					<div class="row justify-content-center">
						<div class="col-md-4">
							<img src="<?php echo base_url().$value->path_big; ?>" alt="<?php echo html_escape($value->title); ?>" class="img-responsive img-fluid">
						</div>
						<div class="col-md-8 text-center">
							<img src="<?php echo base_url();?>cdti_assets/images/logo-edit.png" alt="สถานบันติตรลดา" class="teamleader-logo img-fluid mx-auto d-block mt-4 mb-4">
							<p class="teamleader-name"><?php echo html_escape($value->title); ?></p>
							<h3 class="teamleader-title"><?php echo html_escape($value->description); ?></h3>
							<h6 class="teamleader-title"><?php echo html_escape($value->description2); ?></h6>
						</div>
					</div>
		   		<?php $i++;?>
		   <?php endif;?>
		   <?php endforeach;?>
	</div>
</section>
<?php endif;?>

<?php if($teams_type3): $i = 0;?>
  <section class="teamlist spacer">
        <div class="container ">
          <?php foreach ($teams_type3 as $key => $value):
								if($key == 0)continue;
						?>
                <?php if($i !=  $value->gallery_level)
                      {
                          if($i != 0)echo '</div>';
                          $i = $value->gallery_level;
                          echo '<div class="row justify-content-md-center">';
                      }?>
                <div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 mb-3">
                    <div class="team text-center">
                      <img src="<?php echo base_url().$value->path_big; ?>" alt="<?php echo html_escape($value->title); ?>" class="w-90-per d-block mx-auto mb-2">
                      <p class="team-name mb-0 color-darkgray"><?php echo html_escape($value->title); ?></p>
                      <p class="color-3"><small><?php echo html_escape($value->description); ?></small></p>
                    </div>
                </div>
           <?php endforeach;?>
    	</div>
  </section>
<?php endif;?>
