<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
						<h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> รายงานการประชุมคณะกรรมการบริหารสถาบันฯ</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container" style="min-height: 300px;">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-1" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">ประจำปีการศึกษา 2562</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_12.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 12 วันที่ 16 ตุลาคม 2562</a><br>
												<a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_13.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 13 วันที่ 20 พฤศจิกายน 2562</a><br>
												<a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_14.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 14 วันที่ 18 ธันวาคม 2562</a><br>
												<a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_15.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 15 วันที่ 15 มกราคม 2563</a><br>
												<a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_16.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 16 วันที่ 19 กุมภาพันธ์ 2563</a><br>
												<a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_17.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 17 วันที่ 18 มีนาคม 2563</a><br>
												<a target="_blank" href="<?=base_url()?>/uploads/files/bod/62_18.pdf">รายงานการประชุมคณะกรรมการบริหารสถาบันฯ ครั้งที่ 18 วันที่ 22 เมษายน 2563</a><br>
                    </p>
                </div>
            </div>
        </div>
      </section>

    </div>
</div>
