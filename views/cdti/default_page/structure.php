<section class="mini-spacer campus-box mt-5">
	<div class="container">
		<div class="section-header campus-header m-b-60">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title color-primary mt-4"><img src="<?php echo base_url();?>cdti_assets/images/seminar.png" alt=""> โครงสร้างสถาบันฯ </h3>
				</div>
			</div>

		</div>
		<nav class="nav nav-tabs navbar-team">
			<a class="nav-link " href="<?php echo base_url('สภาสถาบัน')?>">สภาสถาบันฯ</a>
			<a class="nav-link " href="<?php echo base_url('ผู้บริหาร')?>">ผู้บริหาร</a>
			<a class="nav-link active" href="<?php echo base_url('โครงสร้างสถาบัน')?>">โครงสร้างสถาบันฯ </a>
		</nav>
	</div>
</section>



<div class="row m-b-60">
    <div class="col-md-12">
        <img src="<?php echo base_url()?>uploads/images/โครงสร้างสถาบันเทคโนโลยีจิตรลดา.jpg" alt="" class="img-responsive">
    </div>

</div>
