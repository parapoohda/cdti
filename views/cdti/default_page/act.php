<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> พระราชบัญญัติ/ข้อบังคับ/ระเบียบ/ประกาศ/คำสั่งสถาบันเทคโนโลยีจิตรลดา</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container mb-5">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-1" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">1.พระราชบัญญัติ</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/act/01_01.pdf">- พระราชบัญญัติสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2561</a><br>
                    </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-2" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">2.โครงสร้างองค์กร</a>
                </h4>
            </div>
            <div id="panelBody-2" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_01.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งส่วนงานของสถาบัน พ.ศ. 2561</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_02.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานภายในสำนักงานสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_03.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในสำนักงานสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา ตรา เครื่องหมาย หรือสัญลักษณ์ของสถาบันหรือส่วนงานของสถาบัน พ.ศ. 2562</a><br>
                      <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/02_05.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการรักษาราชการแทน การมอบอำนาจให้ปฏิบัติการแทน และการมอบอำนาจช่วงให้ปฏิบัติการแทนของผู้ดำรงตำแหน่งต่างๆในสถาบัน พ.ศ. 2562</a><br>-->
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_06.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในคณะและส่วนงานที่มีฐานะเทียบเท่าคณะ พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_07.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งส่วนงานของสถาบัน (ฉบับที่ 2) พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง งานตรวจสอบภายในของสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_09.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานภายในสำนักงานสถาบัน (ฉบับที่ 2) พ.ศ. 2563</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในสำนักงานสถาบัน(ฉบับที่ 2) พ.ศ. 2563</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_11.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานภายในสำนักงานสถาบัน (ฉบับที่ 3) พ.ศ. 2564</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_12.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในสำนักงานสถาบัน(ฉบับที่ 3) พ.ศ. 2564</a><br>
                  
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_13.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานภายในสำนักงานสถาบัน (ฉบับที่ 4) พ.ศ. 2564</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_14.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในสำนักงานสถาบัน(ฉบับที่ 4) พ.ศ. 2564</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_15.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การใช้ชื่อย่อ รหัสพยัญชนะของสถาบันฯ และเลขที่หนังสือออกจากสถาบันฯ ส่วนงานและงานภายในสำนักงานสถาบันฯ</a><br>
                    
                    </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-3" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">3.สภาสถาบันฯ</a>
                </h4>
            </div>
            <div id="panelBody-3" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งนายกสภาสถาบัน พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_02.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งกรรมการสภาสถาบันผู้ทรงคุณวุฒิ พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_03.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์และการได้มาซึ่งกรรมการสภาสถาบันประเภทหัวหน้าส่วนงานและผู้ปฏิบัติงานในสถาบัน พ.ศ. 2561</a><br>
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/03_13.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์และการได้มาซึ่งกรรมการสภาสถาบันประเภทหัวหน้าส่วนงานและผู้ปฏิบัติงานในสถาบัน พ.ศ. 2561</a><br>
-->                 <a target="_blank" href="<?=base_url()?>uploads/files/act/03_04.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์และวิธีการประชุมสภาสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_05.pdf">- ประกาศสำนักนายกรัฐมนตรี เรื่อง แต่งตั้งนายกสภาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_06 new.pdf">- ประกาศสำนักนายกรัฐมนตรี เรื่อง แต่งตั้งกรรมการสภาสถาบันผู้ทรงคุณวุฒิของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_07.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งอุปนายกสภาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_08.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งกรรมการและเลขานุการสภาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_09.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งกรรมการสภาสถาบันประเภทหัวหน้าส่วนงานและผู้ปฏิบัติงานในสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_11.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการตรวจสอบ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_10.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการติดตาม ตรวจสอบ และประเมินผลการปฏิบัติงานของสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_12.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการติดตาม ตรวจสอบ และประเมินผลการปฏิบัติงานของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_14.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการที่ปรึกษาด้านการเงิน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_15.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการติดตาม ตรวจสอบ และประเมินผลการปฏิบัติงานของสถาบันเทคโนโลยีจิตรลดา (เพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_16.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพิจารณาตำแหน่งทางวิชาการ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_17.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการธรรมาภิบาลและคุณธรรมจริยธรรมสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งอธิการบดี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_18.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา  เรื่อง แต่งตั้งคณะกรรมการตามกฎกระทรวงการจัดกลุ่มสถาบันอุดมศึกษา พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_19.pdf">- ประมวลจริยธรรมของสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2564</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-4" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">4.สภาวิชาการ</a>
                </h4>
            </div>
            <div id="panelBody-4" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/04_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสภาวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/04_02.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งประธานสภาวิชาการ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/04_03.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งกรรมการสภาวิชาการ</a><br>

                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-5" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">5.การบริหารสถาบัน</a>
                </h4>
            </div>
            <div id="panelBody-5" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_nogift.pdf">- ประกาศเจตจำนงสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายไม่รับ - ไม่ให้ของขวัญและของกำนัลทุกชนิดจากการปฏิบัติหน้าที่ตามธรรมจรรยา "No Gift Policy"</a><br>
                    
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_02.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์ คุณสมบัติ วิธีการได้มา วาระการดำรงตำแหน่ง การพ้นจากตำแหน่ง อำนาจหน้าที่ และการบริหารงานของหัวหน้าส่วนงาน พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_03.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้บริหารเทคโนโลยีสารสนเทศระดับสูง (Chief Information Officer : CIO) ของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_04.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบอำนาจให้รองอธิการบดีและหัวหน้าส่วนงาน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_05.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการเทคโนโลยีสารสนเทศ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/02_05.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการรักษาราชการแทน การมอบอำนาจให้ปฏิบัติการแทน และการมอบอำนาจช่วงให้ปฏิบัติการแทนของผู้ดำรงตำแหน่งต่างๆในสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_07.pdf">- ประกาศสำนักนายกรัฐมนตรี เรื่อง แต่งตั้งอธิการบดีสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_10.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งรองอธิการบดีสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_11.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งหัวหน้าส่วนงาน</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_08.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยคณะกรรมการประจำส่วนงานด้านวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_09.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการประชุมและวิธีการดำเนินการประชุม พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_12.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้รักษาการแทนอธิการบดี สถาบันเทคโนโลยีจิตรลดา</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_13.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้รักษาการแทนหัวหน้าส่วนงาน และตำแหน่งอื่นใดของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_14.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบอำนาจให้รองอธิการบดีและหัวหน้าส่วนงานปฏิบัติหน้าที่แทน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_15.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยระบบ หลักเกณฑ์ และวิธีการประกันคุณภาพการศึกษาและการวิจัย พ.ศ. 2563</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_17.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะเทคโนโลยีอุตสาหกรรม</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_19.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการส่งเสริมงานวิจัย นวัตกรรม และผลงานสร้างสรรค์</a><br>
                  <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/05_20.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการวางแผน กำกับ ติดตามและดูแลโครงการจัดการขยะ สถาบันเทคโนโลยีจิตรลดา</a><br>-->

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_21.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะเทคโนโลยีดิจิทัล</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_22.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำโรงเรียนจิตรลดาวิชาชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_23.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะบริหารธุรกิจ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_24.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพัฒนาสำนักงานสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_25.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารการประกันคุณภาพการศึกษา พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_26.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกฎหมาย สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_27.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพัฒนาการสื่อสารองค์กรของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_28.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง เจตจำนงสุจริตในการบริหารงานของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_29.pdf">- Announcement on Integrity and Transparency Intention of Chitralada Technology Institute</a><br>
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/05_30.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะทำงานเพื่อเข้ารับการประเมินคุณธรรมและความโปร่งใส ในการดำเนินงานของหน่วยงานภาครัฐ ปีงบประมาณ พ.ศ. 2563</a><br>
-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_31.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการประชุมผ่านสื่ออิเล็กทรอนิกส์ พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_32.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการจัดการควบคุมภายในและบริหารจัดการความเสี่ยง สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_33.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริการวิชาการ สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_34.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์ คุณสมบัติ วิธีการได้มา วาระการดำรงตำแหน่ง การพ้นจากตำแหน่ง อำนาจหน้าที่ และการบริหารงานของหัวหน้าส่วนงาน (ฉบับที่ 2) พ.ศ. 2563</a><br>
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/file_5fb2392d7b6e4.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะทำงานเพื่อเข้ารับการประเมินคุณธรรมและความโปร่งใสในการดำเนินงานของหน่วยงานภาครัฐ ปีงบประมาณ พ.ศ. 2564</a><br>
-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_5fb2397b436e4.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้รับผิดชอบการดำเนินการประเมินคุณธรรมและความโปร่งใสในการดำเนินงานของหน่วยงานภาครัฐ ปีงบประมาณ พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_35.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารการประกันคุณภาพการศึกษา(เพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_16.pdf">- กฏบัตรของคณะกรรมการตรวจสอบ สถาบันเทคโนโลยีจิตรลดา</a><br>
                    
                  <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/05_16.pdf">- กฎบัตรการตรวจสอบภายในสถาบันเทคโนโลยีจิตรลดา ว่าด้วยวัตถุประสงค์ อำนาจหน้าที่ และความรับผิดชอบของงานตรวจสอบภายใน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_36.pdf">- กฏบัตรของคณะกรรมการตรวจสอบ สถาบันเทคโนโลยีจิตรลดา</a><br>
--><a target="_blank" href="<?=base_url()?>uploads/files/act/05_42.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการจัดการควบคุมภายในและบริหารจัดการความเสี่ยง ประจำปีงบประมาณ พ.ศ. 2565 (แก้ไขเพิ่มเติม)</a><br>
                    
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_38.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา  เรื่อง แต่งตั้งคณะกรรมการบริหารระบบเทคโนโลยีสารสนเทศ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_39.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา  เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะบริหารธุรกิจ (แก้ไขเพิ่มเติม) ปี 2565.pdf</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_40.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา  เรื่อง มอบอำนาจให้หัวหน้างานศูนย์เรียนรู้เกษตรนวัต ปฏิบัติหน้าที่แทน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_41.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง เปลี่ยนแปลงกรรมการ ในคณะอนุกรรมการพัฒนาระบบการประกันคุณภาพการศึกษาตามเกณฑ์ EdPEx</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_43.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการจัดการควบคุมภายในและบริหารจัดการความเสี่ยงสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_44.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการจัดทำนิตยสาร CDTI</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_45.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารเครือข่ายเชิงบูรณาการ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_46.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารหอพักนักเรียนนักศึกษาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_47.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน โรงเรียนจิตรลดาวิชาชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_48.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะบริหารธุรกิจ (แก้ไขเพิ่มเติม) ปี 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_49.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะบริหารธุรกิจ ปี 2565</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_50.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพัฒนาแผนและนโยบายเทคโนโลยีสารสนเทศ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_51.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งบุคลากรปฏิบัติหน้าที่หัวหน้าแผนกวิชา โรงเรียนจิตรลดาวิชาชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_52.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งบุคลากรปฏิบัติหน้าที่หัวหน้าแผนกวิชาเทคโนโลยีสารสนเทศและพาณิชยกรรม โรงเรียนจิตรลดาวิชาชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_53.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้รับผิดชอบการประเมินคุณธรรมและความโปร่งใสในการการดำเนินงานของหน่วยงานภาครัฐ พ.ศ. 2565</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_54.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งอนุศาสกภายใต้โครงการสวัสดิการหอพักนักศึกษา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_55.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบอำนาจให้หัวหน้างานศูนย์เรียนรู้เกษตรนวัต ปฎิบัติหน้าที่แทน (เพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_56.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดาเรื่อง แต่งตั้งบุคลากรปฏิบัติหน้าที่รองผู้อำนวยการ ผู้ช่วยผู้อำนวยการ โรงเรียนจิตรลดาวิชาชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_57.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายการบริหารความเสี่ยง สถาบันเทคโนโลยีจิตรลดา (CDTI Enterprise Risk Management Policy)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_58.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน สำนักวิชาศึกษาทั่วไป พ.ศ. 2565</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-6" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">6.การศึกษา วิชาการ และนักเรียนนักศึกษา</a>
                </h4>
            </div>
            <div id="panelBody-6" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_02.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยครุยวิทยฐานะ เข็มวิทยฐานะ และครุยประจำตำแหน่ง โอกาสและเงื่อนไขที่ใช้ พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_03.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยเครื่องแบบ เครื่องหมาย และเครื่องแต่งกายของนักศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_04.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยวินัยนักศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_05.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสโมสรนักศึกษา พ.ศ. 2561</a><br>
                    <!--a target="_blank" href="<?=base_url()?>uploads/files/act/06_06.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การกำหนดลักษณะ ชนิด ประเภท และส่วนประกอบของครุยวิทยฐานะ เข็มวิทยฐานะ และครุยประจำตำแหน่ง พ.ศ. 2561</a><br>-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_07.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง อัตราค่าธรรมเนียมการศึกษา และเงินเรียกเก็บประเภทอื่น ๆ สำหรับนักศึกษาระดับปริญญาตรี พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ค่าธรรมเนียมและเงินเรียกเก็บประเภทอื่นๆสำหรับนักเรียน นักศึกษา โรงเรียนจิตรลดาวิชาชีพ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ค่าธรรมเนียมและเงินเรียกเก็บประเภทอื่นๆสำหรับนักเรียนระดับ ปวช. โรงเรียนจิตรลดาวิชาชีพ โครงการพิเศษเกษตรนวัต พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวปฏิบัติตนของนักเรียนและนักศึกษาในสถาบันและพื้นที่สำนักพระราชวัง</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_11.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการจัดการศึกษาระดับประกาศนียบัตรวิชาชีพ (ปวช.) และระดับประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.) พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_12.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกิจการนักเรียนและนักศึกษา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_13.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การส่งเสริมคุณธรรม จริยธรรม ของนักเรียนและนักศึกษาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_5fd04a1e6d1b2.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสงเคราะห์นักเรียนและนักศึกษา สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2563</a><br>
                    
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_14.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การกำหนดปริญญาในสาขาวิชา อักษรย่อสำหรับสาขาวิชา ครุยวิทยฐานะ เข็มวิทยฐานะ และครุยประจำตำแหน่ง พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_15.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การคืนเงินประกันของเสียหายให้แก่ผู้มีสิทธิ์รับเงินประกันของเสียหาย พ.ศ. 2564</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_16.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดา ว่าด้วยการศึกษาระดับปริญญาตรี (ฉบับที่ 2) พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_17.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารกิจการนักเรียนนักศึกษา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_18.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพิจารณาทุนการศึกษา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_19.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง เครื่องแบบ เครื่องหมาย และเครื่องแต่งกายของผู้เรียน ระดับประกาศนียบัตรวิชาชีพ (ปวช.) และระดับประกาศนียบัตรวิชาชีพขั้นสูง (ปวส.) พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_20.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารสโมสรนักศึกษา ประจำปีการศึกษา 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_21.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารสโมสรนักศึกษา ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_22.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแต่งกายและข้อปฏิบัติสำหรับบัณฑิตในพิธีพระราชทานปริญญาบัตร ประจำปีการศึกษา 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_23.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การกำหนดลักษณะ ชนิด ประเภท และส่วนประกอบของครุยวิทยฐานะ เข็มวิทยฐานะและครุยประจำตำแหน่ง พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_24.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดวันเปิด-ปิด ภาคการศึกษา ประจำปีการศึกษา 2563 (ฉบับที่ 2)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_25.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง วินัยผู้เรียนระดับประกาศนียบัตรวิชาชีพ (ปวช.) และระดับประกาศนียบัตรวิชาชีพขั้นสูง (ปวส.) พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_26.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราการจ่ายทุนการศึกษาประเภทต่าง ๆ พ.ศ. 2563 (แก้ไขเพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_27.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง อัตราค่าธรรมเนียมการศึกษา และเงินเรียกเก็บประเภทอื่นๆ สำหรับนักศึกษาระดับปริญญาตรี พ.ศ. 2564</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-7" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">7.การบริหารงานบุคคล</a>
                </h4>
            </div>
            <div id="panelBody-7" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_02.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยเครื่องแบบพิธีการของพนักงานสถาบัน พ.ศ. 2561</a><br>
                  <!--  <a target="_blank" href="<?=base_url()?>uploads/files/act/07_03.pdf">3. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการกำหนดเงินเดือน และเงินประจำตำแหน่งสำหรับผู้ปฏิบัติงานในสถาบัน พ.ศ. 2562</a><br> -->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง วันเวลาปฏิบัติงาน วันหยุดและวันลา พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_05.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสวัสดิการและสิทธิประโยชน์ของบุคลากร พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_06.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_07.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล (ฉบับที่ 2) พ.ศ.2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายสนับสนุน พ.ศ. 2562</a><br>
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/07_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน พ.ศ. 2562</a><br>-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_11.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดเกณฑ์คะแนนทดสอบความสามารถทางภาษาอังกฤษของอาจารย์ประจำ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_12.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการสวัสดิการและสิทธิประโยชน์</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_13.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล (ฉบับที่ 3) พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_14.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ (ฉบับที่ 2) พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_20.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสวัสดิการและสิทธิประโยชน์ของบุคลากร พ.ศ. 2562</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_15.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกองทุนสำรองเลี้ยงชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_16.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายการบริหารทรัพยากรบุคคล และการพัฒนาบุคลากรของสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_17.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายส่งเสริมจรรยาบรรณของบุคลากรสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_19.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง จรรยาบรรณของครูวิชาชีพและคณาจารย์สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_18.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษาต่อระดับปริญญาโทหรือปริญญาเอกแบบลาศึกษาบางเวลา พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_21.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_22.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน (ฉบับที่ 2) พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_23.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การขอมีบัตรประจำตัวผู้ปฏิบัติงานในสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_24.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยค่าชดเชย พ.ศ. 2563</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_25.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดา ว่าด้วยกองทุนพระราชทานเพื่อสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_26.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดา ว่าด้วยชุดปฏิบัติการผู้ปฏิบัติหน้าที่นอกสถานที่ พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_27.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา  เรื่อง แต่งตั้งคณะกรรมการสวัสดิการและสิทธิประโยชน์ (แก้ไขเพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_28.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา  เรื่อง แต่งตั้งคณะอนุกรรมการร้านค้าสวัสดิการสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_29.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกองทุนพระราชทานเพื่อสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_30.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการสโมสรบุคลากรสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_31.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการสวัสดิการสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_32.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะทำงานเฉพาะกิจวิเคราะห์งานและอัตรากำลังสายสนับสนุน สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_33.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_34.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การให้คณาจารย์ลาไปเพิ่มพูนความรู้ทางวิชาการ พ.ศ. 2565</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_35.pdf">- ระเบียบคณะกรรมการสวัสดิการและสิทธิประโยชน์ สถาบันเทคโนโลยีจิตรลดา ว่าด้วยสโมสรบุคลากรสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2563</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-8" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">8.งบประมาณ การเงิน การบัญชี และการพัสดุ</a>
                </h4>
            </div>
            <div id="panelBody-8" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงบประมาณ การเงิน การพัสดุ และทรัพย์สินของสถาบัน พ.ศ. 2561</a><br>
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/08_02.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการตรวจสอบพัสดุประจำปี 2562</a><br>-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_03.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบหมายผู้รับผิดชอบลงชื่อเป็นผู้อนุญาตให้นำพัสดุออกนอกพื้นที่</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการจัดสรรงบประมาณรายจ่าย พ.ศ. 2562</a><br>
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/08_05.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้ปฏิบัติงานในสถาบันเป็นกรรมการเก็บรักษาเงินคงเหลือประจำวัน</a><br>-->
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/08_06.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง เปลี่ยนแปลงกรรมการเก็บรักษาเงินคงเหลือประจำวัน</a><br>-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_07.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการจ่ายเงินสำรองจ่ายและเงินสดย่อย พ.ศ.2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการงบประมาณ การรับเงิน การเก็บรักษาเงิน การเบิกเงิน การจ่ายเงิน และการควบคุมดูแลการจ่าย พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การบริหารงานบริการวิชาการที่มีการจัดเก็บรายได้ พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง เกณฑ์และอัตราการเบิกจ่ายค่าใช้จ่ายและการจัดสรรเงินรายได้จากการให้บริการวิชาการที่มีการจัดเก็บรายได้ พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_11.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราการจ่ายเงินตอบแทนในลักษณะต่างๆ (แก้ไขเพิ่มเติม) ฉบับที่ 1 พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_12.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การสนับสนุนทุนอุดหนุนวิจัย นวัตกรรม และผลงานสร้างสรรค์ และการจ่ายเงินรางวัลผลงานทางวิชาการ พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_13.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราค่าใช้จ่ายในการเดินทางไปปฏิบัติงาน พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_14.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราการจ่ายค่าใช้จ่ายในการฝึกอบรม ดูงาน การจัดงาน และการจัดประชุม ภายในประเทศและต่างประเทศ พ.ศ. 2563</a><br>
                    
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_15.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบหมายผู้รับผิดชอบลงชื่อเป็นผู้อนุญาตให้นำพัสดุออกนอกพื้นที่ (แก้ไขเพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_16.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจ่ายค่าตอบแทนให้นัเรียนนักศึกษาช่วยปฏิบัติงาน (ฉบับที่ 2) พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_17.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจ่ายค่าตอบแทนให้นัเรียนนักศึกษาช่วยปฏิบัติงาน พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_18.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การประมาณการค่าเผื่อหนี้สงสัยจะสูญและการจำหน่ายหนี้สูญ พ.ศ. 2564</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-9" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">9.อื่นๆ</a>
                </h4>
            </div>
            <div id="panelBody-9" class="panel-collapse collapse">
                <div class="panel-body">
                  <h5 class="panel-title">ทุนการศึกษา</h5>
                  <p class="pt10-pl30">
                    <!--<a target="_blank" href="<?=base_url()?>uploads/files/act/09_01.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษาพระราชทาน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_12.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แก้ไขเพิ่มเติมรายละเอียดทุนการศึกษาพระราชทาน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพิจารณาทุนการศึกษา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_5fe563ef7c731.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราการจ่ายทุนการศึกษาประเภทต่างๆ พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_01.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกองทุนสงเคราะห์นักเรียนและนักศึกษา สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_02.pdf">- ประกาศ เรื่อง กำหนดรายละเอียด “ทุนคุณแม่ประพันธ์ สิงคาลวณิช”</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_03.pdf">- ประกาศ เรื่อง กำหนดรายละเอียด “ทุนพลังแผ่นดิน กลุ่มปฐพีจุฬา”</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การขอรับทุนช่วยเหลือการศึกษา สำหรับผู้ที่จะเข้าศึกษา ระดับประกาศนียบัตรวิชาชีพ ชั้นปีที่ 1 ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_05.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดรายละเอียด “ทุนโควตาสำหรับนักเรียนจากโรงเรียนจิตรลดา นักเรียนนักศึกษาโรงเรียนจิตรลดาวิชาชีพ และบุตรข้าราชบริพาร ประจำปีการศึกษา 2564“</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_06.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดรายละเอียด “ทุนส่งเสริมการศึกษาสำหรับนักเรียนนักศึกษาในส่วนภูมิภาค“ ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_07.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดรายละเอียด “ทุนส่งเสริมการศึกษาสำหรับนักเรียนนักศึกษาในส่วนภูมิภาค” ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดรายละเอียดการรับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2563 (เพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาการขอรับทุนช่วยเหลือการศึกษา สำหรับผู้ที่จะเข้าศึกษาระดับประกาศนียบัตรวิชาชีพ ชั้นปีที่ 1 ประจำปีการศึกษา 2564</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง คุณสมบัติและหลักเกณฑ์การพิจารณาทุนโควตาและทุนส่งเสริมการศึกษาในส่วนภูมิภาค และทุนโควตาสถานศึกษา พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_11.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง คุณสมบัติและหลักเกณฑ์การพิจารณาทุนการศึกษา สำหรับนักเรียนและนักศึกษาที่ขาดแคลนทุนทรัพย์ พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_12.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษา ดร.ท่านผู้หญิงทัศนีย์ บุณยคุปต์</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_13.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นักเรียนที่ได้รับทุนการศึกษา กองทุนธวัช ยิบอินซอย เพื่อการศึกษาพัฒนา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_14.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นักศึกษาที่ได้รับทุนการศึกษา ทุนคุณเจริญ - คุณหญิงวรรณา สิริวัฒนภั</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_15.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นักศึกษาที่ได้รับทุนการศึกษา ทุนศาสตราจารย์กิตติคุณเติมศักดิ์ กฤษณามระ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_16.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักเรียนที่ได้รับทุนการศึกษา “มูลนิธิวิชัย ศรีวัฒนประภา” ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_17.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักเรียนนักศึกษา โรงเรียนจิตรลดาวิชาชีพ ผู้มีสิทธิ์เข้าสัมภาษณ์รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2565 (รอบที่ 1)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_18.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักเรียนนักศึกษาที่ได้รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2564 (รอบที่ 2)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_19.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักเรียนนักศึกษาที่ได้รับทุนรางวัล ประจำปีการศึกษา 2563</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_20.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักเรียนระดับประกาศนียบัตรวิชาชีพ (ปวช.) สาขาเกษตรนวัต ที่ได้รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_21.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักศึกษา ผู้มีสิทธิ์เข้ารับการสัมภาษณ์รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2565 (รอบระดับประกาศนียบัตรวิชาชีพชั้นสูงปีที่ 1)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_22.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักศึกษาที่ได้รับทุนการศึกษา “ดร.ท่านผู้หญิงทัศนีย์ บุณยคุปต์“</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_23.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักศึกษาที่ได้รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2564 (2)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_24.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักศึกษาที่ได้รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2564 (รอบที่ 2)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_25.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักศึกษาที่ได้รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2564 (รอบที่ 3)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_26.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อนักศึกษาที่ได้รับทุนรางวัล ประจำปีการศึกษา 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_27.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง รายชื่อผู้มีสิทธิ์ได้รับทุนช่วยเหลือการศึกษา ประจำปีการศึกษา 2565 (รอบแฟ้มสะสมผลงาน)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_28.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราการจ่ายทุนการศึกษาประเภทต่างๆ พ.ศ. 2563 (แก้ไขเพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_29.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และอัตราการจ่ายทุนการศึกษาประเภทต่างๆ พ.ศ.2563 (แก้ไขเพิ่มเติม)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_30.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การจ่ายทุนช่วยเหลือการศึกษาสำหรับผู้ขาดแคลนทุนทรัพย์ พ.ศ. 2565</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_31.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การจ่ายทุนพัฒนาและส่งเสริมศักยภาพผู้เรียน พ.ศ. 2565</a><br>
-->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_32.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การจ่ายทุนพัฒนาและส่งเสริมศักยภาพผู้เรียน พ.ศ. 2565</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_33.pdf">- คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกองทุนสงเคราะห์นักเรียนนักศึกษา สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_34.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดา ว่าด้วยกองทุนสงเคราะห์นักเรียนนักศึกษา สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_35.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์การจ่ายทุนช่วยเหลือการศึกษาสำหรับผู้ขาดแคลนทุนทรัพย์ สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2565</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01_36.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการจ่ายเงินกองทุนสงเคราะห์นักเรียนนักศึกษา สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2564</a><br>

                  </p>
                  <h5 class="panel-title">มาตรการและการเฝ้าระวังการระบาดของโรคไวรัสโคโรนาสายพันธุ์ใหม่ 2019 </h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_03.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การเรียนการสอนและการปฏิบัติงานในช่วงที่เชื้อไวรัสโคโรน่าสายพันธุ์ใหม่กำลังระบาด</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง งดหรือเลื่อนการจัดกิจกรรมนักเรียนนักศึกษา (ฉบับที่ 1)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_05.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจัดการเรียนการสอน การฝึกปฏิบัติวิชาชีพ และการปฏิบัติงานของบุคลากร ในสถานการณ์ไม่ปกติอันเนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 3)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_06.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจัดการเรียนการสอน การฝึกปฏิบัติวิชาชีพ และการปฏิบัติงานของบุคลากร ในสถานการณ์ไม่ปกติอันเนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 4)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_07.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาปิดที่ทำการเป็นการชั่วคราว เนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 5) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาปิดที่ทำการเป็นการชั่วคราว เนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 6)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรการช่วยเหลือนักเรียน นักศึกษา อันเนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาปิดที่ทำการเป็นการชั่วคราว เนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 7)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_11.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การปฎิบัติงานของบุคลากรในสถานการณ์ COVID-19 (ฉบับที่ 8)</a><br>
                    
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_5f758b9884671.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การปฏิบัติงานของบุคลากรในสถานการณ์ COVID-19 (ฉบับที่ 10)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_5ff2dee329481.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การปฏิบัติงานของบุคลากรในสถานการณ์ COVID-19 (ฉบับที่ 11)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_5ff81997a837b.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาของประกาศสถาบันเทคโนโลยีจิตรลดาเนื่องจากสถานการณ์การแพร่ระบาดของโรค COVID-19 (ฉบับที่ 12)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_13.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจัดการเรียนการสอนและการปฏิบัติงานของบุคลากรเดือนกุมภาพันธ์ 2564 เป็นต้นไป (ฉบับที่ 13)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/file_6013b1d0555e8.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางการจัดการเรียนการสอน เดือนกุมภาพันธ์ 2564 เป็นต้นไป (ฉบับที่ 14)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_14.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรการช่วยเหลือนักเรียน นักศึกษา อันเนื่องจากการแพร่ระบาดของเชื้อไวรัสโคโรนา 2019 (COVID-19) พ.ศ. 2564</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_15.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางและมาตรการควบคุมการระบาดของโรคติดเชื้อไวรัสโคโรนา 2019 (COVID-19) ในช่วงเทศกาลสงกรานต์ปี 2564 และเดือนเมษายน 2564 (ฉบับที่ 15)</a><br>
                    
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_01.pdf">- ประกาศแนวทางและหลักเกณฑ์การเปิดสถานที่ทำการของสถาบันอุดมศึกษา (กระทรวง อว.) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_02.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางและมาตรการในการปฏิบัติงานของบุคลากร และการจัดการเรียนการสอนในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 ระลอกที่สาม (ฉบับที่ 17) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_03.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางและมาตรการในการปฏิบัติงานของบุคลากร และการจัดการเรียนการสอนในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 ระลอกที่สาม (ฉบับที่ 18) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางและมาตรการในการปฏิบัติงานของบุคลากร ในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 ระลอกที่ 3 (ฉบับที่ 16) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_05.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางการจัดการเรียนการสอน และการปฏิบัติงานของบุคลากรในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 25) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_06.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางการจัดการเรียนการสอน ในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 27) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_07.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางการปฏิบัติงานของบุคลากร และการจัดการเรียนการสอนสำหรับนักเรียนในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 24) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางปฏิบัติในการจัดการเรียนการสอน และการปฏิบัติงานของบุคลากรในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 19) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางปฏิบัติในการจัดการเรียนการสอน และการปฏิบัติงานของบุคลากรในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 20) </a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_10.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางปฏิบัติในการจัดการเรียนการสอน และการปฏิบัติงานของบุคลากรในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 21) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_11.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางปฏิบัติในการจัดการเรียนการสอน และการปฏิบัติงานของบุคลากรในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 22) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_12.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวทางปฏิบัติในการจัดการเรียนการสอน และการปฏิบัติงานของบุคลากรในช่วงการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 (ฉบับที่ 23) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_13.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การเปิดสถานที่ทำการ การจัดการเรียนการสอน และการปฏิบัติงานของบุคลากร ในช่วงการแพร่ระบาด COVID-19 (ฉบับที่ 26) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_14.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การปฏิบัติงานของบุคลากรในสถานการณ์ COVID-19 (ฉบับที่ 9) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_15.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรการช่วยเหลือนักเรียน นักศึกษา อันเนื่องจากการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 พ.ศ. 2564 (ฉบับที่ 2) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_16.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรการช่วยเหลือนักเรียน นักศึกษา อันเนื่องจากการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 พ.ศ. 2564 (ฉบับที่ 3) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02_17.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรการช่วยเหลือนักเรียน นักศึกษา อันเนื่องจากการแพร่ระบาดของโรคติดเชื้อไวรัส COVID-19 พ.ศ. 2564 (ฉบับที่ 4) </a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>
<!--
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-10" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">10.คำสั่งใหม่สภาสถาบัน</a>
                </h4>
            </div>
            <div id="panelBody-10" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="">เอกสาร 10</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-11" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">อื่นๆ</a>
                </h4>
            </div>
            <div id="panelBody-11" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="">เอกสาร อื่นๆ</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>
      -->
    </div>
</div>

</style>
<!--<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt="">สืบค้นเอกสาร</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container" style="margin-bottom : 3em;">
    <div id="accordion" class="panel-group">
      <div class="row">
  			<div class="col-sm-12">
        <iframe class="WebPublish_iframe" src="https://edoc.cdti.ac.th/docweb/v2/publishDoc.aspx" frameborder="0" width="100%" height="600px"></iframe>
        </div>
    </div>
  </div>
</div>
-->