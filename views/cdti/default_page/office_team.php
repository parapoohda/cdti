<div class="banner-innerpage  mb-4" style="background-image:url(<?php echo base_url().'/cdti_assets/images/banner-cdtc-3.jpg'?>)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<div class="whats ">
	<div class="container">
		<div class="section-header text-center pt-3">
            <h2 class="section-title color-2"><?php echo $page->title ?></h2>
            <?php if ($page->branch_title_en): ?>
                <p class="color-2 mb-4"><?php echo $page->branch_title_en ?></p>
            <?php endif; ?>
		</div>
	</div>
</div>
<div class="container mt-4">
  <div class="row justify-content-left">
    <div class="col offset-1 col-11 offset-sm-2 col-sm-8 col-md-5 offset-md-1 my-4">
		<a href="<?php echo base_url()."personnel-administration"?>" class="section-title color-2 my-1" style="font-size: 21px;font-weight: bold;">ฝ่ายบริหาร</a>
		<ul class="pl-4 text-dark">
			<li>งานบริหารทั่วไปและสื่อสารองค์กร</li>
			<li>งานยุทธศาสตร์ แผน และงบประมาณ</li>
			<li>งานการเงินและบัญชี</li>
			<li>งานพัสดุ</li>
			<li>งานเทคโนโลยีสารสนเทศ</li>
			<li>งานระบบกายภาพและโสตทัศนูปกรณ์</li>
			<li>งานบริหารทรัพยากรมนุษย์และนิติการ</li>
		</ul>
    </div>
    <div class="col offset-1 col-11 offset-sm-2 col-sm-8 col-md-5 offset-md-1 my-4">
		<a href="<?php echo base_url()."personnel-professional-academic"?>" class="section-title color-2 my-1" style="font-size: 21px;font-weight: bold;">ฝ่ายวิชาการวิชาชีพ</a>
		<ul class="pl-4 text-dark">
			<li>งานพัฒนาคุณภาพการศึกษา</li>
			<li>งานวิจัยและวิรัชกิจ</li>
			<li>งานทะเบียนและวัดผล</li>
			<li>งานวิทยทรัพยากร</li>
			<li>งานนวัตกรรมและสื่อการเรียนการสอน</li>
			<li>ศูนย์บริการวิชาการ</li>
		</ul>
    </div>
    <div class="col offset-1 col-11 offset-sm-2 col-sm-8 col-md-5 offset-md-1 my-4">
		<a href="<?php echo base_url()."personnel-student-affairs"?>" class="section-title color-2 my-1" style="font-size: 21px;font-weight: bold;">ฝ่ายกิจการนักเรียนและนักศึกษา</a>
		<ul class="pl-4 text-dark">
			<li>งานทุนการศึกษาและบริการนักเรียนนักศึกษา</li>
			<li>งานพัฒนาและติดตามผลงานนักเรียนนักศึกษา</li>
		</ul>
    </div>
	
	<?php

echo "
<div  width=\"100%\" style=\"height: 1200px;\" class=\"col-12 wrapper\"><iframe  src=\"";
echo base_url();
echo"uploads/files/รายชื่อติดต่อ บุคลากรสำนักงานสถาบัน.pdf\" width=\"100%\" style=\"height: 1000px;\"></iframe></div>";

?>