<section class="campus-box mini-spacer">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> ประวัติสถาบันเทคโนโลยีจิตรลดา</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section  class="banner-history" style="background-image:url(<?php echo base_url()?>/cdti_assets/images/bg_history.png)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center  align-self-center">
            <!-- Column -->
            <div class="col-md-4 text-center  bg-yellow">
               <div class="spacer">
               		<img src="<?php echo base_url()?>/cdti_assets/images/logo.png" alt="" class="img-responsive ">
               </div>
            </div>
            <div class="col-md-8">
            	<div class="history-title">
            		<h1>“จิตรลดาแดนสรวง.. เราทั้งปวงร่วมมิตรไมตรี.. <br/> มาเถิดมาวันนี้... สำราญเต็มที่เปรมปรีดิ์ฤทัย...”</h1>
            		<hr>
            		<p>
						ใครที่เป็นศิษย์จิตรลดาในรุ่นแรกๆ คงจะจำได้ดีถึงเพลงจิตรลดาเพลงแรกนี้ และ จากนั้น ภาพอันงดงามแห่งความทรงจำรำลึกในความสุขของการได้เป็น “ครอบครัว จิตรลดา” โรงเรียนเล็กๆ ที่ยิ่งใหญ่ด้วยพระมหากรุณาธิคุณแห่งองค์
						บ&#65279ร&#65279ม&#65279ช&#65279น&#65279ก&#65279า&#65279ธิ&#65279เ&#65279บ&#65279ศ&#65279ร ม&#65279ห&#65279า&#65279ภู&#65279มิ&#65279พ&#65279ล&#65279อ&#65279ดุ&#65279ล&#65279ย&#65279เ&#65279ด&#65279ชและ 
						ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279น&#65279า&#65279ง&#65279เ&#65279จ้&#65279า&#65279สิ&#65279ริ&#65279กิ&#65279ติ์ 
พ&#65279ร&#65279ะ&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ชิ&#65279นี&#65279น&#65279า&#65279ถ 
พ&#65279ร&#65279ะ&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279ช&#65279น&#65279นี&#65279พั&#65279น&#65279ปี&#65279ห&#65279ล&#65279ว&#65279ง อบอุ่นด้วย ความรักจาก “ครู” ก็จะรินไหลเข้ามาสู่ความทรงจำที่ให้ความรู้สึกฉ่ำเย็นดุจสายน้ำ...
            		</p>
            	</div>
            </div>
            <!-- Column -->
        </div>
    </div>
</section>
<section id="history-slide" class="history-slide mini-spacer">
	<div class="container">
		<div class="row mb-4">
			<div class="col-sm-12 text-center">
				<h3 class="color-primary history-slide-title">ประวัติความเป็นมาของสถาบันเทคโนโลยีจิตรลดา</h3>
			</div>
		</div>
		<div class="tabs-year">
			<ul class="nav nav-pills nav-years">
				<div class="slider"></div>
			  	<li class="nav-item">
			    	<a class="nav-link active" href="#y2498" id="navy0">
				    	<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2498</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2499" id="navy1">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2499</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2500" id="navy2">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2500</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2501" id="navy3">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2501</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2504" id="navy4">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2504</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2507" id="navy5">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2507</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2511" id="navy6">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2511</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2526" id="navy7">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2526</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2528" id="navy8">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2528</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2547" id="navy9">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2547</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2557" id="navy10">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2557</p>
			    	</a>
			  	</li>
			  	<li class="nav-item">
			    	<a class="nav-link" href="#y2561" id="navy11">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2561</p>
			    	</a>
			  	</li>
				  <li class="nav-item">
			    	<a class="nav-link" href="#y2562" id="navy12">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2562</p>
			    	</a>
			  	</li><li class="nav-item">
			    	<a class="nav-link" href="#y2563" id="navy13">
			    		<p class="color-primary">พุทธศักราช</p>
						<p class="year color-primary">2563</p>
			    	</a>
			  	</li>
			</ul>
		</div>
		<div class="mini-spacer">
			<div class="history-slider owl-carousel owl-theme">
				<div class="item" data-hash="y2498">
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_01.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2498</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">พระบาทสมเด็จพระบ&#65279ร&#65279ม&#65279ช&#65279น&#65279ก&#65279า&#65279ธิ&#65279เ&#65279บ&#65279ศ&#65279ร ม&#65279ห&#65279า&#65279ภู&#65279มิ&#65279พ&#65279ล&#65279อ&#65279ดุ&#65279ล&#65279ย&#65279เ&#65279ด&#65279ชม&#65279ห&#65279า&#65279ร&#65279า&#65279ช บ&#65279ร&#65279ม&#65279น&#65279า&#65279ถ&#65279บ&#65279พิ&#65279ต&#65279ร <span class="color-darkgray">ได้ทรงพระกรุณาโปรดเกล้าฯ ให้จัดการศึกษาในระดับชั้นอนุบาลขึ้น ณ พระที่นั่งอุดร ในพระที่นั่งอัมพรสถาน พระราชวังดุสิต</span></p>
								</li>

							</ul>
							<div class="row">
								<div class="col-6">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_02.png" alt="" class="img-responsive">
								</div>
								<div class="col-6">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_03.png" alt="" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_05.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">ทูลกระหม่อมหญิงอุบลรัตนราชกัญญา สิริวัฒนาพรรณวดี  <br/> <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล</span></p>
								</li>
							</ul>

						</div>
					</div>
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_04.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">ท่านผู้หญิง ดร.ทัศนีย์ บุณยคุปต์  <span class="color-darkgray"> เริ่มเข้าทำงานเป็นอาจารย์ใหญ่โรงเรียนจิตรลดา เมื่อ พ.ศ. 2498 โดยเป็นพระอาจารย์ท่านแรกที่ถวายพระอักษร ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279ลู&#65279ก&#65279เ&#65279ธ&#65279อ
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279อุ&#65279บ&#65279ล&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279กั&#65279ญ&#65279ญ&#65279า 
สิ&#65279ริ&#65279วั&#65279ฒ&#65279น&#65279า&#65279พ&#65279ร&#65279ร&#65279ณ&#65279ว&#65279ดี เพียงท่านเดียว</span></p>
								</li>
							</ul>

						</div>
					</div>
				</div>
				<div class="item" data-hash="y2499">
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_06.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2499</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">พ&#65279ร&#65279ะ&#65279บ&#65279า&#65279ท&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ป&#65279ร&#65279เ&#65279ม&#65279น&#65279ท&#65279ร&#65279ร&#65279า&#65279ม&#65279า&#65279ธิ&#65279บ&#65279ดี&#65279ศ&#65279รี&#65279สิ&#65279น&#65279ท&#65279ร&#65279ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279 พ&#65279ร&#65279ะ&#65279ว&#65279ชิ&#65279ร&#65279เ&#65279ก&#65279ล้&#65279า&#65279เ&#65279จ้&#65279า&#65279อ&#65279ยู่&#65279หั&#65279ว <br/>  <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล </span></p>
								</li>

							</ul>
							<div class="row">
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_07.png" alt="" class="img-responsive">
								</div>
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_08.png" alt="" class="img-responsive">
								</div>
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_09.png" alt="" class="img-responsive">
								</div>
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_10.png" alt="" class="img-responsive">
								</div>
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_11.png" alt="" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2500">
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/logo_circle.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2500</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">พระบาทสมเด็จพระบ&#65279ร&#65279ม&#65279ช&#65279น&#65279ก&#65279า&#65279ธิ&#65279เ&#65279บ&#65279ศ&#65279ร ม&#65279ห&#65279า&#65279ภู&#65279มิ&#65279พ&#65279ล&#65279อ&#65279ดุ&#65279ล&#65279ย&#65279เ&#65279ด&#65279ชม&#65279ห&#65279า&#65279ร&#65279า&#65279ช บ&#65279ร&#65279ม&#65279น&#65279า&#65279ถ&#65279บ&#65279พิ&#65279ต&#65279ร <span class="color-darkgray">  ได้ย้ายพระราชฐาน ที่ประทับจากพระที่นั่งอัมพรสถานมาประทับ
									ณ พระตำหนักจิตรลดารโหฐาน พระราชวังดุสิตและได้ทรงพระกรุณาโปรดเกล้าฯ ให้สร้างอาคารเรียนถาวรในบริเวณพระตำหนักจิตรลดารโหฐาน พระราชวังดุสิต พระราชทานนามโรงเรียนว่า "โรงเรียนจิตรลดา"  </span></p>
								</li>
							</ul>
							<div class="row">
								<div class="col-6 col-md-6 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_12.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-6 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_13.png" alt="" class="img-responsive">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2501">
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_18.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2501</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล </span></p>
								</li>
								<li>
									<p class="color-darkgray">โรงเรียนจิตรลดาได้ทำการจดทะเบียนให้ถูกต้องตามกฏของกระทรวงศึกษาธิการ เปิดสอนนักเรียนตั้งแต่ระดับชั้นอนุบาลถึงระดับชั้นประถมศึกษาปีที่ 4 </p>
								</li>
							</ul>
							<div class="row">
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_19.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_20.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_21.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_22.png" alt="" class="img-responsive">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2504">
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_14.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2504</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279น้&#65279อ&#65279ง&#65279น&#65279า&#65279ง&#65279เ&#65279ธ&#65279อ 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279จุ&#65279ฬ&#65279า&#65279ภ&#65279ร&#65279ณ&#65279ว&#65279ลั&#65279ย&#65279ลั&#65279ก&#65279ษ&#65279ณ์ 
อั&#65279ค&#65279ร&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี 
ก&#65279ร&#65279ม&#65279พ&#65279ร&#65279ะ&#65279ศ&#65279รี&#65279ส&#65279ว&#65279า&#65279ง&#65279ค&#65279วั&#65279ฒ&#65279น 
ว&#65279ร&#65279ขั&#65279ต&#65279ติ&#65279ย&#65279ร&#65279า&#65279ช&#65279น&#65279า&#65279รี  <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล </span></p>
								</li>
							</ul>

						</div>
					</div>
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_15.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">พระเจ้าวรวงศ์เธอ พระองค์เจ้าโสมสวลี กรมหมื่นสุทธนารีนาถ <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล  </span></p>
								</li>
							</ul>

						</div>
					</div>
				</div>
				<div class="item" data-hash="y2507">
					<div class="row m-b-60">
						<div class="col-md-12">
							<h3 class="color-primary">พุทธศักราช 2507</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">โรงเรียนจิตรลดาขยายชั้นเรียนจนถึงระดับชั้นมัธยมศึกษาตอนต้น</p>
								</li>
							</ul>
							<div class="row">
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_12.png" alt="" class="img-responsive">
								</div>
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_13.png" alt="" class="img-responsive">
								</div>
								<div class="col-4 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_13.png" alt="" class="img-responsive">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2511">
					<div class="row m-b-60">
						<div class="col-md-12">
							<h3 class="color-primary">พุทธศักราช 2511</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">โรงเรียนจิตรลดาขยายชั้นเรียนจนถึงระดับชั้นมัธยมศึกษาตอนปลาย</p>
								</li>
							</ul>
							<div class="row">
								<div class="col-6 col-md-6 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_12.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-6 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_13.png" alt="" class="img-responsive">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2526">
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_23.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2526</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20">พระบาทสมเด็จพระบ&#65279ร&#65279ม&#65279ช&#65279น&#65279ก&#65279า&#65279ธิ&#65279เ&#65279บ&#65279ศ&#65279ร ม&#65279ห&#65279า&#65279ภู&#65279มิ&#65279พ&#65279ล&#65279อ&#65279ดุ&#65279ล&#65279ย&#65279เ&#65279ด&#65279ชม&#65279ห&#65279า&#65279ร&#65279า&#65279ช บ&#65279ร&#65279ม&#65279น&#65279า&#65279ถ&#65279บ&#65279พิ&#65279ต&#65279ร <span class="color-darkgray"> ให้ </span> ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี <span class="color-darkgray">ทรงดำรงตำแหน่งประธานบริหารโรงเรียนจิตรลดา</span></p>
								</li>
							</ul>
							<div class="row">
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_24.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_25.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_26.png" alt="" class="img-responsive">
								</div>
								<div class="col-6 col-md-3 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_27.png" alt="" class="img-responsive">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2528">
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_28.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8 ">
							<h3 class="color-primary">พุทธศักราช 2528 - 2551</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20"><span class="color-darkgray">พุทธศักราช 2528 </span> พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279ว&#65279ร&#65279ว&#65279ง&#65279ศ์&#65279เ&#65279ธ&#65279อ 
พ&#65279ร&#65279ะ&#65279อ&#65279ง&#65279ค์&#65279เ&#65279จ้&#65279า&#65279สิ&#65279ริ&#65279ภ&#65279า&#65279จุ&#65279ฑ&#65279า&#65279ภ&#65279ร&#65279ณ์ <br/> <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล  </span></p>
								</li>

							</ul>
						</div>
					</div>
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_29.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8 p-t-40">
							<ul class="mb-5">

							<li class="color-primary">
									<p class="color-primary font-20"><span class="color-darkgray">พุทธศักราช 2530 </span> พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279ว&#65279ร&#65279ว&#65279ง&#65279ศ์&#65279เ&#65279ธ&#65279อ 
พ&#65279ร&#65279ะ&#65279อ&#65279ง&#65279ค์&#65279เ&#65279จ้&#65279า&#65279อ&#65279ทิ&#65279ต&#65279ย&#65279า&#65279ท&#65279ร&#65279กิ&#65279ติ&#65279คุ&#65279ณ  <br/> <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล  </span></p>
								</li>
							</ul>
						</div>
					</div>
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_30.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8 p-t-40">
							<ul class="mb-5">
								
								<li class="color-primary">
									<p class="color-primary font-20"><span class="color-darkgray">พุทธศักราช 2534 </span> ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279ลู&#65279ก&#65279เ&#65279ธ&#65279อ 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279สิ&#65279ริ&#65279วั&#65279ณ&#65279ณ&#65279ว&#65279รี 
น&#65279า&#65279รี&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279กั&#65279ญ&#65279ญ&#65279า <br/>  <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล  </span></p>
								</li>

							</ul>
						</div>
					</div>
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_31.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8 p-t-40">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20"><span class="color-darkgray">พุทธศักราช 2538 </span> ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279ลู&#65279ก&#65279เ&#65279ธ&#65279อ 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279พั&#65279ช&#65279ร&#65279กิ&#65279ติ&#65279ย&#65279า&#65279ภ&#65279า 
น&#65279เ&#65279ร&#65279น&#65279ทิ&#65279ร&#65279า&#65279เ&#65279ท&#65279พ&#65279ย&#65279ว&#65279ดี  <br/> <span class="color-darkgray">ทรงพระอักษรในระดับชั้นมัธยมศึกษาตอนปลาย  </span></p>
								</li>

							</ul>
						</div>
					</div>
					<div class="row m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_32.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8 p-t-40">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-20"><span class="color-darkgray">พุทธศักราช 2551 </span> ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279จ้&#65279า&#65279ลู&#65279ก&#65279เ&#65279ธ&#65279อ 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ที&#65279ปั&#65279ง&#65279ก&#65279ร&#65279รั&#65279ศ&#65279มี&#65279โ&#65279ช&#65279ติ 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279โ&#65279ร&#65279ต&#65279ต&#65279ม&#65279า&#65279ง&#65279กู&#65279ร  <br/> <span class="color-darkgray">ทรงพระอักษรในระดับชั้นอนุบาล  </span></p>
								</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2547">
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_18.png" alt="" class="img-responsive">
							<p class="font-14 text-center color-darkgray">
								ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี ประธานบริหารโรงเรียนจิตรลดา  (สายวิชาชีพ)
							</p>
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2547 ก่อตั้งโรงเรียนจิตรลดา (สายวิชาชีพ)</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<p class="color-primary font-16">ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี <span class="color-darkgray">
										 ทรงพระกรุณาโปรดเกล้าฯให้ขยายโอกาสการสอนจากสายสามัญ เพิ่มเป็นสายวิชาชีพ และพระราชทานนามว่า </span><span class="color-light-blue"> "โรงเรียนจิตรลดา (สายวิชาชีพ)" </span> <span class="color-darkgray">เปิดการศึกษาในระดับประกาศนียบัตรวิชาชีพ (ปวช).

									</span> </p>
								</li>
							</ul>
							<div class="row">
								<div class="col-md-4 text-center">
									<img src="<?php echo base_url()?>cdti_assets/images/slogo_circle.png" alt="" class="img-responsive mx-auto">
									<p class="font-14">ตราสัญลักษณ์โรงเรียนจิตรลดา (สายวิชาชีพ)</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<h3 class="color-primary">พุทธศักราช 2550</h3>
							<hr class="hr-yellow">
							<p class="color-primary"><span class="color-darkgray"> โรงเรียนจิตรลดา (สายวิชาชีพ)เปิดการศึกษาในระดับประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.)</span></p>

							<div class="row justify-content-center">
								<div class="col-sm-6 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_36.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_37.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_38.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_39.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_40.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_41.png" alt="">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2557">
					<div class="row  m-b-60">
						<div class="col-md-4">
							<img src="<?php echo base_url()?>cdti_assets/images/pic_18.png" alt="" class="img-responsive">
						</div>
						<div class="col-md-8">
							<h3 class="color-primary">พุทธศักราช 2557 วิทยาลัยเทคโนโลยีจิตรลดา</h3>
							<hr class="hr-yellow">
							<ul class="mb-5">
								<li class="color-primary">
									<!--<p class="color-primary font-16">ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี <span class="color-darkgray">ทรงพระกรุณาโปรดเกล้าฯ ให้ขยายการเรียนการสอนในระดับอุดมศึกษาขึ้นและพระราชทานนามว่า </span> "สถาบันเทคโนโลยีจิตรลดา" <span class="color-darkgray">โดยสถาบันอุดมศึกษาเอกชน ตั้งแต่วันที่ 24 กุมภาพันธ์ พ.ศ. 2557 โดยมีมูลนิธิสยามบรมราชกุมารี ทรงดำรงตำแหน่ง นายกสภาสถาบันและถือเอาวันคล้ายวันพระราชสมภพคือ วันพุธที่ 2 เมษายน พ.ศ. 2557 เป็นวันสถาบันของสถาบัน เปิดสอนในคณะเทคโนโลยีอุตสาหกรรม สาขาอิเล็กทรอนิกส์และสาขาไฟฟ้ากำลัง และคณะบริหารธุรกิจ สาขาการตลาด สาขาคอมพิวเตอร์ธุรกิจ และสาขาธุรกิจอาหาร โดยมี</span> ท่านผู้หญิงอังกาบ บุณยัษฐิติ <span class="color-darkgray"></span>ดำรงตำแหน่งอธิการบดีท่านแรก ขยายโอกาสการสอนจากสายสามัญ เพิ่มเป็นสายวิชาชีพ และพระราชทานนามว่า "โรงเรียนจิตรลดา (สายวิชาชีพ)" เปิดการศึกษาในระดับประกาศนียบัตรวิชาชีพ (ปวช). </p>-->
									<p class="color-primary font-16px-important lg">ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า
										ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า
										เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร
										ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี
										สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น
										รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ
										ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี<span class="color-darkgray">ทรงพระกรุณาโปรดเกล้าฯ ให้ขยายการเรียนการสอนในระดับอุดมศึกษาขึ้นและพระราชทานนามว่า</span> "วิทยาลัยเทคโนโลยีจิตรลดา" <span class="color-darkgray">โดยเป็นสถาบันอุดมศึกษาเอกชน ตั้งแต่วันที่ 24 กุมภาพันธ์ พ.ศ. 2557 โดยได้รับการสนับสนุนจาก มูลนิธิสยามบรมราชกุมารี เพื่อโรงเรียนจิตรลดา และทรงดำรงตำแหน่ง นายกสภาวิทยาลัย เปิดสอนคณะเทคโนโลยีอุตสาหกรรม สาขาอิเล็กทรอนิกส์และสาขาไฟฟ้ากำลัง และคณะบริหารธุรกิจ สาขาการตลาด สาขาคอมพิวเตอร์ธุรกิจ และสาขาธุรกิจอาหาร โดยมี</span>ท่านผู้หญิงอังกาบ บุณยัษฐิติ ดำรงตำแหน่งอธิการบดีท่านแรก</p>
									</li>
							</ul>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-sm-3">
							<div class="row">
								<div class="col-md-12 pro-pic">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_33.png" alt="" class="img-responsive">
								</div>
								<div class="col-md-12">
									<p class="text-center color-darkgray">เทคโนโลยีจิตรลดา</p>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="row">
								<div class="col-md-12 pro-pic">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_34.png" alt="" class="img-responsive">
								</div>
								<div class="col-md-12">
									<p class="text-center color-darkgray">ท่านผู้หญิงอังกาบ บุณยัษฐิติ </p>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="row">
								<div class="col-md-12 pro-pic">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_35.png" alt="" class="img-responsive">
								</div>
								<div class="col-md-12">
									<p class="text-center color-darkgray">รศ.ดร.คุณหญิง สุมณฑา พรหมบุญ</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<h3 class="color-primary">1 สิงหาคม 2560</h3>
							<hr class="hr-yellow">
							<p class="color-primary">รศ. ดร.คุณหญิงสุมณฑา พรหมบุญ <span class="color-darkgray"> ดำรงตำแหน่งอธิการบดีสถาบัน</span></p>
							<ul>
								<li class="color-primary">
									<p class="color-darkgray">จัดหลักสูตรการเรียนการสอนภายใต้นโยบาย <span class="color-darkgray">"เรียนคู่งาน งานคู่เรียน"</span> ตอบสนองต่อการผลิตบัณฑิตที่มีสมรรถนะการทำงาน</p>
								</li>
								<li class="color-primary">
									<p class="color-darkgray">ในปีการศึกษา 2560 คณะบริหารธุรกิจได้ปรับหลักสูตรจากเดิมเป็น "สาขาการจัดการธุรกิจอาหาร" และเทคโนโลยีอุตสาหกรรมได้ปรับหลักสูตรจากเดิมเป็น "สาขาเทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์"</p>
								</li>
							</ul>
							<div class="row justify-content-center">
								<div class="col-sm-6 col-md-4 mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_36.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_37.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_38.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_39.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_40.png" alt="">
								</div>
								<div class="col-sm-6 col-md-4  mb-4">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_41.png" alt="">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="item" data-hash="y2561">
					<div class="row">
						<div class="col-sm-8 order-md-12">
							<h3 class="color-primary">สถาบันเทคโนโลยีจิตรลดา</h3>
							<p class="color-primary">สถาบันเทคโนโลยีจิตรลดา ควบรวมกับโรงเรียนจิตรลดา (สายวิชาชีพ) <br/> ภายใต้สถาบันเทคโนโลยีจิตรดา <span class="color-darkgray"> โดยเป็นสถาบันอุดมศึกษาในกำกับของรัฐ</span> </p>
							<hr class="hr-yellow">
							<p class="color-primary mb-4">11 พฤศจิกายน 2561  <br/> วิทยาลัยเทคโนโลยีจิตรลดา <span class="color-darkgray">เปลี่ยนสถานะเป็น</span> "สถาบันเทคโนโลยีจิตรลดา"<span class="color-darkgray"> ตาม พ.ร.บ. สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2561 และถือเอาวันคล้ายวันพระราชสมภพ</span> ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า
										ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า
										เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร
										ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี
										สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น
										รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ
										ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี <span class="color-darkgray">คือวันที่ 2 เมษายน เป็นวันสถาบัน  </span></p>
							<p class="color-primary mb-4"><strong>พ&#65279ร&#65279ะ&#65279บ&#65279า&#65279ท&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ป&#65279ร&#65279เ&#65279ม&#65279น&#65279ท&#65279ร&#65279ร&#65279า&#65279ม&#65279า&#65279ธิ&#65279บ&#65279ดี&#65279ศ&#65279รี&#65279สิ&#65279น&#65279ท&#65279ร&#65279ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279 พ&#65279ร&#65279ะ&#65279ว&#65279ชิ&#65279ร&#65279เ&#65279ก&#65279ล้&#65279า&#65279เ&#65279จ้&#65279า&#65279อ&#65279ยู่&#65279หั&#65279ว</strong></p>

							<p class="color-primary">ทรงลงพระปรมาภิไธยเมื่อวันที่ 10 สิงหาคม 2561</p>
							<ul class="mb-4">
								<li class="color-primary">
									<p class="color-darkgray">
										สถาบันเริ่มเปิดหลักสูตรใหม่ "แบบงานคู่เรียน" ได้แก่หลักสูตร เทคโนโลยีอุตสาหกรรม (ต่อเนื่อง) เพื่อให้คนที่ทำงานแล้วสามารถกลับมาเรียนได้
									</p>
								</li>
								<li class="color-primary">
									<p class="color-darkgray">มีหลักสูตรระยะสั้นให้แก่ผู้ทำงานแล้วมาเพิ่มพูนความรู้และประสบการณ์</p>
								</li>
								<li class="color-primary">
									<p class="color-darkgray">และเปิดห้องปฏิบัติการธุรกิจอาหาร <span class="color-primary">"ร้านปรุงสารพัด"</span> ให้นักศึกษาเรียนรู้จากการปฏิบัติจริง</p>
								</li>
							</ul>
							<h3 class="color-primary">"เรียนคู่งาน งานคู่เรียน"</h3>
							<hr class="hr-yellow">
							<p class="mb-4">เรียนคู่งาน หมายถึง นักศึกษาเรียน และฝึกทำงานด้วย <br/> งานคู่เรียน หมายถึง ผู้ที่ทำงานกลับมาศึกษา/เรียนรู้ ตลอดชีวิต</p>
							<h3 class="color-primary">โรงเรียนจิตรลดาวิชาชีพ เปิดหลักสูตรระดับประกาศนียบัตรวิชาชีพ (ปวช.) </h3>
							<hr class="hr-yellow">
							<p class="mb-4"> สาขาวิชาการสร้างเครื่องดนตรีไทย และสาขาวิชาเทคโนโลยีระบบสมองกลฝังตัว</p>
							<!-- <p class="color-darkgray">ฝึกปฏิบัติงานกับสถานประกอบการชั้นนำ</p> -->

							<!-- <div class="row">
								<?Php for ($i=0; $i < 12 ; $i++):?>
								<div class="col-3 col-sm-2 mb-3">
									<img src="<?php echo base_url()?>cdti_assets/images/pt_1.png" alt="" class="img-responsive">
								</div>
								<?php endfor;?>
							</div> -->

						</div>
						<div class="col-sm-4 order-md-1">
							<div class="text-center mb-4">
								<img src="<?php echo base_url()?>cdti_assets/images/pic_33.png" alt="" class="img-responsive">
								<p class="font-14 color-darkgray">พระราชบัญญัติ สถาบันเทคโนโลยีจิตรลดา <br/>พุทธศักราช 2561 </p>
							</div>
							<div class="text-center mb-4">
								<img src="<?php echo base_url()?>cdti_assets/images/pic_42.png" alt="" class="img-responsive">
								<p class="font-14 color-darkgray">พระราชบัญญัติ สถาบันเทคโนโลยีจิตรลดา <br/>พุทธศักราช 2561 </p>
							</div>
							<div class="text-center mb-4">
								<img src="<?php echo base_url()?>cdti_assets/images/pic_43.png" alt="" class="img-responsive">
								<p class="font-14 color-darkgray">ภาพวาดฝีพระหัตถ์ ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี </p>
							</div>
						</div>
					</div>
				</div>

				<div class="item" data-hash="y2562">
					<div class="row">
						<div class="col-sm-8 order-md-12">
							<h3 class="color-primary">พุทธศักราช 2562 สถาบันเทคโนโลยีจิตรลดา</h3>
							<hr class="hr-yellow">

							<p class="color-primary">โรงเรียนจิตรลดาวิชาชีพ เปิดหลักสูตรระดับประกาศนียบัตรวิชาชีพ (ปวช.) <br> สาขาวิชาเกษตรนวัต (ทวิศึกษาร่วมกับโรงเรียนวังจันทร์วิทยา)</p>
							<hr class="hr-yellow">
							
							<p class="color-primary">คณะเทคโนโลยีดิจิทัล ได้ประกาศจัดตั้งในราชกิจจานุเบกษาโดยเปิดหลักสูตรวิศวกรรมศาสตรบัณฑิต<br> สาขาวิศวกรรมคอมพิวเตอร์</p>
							
						</div>
					</div>
				</div>

				<div class="item" data-hash="y2563">
				<div class="row">
				
						<div class="col-sm-8 order-md-12">
							<h3 class="color-primary">พุทธศักราช 2563 สถาบันเทคโนโลยีจิตรลดา</h3>
							<hr class="hr-yellow">

							<p class="color-primary">โรงเรียนจิตรลดาวิชาชีพ เปิดหลักสูตรระดับประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.)<br> สาขาวิชาเทคนิคควบคุมและซ่อมบำรุงระบบขนส่งทางราง (ทวิวุฒิ)</p>
							<hr class="hr-yellow">
							<div class="row">
				
								<div class="col-sm-12 col-md-6">
									<img src="<?php echo base_url()?>cdti_assets/images/pic_44.jpg" alt="" class="img-responsive">
								</div>
								<div class="col-sm-12 col-md-6">
								
								<p class="color-primary"><br>คณะเทคโนโลยีดิจิทัล เปิดรับนักศึกษารุ่นแรกในปีการศึกษา 2563</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>
