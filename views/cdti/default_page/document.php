<style>
@media (min-width: 0px) {

	.bursary-table thead *
	{
		font-size: 12px;
	}

	.content *{
		font-size: 10px;
	}
}

@media (min-width: 768px) {
	
	.bursary-table thead *
	{
		font-size: 14px;
	}

	.content *{
		font-size: 12px;
	}
}
@media (min-width: 992px) {
	
	.bursary-table thead *
	{
		font-size: 14px;
	}

	.content *{
		font-size: 14px;
	}
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> <?=$page->title?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="bursary mini-spacer pb-0">
	<?php //dd($post)?>

	<section class="bursary-table">
		<table class="bg-white container">
			<thead>
				<tr class="">
					<th class="text-center"><p class="bursary-table-title color-primary text-center">วันที่</p></th>
					<th class="text-center"><p class="bursary-table-title color-primary text-center">หมวดหมู่</p></th>
					<th class="text-center"><p class="bursary-table-title color-primary text-center">ประกาศ</p></th>
					<th class="text-center"><p class="bursary-table-title color-primary text-center">ดาวน์โหลด</p></th>
				</tr>
			</thead>
			<tbody class="content">
				<?php foreach($posts as $post): ?>
					<tr class="">
						<th class="text-center">
							<p class="bursary-table-title color-primary text-center" ><?php echo $post->date; ?></p>
						</th>
						<th class="text-center">
							<p class="bursary-table-title color-primary text-center">
								<label class="rounded text-light px-2 my-1" style="background-color: <?php echo $post->category_color; ?>!important;"><?php echo $post->category_name; ?></label>
							</p>
						</th>
						<th class="text-center"><?php echo $post->title; ?>
						</th>
						<th class="text-center">
							<?php if($post->file_path) : ?>
								<a href="<?php echo $post->file_path;?>" download>
								<i class="fa fa-download"></i>
								</a>
							<?php endif; ?>
						</th>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<!--<div class="bg-white">
			<div class="container">
				<div class="row justify-content-end bursary-header-row">
					<div class="col-md-2 text-center">
						<p class="bursary-table-title color-primary text-center">วันที่</p>
                    </div>
                    <div class="col-md-2 text-center">
						<p class="bursary-table-title color-primary text-center">หมวดหมู่</p>
                    </div>
					<div class="col-md-6 text-center">
						<p class="bursary-table-title color-primary text-center">ประกาศ</p>
					</div>
					<div class="col-md-2 text-center">
						<p class="bursary-table-title color-primary text-center">ดาวน์โหลด</p>
                    </div>
				</div>
			</div>
		</div>
		</div>-->
	</section>
</div>
