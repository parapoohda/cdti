<section class="campus-box ">
	<div class="container">
		<div class="section-header campus-header mb-4">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 text-primary"><i class="far fa-newspaper"></i> รู้จักสำนัก</h3>
				</div>
				<div class="col-sm-6 text-right">
					<ol class="breadcrumb ">
              <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
              <li class="breadcrumb-item active">รู้จักสำนักวิชาศึกษาทั่วไป</li>
          </ol>
				</div>
			</div>
		</div>
		<div class="campus-content">
			
			<div class="row m-b-40">
				<div class="col-sm-2 col-lg-12">
					
					<div class="campus-paragraph ">
					<?php echo html_entity_decode($page->page_content) ?>
					</div>
				</div>
				
			</div>
			<div class="feature7">
			</div>
		</div>

	</div>
</section>


