<section class="mini-spacer campus-box mt-5">
	<div class="container">
		<div class="section-header campus-header m-b-60">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title color-primary mt-4"><img src="<?php echo base_url();?>cdti_assets/images/seminar.png" alt=""> สภาสถาบันฯ</h3>
				</div>
			</div>

		</div>
		<nav class="nav nav-tabs navbar-team">
			<a class="nav-link active" href="<?php echo base_url('สภาสถาบัน')?>">สภาสถาบันฯ</a>
			<a class="nav-link " href="<?php echo base_url('ผู้บริหาร')?>">ผู้บริหาร</a>
			<a class="nav-link " href="<?php echo base_url('โครงสร้างสถาบัน')?>">โครงสร้างสถาบันฯ</a>
		</nav>
	</div>
</section>
<section id="teambanner" class="teambanner spacer">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-4">
				<img src="<?php echo base_url();?>cdti_assets/images/teamleader.png" alt="ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี" class="img-responsive img-fluid">
			</div>
			<div class="col-md-8 text-center">
				<img src="<?php echo base_url();?>cdti_assets/images/Royal_Flag_of_Princess_Maha_Chakri_Sirindhorn.png" alt="ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279ก&#65279นิ&#65279ษ&#65279ฐ&#65279า&#65279ธิ&#65279ร&#65279า&#65279ช&#65279เ&#65279จ้&#65279า 
ก&#65279ร&#65279ม&#65279ส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279เ&#65279ท&#65279พ&#65279รั&#65279ต&#65279น&#65279ร&#65279า&#65279ช&#65279สุ&#65279ด&#65279า 
เ&#65279จ้&#65279า&#65279ฟ้&#65279า&#65279ม&#65279ห&#65279า&#65279จั&#65279ก&#65279รี&#65279สิ&#65279ริ&#65279น&#65279ธ&#65279ร 
ม&#65279ห&#65279า&#65279ว&#65279ชิ&#65279ร&#65279า&#65279ล&#65279ง&#65279ก&#65279ร&#65279ณ&#65279ว&#65279ร&#65279ร&#65279า&#65279ช&#65279ภั&#65279ก&#65279ดี 
สิ&#65279ริ&#65279กิ&#65279จ&#65279ก&#65279า&#65279ริ&#65279ณี&#65279พี&#65279ร&#65279ย&#65279พั&#65279ฒ&#65279น 
รั&#65279ฐ&#65279สี&#65279ม&#65279า&#65279คุ&#65279ณ&#65279า&#65279ก&#65279ร&#65279ปิ&#65279ย&#65279ช&#65279า&#65279ติ 
ส&#65279ย&#65279า&#65279ม&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279กุ&#65279ม&#65279า&#65279รี" class="teamleader-logo img-fluid mx-auto d-block mt-4 mb-4">
				<p class="teamleader-name">
					สมเด็จพระกนิษฐาธิราชเจ้า กรมสมเด็จพระเทพรัตนราชสุดา <br class="hidden-xs" /> เจ้าฟ้ามหาจักรีสิรินธร มหาวชิราลงกรณวรราชภักดี <br class="hidden-xs" /> สิริกิจการิณีพีรยพัฒน รัฐสีมาคุณากรปิยชาติ สยามบรมราชกุมารี
				</p>
				<h3 class="teamleader-title">
					นายกสภาสถาบันเทคโนโลยีจิตรลดา
				</h3>
			</div>
		</div>
	</div>
</section>

<?php if($teams_type2): $i = 0;?>
  <section class="teamlist spacer">
        <div class="container ">
          <?php foreach ($teams_type2 as $key => $value):?>
                <?php if($i !=  $value->gallery_level)
                      {
                          if($i != 0)echo '</div>';
                          $i = $value->gallery_level;
                          echo '<div class="row justify-content-md-center">';
                      }?>
                <div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 mb-3">
                    <div class="team text-center">
                      <img src="<?php echo base_url().$value->path_big; ?>" alt="<?php echo html_escape($value->title); ?>" class="w-90-per d-block mx-auto mb-2">
                      <p class="team-name mb-0 color-darkgray"><?php echo html_escape($value->title); ?></p>
                      <p class="color-3"><small><?php echo html_escape($value->description); ?></small></p>
                    </div>
                </div>
           <?php endforeach;?>
    </div>
  </section>
<?php endif;?>
