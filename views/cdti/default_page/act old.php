<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> พระราชบัญญัติ/ข้อบังคับ/ระเบียบ/ประกาศ/คำสั่งสถาบันเทคโนโลยีจิตรลดา</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container mb-5">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-1" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">1.พระราชบัญญัติ</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/act/01_01.pdf">1.พระราชบัญญัติสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2561</a><br>
                    </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-2" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">2.โครงสร้างองค์กร</a>
                </h4>
            </div>
            <div id="panelBody-2" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_01.pdf">1. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งส่วนงานของสถาบัน พ.ศ. 2561</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_02.pdf">2. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานภายในสำนักงานสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_03.pdf">3. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในสำนักงานสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_04.pdf">4. ประกาศสถาบันเทคโนโลยีจิตรลดา ตรา เครื่องหมาย หรือสัญลักษณ์ของสถาบันหรือส่วนงานของสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_05.pdf">5. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการรักษาราชการแทน การมอบอำนาจให้ปฏิบัติการแทน และการมอบอำนาจช่วงให้ปฏิบัติการแทนของผู้ดำรงตำแหน่งต่างๆในสถาบัน พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_06.pdf">6. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งหน่วยงานภายในคณะและส่วนงานที่มีฐานะเทียบเท่าคณะ พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_07.pdf">7. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งส่วนงานของสถาบัน (ฉบับที่ 2) พ.ศ. 2562</a><br>
                      <a target="_blank" href="<?=base_url()?>uploads/files/act/02_08.pdf">8. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง งานตรวจสอบภายในของสถาบัน พ.ศ. 2562</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-3" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">3.สภาสถาบันฯ</a>
                </h4>
            </div>
            <div id="panelBody-3" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_01.pdf">1. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งนายกสภาสถาบัน พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_02.pdf">2. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งกรรมการสภาสถาบันผู้ทรงคุณวุฒิ พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_03.pdf">3. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์และการได้มาซึ่งกรรมการสภาสถาบันประเภทหัวหน้าส่วนงานและผู้ปฏิบัติงานในสถาบัน พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_04.pdf">4. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์และวิธีการประชุมสภาสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_05.pdf">5. ประกาศสำนักนายกรัฐมนตรี เรื่อง แต่งตั้งนายกสภาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_06.pdf">6. ประกาศสำนักนายกรัฐมนตรี เรื่อง แต่งตั้งกรรมการสภาสถาบันผู้ทรงคุณวุฒิของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_07.pdf">7. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งอุปนายกสภาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_08.pdf">8. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งกรรมการและเลขานุการสภาสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_09.pdf">9. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งกรรมการสภาสถาบันประเภทหัวหน้าส่วนงานและผู้ปฏิบัติงานในสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_10.pdf">10. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการติดตาม ตรวจสอบ และประเมินผลการปฏิบัติงานของสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_11.pdf">11. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการตรวจสอบ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/03_12.pdf">12. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการติดตาม ตรวจสอบ และประเมินผลการปฏิบัติงานของสถาบันเทคโนโลยีจิตรลดา</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-4" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">4.สภาวิชาการ</a>
                </h4>
            </div>
            <div id="panelBody-4" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/04_01.pdf">1. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสภาวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/04_02.pdf">2. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งประธานสภาวิชาการ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/04_03.pdf">3. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งกรรมการสภาวิชาการ</a><br>

                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-5" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">5.การบริหารสถาบัน</a>
                </h4>
            </div>
            <div id="panelBody-5" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_01.pdf">1. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งอธิการบดี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_02.pdf">2. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์ คุณสมบัติ วิธีการได้มา วาระการดำรงตำแหน่ง การพ้นจากตำแหน่ง อำนาจหน้าที่ และการบริหารงานของหัวหน้าส่วนงาน พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_03.pdf">3. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้บริหารเทคโนโลยีสารสนเทศระดับสูง (Chief Information Officer : CIO) ของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_04.pdf">4. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบอำนาจให้รองอธิการบดีและหัวหน้าส่วนงาน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_05.pdf">5. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการเทคโนโลยีสารสนเทศ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_06.pdf">6. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบหมายผู้รับผิดชอบลงชื่อย่อกำกับตรา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_07.pdf">7. ประกาศสำนักนายกรัฐมนตรี เรื่อง แต่งตั้งอธิการบดีสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_08.pdf">8. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยคณะกรรมการประจำส่วนงานด้านวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_09.pdf">9. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการประชุมและวิธีการดำเนินการประชุม พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_10.pdf">10. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งรองอธิการบดีสถาบันเทคโนโลยีจิตรลดา</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_11.pdf">11. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งหัวหน้าส่วนงาน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_12.pdf">12. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้รักษาการแทนอธิการบดี สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_13.pdf">13. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้รักษาการแทนหัวหน้าส่วนงาน และตำแหน่งอื่นใดของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_14.pdf">14. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบอำนาจให้รองอธิการบดีและหัวหน้าส่วนงานปฏิบัติหน้าที่แทน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_15.pdf">15. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยระบบ หลักเกณฑ์ และวิธีการประกันคุณภาพการศึกษาและการวิจัย พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_16.pdf">16. กฎบัตรการตรวจสอบภายในสถาบันเทคโนโลยีจิตรลดา ว่าด้วยวัตถุประสงค์ อำนาจหน้าที่ และความรับผิดชอบของงานตรวจสอบภายใน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_17.pdf">17. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะเทคโนโลยีอุตสาหกรรม</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_18.pdf">18. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน สำนักวิชาศึกษาทั่วไป</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_19.pdf">19. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการส่งเสริมงานวิจัย นวัตกรรม และผลงานสร้างสรรค์</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_20.pdf">20. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการวางแผน กำกับ ติดตามและดูแลโครงการจัดการขยะ สถาบันเทคโนโลยีจิตรลดา</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_21.pdf">21. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะเทคโนโลยีดิจิทัล</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_22.pdf">22. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำโรงเรียนจิตรลดาวิชาชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_23.pdf">23. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการประจำส่วนงาน คณะบริหารธุรกิจ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_24.pdf">24. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพัฒนาสำนักงานสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_25.pdf">25. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการบริหารการประกันคุณภาพการศึกษา พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_26.pdf">26. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกฎหมาย สถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_27.pdf">27. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพัฒนาการสื่อสารองค์กรของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_28.pdf">28. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง เจตจำนงสุจริตในการบริหารงานของสถาบันเทคโนโลยีจิตรลดา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_29.pdf">29. Announcement on Integrity and Transparency Intention of Chitralada Technology Institute</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/05_30.pdf">30. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะทำงานเพื่อเข้ารับการประเมินคุณธรรมและความโปร่งใส ในการดำเนินงานของหน่วยงานภาครัฐ ปีงบประมาณ พ.ศ. 2563</a><br>

                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-6" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">6.การศึกษา วิชาการ และนักเรียนนักศึกษา</a>
                </h4>
            </div>
            <div id="panelBody-6" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_01.pdf">1. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_02.pdf">2. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยครุยวิทยฐานะ เข็มวิทยฐานะ และครุยประจำตำแหน่ง โอกาสและเงื่อนไขที่ใช้ พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_03.pdf">3. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยเครื่องแบบ เครื่องหมาย และเครื่องแต่งกายของนักศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_04.pdf">4. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยวินัยนักศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_05.pdf">5. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสโมสรนักศึกษา พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_06.pdf">6. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การกำหนดลักษณะ ชนิด ประเภท และส่วนประกอบของครุยวิทยฐานะ เข็มวิทยฐานะ และครุยประจำตำแหน่ง พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_07.pdf">7. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง อัตราค่าธรรมเนียมการศึกษา ค่าบำรุงการศึกษาหรือค่าเล่าเรียน และเงินเรียกเก็บประเภทอื่นๆ สำหรับนักศึกษาระดับปริญญาตรี พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_08.pdf">8. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ค่าธรรมเนียมและเงินเรียกเก็บประเภทอื่นๆสำหรับนักเรียน นักศึกษา โรงเรียนจิตรลดาวิชาชีพ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_09.pdf">9. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ค่าธรรมเนียมและเงินเรียกเก็บประเภทอื่นๆสำหรับนักเรียนระดับ ปวช. โรงเรียนจิตรลดาวิชาชีพ โครงการพิเศษเกษตรนวัต พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_10.pdf">10. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง แนวปฏิบัติตนของนักเรียนและนักศึกษาในสถาบันและพื้นที่สำนักพระราชวัง</a><br>

                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_11.pdf">11. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการจัดการศึกษาระดับประกาศนียบัตรวิชาชีพ (ปวช.) และระดับประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.) พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_12.pdf">12. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกิจการนักเรียนและนักศึกษา</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/06_13.pdf">13. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การส่งเสริมคุณธรรม จริยธรรม ของนักเรียนและนักศึกษาสถาบันเทคโนโลยีจิตรลดา</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-7" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">7.การบริหารงานบุคคล</a>
                </h4>
            </div>
            <div id="panelBody-7" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_01.pdf">1. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_02.pdf">2. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยเครื่องแบบพิธีการของพนักงานสถาบัน พ.ศ. 2561</a><br>
                  <!--  <a target="_blank" href="<?=base_url()?>uploads/files/act/07_03.pdf">3. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการกำหนดเงินเดือน และเงินประจำตำแหน่งสำหรับผู้ปฏิบัติงานในสถาบัน พ.ศ. 2562</a><br> -->
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_04.pdf">3. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง วันเวลาปฏิบัติงาน วันหยุดและวันลา พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_05.pdf">4. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสวัสดิการและสิทธิประโยชน์ของบุคลากร พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_06.pdf">5. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_07.pdf">6. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล (ฉบับที่ 2) พ.ศ.2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_08.pdf">7. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_09.pdf">8. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายสนับสนุน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_10.pdf">9. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_11.pdf">10. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดเกณฑ์คะแนนทดสอบความสามารถทางภาษาอังกฤษของอาจารย์ประจำ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_12.pdf">11. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการสวัสดิการและสิทธิประโยชน์</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_13.pdf">12. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล (ฉบับที่ 3) พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_14.pdf">13. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ (ฉบับที่ 2) พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_15.pdf">14. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการกองทุนสำรองเลี้ยงชีพ</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_16.pdf">15. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายการบริหารทรัพยากรบุคคล และการพัฒนาบุคลากรของสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_17.pdf">16. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายส่งเสริมจรรยาบรรณของบุคลากรสถาบัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_18.pdf">17. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษาต่อระดับปริญญาโทหรือปริญญาเอกแบบลาศึกษาบางเวลา พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_19.pdf">18. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง จรรยาบรรณของครูวิชาชีพและคณาจารย์สถาบันเทคโนโลยีจิตรลดา</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-8" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">8.งบประมาณ การเงิน การบัญชี และการพัสดุ</a>
                </h4>
            </div>
            <div id="panelBody-8" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_01.pdf">1. ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงบประมาณ การเงิน การพัสดุ และทรัพย์สินของสถาบัน พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_02.pdf">2. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการตรวจสอบพัสดุประจำปี 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_03.pdf">3. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง มอบหมายผู้รับผิดชอบลงชื่อเป็นผู้อนุญาตให้นำพัสดุออกนอกพื้นที่</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_04.pdf">4. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการจัดสรรงบประมาณรายจ่าย พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_05.pdf">5. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งผู้ปฏิบัติงานในสถาบันเป็นกรรมการเก็บรักษาเงินคงเหลือประจำวัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_06.pdf">6. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง เปลี่ยนแปลงกรรมการเก็บรักษาเงินคงเหลือประจำวัน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_07.pdf">7. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการจ่ายเงินสำรองจ่ายและเงินสดย่อย พ.ศ.2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_08.pdf">8. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการงบประมาณ การรับเงิน การเก็บรักษาเงิน การเบิกเงิน การจ่ายเงิน และการควบคุมดูแลการจ่าย พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/08_09.pdf">9. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการที่ปรึกษาด้านการเงิน</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-9" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">9.อื่นๆ</a>
                </h4>
            </div>
            <div id="panelBody-9" class="panel-collapse collapse">
                <div class="panel-body">
                  <h5 class="panel-title">ทุนการศึกษา</h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_01.pdf">1. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษาพระราชทาน</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_02.pdf">2. คำสั่งสถาบันเทคโนโลยีจิตรลดา เรื่อง แต่งตั้งคณะกรรมการพิจารณาทุนการศึกษา</a><br>
                  </p>
                  <h5 class="panel-title">มาตรการและการเฝ้าระวังการระบาดของโรคไวรัสโคโรนาสายพันธุ์ใหม่ 2019 </h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_03.pdf">1. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การเรียนการสอนและการปฏิบัติงานในช่วงที่เชื้อไวรัสโคโรน่าสายพันธุ์ใหม่กำลังระบาด</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_04.pdf">2. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง งดหรือเลื่อนการจัดกิจกรรมนักเรียนนักศึกษา (ฉบับที่ 1)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_05.pdf">3. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจัดการเรียนการสอน การฝึกปฏิบัติวิชาชีพ และการปฏิบัติงานของบุคลากร ในสถานการณ์ไม่ปกติอันเนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 3)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_06.pdf">4. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การจัดการเรียนการสอน การฝึกปฏิบัติวิชาชีพ และการปฏิบัติงานของบุคลากร ในสถานการณ์ไม่ปกติอันเนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 4)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_07.pdf">5. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาปิดที่ทำการเป็นการชั่วคราว เนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 5) </a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_08.pdf">6. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาปิดที่ทำการเป็นการชั่วคราว เนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 6)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_09.pdf">7. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรการช่วยเหลือนักเรียน นักศึกษา อันเนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_10.pdf">8. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ขยายระยะเวลาปิดที่ทำการเป็นการชั่วคราว เนื่องจากการแพร่ระบาดของเชื้อไวรัส COVID-19 (ฉบับที่ 7)</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/09_11.pdf">9. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การปฎิบัติงานของบุคลากรในสถานการณ์ COVID-19 (ฉบับที่ 8)</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>
<!--
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-10" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">10.คำสั่งใหม่สภาสถาบัน</a>
                </h4>
            </div>
            <div id="panelBody-10" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="">เอกสาร 10</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-11" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">อื่นๆ</a>
                </h4>
            </div>
            <div id="panelBody-11" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="pt10-pl30">
                      <a target="_blank" href="">เอกสาร อื่นๆ</a><br>
                  </p>
                </div>
            </div>
        </div>
      </section>
      -->
    </div>
</div>

</style>
<!--<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt="">สืบค้นเอกสาร</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container" style="margin-bottom : 3em;">
    <div id="accordion" class="panel-group">
      <div class="row">
  			<div class="col-sm-12">
        <iframe class="WebPublish_iframe" src="https://edoc.cdti.ac.th/docweb/v2/publishDoc.aspx" frameborder="0" width="100%" height="600px"></iframe>
        </div>
    </div>
  </div>
</div>
-->