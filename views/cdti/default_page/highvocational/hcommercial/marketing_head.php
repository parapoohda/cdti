<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing10.jpg"
        class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาการตลาด</h3>
        <h3 class="font-light color-primary">Marketing</h3>
        <hr class="hr-info">
        <p class="text-dark">มุ่งเน้นการเรียนรู้แนวคิดการตลาดสมัยใหม่
ทั้งด้านการวางแผนการตลาด การขายออนไลน์
การตลาดอิเล็กทรอนิกส์ การเขียนแผนธุรกิจอย่างมีคุณภาพ
การสื่อสารทางด้านภาษา
เพื่อพัฒนาศักยภาพด้านธุรกิจให้ก้าวทันการเปลี่ยนแปลงของเทคโนโล
ยีและก้าวทันโลก และเพื่อตอบสนอง
ความต้องการของสถานประกอบการหรือการประกอบธุรกิจส่วนตัวอย่า
งผู้ประกอบการมืออาชีพ
นอกจากนี้ยังสามารถศึกษาต่อในระดับสูงต่อไปได้</p>
    </div>