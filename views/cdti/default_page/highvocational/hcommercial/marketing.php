<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาการตลาด</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาการตลาด<br /> <small>Marketing</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">มุ่งเน้นการเรียนรู้แนวคิดการตลาดสมัยใหม่
                            ทั้งด้านการวางแผนการตลาด การขายออนไลน์
                            การตลาดอิเล็กทรอนิกส์ การเขียนแผนธุรกิจอย่างมีคุณภาพ
                            การสื่อสารทางด้านภาษา
                            เพื่อพัฒนาศักยภาพด้านธุรกิจให้ก้าวทันการเปลี่ยนแปลงของเทคโนโล
                            ยีและก้าวทันโลก และเพื่อตอบสนอง
                            ความต้องการของสถานประกอบการหรือการประกอบธุรกิจส่วนตัวอย่า
                            งผู้ประกอบการมืออาชีพ
                            นอกจากนี้ยังสามารถศึกษาต่อในระดับสูงต่อไปได้</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/news/40.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร ?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ ?</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">สาขาวิชาการตลาด</h4>
                                        <p>ได้รับความร่วมมือจากผู้ทรงคุณวุฒิขององค์กรที่มีชื่อเสียง
                                            ถ่ายทอดความรู้และประสบการณ์
                                            ทำให้ได้เปิดโลกทัศน์แห่งการเรียนรู้
                                            พร้อมที่จะไปศึกษาต่อในสถาบันที่มีชื่อเสียงทั้งในประเทศและต่างประเ
                                            ทศ</p>
                                        <br />
                                        <p>ได้รับการฝึกทักษะทางวิชาชีพจากสถานประกอบการชั้นนำ
                                            สร้างให้ผู้เรียนเป็นนักการตลาด ยุคใหม่
                                            ที่มีพื้นฐานความรู้ทางภาษาอย่างได้แม่นยำ
                                            พร้อมก้าวเข้าสู่โลกธุรกิจที่ยั่งยืน โดยสามารถ
                                            นำความรู้มาประยุกต์ใช้ให้เกิดทักษะด้านการสื่อสาร ทักษะการคิด
                                            การแก้ปัญหา ทักษะทางสังคม
                                            และการดำรงชีวิตเพื่อพัฒนาตนเองให้สามารถประสบความสำเร็จในส
                                            ายอาชีพของตน</p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ระดับ ปวช. รับนักเรียนที่สำเร็จการศึกษา ระดับมัธยมศึกษาปีที่ 3</p>
                                        <p>ระดับ ปวส. รับนักศึกษาที่สำเร็จการศึกษา ระดับประกาศนียบัตรวิชาชีพ (ปวช.)
                                            ชั้นปีที่ 3</p>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โครงสร้างหลักสูตร</h4>
                                        <p>รอข้อมูล</p>

                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชาการตลาด เรียนอะไร</h4>
                                        <p>ศึกษาความรู้ที่สัมพันธ์กับวิชาชีพทางการตลาด อาทิ ภาษาจีน ภาษาอังกฤษ
                                            คณิตศาสตร์พาณิชยกรรม วิทยาศาสตร์เพื่ออาชีพและบริการ
                                            คอมพิวเตอร์และสารสนเทศเพื่องานอาชีพ อินเทอร์เน็ตในงานธุรกิจ
                                            ธุรกิจในการเป็นผู้ประกอบการ การบัญชี การจัดซื้อ การขาย ฯลฯ</p>
                                        <p>ศึกษาความรู้ที่เจาะลึกในวิชาชีพการตลาด อาทิ การขายออนไลน์ การโฆษณา
                                            และการส่งเสริมการขาย การพัฒนาบุคลิกภาพนักขาย การจัดจำหน่ายสินค้าและบริการ
                                            การาตลาดอิเล็กทรอนิกส์ พฤติกรรมผู้บริโภค การบริการธุรกิจขนาดย่อม
                                            การสื่อสารการตลาด กลยุทธ์การตลาด การวิจัยการตลาด การสร้างตราสินค้า ฯลฯ</p>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing10.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing11.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โอกาศทางธุรกิจหลังสำเร็จการศึกษา</h4>
                                        <p>เมื่อจบการศึกษาสามารถทำงานในสถานประกอบการ หรือองค์การของรัฐในหลายด้าน เช่น
                                        </p>

                                        <ul>
                                            <li>ทุนสนับสนุนการเนักวางแผนกลยุทธ์ทางการตลาด นักสื่อสารทางการตลาด</li>
                                            <li>นักวิจัยตลาด / นักวิเคราะห์การตลาด นักการตลาดดิจิตอล</li>
                                            <li>นักการตลาดระหว่างประเทศ นักจัดกิจกรรมทางการตลาด</li>
                                            <li>นักโฆษณา / นักประชาสัมพันธ์ ที่ปรึกษาด้านการตลาด</li>
                                        </ul>
                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing12.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/marketing3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>