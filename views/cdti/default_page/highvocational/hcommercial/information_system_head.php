<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/hcommercial/InformationTechnology1.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาเทคโนโลยีสารสนเทศ</h3>
        <h3 class="font-light color-primary">Information and Communication Technology</h3>
        <hr class="hr-info">
        <p class="text-indent">สาขาวิชาเทคโนโลยีสารสนเทศและการสื่อสาร โรงเรียนจิตรลดาวิชาชีพ
            เป็นสาขาวิชาประยุกต์ที่มีการเรียนการสอนคล้าย กับสาขาวิชาวิทยาการคอมพิวเตอร์
            แต่ไม่ลึกเท่ากับวิทยาการคอมพิวเตอร์ โดยที่สาขาวิชานี้จะเน้นให้เรียนรู้เกี่ยวกับการใช้โปรแกรมเบื้องต้น
            ระบบเทคโนโลยีสารสนเทศเบื้องต้น การพัฒนาเว็บ การออกแบบกราฟิก การเขียนโปรแกรม
            งานไฟฟ้าและอิเล็กทรอนิกส์เบื้องต้น และศาสตร์อื่นๆ ที่มีความเกี่ยวข้องทางด้านเทคโนโลยีสารสนเทศ
            ทั้งนี้เพื่อให้ก้าวทันโลกเทคโนโลยีด้านโทรคมนาคมและเครือข่าย</p>
        
    </div>