<section class="campus-box  mini-spacer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url(); ?>cdti_assets/images/trophy.png" alt=""> บรรยากาศการเรียนการสอน</h3>
						</div>
						<div class="col-sm-8 text-right">
							<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
                <li class="breadcrumb-item active">บรรยากาศการเรียนการสอน</li>
              </ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="campus-gallery mini-spacer">
	<div class="container">
		<div class="row">
      <div class="container">
        	<div class="portfolio4 campus-gallery-row">
            <div class="row portfolio-box4 popup-gallery">
               <?php foreach ($posts as $key => $post): ?>
                    <div class="col-sm-4 filter">
                      <div class="img-gallery">
                        <a href="<?php echo fac_url('fac-gellery/'.$page->slug,$post); ?>">
                          <img src="<?php echo get_post_image($post,'mid'); ?>" alt="<?php echo html_escape($post->title); ?>" class="img-responsive">
                          <div class="img-overlay">
                            <p><?php echo html_escape($post->title); ?></p>
                          </div>
                        </a>
                      </div>
											 <p><?php echo html_escape($post->title); ?></p>
                    </div>
                <?php endforeach; ?>
          </div>
        </div>
        <div class="row mb-4">
          <div class="col-md-10 mx-auto text-center">
              <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
      </div>
		</div>
	</div>
</section>
