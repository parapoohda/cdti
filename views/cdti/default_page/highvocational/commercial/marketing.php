<section class="campus-box mini-spacer pb-0">
	<div class="container">
		<div class="section-header campus-header mb-4">
			<div class="row">
				<div class="col-sm-4 align-middle">
					 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา</h3>
				</div>
				<div class="col-sm-8 text-right">
					<ol class="breadcrumb">
	                    <li class="breadcrumb-item"><a href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
	                    <li class="breadcrumb-item"><a href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
	                    <li class="breadcrumb-item active">สาขาวิชาการตลาด</li>
	                </ol>
				</div>
			</div>
		</div>
		<div class="campus-content p-t-20">
			<div class="row">
				<div class="col-md-6">
					<div class="campus-detail">
						<h4 class="text-primary">สาขาวิชาการตลาด<br/> <small>Marketing</small></h4>
						<hr class="hr-primary">
						<p class="text-indent">เป็นเจ้าของกิจการผู้มีความสามารถประกอบอาชีพได้หลากหลาย อยู่ในภาคส่วนการบริหารการจัดการได้ ทั้งภาครัฐและเอกชน เช่น เจ้าหน้าฝ่ายการตลาด (Marketing) ผู้จัดการฝ่ายการตลาด (Marketing Manager) ผู้จัดการผลิตภัณฑ์ (Product Manager) 
						เจ้าหน้าที่ฝ่ายวิจัยตลาด (Marketing Researcher) เจ้าหน้าที่อบรมด้านการตลาด เป็นต้น สามารถประกอบอาชีพอิสระและธุรกิจส่วนตัวได้เป็นอย่างดีและสามารถศึกษาต่อในระดับสูงต่อไปได้</p>
					</div>
				</div>
				<div class="col-md-6">
					<img src="<?php echo base_url(); ?>cdti_assets/images/news/40.jpg" alt="" class="img-responsive img-responsive">
				</div>
			</div>
		</div>
		
	</div>
		
	<div class="campus-tabs ">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                    	<div class="slider"></div>
                        <li class=" nav-item"> 
                        	<a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false"> 
                        		<img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt="" class="d-block mx-auto"> <br/> 
                        		<p>จุดเด่นของสาขา</p>
                        	</a> 
                        </li>
                        <li class="nav-item"> 
                        	<a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                        		<img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt="" class="d-block mx-auto"> <br/> <p>คุณสมบัติของผู้เข้าศึกษา</p>
                        	</a> 
                        </li>
                        <li class="nav-item"> 
                        	<a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                        		<img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt="" class="d-block mx-auto"> <br/> 
                        		<p>โครงสร้างหลักสูตร</p>
                        	</a> 
                        </li>
                        <li class="nav-item"> 
                        	<a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                        		<img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt="" class="d-block mx-auto"> <br/> 
                        		<p>สาขานี้เรียนอะไร ?</p>
                        	</a> 
                        </li>
                        <li class="nav-item"> 
                        	<a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                        		<img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt="" class="d-block mx-auto"> <br/> 
                        		<p>จบแล้วมีงานทำ ?</p>
                        	</a> 
                        </li>
                    </ul>
                </div>
            </div>
      	 </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
        	<div class="container">
        		<div class="row">
	        		<div class="col-sm-12">
	                    <div class="tab-content br-n pn p-t-30">
	                        <div id="navpills-1" class="tab-pane fadeIn animated slow active">
	                            <div class="row">
	                            	<div class="col-md-6">
	                                	<h4 class="text-primary">สาขาวิชาการตลาด</h4>
	                               		<p>รอข้อมูล</p>
	                                	<br/>
	                                </div>
	                                <div class="col-md-6">
	                                	<div class="row">
	                                		<div class="col-sm-12 mb-4">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/17.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/16.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/19.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                	</div>
	                                </div>

	                            </div>
	                        </div>
	                        <div id="navpills-2" class="tab-pane fadeIn animated slow">
	                            <div class="row">
	                            	<div class="col-md-6">
	                                	<h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
	                                	<p>รอข้อมูล</p>
	                                </div>
	                                <div class="col-md-6 ">
	                                	<div class="row">
										<div class="col-sm-12 mb-4">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/24.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/23.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/21.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                	</div>
	                                </div>

	                            </div>
	                        </div>
	                        <div id="navpills-3" class="tab-pane fadeIn animated slow">
	                            <div class="row">
	                                <div class="col-md-6 6 order-md-12">
	                                	<h4 class="text-primary mb-2">ปริญญาและสาขาวิชา</h4>
	                                	<p>รอข้อมูล</p>
	                                	
	                                </div>
	                                <div class="col-md-6 6 order-md-1 ">
	                                	<div class="row">
										<div class="col-sm-12 mb-4">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/28.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/27.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/29.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                	</div>
	                                </div>
	                            </div>
	                        </div>
	                        <div id="navpills-4" class="tab-pane fadeIn animated slow">
	                            <div class="row">
	                                <div class="col-md-6 ">
	                                	<h4 class="text-primary mb-2">หลักสูตรการจัดการธุรกิจอาหาร</h4>
	                                	<p>รอข้อมูล</p>

	                                </div>
	                                <div class="col-md-6 ">
	                                	<div class="row">
										<div class="col-sm-12 mb-4">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/17.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/16.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/19.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                	</div>
	                                </div>
	                            </div>
	                        </div>
	                        <div id="navpills-5" class="tab-pane fadeIn animated slow">
	                            <div class="row">
	                                <div class="col-md-6 6 order-md-12">
	                                	<h4 class="text-primary mb-2">โอกาศทางธุรกิจหลังสำเร็จการศึกษา</h4>
	                                	<p>รอข้อมูล</p>
	                                	<h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
	                                	<div class="row">
	                                		<div class="col-sm-12">
	                                			<div id="partner-slider"  class="owl-carousel owl-theme">
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png" alt="">
				                                    </div>
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png" alt="">
				                                    </div>
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png" alt="">
				                                    </div>
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png" alt="">
				                                    </div>
				                                </div>
	                                		</div>
	                                	</div>
	                                </div>
	                                <div class="col-md-6 6 order-md-1 ">
	                                	<div class="row">
										<div class="col-sm-12 mb-4">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/17.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/16.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                		<div class="col-sm-6">
	                                			<img src="<?php echo base_url(); ?>cdti_assets/images/news/19.jpg" alt="" class="img-responsive m-b-10">
	                                		</div>
	                                	</div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>
        	</div>
		</div>
	</div>
</section>
<script>
	$(".campus-tabs .nav-pills a").click(function() {
	  var position = $(this).parent().position();
	  var width = $(this).parent().width();
	    $(".campus-tabs .slider").css({"left":+ position.left,"width":width});
	});
	var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
	var actPosition = $(".campus-tabs .nav-pills .active").position();
	$(".campus-tabs .slider").css({"left":+ actPosition.left,"width": actWidth});

</script>
