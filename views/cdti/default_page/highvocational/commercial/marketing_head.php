<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/40.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาการตลาด</h3>
        <h3 class="font-light color-primary">Marketing</h3>
        <hr class="hr-info">
        <p class="text-dark">เป็นเจ้าของกิจการผู้มีความสามารถประกอบอาชีพได้หลากหลาย อยู่ในภาคส่วนการบริหารการจัดการได้ ทั้งภาครัฐและเอกชน เช่น เจ้าหน้าฝ่ายการตลาด (Marketing) 
        ผู้จัดการฝ่ายการตลาด (Marketing Manager) ผู้จัดการผลิตภัณฑ์ (Product Manager) เจ้าหน้าที่ฝ่ายวิจัยตลาด (Marketing Researcher) เจ้าหน้าที่อบรมด้านการตลาดเป็นต้น
        สามารถประกอบอาชีพอิสระและธุรกิจส่วนตัวได้เป็นอย่างดีและสามารถศึกษาต่อในระดับสูงต่อไปได้</p>
      </div>
