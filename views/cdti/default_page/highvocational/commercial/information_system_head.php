<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/34.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาเทคโนโลยีสารสนเทศ</h3>
        <h3 class="font-light color-primary">Information and Communication Technology</h3>
        <hr class="hr-info">
        <p class="text-dark">ผลิตนักวิเคราะห์และออกแบบระบบ ฝีมือดี เป็นผู้เชี่ยวชาญใช้โปรแกรมในหน่วยงานภาครัฐและเอกชน  
        เป็นผู้นำทางด้านออกแบบพัฒนาและติดตั้งระบบ NETWORK, E-Commerce, E-Business,เขียนโปรแกรม, เรียนรู้ระบบ E-Learning
        สามารถผลิตสื่อสั่งพิมพ์ได้ สามารถออกแบบและสร้างงานแอนนิเมชั่น ออกแบบและพัฒนาเว็บเพจ รวมถึงการเขียนคำสั่งควบคุมคอมพิวเตอร์ และสามารถศึกษาต่อในระดับสูงต่อไปได้</p>
      </div>
