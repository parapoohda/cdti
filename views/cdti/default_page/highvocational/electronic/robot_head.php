<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/45.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาเมคคาทรอนิกส์และหุ่นยนต์</h3>
        <h3 class="font-light color-primary">Mechatronics</h3>
        <hr class="hr-info">
        <p class="text-dark"> ในยุคไทยแลนด์ 4.0  ภาคอุตสาหกรรมการผลิต มุ่งเน้นการนำเอาเทคโนโลยี หุ่นยนต์อัตโนมัติ เข้ามาทำงานแทนมนุษย์มากขึ้น  
		ไม่ว่าจะเป็นอุตสาหกรรมหนัก หรือ อุตสาหกรรมอาหารนอกจากนั้นในชีวิตประจำวัน ก็ได้มีการนำหุ่นยนต์ และ ระบบอัตโนมัติเข้ามาใช้งานเพื่ออำนวยความสะดวกและ
		เพิ่มประสิทธิภาพมากขึ้น สาขาวิชาเมคคาทรอนิกส์และหุ่นยนต์จังเป็นสาขาวิชา ที่ตอบโจทย์ กับสถานการณ์ในยุคสมัยนี้อย่างยิ่ง</p>
      </div>
