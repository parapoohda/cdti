<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="../../cdti_assets/images/news/47.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาไฟฟ้า</h3>
        <h3 class="font-light color-primary">Electrical Power</h3>
        <hr class="hr-info">
        <p class="text-dark">การใช้ชีวิตในปัจจุบัน อุปกรณ์ไฟฟ้าถือเป็นปัจจัยสำคัญในการดำเนินชีวิต
            ตั้งแต่ลืมตาจนถึงเวลาเข้านอน ต้องมีการใช้อุปกรณ์ไฟฟ้าทั้งสิ้น เช่น เครื่องปรับอากาศ ปั๊มน้ำ
            พัดลม กาต้มน้ำ หม้อหุงเข้า นอกจากนั้นการขับเคลื่อนงานในด้านต่าง ๆ
            ล้วนแล้วต้องใช้พลังงานไฟฟ้าทั้งสิ้น ดังนั้นสาขาวิชาไฟฟ้ากำลังจึงเป็นสาขาที่มีความสำคัญ
            และมีความต้องการในทุกภาคส่วนของโลกอาชีพในปัจจุบัน</p>
    </div>