<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="../../cdti_assets/images/news/46.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาอิเล็กทรอนิกส์</h3>
        <h3 class="font-light color-primary">Electronic</h3>
        <hr class="hr-info">
        <p class="text-dark"> การใช้ชีวิตในปัจจุบัน อุปกรณ์อิเล็กทรอนิกส์เข้ามามีบทบาท กับการดำเนินชีวิตประจำวัน
            อุปกรณ์ที่ใช้อำนวยความสะดวก ล้วนแต่ใช้เทคโนโลยีที่มีวงจรอิเล็กทรอนิกส์ในการควบคุม
            และเป็นส่วนประกอบแทบทั้งสิ้น เช่น โทรทัศน์ โทรศัพท์มือถือ วิทยุสื่อสาร คอมพิวเตอร์
            ระบบเซนเซอร์เพื่อควบคุมการทำงานแบบอัตโนมัติต่าง ๆ จึงถือได้ว่า
            งานอิเล็กทรอนิกส์เป็นงานที่มีความสำคัญและมีความต้องการอย่างมากในวิชาชีพปัจจุบัน</p>
    </div>