<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาเมคคาทรอนิกส์และหุ่นยนต์</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาเมคคาทรอนิกส์และหุ่นยนต์<br /><small>Mechatronics</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">ในยุคไทยแลนด์ 4.0 ภาคอุตสาหกรรมการผลิต มุ่งเน้นการนำเอาเทคโนโลยี
                            หุ่นยนต์อัตโนมัติ เข้ามาทำงานแทนมนุษย์มากขึ้น
                            ไม่ว่าจะเป็นอุตสาหกรรมหนัก หรือ อุตสาหกรรมอาหารนอกจากนั้นในชีวิตประจำวัน
                            ก็ได้มีการนำหุ่นยนต์ และ ระบบอัตโนมัติเข้ามาใช้งานเพื่ออำนวยความสะดวกและ
                            เพิ่มประสิทธิภาพมากขึ้น สาขาวิชาเมคคาทรอนิกส์และหุ่นยนต์จังเป็นสาขาวิชา ที่ตอบโจทย์
                            กับสถานการณ์ในยุคสมัยนี้อย่างยิ่ง</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/news/45.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร ?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ ?</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>

                                        <p>เป็นสาขาที่ได้เรียนรู้เกี่ยวกับ การบำรุงรักษา การควบคุม และ
                                            ออกแบบติดตั้งระบบเมคคาทรอนิกส์และหุ่นยนต์
                                            ซึ่งอุปกรณ์ในสาขามีได้รับการสนับสนุน
                                            จากสถานประกอบการชั้นนำของประเทศ
                                            จึงได้ศึกษาเรียนรู้กับอุปกรณ์และเครื่องมือที่ทันสมัย
                                            มีมาตรฐาน และ ยังเป็นการเรียนในระบบทวิภาคีกับ บริษัท
                                            ที่มีมาตรฐานจากต่างประเทศ
                                            สามารถทำงาน และ เรียนไปพร้อมกัน
                                            นักศึกษาได้มีโอกาสสัมผัสกับประสบการณ์วิชาชีพในสถานประกอบการณ์
                                            เมื่อจบการเรียนมีโอกาสได้ทำงานต่อกับทางสถานประกอบการณ์
                                        </p>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>รับนักเรียนที่เรียนจบ ระดับ ปวช. ชั้นปีที่ 3 ทุกสาขาในประเภทวิชาอุตสาหกรรม
                                            เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.75</p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โครงสร้างหลักสูตร</h4>

                                        <p>หมวดวิชาทักษะชีวิต ไม่น้อยกว่า 21 หน่วยกิต</p>
                                        <ul>
                                            <li>กลุ่มทักษะภาษาและการสื่อสาร  (ไม่น้อยกว่า 9 หน่วยกิต)</li>
                                            <li>กลุ่มทักษะการคิดและการแก้ปัญหา (ไม่น้อยกว่า 6 หน่วยกิต)</li>
                                            <li>กลุ่มทักษะทางสังคมและการดำรงชีวิต (ไม่น้อยกว่า 6 หน่วยกิต)</li>
                                        </ul>
                                        <p>หมวดวิชาทักษะวิชาชีพ ไม่น้อยกว่า 60 หน่วยกิต</p>
                                        <ul>
                                            <li>กลุ่มทักษะวิชาชีพพื้นฐาน ( 19 หน่วยกิต)</li>
                                            <li>กลุ่มทักษะวิชาชีพเฉพาะ ( 21 หน่วยกิต)</li>
											<li>กลุ่มทักษะวิชาชีพเลือก  (ไม่น้อยกว่า 12 หน่วยกิต)</li>
											<li>ฝึกประสบการณ์ทักษะวิชาชีพ  ( 4 หน่วยกิต)</li>
											<li>โครงการพัฒนาทักษะวิชาชีพ  ( 4 หน่วยกิต)</li>
										</ul>
										<p>หมวดวิชาเลือกเสรี ไม่น้อยกว่า 60 หน่วยกิต</p>
										<p>กิจกรรมเสริมหลักสูตร (2 ชั่วโมงต่อสัปดาห์ )</p>
										<p>รวม ไม่น้อยกว่า 87 หน่วยกิต</p>

                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชาเมคคาทรอนิกส์และหุ่นยนต์ เรียนอะไร</h4>
                                        <p class="">เรียนรู้ความปลอดภัย และอาชีวอนามัยในการทำงาน การคำนวนวงจรไฟฟ้า
                                            การวิเคราะห์ไฟฟ้า
                                            การเขียนแบบและอ่านแบบในงานอุตสาหกรรม การใช้เครื่องมือวัดทางไฟฟ้า
                                            ระบบโปรแกรมเมเบิลลอจิกคอลโทรล ระบบนิวเมตริกและไฮดรอลิกส์ ระบบซีเอ็นซี
                                            การควบคุมแขนกลอุตสาหกรรม การซ่อมบำรุง และ
                                            ติดตั้งระบบเมคคาทรอนิกส์และหุ่นยนต์</p>


                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p>เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ
                                            ในหลายด้านเช่น</p>

                                        <ul>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน ทำงานเกี่ยวกับ
                                                การดูแลซ่อมบำรุง ติดตั้งระบบเมคคาทรอนิกส์และหุ่นยนต์</li>
                                            <li>งานด้านวิจัยพัฒนา ผลิตภัณฑ์ด้านเทคโนโลยีสมัยใหม่
                                                เพื่อรองรับความต้องการของโลกในอนาคต</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>
                                        </ul>

                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/electronic/MechatronicsandRoboticsProgram1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>