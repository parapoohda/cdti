<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาเทคนิคยานยนต์</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาเทคนิคยานยนต์<br /> <small>Mechanical Power Technology</small>
                        </h4>
                        <hr class="hr-primary">
                        <p class="text-dark">เมื่อสำเร็จการศึกษาจากสาขาช่างยนต์ จะสามารถตัดสินใจ
                            วางแผนและแก้ไขปัญหาในงานอาชีพด้านยานยนต์ที่ไม่อยู่ภายใต้การควบคุมในบางเรื่อง
                            ประยุกต์ใช้ความรู้
                            ทักษะทางวิชาชีพเทคโนโลยีสารสนเทศและการสื่อสารในการแก้ปัญหาและการปฏิบัติงานด้านยานยนต์ให้คำแ
                            นะนำพื้นฐานที่ต้องใช้การตัดสินใจและการปฏิบัติงานแก่ผู้ร่วมงาน
                            ซ่อมและบำรุงรักษาเครื่องยนต์เล็กแก๊สโซลีนและดีเซลตามคู่มือ ตรวจสอบข้อขัดข้อง
                            บำรุงรักษาระบบฉีดเชื้อเพลิงด้วยอิเล็กทรอนิกส์ตามคู่มือ บริการระบบปรับอากาศรถยนต์
                            สามารถซ่อมและบำรุงรักษารถยนต์ตามคู่มือ</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology5.jpg"
                        alt="" class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร ?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ ?</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>

                                        <p>นักเรียนสาขาเทคนิคยานยนต์สามารถปฏิบัติงานเกี่ยวกับงานเครื่องยนต์เล็ก
                                            งานจักรยานยนต์
                                            งานอิเล็กทรอนิกส์รถยนต์เบืองต้น งานระบบฉีดเชื้อเพลิงควบคุมด้วยอิเล็กทรอนิกส์
                                            งานปรับอากาศรถยนต์
                                            งานบำรุงรักษารถยนต์ คณิตศาสตร์ช่างยนต์ งานเครื่องมือกลชางยนต์
                                            งานปรับแตงเครื่องยนต์
                                            งานประดับยนต์
                                            งานบริการรถยนต์
                                        </p>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology2.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology3.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology4.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>นักเรียนมัธยมศึกษาปีที่ 6</p>
                                        <br />
                                        <p>นักเรียนที่สำเร็จการศึกษาประกาศนียบัตรวิชาชีพ ชั้นปีที่ 3 สาขาช่างยนต์
                                            หรือสาขาที่เกี่ยวข้อง</p>
                                        <br />

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology8.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology7.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology6.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โครงสร้างหลักสูตร</h4>

                                        <p>ผู้สำเร็จการศึกษาตามหลักสูตรประกาศนียบัตรวิชาชีพชั้นสูงพุทธศักราช2557ประเภทวิชา
                                            อุตสาหกรรมสาขาวิชาเทคนิคเครื่องกลจะต้องศึกษารายวิชาจากหมวดวิชาต่างๆ
                                            รวมไม่น้อยกว่า 83 หน่วยกิต
                                            และเข้าร่วมกจกรรมเสริมหลักสูตรดังโครงสร้างต่อไปนี้</p>
                                        <h4 class="text-primary mb-2">1. หมวดวิชาทักษะชีวิต ไม่น้อยกว่า 21 หน่วยกิต</h4>
                                        <ul>
                                            <li>1.1 กลุ่มทักษะภาษาและการสื่อสาร (ไม่น้อยกว่า 9 หน่วยกิต)
                                            </li>
                                            <li>1.2 กลุ่มทักษะการคิดและการแก้ปัญหา (ไม่น้อยกว่า 6 หน่วยกิต)
                                            </li>
                                            <li>กลุ่มทักษะทางสังคมและการดำรงชีวิต (ไม่น้อยกว่า 6 หน่วยกิต)
                                            </li>
                                        </ul>
                                        <h4 class="text-primary mb-2">2. หมวดวิชาทักษะวิชาชีพไม่น้อยกว่า 56 หน่วยกิต
                                        </h4>
                                        <ul>
                                            <li>2.1 กลุ่มทักษะวิชาชีพพื้นฐาน (15หน่วยกิต) </li>
                                            <li>2.2 กลุ่มทักษะวิชาชีพเฉพาะ (21หน่วยกิต)
                                            </li>
                                            <li>2.3 กลุ่มทักษะวิชาชีพเลือก (ไม่น้อยกว่า 12 หน่วยกิต)</li>
                                            <li>2.4 ฝึกประสบการณ์ทักษะวิชาชีพ (4 หน่วยกิต)</li>
                                            <li>2.5 โครงการพัฒนาทักษะวิชาชีพ (4 หน่วยกิต)</li>
                                        </ul>
                                        <h4 class="text-primary mb-2">3. หมวดวิชาเลือกเสรีไม่น้อยกว่า 6 หน่วยกิต
                                        </h4>
                                        <h4 class="text-primary mb-2">4. กิจกรรมเสริมหลักสูตร(2 ชั่วโมงต่่อสัปดาห์) รวม
                                            ไม่น้อยกว่า 83 หน่วยกิต
                                        </h4>

                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology1.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology2.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology3.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขานี้เรียนอะไร</h4>
                                        <p class="">เมื่อเรียนสาขานี้แล้วสามารถทำงานเกี่ยวกับงานเครื่องยนต์เล็ก
                                            งานจักรยานยนต์ งานอิเล็กทรอนิกส์รถยนต์เบืองต้น
                                            งานระบบฉีดเชื้อเพลิงควบคุมด้วยอิเล็กทรอนิกส์ งานปรับอากาศรถยนต์
                                            งานบำรุงรักษารถยนต์ คณิตศาสตร์ช่างยนต์ งานเครื่องมือกลช่างยนต์
                                            งานปรับแต่งเครื่องยนต์ งานประดับยนต์งานบริการรถยนต์ </p>


                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology4.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology5.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology6.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>

                                        <p>เมื่อนักศึกษาสำเร็จการศึกษาแล้วสามรถไปทำงานที่สถานประกอบการยานยนต์ชั้นนำ เช่น
                                            BMW งานซ่อมระบบควบคุมอิเล็กทรอนิกส์ งานซ่อมเครื่องยนต์ดีเซลและแก๊สโซลีน
                                            อีกทั้งประกอบอาชีพเกี่ยวกับงาน เครื่องกล เครื่องยนต์ ในสาขาที่เกี่ยวข้องได้
                                            ซึ่งนักศึกษาสามารถนำความรู้ที่ได้ไปใช้ในการทำงานได้</p>
                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology2.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology7.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology8.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>