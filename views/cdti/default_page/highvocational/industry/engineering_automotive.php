<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาผลิตชิ้นส่วนยานยนต์</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาผลิตชิ้นส่วนยานยนต์ <br /> <small>B.Eng. (Automotive Part
                                Manufacturing Engineering)</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent"> ศึกษาหลักการและกระบวนการทำงานในกลุ่มงานพื้นฐานด้านอุตสาหกรรมการผลิต
                            เพื่อให้สามารถสร้างสรรค์ แก้ปัญหา
                            และนำเทคโนโลยีมาประยุกต์ใช้งานโดยใช้โปรแกรมควบคุมแบบอัตโนมัติในการพัฒนาอุตสาหกรร
                            มการผลิตเครื่องมือกล แม่พิมพ์โลหะ แม่พิมพ์พลาสติก และชิ้นส่วนยานยนต์ รวมทั้งตรวจซ่อม
                            และบำรุงรักษาอุปกรณ์ เครื่องมือกล แม่พิมพ์โลหะ แม่พิมพ์พลาสติก และชิ้นส่วนยานยนต์
                            เพื่อให้สามารถปฏิบัติงานด้านช่างเทคนิคการผลิตในสถานประกอบการ และประกอบอาชีพอิสระ
                            รวมทั้งการใช้ความรู้และทักษะเป็นพื้นฐานในการศึกษาต่อในระดับสูงขึ้นได้
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts4.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร ?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ ?</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขาวิชาเทคนิคการผลิต</h4>
                                        <p>เป็นสาขาที่ได้เรียนรู้เทคโนโลยีที่ทันสมัย มีการพัฒนาอย่างต่อเนื่อง
                                            เพื่อรองรับเทคโนโลยีในอนาคต อุปกรณ์ในสาขามีได้รับการสนับสนุน
                                            จากสถานประกอบการทางด้านเทคโนโลยีด้านการผลิตชิ้นส่วนยานยนต์ชั้นนำของประเทศ
                                            จึงได้ศึกษาเรียนรู้กับอุปกรณ์และเครื่องมือที่ทันสมัย และ
                                            ได้เข้าฝึกทักษะวิชาชีพ
                                            จากสถานประกอบการที่มีคุณภาพ และ สามารถสร้างโอกาสในการมีอาชีพ
                                            กับหน่วยงานที่มีมาตรฐาน และความร่วมมือจัดการอาชีวศึกษาระบบทวิภาคี
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts3.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts2.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts8.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ระดับ ปวส. รับนักเรียนที่เรียนจบ ระดับ ปวช. ชั้นปีที่ 3
                                            จากสาขาวิชาช่างกลโรงงาน
                                            สาขาวิชาช่างยนต์ เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.75</p>
                                        <br />


                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts4.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts6.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts1.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>

                                        <p>---</p>
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts6.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts9.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts10.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขานี้เรียนอะไร?</h4>
                                        <p>ศึกษาการนำเทคโนโลยีมาใช้ในการพัฒนาอุตสาหกรรมการผลิตเครื่องมือกล แม่พิมพ์โลหะ
                                            แม่พิมพ์พลาสติก และชิ้นส่วนยานยนต์ หลักความปลอดภัยและอาชีวอนามัย
                                            ประยุกต์ใช้เทคโนโลยีคอมพิวเตอร์และสารสนเทศเพื่อพัฒนางานอาชีพ อ่านแบบ
                                            เขียนแบบ
                                            และวิเคราะห์แบบงาน เลือกใช้ วัสดุอุตสาหกรรมตามคุณลักษณะงาน ปรับ
                                            แปรรูปและขึ้นรูปงานด้วยเครื่องมือกล เขียนโปรแกรมเอ็นซี
                                            ตรวจสอบชิ้นงานด้วยเครื่องมือวัดที่มีความละเอียดสูง ปรับปรุง
                                            ทดสอบคุณสมบัติโลหะ
                                            ตรวจสอบ และวางแผนการซ่อมบำรุงเครื่องมือกล
                                        </p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts4.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts5.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts7.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p>เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ
                                            ในหลายด้านเช่น
                                        </p>

                                        <ul>
                                            <li>ผู้ประกอบการใช้แม่พิมพ์ผลิตชิ้นส่วนยานยนต์ ทำงานเกี่ยวกับ
                                                การดูแลซ่อมบำรุง
                                                ติดตั้งเครื่องมือ เครื่องจักรอัตโนมัติ</li>
                                            <li>เจ้าหน้าที่ควบคุมการผลิตในโรงงานอุตสาหกรรม</li>
                                            <li>ผู้ให้บริการ (Service provider) อ่านแบบ เขียนแบบ
                                                และวิเคราะห์แบบงานชิ้นส่วนยานยนต์</li>
                                            <li>พนักงานรัฐและเอกชน</li>
                                            <li>ผู้ผลิต (Manufacturer &amp; HW, SW supplier) ผลิตชิ้นส่วนยานยนต์</li>
                                            <li>เจ้าของโครงการ (Integrator) ผลิตชิ้นส่วนยานยนต์ ด้วยเครื่องมือกล
                                                และเครื่องมือกลซีเอ็นซี</li>
                                            <li>Consult ตรวจสอบ ถอดและประกอบชิ้นส่วนแม่พิมพ์</li>
                                            <li>นักวิจัย งานด้านวิจัยพัฒนา ผลิตภัณฑ์ด้านเทคโนโลยีสมัยใหม่
                                                เพื่อรองรับความต้องการของโลกในอนาคต</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>
                                        </ul>
                                        <!-- <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
										<div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts3.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts8.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts10.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>