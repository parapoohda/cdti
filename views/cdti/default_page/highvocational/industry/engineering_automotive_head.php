<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/Autoparts4.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาผลิตชิ้นส่วนยานยนต์</h3>
        <h3 class="font-light color-primary">Automotive Part Manufacturing Engineering</h3>
        <hr class="hr-info">
        <p class="text-dark"> ศึกษาหลักการและกระบวนการทำงานในกลุ่มงานพื้นฐานด้านอุตสาหกรรมการผลิต
เพื่อให้สามารถสร้างสรรค์ แก้ปัญหา
และนำเทคโนโลยีมาประยุกต์ใช้งานโดยใช้โปรแกรมควบคุมแบบอัตโนมัติในการพัฒนาอุตสาหกรร
มการผลิตเครื่องมือกล แม่พิมพ์โลหะ แม่พิมพ์พลาสติก และชิ้นส่วนยานยนต์ รวมทั้งตรวจซ่อม
และบำรุงรักษาอุปกรณ์ เครื่องมือกล แม่พิมพ์โลหะ แม่พิมพ์พลาสติก และชิ้นส่วนยานยนต์
เพื่อให้สามารถปฏิบัติงานด้านช่างเทคนิคการผลิตในสถานประกอบการ และประกอบอาชีพอิสระ
รวมทั้งการใช้ความรู้และทักษะเป็นพื้นฐานในการศึกษาต่อในระดับสูงขึ้นได้
        </p>
    </div>