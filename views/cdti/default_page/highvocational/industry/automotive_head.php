<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/industry/technology5.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาเทคนิคยานยนต์</h3>
        <h3 class="font-light color-primary">Mechanical Power Technology</h3>
        <hr class="hr-info">
        <p class="text-dark">เมื่อสำเร็จการศึกษาจากสาขาช่างยนต์ จะสามารถตัดสินใจ
วางแผนและแก้ไขปัญหาในงานอาชีพด้านยานยนต์ที่ไม่อยู่ภายใต้การควบคุมในบางเรื่อง ประยุกต์ใช้ความรู้
ทักษะทางวิชาชีพเทคโนโลยีสารสนเทศและการสื่อสารในการแก้ปัญหาและการปฏิบัติงานด้านยานยนต์ให้คำแ
นะนำพื้นฐานที่ต้องใช้การตัดสินใจและการปฏิบัติงานแก่ผู้ร่วมงาน
ซ่อมและบำรุงรักษาเครื่องยนต์เล็กแก๊สโซลีนและดีเซลตามคู่มือ ตรวจสอบข้อขัดข้อง
บำรุงรักษาระบบฉีดเชื้อเพลิงด้วยอิเล็กทรอนิกส์ตามคู่มือ บริการระบบปรับอากาศรถยนต์
สามารถซ่อมและบำรุงรักษารถยนต์ตามคู่มือ</p>
    </div>