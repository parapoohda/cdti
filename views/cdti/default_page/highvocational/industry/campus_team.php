 <section class="campus-box  mini-spacer p-b-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url(); ?>cdti_assets/images/seminar.png" alt=""> บุคลากร</h3>
						</div>
						<div class="col-sm-8 text-right">
							<ol class="breadcrumb">
			                   <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
			                    <li class="breadcrumb-item active">บุคลากร</li>
			                </ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php if($teams): $i = 0;?>
  <section class="teamlist ">
        <div class="container ">
          <?php foreach ($teams as $key => $value):?>
                <?php if($i !=  $value->gallery_level)
                      {
                          if($i != 0)echo '</div>';
                          $i = $value->gallery_level;
                          echo '<div class="row justify-content-md-center">';
                      }?>
          			<div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 mb-3">
            				<div class="team text-center">
            					<img src="<?php echo base_url().$value->path_big; ?>" alt="<?php echo html_escape($value->title); ?>" class="w-90-per d-block mx-auto mb-2">
            					<p class="team-name mb-0 color-darkgray"><?php echo html_escape($value->title); ?></p>
            					<p class="color-3"><small><?php echo html_escape($value->description); ?></small></p>
            				</div>
          			</div>
           <?php endforeach;?>
    </div>
  </section>
<?php endif;?>
