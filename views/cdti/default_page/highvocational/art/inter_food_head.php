<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/20.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขางานเชฟอาหารนานาชาติ  </h3>
        <h3 class="font-light color-primary">International Chef Course</h3>
        <hr class="hr-info">
        <p class="text-dark"> ปัจจุบันวัฒนธรรมด้านอาหาร ได้รับการตอบรับจากบุคคลทั่วไปมีการแลกเปลี่ยนเรียนรู้ซึ่งกันและกัน  โดยอาหารแต่ละชนชาตินั้นมีความแตกต่างกัน ในส่วนของวัตถุดิบ วิธีประกอบอาหาร รวมทั้งศิลปะในการตกแต่งอาหารที่หลากหลายและทันสมัย ทำให้การประกอบอาหารนานาชาติเป็นที่ยอมรับและเป็นที่น่าสนใจมากขึ้น</p>
      </div>
