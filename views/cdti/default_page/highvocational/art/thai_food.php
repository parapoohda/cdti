<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขางานเชฟอาหารไทย</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขางานเชฟอาหารไทย<br /> <small>Thai Chef </small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">ศึกษาเกี่ยวกับอาหารไทย ที่เป็นที่นิยม
                            และควรค่าแก่การอนุรักษ์รวบรวมตำรับอาหารไทยสูตรดั้งเดิมจากห้องเครื่องต้นและตำรับสูตรราชสกุลต่างๆ
                            นักศึกษาได้ฝึกฝนลงมือปฏิบัติจริง จากผู้เชี่ยวชาญเจ้าของสูตรต้นตำรับ</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/news/25.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร ?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ ?</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">สาขางานเชฟอาหารไทย</h4>
                                        <p>ได้เรียนรู้ตำรับอาหารไทยจากผู้เชี่ยวชาญ จากห้องเครื่องในราชสำนัก และราชสกุล
                                            และฝึกงานในสถานประกอบการที่มีชื่อเสียงด้านอาหารไทยและมีการทดสอบมาตรฐานฝีมือแรงงานด้วยทุกคน
                                        </p>
                                        <br />
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ระดับ ปวส. รับนักเรียนที่เรียนจบ ระดับ ปวช. ชั้นปีที่ 3
                                            จากสาขาวิชาอาหารและโภชนาการ เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.50</p>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">ปริญญาและสาขาวิชา</h4>
                                        <p>รอข้อมูล</p>

                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชา เชฟอาหารไทย เรียนอะไร</h4>
                                        <p>การประกอบอาหารไทย ครัวและเครื่องใช้ในงานอาหาร การประกอบอาหารไทยประจำภาค
                                            การทำขนมไทยในวรรณคดี การนำเสนอเพื่องานอาหาร การประกอบอาหารไทยโบราณ
                                            การจัดการธุรกิจอาหารไทย ศิลปะการออกแบบและตกแต่งอาหารไทย
                                            การประกอบอาหารเชฟมืออาชีพ และธุรกิจการจัดเลี้ยงอาหารไทย</p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/high_vocational/economic/Chefthai5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โอกาศทางธุรกิจหลังสำเร็จการศึกษา</h4>
                                        <p> เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ
                                            ในหลายด้านเช่น </p>
                                        <ul>
                                            <li>เชฟอาหารไทยในโรงแรม / ร้านอาหารทั้งในประเทศและต่างประเทศ</li>
                                            <li>เจ้าของธุรกิจร้านอาหารทั้งในประเทศและต่างประเทศ</li>
                                            <li>ครูสอนทำอาหารมืออาชีพ</li>
                                            <li>ที่ปรึกษาด้านอาหารในอุตสาหกรรมอาหาร</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>
                                        </ul>
                                        
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/news/17.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/news/16.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/news/19.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>