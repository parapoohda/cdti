<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/15.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาอุตสาหกรรมอาหาร</h3>
        <h3 class="font-light color-primary">Food Industry</h3>
        <hr class="hr-info">
        <p class="text-dark"> ปัจจุบันประเทศไทยได้กำหนดแผนเศรษฐกิจในหัวข้อ “ไทยแลนด์ 4.0” ซึ่งทำให้อุตสาหกรรมทางด้านอาหารมีการเจริญเติบโตขึ้นอย่างรวดเร็วโดยเฉพาะ ผลิตภัณฑ์อาหารแปรรูป เช่นอาหารทะเลแช่แข็ง ผัก-ผลไม้อบแห้งรวมทั้งเทคโนโลยีหรือนวัตกรรมใหม่ๆ เข้ามาใช้ในอุตสาหกรรมการผลิตอาหารให้ได้ตามมาตรฐานการส่งออกของประเทศต่างๆ สามารถนำความรู้ที่ได้มาปรับใช้ในการพัฒนาผลิตภัณฑ์อาหาร หรือการทำธุรกิจขนาดเล็ก ออกสู่ตลาดต่อไป</p>
      </div>
