<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/24.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาอาหารและโภชนาการ</h3>
        <h3 class="font-light color-primary">Food and Nutrition Course</h3>
        <hr class="hr-info">
        <p class="text-dark">เป็นเชฟมืออาชีพ ผู้ที่มีความรู้ ความสามารถการประกอบอาหารตำรับมาตรฐานนานาชาติ อาหารไทย  อาหารว่าง  ผลิตภัณฑ์เบเกอรี่  การสุขาภิบาล และความปลอดภัย ในงานอาหารนานาชาติ  ศิลปะการออกแบบและตกแต่งอาหารนานาชาติ  นักธุรกิจการจัดเลี้ยงอาหารนานาชาติ นักจัดรายการโทรทัศน์  เชฟในโรงแรมชั้นนำต่างๆ  เชฟร้านอาหาร  เชฟในโรงแรม เป็นต้น  สามารถทำงาน         ในหน่วยงานภาครัฐ และเอกชน ทั้งยังประกอบอาชีพธุรกิจส่วนตัวได้หลากหลาย และสามารถศึกษาในระดับสูงต่อไปได้
        </p>
      </div>
