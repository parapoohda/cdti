<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>" >
    <img src="../../cdti_assets/images/news/25.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขางานเชฟอาหารไทย</h3>
        <h3 class="font-light color-primary">Thai Chef </h3>
        <hr class="hr-info">
        <p class="text-dark"> ศึกษาเกี่ยวกับอาหารไทย ที่เป็นที่นิยม และควรค่าแก่การอนุรักษ์รวบรวมตำรับอาหารไทยสูตรดั้งเดิมจากห้องเครื่องต้นและตำรับสูตรราชสกุลต่างๆ นักศึกษาได้ฝึกฝนลงมือปฏิบัติจริง จากผู้เชี่ยวชาญเจ้าของสูตรต้นตำรับ</p>
      </div>
