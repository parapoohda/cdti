<section class="campus-box  mini-spacer p-b-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url(); ?>cdti_assets/images/seminar.png" alt=""> บุคลากร</h3>
						</div>
						<div class="col-sm-8 text-right">
							<ol class="breadcrumb">
			                   <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
			                    <li class="breadcrumb-item active">บุคลากร</li>
			                </ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if($file_path) : ?>
		<div class="container">
			<iframe width="100%" height="600" src="cdti_assets/pdf.js-gh-pages/web/viewer.html?file=<?php echo $file_path;?>"></iframe>

			<div class="row justify-content-center">
				<a class="font-weight-bold" href="cdti_assets/pdf.js-gh-pages/web/viewer.html?file=<?php echo $file_path;?>">>>ดาวน์โหลดเอกสาร/เปิดเอกสาร<<</a>
			</div>
		</div>
	<?php endif; ?>
	</div>
</section>

