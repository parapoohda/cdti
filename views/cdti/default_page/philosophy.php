<div class="static-slider10" style="background-image:url(cdti_assets/images/banner_phiosophy.png)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="col-md-6">
            	<img src="cdti_assets/images/about-logo.png" alt="" class="img-repsonsive img-fluid">
            </div>
            <div class="col-md-6 align-self-center text-center aos-init" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">เกี่ยวกับสถาบัน</h1>
            </div>
            <!-- Column -->

        </div>
    </div>
</div>
<section class="about_content">
	<div class="container">
		<div class="row spacer">
			<div class="col-sm-12">
				<h1 class="color-primary  text-center">“ใต้ร่มพระบารมี.. จากวันนั้นถึงวันนี้... ๖๐ ปี จิตรลดา“</h1>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-sm-12">
				<h3 class="color-primary mb-4">ปรัชญา</h3>
				<p class="text-dark text-indent">รักชาติ ศาสนา พระมหากษัตริย์ มีวินัย ใฝ่คุณธรรม นำความรู้ สู้งานหนัก</p>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-sm-12">
				<h3 class="color-primary mb-4">วิสัยทัศน์</h3>
				<p class="text-dark text-indent">สถาบันที่จัดการศึกษาตามแนวพระราชดำริ "เรียนคู่งาน งานคู่เรียน" สร้างคนดี มีฝีมือ มีวินัยและจรรยาบรรณวิชาชีพ</p>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-sm-12">
				<h3 class="color-primary mb-4">พันธกิจ</h3>

				<p class="text-dark mb-0 font-16"> - สอนในระดับประกาศนียบัตรวิชาชีพและปริญญาตรีแบบ "เรียนคู่งาน งานคู่เรียน"</p>
				<p class="text-dark mb-0 font-16"> - วิจัยและพัฒนาเทคโนโลยีและนวัตกรรมด้านวิชาชีพ</p>
				<p class="text-dark mb-0 font-16"> - บริการวิชาการและวิชาชีพแก่ชุมชนและสังคม</p>
				<p class="text-dark mb-0 font-16"> - อนุรักษ์และส่งเสริมศิลปวัฒนธรรมไทย และเข้าใจวัฒนธรรมที่หลากหลาย</p>
			</div>
		</div>
		
		<div class="row mini-spacer">
		
		
		<div class="row mini-spacer">
			<div class="col-sm-12">
				<h1 class="color-primary  text-center">สัญลักษณ์ สถาบันเทคโนโลยีจิตรลดา</h1>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-md-12  mb-4">
				
				<h3 class="color-primary">ตราประจำสถาบัน</h3>
			</div>
			<div class="col-md-5">
				<img src="cdti_assets/images/about/logo.png" alt="" class="img-responsive img-fluid">
			</div>
			<div class="col-md-7 ">
				<h5 class="color-primary mb-2">“พระมหามงกุฎประจำรัชกาลที่ ๙” ตราสัญลักษณ์โรงเรียนจิตรลดา</h5>
				<p class="text-dark">นอกจากชื่อของโรงเรียนแล้ว การจดทะเบียนเป็นโรงเรียนจิตรลดาในครั้งนั้น ก็จะต้องมีตรา สัญลักษณ์ของโรงเรียน ซึ่ง <span class="color-primary">“ครูทัศนีย์”</span> ท่านเล่าว่า “ ตราของโรงเรียนที่ใช้ เป็นตรามงกุฎที่มีเลข ๙ นั้น หมายถึงพระมหามงกุฎของรัชกาลที่ ๙ เป็นสัญลักษณ์ของพระบาทสมเด็จพระเจ้าอยู่หัวที่อยู่ติดตัว นักเรียน ส่วนสัญลักษณ์ตัวอักษร โรงเรียนจิตรลดา ที่อยู่ใต้มงกุฎนั้น พระบาทสมเด็จพระเจ้าอยู่หัว โปรดเกล้าฯ ให้ครูประพาส ปานพิพัฒน์ ครูสอนศิลปะของโรงเรียนในขณะนั้นเป็นผู้ออกแบบถวาย และพระองค์ท่านทรงเลือกตัวอักษรที่มีส่วนโค้งขึ้น รับสั่งว่าหมายความถึง โรงเรียนจิตรลดาจะไปสู่ ความเจริญรุ่งเรืองต่อไป เพราะฉะนั้นเมื่อเราเข้ามาในโรงเรียนนี้ เราจะต้องพยายามรักษาคุณงามความดีของโรงเรียนให้เจริญก้าวหน้าต่อไปในทุกด้าน”</p>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-md-12 mb-4">
				<h3 class="color-primary ">สีประจำสถาบัน </h3>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col-6"><img src="cdti_assets/images/about/color_yellow.png" alt="" class="img-responsive img-fluid"></div>
					<div class="col-6"><img src="cdti_assets/images/about/color_sky.png" alt="" class="img-responsive img-fluid"></div>
				</div>
			</div>
			<div class="col-md-7">
				<p class="text-dark ">สี <span class="text-yellow"><strong>เหลือง</strong></span> - <span class="text-sky"><strong>ฟ้า</strong></span> หมายถึง <span class="text-yellow"><strong>สีวันพระบรมราชสมภพของพระบาทสมเด็จพระบ&#65279ร&#65279ม&#65279ช&#65279น&#65279ก&#65279า&#65279ธิ&#65279เ&#65279บ&#65279ศ&#65279ร ม&#65279ห&#65279า&#65279ภู&#65279มิ&#65279พ&#65279ล&#65279อ&#65279ดุ&#65279ล&#65279ย&#65279เ&#65279ด&#65279ชม&#65279ห&#65279า&#65279ร&#65279า&#65279ช บ&#65279ร&#65279ม&#65279น&#65279า&#65279ถ&#65279บ&#65279พิ&#65279ต&#65279ร คือวันจันทร์</strong></span> และ<span class="text-sky"><strong>สีวันพระราชสมภพของส&#65279ม&#65279เ&#65279ด็&#65279จ&#65279พ&#65279ร&#65279ะ&#65279น&#65279า&#65279ง&#65279เ&#65279จ้&#65279า&#65279สิ&#65279ริ&#65279กิ&#65279ติ์ 
พ&#65279ร&#65279ะ&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ชิ&#65279นี&#65279น&#65279า&#65279ถ 
พ&#65279ร&#65279ะ&#65279บ&#65279ร&#65279ม&#65279ร&#65279า&#65279ช&#65279ช&#65279น&#65279นี&#65279พั&#65279น&#65279ปี&#65279ห&#65279ล&#65279ว&#65279ง คือวันศุกร์</strong></span> ทั้งสองพระองค์ คือ ผู้ทรงก่อตั้งโรงเรียนจิตรลดา</p>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-md-12 mb-4">
				<h3 class="color-primary mb-2">สีประจำคณะสถาบันเทคโนโลยีจิตรลดา / โรงเรียนจิตรลดาวิชาชีพ</h3>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col">
						<img src="cdti_assets/images/about/color_blue.png" alt="" class="img-responsive img-fluid">
					</div>
					<div class="col">
						<img src="cdti_assets/images/about/color_red.png" alt="" class="img-responsive img-fluid">
					</div>
					<div class="col">
						<img src="cdti_assets/images/about/color_orange.png" alt="" class="img-responsive img-fluid">
					</div>
					<div class="col">
						<img src="cdti_assets/images/about/color_purple.png" alt="" class="img-responsive img-fluid">
					</div>

					
					
				</div>
			</div>
			<div class="col-md-7 ">
				<p class="text-dark mb-0 font-16">คณะบริหารธุรกิจ : <span class="text-info-dark">สีฟ้าหม่น</span>  </p>
				<p class="text-dark mb-0 font-16">คณะเทคโนโลยีอุตสาหกรรม : <span class="text-red-dark">สีเเดงเลือดหมู</span></p>
				<p class="text-dark mb-0 font-16">คณะเทคโนโลยีดิจิทัล : <span class="text-orange">สีส้ม</span></p>
				<p class="text-dark mb-0 font-16">ระดับประกาศนียบัตรวิชาชีพ (ปวช.) และระดับประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.) : <span class="text-purple">สีม่วง</span></p>
			</div>
		</div>
		<div class="row mini-spacer">
			<div class="col-md-12 mb-4">
				<h3 class="color-primary mb-2">ต้นไม้ประจำสถาบัน</h3>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col">
						<img src="cdti_assets/images/about/img_flower_1.png" alt="" class="img-responsive img-fluid">
					</div>
				</div>
			</div>
			<div class="col-md-7 ">
				<p class="text-dark mb-0 font-16"><span class="color-primary">ต้นราชพฤกษ์</span> หรือ ต้นคูน เป็นต้นไม้พื้นเมืองของเอเชียใต้ ตั้งแต่ปากีสถาน อินเดีย พม่า และศรีลังกา โดยนิยมปลูกกันมากในเขตร้อน สามารถเจริญเติบโตได้ดีในที่โล่งแจ้ง และเป็นที่รู้จักในประเทศไทยมาหลายสิบปี โดยมีการเสนอให้ดอกราชพฤกษ์ เป็นดอกไม้ประจำชาติไทยตั้งแต่ปี พ.ศ. 2506 แต่ก็ยังไม่ได้ข้อสรุปแน่ชัด จนกระทั่งมีการลงนามให้เป็นดอกไม้ประจำชาติไทย เมื่อวันที่ 26 ตุลาคม พ.ศ. 2544</p>
			</div>
		</div>
		
		<div class="row mini-spacer">
			<div class="col-md-12 mb-4">
				
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col">
						<img src="cdti_assets/images/about/img_flower_2.jpg" alt="" class="img-responsive img-fluid">
					</div>
				</div>
			</div>
			<div class="col-md-7 ">
				<p class="text-dark mb-0 font-16">เนื่องจาก ต้นราชพฤกษ์ ออกดอกสีเหลืองชูช่อ ดูสง่างาม อีกทั้งยังมีสีตรงกับ สีประจำวันพระราชสมภพของพระบาทสมเด็จพระบ&#65279ร&#65279ม&#65279ช&#65279น&#65279ก&#65279า&#65279ธิ&#65279เ&#65279บ&#65279ศ&#65279ร ม&#65279ห&#65279า&#65279ภู&#65279มิ&#65279พ&#65279ล&#65279อ&#65279ดุ&#65279ล&#65279ย&#65279เ&#65279ด&#65279ชม&#65279ห&#65279า&#65279ร&#65279า&#65279ช บ&#65279ร&#65279ม&#65279น&#65279า&#65279ถ&#65279บ&#65279พิ&#65279ต&#65279ร จึงถูกตั้งชื่อว่าเป็น "ต้นไม้ของรัชกาลที่ 9" และมีการลงนามให้ต้นราชพฤกษ์ เป็นหนึ่งใน 3 สัญลักษณ์ประจำชาติไทย โดยมี 
				</p>
				<ol class="mb-2 text-dark">
					<li>ช้าง เป็นสัตว์ประจำชาติไทย </li>
					<li>ศาลาไทย เป็นสถาปัตยกรรมประจำชาติไทย  </li>
					<li>ดอกราชพฤกษ์ เป็นดอกไม้ประจำชาติไทย</li>
				</ol>
				<p class="text-dark">เหตุผลที่เลือกเป็นดอกไม้ประจำชาติไทย </p>
				<ol class="text-dark">
					<li>เนื่องจากเป็นต้นไม้พื้นเมืองที่รู้จักกันอย่างแพร่หลาย และมีอยู่ทุกภาคของประเทศไทย</li>
					<li>มีประวัติเกี่ยวข้องกับประเพณีสำคัญ ๆ ในไทยและเป็นต้นไม้มงคลที่นิยมปลูก</li>
					<li>ใช้ประโยชน์ได้หลากหลาย เช่น ใช้เป็นยารักษาโรค อีกทั้งยังใช้ลำต้นเป็นเสาเรือนได้ เป็นต้น</li>
					<li>มีสีเหลืองอร่าม พุ่มงามเต็มต้น เปรียบเป็นสัญลักษณ์แห่งพุทธศาสนา</li>
					<li>มีอายุยืนนาน และทนทาน</li>
				</ol>
			</div>
		</div>
			
		<div class="col-sm-12">
				<h3 class="color-primary mb-4">อำนาจหน้าที่และภารกิจตามพระราชบัญญัติสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2561</h3>
				<p class="text-dark text-indent">
				สถาบันเทคโนโลยีจิตรลดา เป็นสถานศึกษาด้านวิชาชีพและเทคโนโลยี มีวัตถุประสงค์ในการส่งเสริมวิชาการและวิชาชีพชั้นสูง มุ่งเน้นให้การศึกษาด้านทักษะวิชาชีพและเทคโนโลยีบนฐานของวิทยาศาสตร์และวิชาอื่นที่เกี่ยวข้อง เพื่อสร้างความชำนาญในการปฏิบัติ ทำการสอน วิจัย ริเริ่ม ถ่ายทอด ฝึกอบรม พัฒนาบุคลากร พัฒนาเทคโนโลยีและนวัตกรรม โดยให้ผู้เข้ารับการศึกษามีโอกาสศึกษาด้านวิชาชีพเฉพาะทางจนถึงระดับอุดมศึกษา ให้บริการทางวิชาการแก่สังคม ทะนุบำรุงศิลปวัฒนธรรม ปลูกฝังให้นักเรียนและนักศึกษาเป็นพลเมืองดี มีจิตอาสา และมีวินัย <br>
				(อ้างอิงตามมาตรา 6 แห่งพระราชบัญญัติสถาบันเทคโนโลยีจิตรลดา พ.ศ. 2561)</p>
				
				
			</div>
		</div>
		
		
	</div>
	
</section>