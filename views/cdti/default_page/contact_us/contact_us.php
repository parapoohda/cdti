<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

.margin{
	margin-top:20px;
	margin-bottom: 50px;
}
/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */
p.form_text1>span {
  display: inline-block;
  min-width: 100px;
  margin-bottom : 0px !important;
}
p.form_text2>span {
  display: inline-block;
  min-width: 60px;
  margin-bottom : 0px !important;
}
p.form_text1>space {
  display: inline-block;
  min-width: 30px;
  margin-bottom : 0px ;
}
.no-wrap-top{
	vertical-align: top;
  white-space: nowrap;
}

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-12">
			<div class="section-header">
                <h3 class="mr-auto d-inline text-primary"> สถานที่ตั้ง</h3>
				<p class="text-dark">สถาบันเทคโนโลยีจิตรลดา อาคาร 60 พรรษาราชสุดาสมภพ 604<br><!--
				-->สำนักพระราชวัง สนามเสือป่า ถนนศรีอยุธยา เขตดุสิต กรุงเทพฯ 10300</p><br>

				<h3 class="mr-auto d-inline  text-primary"> เวลาทำการ</h3>
				<p class="text-dark">ทุกวันจันทร์ - ศุกร์ เวลา 8.30 - 16.30 น.<br><!--
				-->(ยกเว้น วันหยุดนักขัตฤกษ์)</p><br>

				<h3 class="mr-auto d-inline  text-primary"> E-mail</h3>
				<p class="text-dark">office@cdti.ac.th</p><br>

				<h3 class="mr-auto d-inline text-primary"> เบอร์ติดต่อ</h3>
				<p class="text-dark form_text2" style ="margin-bottom: 0;">
				โรงเรียนจิตรลดาวิชาชีพ ระดับปวช., ปวส.<br><!--
				-->
				
				<table class="text-dark form_text1" style="width:100%">
					<tr>
						<th class="no-wrap-top">โทรศัพท์</th>
						<th> : 0-2282-6808 ต่อ 3054<br>
					: อ.จุฬาลักษณ์ 092-225-4808<br>
					: อ.ชาติภักดิ์ 089-814-8218<br></th>
						
					</tr>
				</table>
				<!--
				-->
				
				<p class="text-dark form_text2" >
				สถาบันเทคโนโลยีจิตรลดา ระดับปริญญาตรี<br>
				<span>โทรศัพท์</span> : 0-2280-0551<br>
				<span>โทรสาร</span> : 02-280-0552<br><!--
				-->เบอร์ต่อหน่วยงานภายใน<br><!--
				-->- งานทะเบียนและวัดผล 3296, 3297<br><!--
				-->- งานการเงินและบัญชี 3293<br><!--
				-->- งานบริหารทรัพยากรมนุษย์และนิติการ 3232<br><!--
				-->- งานทุนการศึกษาและบริการนักเรียนนักศึกษา 3305<br><!--
				-->- งานพัสดุ 3307</p><br>

				<h3 class="mr-auto d-inline text-primary"> การเดินทาง</h3>
				<!--<p class="text-dark form_text1"  style="margin-bottom : 0px " ><span>รถประจำทาง</span>: รถเมล์สาย 72, ปอ.503 ลงป้ายวัดเบญจมบพิตร<br>
				<span>BTS	</span>: สถานีพญาไท ทางออก 3 เดินลงมาเลี้ยวซ้ายที่แยกศรีอยุธยา ป้ายรถประจำทาง รร.สันติราษฎร์วิทยาลัย รถเมล์สาย 72, ปอ.503 ลงป้ายวัดเบญจมบพิตรหรือนั่งรถแท็กซี่<br>
				<span>เรือโดยสาร</span>: ท่าเรือเทเวศร์ นั่งวินมอเตอร์ไซค์ หรือนั่งรถแท็กซี่<br>
				<span>อนุสาวรีย์ชัยฯ</span>: เกาะพญาไท ป้ายพงหลี/เซ็นเตอร์วัน รถเมล์สายปอ.503 หรือนั่งรถแท็กซี่</p>
-->
				<table class="text-dark form_text1" style="width:100%">
				<tr>
					<th class="no-wrap-top">รถประจำทาง</th>
					<th  class="center no-wrap-top" width="20px">: </th>
					<th> รถเมล์สาย 72, ปอ.503 ลงป้ายวัดเบญจมบพิตร</th>
					
				</tr>
				<tr height="10px"></tr>
				<tr>
					<td class="no-wrap-top">BTS</td>
					<td class="center no-wrap-top">: </td>
					<td> สถานีพญาไท ทางออก 3 เดินลงมาเลี้ยวซ้ายที่แยกศรีอยุธยา ป้ายรถประจำทาง รร.สันติราษฎร์วิทยาลัย รถเมล์สาย 72, ปอ.503 ลงป้ายวัดเบญจมบพิตรหรือนั่งรถแท็กซี่</td>
				</tr>
				
				<tr height="10px"></tr>
				<tr>
					<td class="no-wrap-top">เรือโดยสาร</td>
					<td class="center no-wrap-top">:</td>
					<td> ท่าเรือเทเวศร์ นั่งวินมอเตอร์ไซค์ หรือนั่งรถแท็กซี่</td>
				</tr>
				
				<tr height="10px"></tr>
				<tr>
					<td class="no-wrap-top">อนุสาวรีย์ชัยฯ</td>
					<td class="center no-wrap-top">: </td>
					<td> เกาะพญาไท ป้ายพงหลี/เซ็นเตอร์วัน รถเมล์สายปอ.503 หรือนั่งรถแท็กซี่</td>
				</tr>
				</table>
                <!-- <a href="" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a> -->
            </div>
			</div>
			
			<div class="col-lg-7 col-md-12">
			<iframe class= "float-right" width="520" height="390" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=สถาบันเทคโนโลยีจิตรลดา+อาคาร+60+พรรษาราชสุดาสมภพ+604+สำนักพระราชวัง+สนามเสือป่า+ถนนศรีอยุธยา+เขตดุสิต+กรุงเทพฯ+10300&key=AIzaSyDXC--VOSECbuWgKwK3t7seCDB5fdIjw4w" allowfullscreen></iframe>
<div class= "float-right"><p></p></div>
<!-- src="https://www.cdti.ac.ths/assets/img/หนัาหลังแผ่นพับ.jpg" -->
<img  class= "margin float-right" width="449" height="340" src="<?php echo base_url(); ?>cdti_assets//images/หนัาหลังแผ่นพับ.jpg" 
alt="แผนที่ภายในสถาบันเทคโนโลยีจิตรลดา">
			</div>
		</div>
	</div>
</section>
<div class= "float-right"><p></p></div>
