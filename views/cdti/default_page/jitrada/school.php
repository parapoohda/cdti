<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?=base_url()?>assets/images/002-newspaper.png" alt=""> <?=$page->title?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="bursary mini-spacer pb-0">
	
</div>