<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt="">37 การดำเนินการเพื่อจัการดำเนินงานเพื่อจัดการความเสี่ยงการทุจริต</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container">

    <div id="accordion" class="panel-group" style="min-height: 300px;">
        <div class="panel" style="padding: 15px;">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyOne" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">ปีงบประมาณ 2562 - 2563</a>
                </h4>
            </div>
            <div id="panelBodyOne" class="panel-collapse collapse in borderone show">
                <div class="panel-body" style="padding-left: 15px;">
                    <p class="pt10-pl30">
                      <div id="sub1">
                        <a href="#panelsub1" class="accordion-toggle" data-toggle="collapse"
                            data-parent="#sub1">- กิจกรรมเพื่อประโยชน์ส่วนรวม</a><br>
                            <div id="panelsub1" class="panel-collapse collapse">
                                <div class="panel-body" style="padding-left: 15px;">
                                  test
                                </div>
                            </div>
                        </div>
                        <a target="_blank" href="//www.cdti.ac.th/ประชุมการประเมินคุณธรรมและความโปร่งใสในการดำเนินงาน">- การประชุมการประเมินคุณธรรมและความโปร่งใสในการดำเนินงานของหน่วยงานภาครัฐ ประจำปีงบประมาณ พ.ศ. 2563</a><br>
                                            </p>
                </div>
            </div>
        </div>
    </div>
</div>
