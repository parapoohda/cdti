
<div class="container">
    <?php $this->load->view('cdti/include/list_tag',['categories' => $categories]);?>
</div>
<script src="<?php echo base_url() ?>cdti_assets/plugin/slick/slick.min.js"></script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="<?php echo base_url(); ?>cdti_assets/js/jquery.touchSwipe.js"></script>
<script>
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    centerPadding: '40px',
    focusOnSelect: true,
    arrows: true,
  });
</script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
#containerIntro h1{

    display: inline-block;
    
  vertical-align: sub;
}
#containerIntro p {
    display: inline-block;
    vertical-align:  sup:
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    line-height: 28px;
}
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campuss-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> การเปิดเผยข้อมูลสาธารณะ (OIT)</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container">
           
    <div id="accordion" class="panel-group" style="padding-bottom: 100px;">
        <?php foreach ($oit->oit as $indexP=>$oit_item): ?>
            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#panel<?php echo $indexP; ?>" class="accordion-toggle collaspe" data-toggle="collapse"
                            data-parent="#accordion"><?php echo $oit_item->title;?></a>
                    </h4>
                </div>
                <div id="panel<?php echo $indexP; ?>" class="panel-collapse in borderone <?php if($indexP!=0){echo 'collapse';}else{
                    echo '';
                }  ?> collapsed" >
                    <div class="panel-body">
                        <div class="pt10-pl30"><?php foreach ($oit_item->children as $indexC1=>$firstLevelChild): ?>
                            <a 
                                href=" <?php if($firstLevelChild->children==null||count ($firstLevelChild->children)==0){ echo $firstLevelChild->link; }else{
                                echo "#panel";
                                echo $indexP;
                                echo "_";
                                echo $indexC1 ;} ?>" 
                                <?php if($firstLevelChild->children==null||count ($firstLevelChild->children)==0){ echo 'target="_blank"'; }else{
                                echo 'class="accordion-toggle" data-toggle="collapse"';} ?>
                                
                                data-parent="#panel<?php echo $indexP?>_<?php echo $indexC1 ?>"><?php echo $firstLevelChild->title;?></a>
                               
                                <br>
                                
                                <?php if($firstLevelChild->children!=null):?>
                                        
                                <div id="panel<?php echo $indexP?>_<?php echo $indexC1 ?>" style="padding-left: 40px;">
                                <div id="panel<?php echo $indexP?>_<?php echo $indexC1 ?>" class="panel-collapse collapse">
                                    <div class="panel-body" style="padding-left: 15px;">
                                    
                                        <?php foreach($firstLevelChild->children as $indexC2=>$secondLevelChild): ?>
                                         <a  href="<?php echo $secondLevelChild->link; ?>" target="_blank" > <?php echo '<div id="containerIntro">'; if ( substr($secondLevelChild->title,0,3)  == '•') {echo '<h1>•</h1>';  echo '<p><strong>';echo substr($secondLevelChild->title,3); echo '</p></strong>';}else{ echo '<p><strong>'; echo $secondLevelChild->title;}echo '</p></strong></div>' ;?>
                                            </a>  
                                        <?php if($secondLevelChild->children!=null):?>
                                            <?php foreach($secondLevelChild->children as $indexC3=>$thirdLevelChild): ?>
                                                <a href="<?php echo $thirdLevelChild->link; ?>" target="_blank">
                                                    <?php echo '<div id="containerIntro" >  '; if ( substr($thirdLevelChild->title,0,3)  == '•') {echo '<h1>•</h1>';  echo '<p>';echo substr($thirdLevelChild->title,3); }else{echo '<p>'; echo $thirdLevelChild->title;}echo '</p></div>'; ?>
                                                </a>
                                            <?php endforeach;?>

                                        <?php  endif; ?>
                                            
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>
                            <?php  endif; ?>
                            
                            <?php endforeach ?>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        </div>
    </div>
<?php include('views/layouts/footer.php'); ?>