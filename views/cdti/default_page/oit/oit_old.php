<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> การเปิดเผยข้อมูลสาธารณะ (OIT)</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container">

    <div id="accordion" class="panel-group" style="padding-bottom: 100px;">
    <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyOne" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">ข้อมูลพื้นฐาน</a>
                </h4>
            </div>
            <div id="panelBodyOne" class="panel-collapse collapse in borderone show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="//www.cdti.ac.th/โครงสร้างสถาบัน">1. โครงสร้างองค์กร</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/ผู้บริหาร">2. ข้อมูลผู้บริหาร</a><br>
                        <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o03.pdf">3. อำนาจหน้าที่</a><br>
                        <a href="#">4. แผนยุทธศาสตร์หรือแผนพัฒนาหน่วยงาน</a><br>
                        <a target="_blank" href="<?=base_url()?>/uploads/files/04_เล่มเเผนยุทธศาตร์สถาบันเทคโนโลยีจิตรลดา_2562_2564.pdf">&emsp;4.1 แผนยุทธศาสตร์ สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2562-2564</a><br>
                        <a target="_blank" href="<?=base_url()?>/uploads/files/แผนยุทธศาสตร์ สถาบันเทคโนโลยีจิตรลดา พ.ศ. 256.pdf">&emsp;4.2 แผนยุทธศาสตร์ สถาบันเทคโนโลยีจิตรลดา พ.ศ. 2562-2564 (ฉบับปรับปรุงตัวชี้วัด)</a><br>
                        <a target="_blank" href="<?=base_url()?>/uploads/files/แผนพัฒนาสถาบันเทคโนโลยีจิตรลดา ระยะ 5 ปี 2565-2.pdf">&emsp;4.3 แผนพัฒนาสถาบันเทคโนโลยีจิตรลดา ระยะ 5 ปี 2565-2569</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/office/สำนักงานสถาบันฯ/contact">5. ข้อมูลติดต่อหน่วยงาน</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/พรบ">6. กฎหมายที่เกี่ยวข้อง</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/featured-posts">7. ข่าวประชาสัมพันธ์ทั่วไป</a><br>
                        <a target="_blank" href="//www.cdti.ac.th/webboard/">8. Q&A</a><br>
                        <a href="#panelsub9" class="accordion-toggle" data-toggle="collapse"
                            data-parent="#sub9">9. Social Network</a><br>
                            <div id="sub9" style="padding-left: 40px;">
                            <div id="panelsub9" class="panel-collapse collapse">
                                <div class="panel-body" style="padding-left: 15px;">
                                  <ul>
                                      <li>
                                          <a target="_blank" href="//www.facebook.com/ChitraladaTechnologyInstitute">Facebook</a>
                                      </li>
                                      <li>
                                          <a target="_blank" href="//twitter.com/cdtiofficial">twitter</a>
                                      </li>
                                      <li>
                                          <a target="_blank" href="//instagram.com/cdtiofficial/">Instagram</a>
                                      </li>
                                      <li>
                                            <a target="_blank" href="//www.youtube.com/channel/UCIAJFdu3ilQF9TJGbbWqP5w">youtube</a>
                                      </li>
                                  </ul>
                                </div>
                            </div>
                        </div>
                    </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">แผนการดำเนินงาน</a>
                </h4>
            </div>
            <div id="panelBodyTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o10.pdf">10. แผนดำเนินงานประจำปี</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o11.pdf">11. รายงานการกำกับติดตาม การดำเนินงาน รอบ 6 เดือน</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/AnnualReport/AnnualReport_2562.pdf">12. รายงานผลการดำเนินงานประจำปี</a><br>
                    </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-3" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การปฏิบัติงาน</a>
                </h4>
            </div>
            <div id="panelBody-3" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o13.pdf">13. มาตรฐานการปฏิบัติงาน</a>
                      </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-4" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การให้บริการงาน</a>
                </h4>
            </div>
            <div id="panelBodyTwo-4" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="pt10-pl30">

                    <a href="#panelsub14" class="accordion-toggle" data-toggle="collapse"
                        data-parent="#sub14">14. มาตรฐานการให้บริการ</a><br>
                        <div id="sub14" style="padding-left: 40px;">
                        <div id="panelsub14" class="panel-collapse collapse">
                            <div class="panel-body" style="padding-left: 15px;">
                              <ul>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o14_1.pdf">1.คู่มืองานทะเบียน</a><br>
                                  </li>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o14_2.jpg">2.แผ่นพับ eLibrary</a><br>
                                  </li>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o14_3.pdf">3.คู่มือการใช้งาน Google Drive ของสถาบันเทคโนโลยีจิตรลดา</a><br>
                                  </li>
                                  <li>
                                        <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o14_4.pdf">4.วิธีการใช้งานระบบสารสนเทศ</a><br>
                                  </li>
                                  <li>
                                        <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o14_5.pdf">5.วิธีการใช้งานMicrosoft teams</a><br>
                                  </li>
                                  <li>
                                        <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o14_6.pdf">6.วิธีการใชังาน_Google Meet</a><br>
                                  </li>
                              </ul>
                            </div>
                        </div>
                    </div>

                    <a href="#panelsub15" class="accordion-toggle" data-toggle="collapse"
                        data-parent="#sub15">15. ข้อมูลเชิงสถิติของการให้บริการ</a><br>
                        <div id="sub15" style="padding-left: 40px;">
                        <div id="panelsub15" class="panel-collapse collapse">
                            <div class="panel-body" style="padding-left: 15px;">
                              <ul>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o15_1.pdf">1.สถิติงานบริการด้านเอกสารทางการศึกษา</a><br>
                                  </li>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o15_2.pdf">2.สถิติการยืมคืน และการใช้บริการวิทยทรัพยากร ปีงบ 2563</a><br>
                                  </li>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o15_3.pdf">3.สถิติการให้บริการงานเทคโนโลยีสารสนเทศ</a><br>
                                  </li>

                              </ul>
                            </div>
                        </div>
                    </div>

                    <a href="#panelsub16" class="accordion-toggle" data-toggle="collapse"
                        data-parent="#sub16">16. รายงานผลการสำรวจความพึงพอใจในการให้บริการ</a><br>
                        <div id="sub16" style="padding-left: 40px;">
                        <div id="panelsub16" class="panel-collapse collapse">
                            <div class="panel-body" style="padding-left: 15px;">
                              <ul>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o16_1.pdf">1.รายงานความพึงพอใจและความคิดเห็นของนักศึกษา2561</a><br>
                                  </li>
                                  <li>
                                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o16_2.pdf">2.รายงานผลสำรวจความพึงพอใจด้านระบบสารสนเทศ 2562</a><br>
                                  </li>

                              </ul>
                            </div>
                        </div>
                    </div>

                      <a href="#panelsub17" class="accordion-toggle" data-toggle="collapse"
                          data-parent="#sub17">17. E-Service</a><br>
                          <div id="sub17" style="padding-left: 40px;">
                          <div id="panelsub17" class="panel-collapse collapse">
                              <div class="panel-body" style="padding-left: 15px;">
                                <ul>
                                    <li>
                                        <a target="_blank" href="//reg.cdti.ac.th">ระบบบริการการศึกษา (REG)</a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="//lib.cdti.ac.th">CDTI Library </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="//elibrary.cdti.ac.th/">ห้องสมุดดิจทัล</a>
                                    </li>
                                    <li>
                                          <a target="_blank" href="//www.cdti.ac.th/webboard">webboard</a>
                                    </li>
                                </ul>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-5" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">แผนการใช้จ่ายงบประมาณประจำปี</a>
                </h4>
            </div>
            <div id="panelBodyTwo-5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o18.pdf">18. แผนการใช้จ่ายงบประมาณประจำปี</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o19.pdf">19. รายงานการกำกับติดตาม การใช้จ่ายงบประมาณ รอบ 6 เดือน</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o20.pdf">20. รายงานผลการใช้จ่ายงบประมาณ ประจำปี</a>
                    </p>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-6" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การจัดซื้อจัดจ้างหรือการจัดหาพัสดุ</a>
                </h4>
            </div>
            <div id="panelBodyTwo-6" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="pt10-pl30">
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o21.pdf">21. แผนการจัดซื้อจัดจ้างหรือแผนการจัดหาพัสดุ</a><br>
                      <a target="_blank" href="//www.cdti.ac.th/category/จัดซื้อจัดจ้าง">22. ประกาศต่าง ๆ เกี่ยวกับการจัดซื้อจัดจ้างหรือการจัดหาพัสดุ</a><br>
                      <a href="#panelsub23" class="accordion-toggle" data-toggle="collapse"
                          data-parent="#sub23">23. สรุปผลการจัดซื้อจัดจ้างหรือการจัดหาพัสดุรายเดือน</a><br>
                          <div id="sub23" style="padding-left: 40px;">
                          <div id="panelsub23" class="panel-collapse collapse">
                              <div class="panel-body" style="padding-left: 15px;">
                                <ul>
                                    <li>
                                        <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o23_1.pdf">เดือนมีนาคม 2563</a><br>
                                    </li>
                                </ul>
                              </div>
                          </div>
                      </div>

                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o24.pdf">24. รายงานผลการจัดซื้อจัดจ้างหรือการจัดหาพัสดุประจำปี</a>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-7" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การบริหารและพัฒนาทรัพยากรบุคคล</a>
                </h4>
            </div>
            <div id="panelBodyTwo-7" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="pt10-pl30">
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o25.pdf">25. นโยบายการบริหารทรัพยากรบุคคล</a><br>
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o26.pdf">26. การดำเนินการตามนโยบายการบริหารทรัพยากรบุคคล</a><br>
                      <a href="#panelsub27" class="accordion-toggle" data-toggle="collapse"
                          data-parent="#sub27">27. หลักเกณฑ์การบริหารและพัฒนาทรัพยากรบุคคล</a>
                          <div id="sub27" style="padding-left: 40px;">
                          <div id="panelsub27" class="panel-collapse collapse">
                              <div class="panel-body" style="padding-left: 15px;">
                                <p><strong>หลักเกณฑ์การสรรหา คัดเลือก บรรจุและแต่งตั้งบุคลากร</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_01.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล พ.ศ. 2561 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_07.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยเทคโนโลยีจิตรลดาว่าด้วย ว่าด้วยการบริหารงานบุคคล (ฉบับที่ 2) พ.ศ. 2562 </a></li>
                                  <li> <a target="_blank" href="">ประกาศสถาบัน ว่าด้วยการสรรหา และการคัดเลือกเพื่อบรรจุแต่งตั้งผู้ปฏิบัติงานในสถาบัน </a></li>
                                </ul>
                                <p><strong>หลักเกณฑ์การประเมินผลการปฏิบัติงาน</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_08.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายวิชาการ พ.ศ. 2562 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_09.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายสนับสนุน พ.ศ. 2562 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_10.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน พ.ศ. 2562 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_11.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดเกณฑ์คะแนนทดสอบความสามารถทางภาษาอังกฤษของอาจารย์ประจำ พ.ศ. 2562 </a></li>
                                </ul>
                                <p><strong>ให้คุณให้โทษและสร้างขวัญกำลังใจ</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_04.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง วันเวลาปฏิบัติงาน วันหยุดและวันลา พ.ศ. 2562 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_05.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสวัสดิการและสิทธิประโยชน์ของบุคลากร พ.ศ. 2562 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_06.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ พ.ศ. 2562 </a></li>
                                </ul>
                                <p><strong>หลักเกณฑ์การลาไปศึกษาต่อ ฝึกอบรม หรือปฏิบัติงานวิจัย</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_18.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษาต่อระดับปริญญาโทหรือปริญญาเอกแบบลาศึกษาบางเวลา พ.ศ. 2563 </a></li>
                                </ul>
                                <p><strong>อื่น ๆ</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/02_01.pdf">ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง การแบ่งส่วนงานของสถาบัน พ.ศ. 2561 </a></li>
                                </ul>
                                <p><strong>หลักเกณฑ์การได้มาซึ่งผู้บริหาร</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/05_01.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการได้มาซึ่งอธิการบดี พ.ศ. 2561 </a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/05_02.pdf">ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยหลักเกณฑ์ คุณสมบัติ วิธีการได้มา วาระการดำรงตำแหน่ง การพ้นจากตำแหน่ง อำนาจหน้าที่และการบริหารของหัวหน้าส่วนงาน พ.ศ. 2561 </a></li>
                                </ul>
                              </div>
                          </div>
                      </div>
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o28.pdf">28. รายงานผลการบริหารและพัฒนาทรัพยากรบุคคลประจำปี</a>

                    </div>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-8" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การจัดการเรื่องร้องเรียนการทุจริตและการเปิดโอกาสให้เกิดการมีส่วนร่วม</a>
                </h4>
            </div>
            <div id="panelBodyTwo-8" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="pt10-pl30">
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o29.pdf">29. แนวปฏิบัติการจัดการเรื่องร้องเรียนการทุจริต</a><br>
                      <a target="_blank" href="//forms.gle/9u9vQNA8obKyLQfn6">30. ช่องทางแจ้งเรื่องร้องเรียนการทุจริต</a><br>
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o31.pdf">31. ข้อมูลเชิงสถิติเรื่องร้องเรียนการทุจริตประจำปี</a><br>
                      <a target="_blank"  href="//forms.gle/9u9vQNA8obKyLQfn6">32. ช่องทางการรับฟังความคิดเห็น</a><br>
                      <a href="#panelsub33" class="accordion-toggle" data-toggle="collapse"
                          data-parent="#sub33">33. การเปิดโอกาสให้เกิดการมีส่วนร่วม</a>
                          <div id="sub33" style="padding-left: 40px;">
                          <div id="panelsub33" class="panel-collapse collapse">
                              <div class="panel-body" style="padding-left: 15px;">
                                <p><strong>1. ผู้มีส่วนได้ส่วนเสียภายใน (บุคลากร)</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/category/จัดซื้อจัดจ้าง">1.1 การประชุมคณะกรรมการพัฒนาสำนักงานสถาบัน ฯ </a></li>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/ประชุมคณะกรรมการพัฒนาสำนักงานและการประเมินคุณธรรมและความโปร่งใส">1.2 การประชุมคณะทำงานเพื่อเข้ารับการประเมินคุณธรรมและความโปร่งใส ฯ</a></li>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/ประชุมคณะกรรมการพัฒนาการสื่อสารองค์กร">1.3 การประชุมคณะกรรมการพัฒนาการสื่อสารองค์กร</a></li>
                                </ul>
                                <p><strong>2. ผู้มีส่วนได้ส่วนเสียภายนอก (นักเรียน นักศึกษา ผู้ปกครอง และเครือข่ายความร่วมมือ)</strong></p>
                                <ul>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/ชี้แจงกระบวนการฝึกงาน-827">2.1 การประชุมชี้แจงกระบวนการฝึกงาน และรับฟังข้อเสนอแนะจากผู้ปกครอง นักเรียนและนักศึกษา</a></li>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/cdti-academic-activity-study-work-showcase-2019">2.2 CDTI Academic Activity Study & Work Showcase 2019 ในงานมีกิจกรรมให้เครือข่ายความร่วมมือและสถานประกอบการ ในการร่วมการบรรยายให้ความรู้กับนักเรียนและนักศึกษาของสถาบัน</a></li>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/หารือทางวิชาการ">2.3 การหารือทางวิชาการ กับ Center for Professional Assessment (Thailand) เพื่อให้ข้อมูลและรับฟังข้อเสนอแนะ รวมถึงหารือร่วมกันในการจัดการเรียนการสอนภาษาอังกฤษของสถาบัน</a></li>
                                  <li> <a target="_blank" href="//www.cdti.ac.th/หารือความร่วมมือทางวิชาการ-ระหว่าง-สจด.-และสถาบันการศึกษาในประเทศสาธารณรัฐสิงคโปร์">2.4 การหารือความร่วมมือทางวิชาการ กับ สถาบันการศึกษาในประเทศสาธารณรัฐสิงคโปร์ เพื่อประสานความร่วมมือทางด้านวิชาการ รวมถึงการจัดกิจกรรมแลกเปลี่ยนเรียนรู้ทางด้านวิชาการและวัฒนธรรมร่วมกัน</a></li>
                                </ul>

                              </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-9" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">การดำเนินการเพื่อป้องกันการทุจริต</a>
                </h4>
            </div>
            <div id="panelBodyTwo-9" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="pt10-pl30">

                    <a href="#panelsub34" class="accordion-toggle" data-toggle="collapse"
                        data-parent="#sub34">34. เจตจำนงของผู้บริหารสูงสุด </a><br>
                        <div id="sub34" style="padding-left: 40px;">
                        <div id="panelsub34" class="panel-collapse collapse">
                            <div class="panel-body" style="padding-left: 15px;">
                              <ul>
                                  <li>
                                      <a target="_blank" href="//www.cdti.ac.th//uploads/files/act/05_28.pdf">1. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง เจตจำนงสุจริตในการบริหารงานของสถาบันเทคโนโลยีจิตรลดา</a>
                                  </li>
                                  <li>
                                      <a target="_blank" href="//www.cdti.ac.th//uploads/files/act/05_29.pdf">2. Announcement on Integrity and Transparency Intention of Chitralada Technology Institute</a>
                                  </li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <a href="#panelsub35" class="accordion-toggle" data-toggle="collapse"
                        data-parent="#sub35">35. การมีส่วนร่วมของผู้บริหาร</a>
                    <div id="sub35" style="padding-left: 40px;">
                        <div id="panelsub35" class="panel-collapse collapse">
                            <div class="panel-body" style="padding-left: 15px;">
                              <p><strong>ผู้บริหารของสถาบันมีการส่วนร่วมในการดำเนินการ ปรับปรุง พัฒนา และส่งเสริมหน่วยงานด้านคุณธรรมและความโปร่งใส ดังนี้</strong></p>
                              <ul>
                                <li> <a target="_blank" href="//www.cdti.ac.th//uploads/files/act/05_28.pdf">1. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง เจตจำนงสุจริตในการบริหารงานของสถาบันเทคโนโลยีจิตรลดา</a></li>
                                <li> <a target="_blank" href="//www.cdti.ac.th//uploads/files/act/05_29.pdf">2. Announcement on Integrity and Transparency Intention of Chitralada Technology Institute</a></li>
                                <li> <a target="_blank" href="//www.cdti.ac.th/ประชุมการประเมินคุณธรรมและความโปร่งใสในการดำเนินงาน">3. มีการเชิญผู้แทนจาก สำนักงาน ป.ป.ช. มาให้ความรู้เกี่ยวกับการประเมินคุณธรรมและความโปร่งใสของหน่วยงานภาครัฐ</a></li>
                                <li> <a target="_blank" href="//www.cdti.ac.th/ประชุมคณะกรรมการพัฒนาสำนักงานและการประเมินคุณธรรมและความโปร่งใส">4. มีการประชุมคณะทำงานเพื่อเข้ารับการประเมินคุณธรรมและความโปร่งใส ฯ </a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o36.pdf">36. การประเมินความเสี่ยงการทุจริตประจำปี</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o37.pdf">37. การดำเนินงานเพื่อจัดการความเสี่ยงการทุจริต</a><br>
                    <a href="#panelsub38" class="accordion-toggle" data-toggle="collapse"
                        data-parent="#sub38">38. การเสริมสร้างวัฒนธรรมองค์กร</a>
                    <div id="sub38" style="padding-left: 40px;">
                        <div id="panelsub38" class="panel-collapse collapse">
                            <div class="panel-body" style="padding-left: 15px;">
                              <p><strong>กิจกรรมที่แสดงถึงการเสริมสร้างวัฒนธรรมองค์กรให้เจ้าหน้าที่ของหน่วยงานมีทัศนคติ ค่านิยม ในการปฏิบัติงานอย่างซื่อสัตย์สุจริต อย่างชัดเจน</strong></p>
                              <ul>
                                <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_19.pdf">1. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง จรรยาบรรณของครูวิชาชีพและคณาจารย์สถาบันเทคโนโลยีจิตรลดา</a></li>
                                <li> <a target="_blank" href="<?=base_url()?>/uploads/files/act/07_17.pdf">2. ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง นโยบายส่งเสริมจรรยาบรรณของบุคลากรสถาบัน</a> </li>
                                <li> <a target="_blank" href="//www.cdti.ac.th/อบรมบุคลากรใหม่สถาบันเทคโนโลยีจิตรลดา-826">3. การอบรมบุคลากรใหม่ของสถาบันเทคโนโลยีจิตรลดา</a></li>
                                <li> <a target="_blank" href="#">4. การประชุมครู อาจารย์ และบุคลากร ทุกภาคการศึกษา/ภาคเรียน </a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o39.pdf">39. แผนปฏิบัติการป้องกันการทุจริตประจำปี</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o40.pdf">40. รายงานการกำกับติดตามการดำเนินการป้องกันการทุจริต</a><br>
                    <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o41.pdf">41. รายงานผลการดำเนินการป้องกันการทุจริต</a>
                  </div>
                </div>
            </div>
        </div>
      </section>
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBodyTwo-10" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">มาตรการภายในเพื่อส่งเสริมความโปร่งใสและป้องกันการทุจริต</a>
                </h4>
            </div>
            <div id="panelBodyTwo-10" class="panel-collapse collapse" >
                <div class="panel-body">
                    <div class="pt10-pl30">
                      <a href="#panelsub42" class="accordion-toggle" data-toggle="collapse"
                          data-parent="#sub42">42. มาตรการส่งเสริมคุณธรรมและความโปร่งใสภายในหน่วยงาน</a>
                          <div id="sub42" style="padding-left: 40px;">
                          <div id="panelsub42" class="panel-collapse collapse">
                              <div class="panel-body" style="padding-left: 15px;">
                                <p><strong> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42.pdf">ประกาศ แผนและมาตรการภายในเพื่อป้องกันการทุจริต</a></strong></p>
                                <ul>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_01.pdf">มาตรการเผยแพร่ข้อมูลต่อสาธารณะ</a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_02.pdf">มาตรการให้ผู้มีส่วนได้ส่วนเสียมีส่วนร่วม</a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_03.pdf">มาตรการส่งเสริมความโปร่งใสในการจัดซื้อจัดจ้าง</a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_04.pdf">มาตรการการจัดการเรื่องร้องเรียนทุจริต</a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_05.pdf">มาตรการป้องกันการรับสินบน</a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_06.pdf">มาตรการป้องกันการขัดกันระหว่างผลประโยชน์ส่วนตนกับผลประโยชน์ส่วนรวม</a></li>
                                  <li> <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o42_07.pdf">มาตรการตรวจสอบการใช้ดุลพินิจ</a></li>

                                </ul>

                              </div>
                          </div>
                      </div>
                      <a target="_blank" href="<?=base_url()?>/uploads/files/oit/oit_o43.pdf">43. มาตรการให้ผู้มีส่วนได้ส่วนเสียมีส่วนร่วม</a><br>
                    </div>
                </div>
            </div>
        </div>
      </section>


    </div>
</div>
