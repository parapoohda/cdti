<div class="banner-innerpage  mb-4" style="background-image:url(<?php echo base_url().'/cdti_assets/images/banner-cdtc-2.jpg'?>)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<div class="whats ">
	<div class="container">
		<div class="section-header text-center">
            <h3 class="section-title color-2"><?php echo $page->title ?></h3>
            <?php
            if($page->branch_title_en){
                ?>
                    <p class="color-2 mb-4"><?php echo $page->branch_title_en ?></p>
                <?
            }
             ?>

		</div>
	</div>
</div>

<?php $this->load->view('cdti/default_page/general_about.php'); ?>


<div class="bg-white">
      	<?php $this->load->view("cdti/include/partner",["link" => $links])?>
</div>
