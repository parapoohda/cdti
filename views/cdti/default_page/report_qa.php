<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
						<h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> รายงานผลการประเมินคุณภาพการศึกษา</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container" style="min-height: 300px;">

  <table align="center" border="1" cellpadding="5" cellspacing="1" class="table table-bordered" style="width: 100%;">
<tbody>
  <tr style="background:#efefef;">
    <td>
      <span style="color:#f00;"><strong>เรื่อง</strong></span></td>
    <td style="text-align: center;">
      <font color="#ff0000"><b>ดาวน์โหลด</b></font></td>
  </tr>
  
  <tr>
    <td>
    รายงานผลการประเมินตนเองระดับสถาบัน ประจำปีการศึกษา 2563</td>
    <td style="text-align: center;">
      <p>
        <a target="_blank" href="<?=base_url()?>uploads/files/selfreport/รายงานผลการประเมินตนเองระดับสถาบัน 2563.pdf" ><img alt="" height="32" src="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/icon download pdf.png" width="32"></a></a></p>
      </td>
  </tr>
  
  <tr>
    <td>
      รายงานผลการประเมินตนเองระดับสถาบัน ประจำปีการศึกษา 2562</td>
    <td style="text-align: center;">
      <a target="_blank" href="<?=base_url()?>uploads/files/selfreport/รายงานผลการประเมินตนเองระดับสถาบัน 2562.pdf"><img alt="" height="32" src="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/icon download pdf.png" width="32"></a></td>
  </tr>

  <tr>
    <td>
    รายงานผลการประเมินตนเองระดับสถาบัน ประจำปีการศึกษา 2561</td>
    <td style="text-align: center;">
      <a target="_blank" href="<?=base_url()?>uploads/files/selfreport/รายงานผลการประเมินตนเองระดับสถาบัน 2561.pdf"><img alt="" height="32" src="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/icon download pdf.png" width="32"></a></td>
  </tr>
  <tr>
    <td>
    รายงานผลการประเมินตนเองระดับสถาบัน ประจำปีการศึกษา 2559</td>
    <td style="text-align: center;">
      <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR CDTC 2559.pdf"><img alt="" height="32" src="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/icon download pdf.png" width="32"></a></td>
  </tr>
  
  <tr>
    <td>
    รายงานผลการประเมินตนเองระดับสถาบัน ประจำปีการศึกษา 2558</td>
    <td style="text-align: center;">
      <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR CDTC 2558.pdf"><img alt="" height="32" src="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/icon download pdf.png" width="32"></a></td>
  </tr>
  
  <tr>
    <td>
    รายงานผลการประเมินตนเองระดับสถาบัน ประจำปีการศึกษา 2557</td>
    <td style="text-align: center;">
      <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน ระดับสถาบัน 2557.pdf"><img alt="" height="32" src="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/icon download pdf.png" width="32"></a></td>
  </tr>
  
  
  <tr  style="background-color:#D4F1F4">
    <td  >
      รายงานผลการประเมินคุณภาพการศึกษาภายใน ระดับคณะ ปีการศึกษา 2558</td>
    <td style="text-align: center;">
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Faculty 2558 - บริหารธุรกิจ.pdf">คณะบริหารธุรกิจ</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Faculty 2558 - เทคโนโลยีอุตสาหกรรม.pdf">คณะเทคโนโลยีอุตสาหกรรม</a></p>
    </td>
  </tr>
  <tr  style="background-color:#D4F1F4">
    <td>
      รายงานผลการประเมินคุณภาพการศึกษาภายใน ระดับคณะ ปีการศึกษา 2557</td>
    <td style="text-align: center;">
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน คณะบริหารธุรกิจ 2557.pdf">คณะบริหารธุรกิจ</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน คณะเทคโนโลยีอุตสาหกรรม 2557.pdf">คณะเทคโนโลยีอุตสาหกรรม</a></p>
    </td>
  </tr>
  <tr style="background-color:#FFFFE0">
    <td>
      รายงานผลการประเมินคุณภาพการศึกษาภายใน ระดับหลักสูตร ปีการศึกษา 2559</td>
    <td style="text-align: center;">
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2559 - การตลาด.pdf">หลักสูตรการตลาด</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2559 - คอมพิวเตอร์ธุรกิจ.pdf">หลักสูตรคอมพิวเตอร์ธุรกิจ</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2559 - ธุรกิจอาหาร.pdf">หลักสูตรธุรกิจอาหาร</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2559 - ไฟฟ้ากำลัง.pdf">หลักสูตรไฟฟ้ากำลัง</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2559 - อิเล็กทรอนิกส์.pdf">หลักสูตรอิเล็กทรอนิกส์</a></p>
    </td>
  </tr>
  <tr style="background-color:#FFFFE0">
    <td>
      รายงานผลการประเมินคุณภาพการศึกษาภายใน ระดับหลักสูตร ปีการศึกษา 2558</td>
    <td style="text-align: center;">
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2558 - การตลาด.pdf">หลักสูตรการตลาด</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2558 - คอมพิวเตอร์ธุรกิจ.pdf">หลักสูตรคอมพิวเตอร์ธุรกิจ</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2558 - ธุรกิจอาหาร.pdf">หลักสูตรธุรกิจอาหาร</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2558 - ไฟฟ้ากำลัง.pdf">หลักสูตรไฟฟ้ากำลัง</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/CAR Curriculum 2558 - อิเล็กทรอนิกส์.pdf">หลักสูตรอิเล็กทรอนิกส์</a></p>
    </td>
  </tr>
  <tr style="background-color:#FFFFE0">
    <td>
      รายงานผลการประเมินคุณภาพการศึกษาภายใน ระดับหลักสูตร ปีการศึกษา 2557</td>
    <td style="text-align: center;">
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน หลักสูตรการตลาด 2557.pdf">หลักสูตรการตลาด</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน หลักสูตรคอมพิวเตอร์ธุรกิจ 2557.pdf">หลักสูตรคอมพิวเตอร์ธุรกิจ</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน หลักสูตรธุรกิจอาหาร 2557.pdf">หลักสูตรธุรกิจอาหาร</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน หลักสูตรไฟฟ้ากำลัง 2557.pdf">หลักสูตรไฟฟ้ากำลัง</a></p>
      <p>
        <a target="_blank" href="http://www.cdtc.ac.th/cdtc/fileman/Uploads/dep_quality/document/รายงานผลการประเมิน หลักสูตรอิเล็กทรอนิกส์ 2557.pdf">หลักสูตรอิเล็กทรอนิกส์</a></p>
    </td>
  </tr>
</tbody>
</table>
</div>
