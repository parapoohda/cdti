<?php
/*if($page->id == 134 || $page->id == 62){
?>
<div class="banner-innerpage" style="background-image:url(<?php echo base_url().'/cdti_assets/images/banner_campus_cover_'.$page->id.'.jpg'?>)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<?
}*/
?>
<div class="banner-innerpage  mb-4" style="background-image:url(<?php echo base_url().'/cdti_assets/images/banner_campus_'.$page->id.'.jpg'?>)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<div class="whats ">
	<div class="container">

		<!-- <div class="section-header text-center">
            <h3 class="section-title color-2"><?php echo $page->title ?></h3>
            <?php
            if($page->branch_title_en){
                ?>
                    <p class="color-2 mb-4"><?php echo $page->branch_title_en ?></p>
                <?php
            }
             ?>
		</div> -->
	</div>
</div>
<?php $this->load->view('cdti/default_page/faculty/campus_about.php'); ?>
<section>
	<?php if($page_branch)$this->load->view('cdti/default_page/faculty/campus_branch.php',['page_branch' => $page_branch,'header' => false]);?>
</section>

<div class="bg-white">
	<section id="news" class="mini-spacer feature7 news">
			<div class="container">
					<div class="row">
							<div class="col-lg-8 col-md-9">
									<?php $this->load->view('cdti/include/post/campus_post',['data' => $latest_posts]);?>
									<?php $this->load->view('cdti/include/post/channel_post',['data' => $channel_posts]);?>
							</div>
							<div class="col-lg-4 col-md-3">
									<div class="sidebar">
										<?php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
									</div>
							</div>
					</div>
			</div>
	</section>
</div>

<div class="bg-white">
      	<!-- <?php $this->load->view("cdti/include/gallery_cdti",["main_category1_cover" => $main_category1_cover])?>
      	<?php $this->load->view("cdti/include/partner",["link" => $links])?> -->
</div>
