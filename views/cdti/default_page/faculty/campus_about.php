 <section class="campus-box ">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 text-primary"><i class="far fa-newspaper"></i> รู้จักคณะ</h3>
				</div>
				<div class="col-sm-6 text-right">
					<ol class="breadcrumb ">
              <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
              <li class="breadcrumb-item active">รู้จักคณะ</li>
          </ol>
				</div>
			</div>
		</div>
		<div class="campus-content">
			<!-- <div class="row m-b-40 m-t-40">
				<div class="col-sm-12 text-center">
					<h3 class="mb-0 color-primary"><?php echo $page->title ?> (<?php echo $page->branch_title_en ?>) </h3>
					<hr class="hr-primary">
				</div>
			</div> -->
			<div class="row m-b-40">
				<div class="col-sm-2 col-lg-6">
					<!-- <div class="campus-detail mb-2">
						<p><strong>ชื่อเต็ม (ไทย) :</strong> <?php echo $page->title ?></p>
						<?php
						if($page->branch_title_en){
							?>
							<p><strong>ชื่อเต็ม (อังกฤษ) :</strong> <?php echo $page->branch_title_en ?></p>
							<?php
						}
						?>

					</div> -->
					<div class="campus-paragraph">
						<br>
					<?php echo html_entity_decode($page->page_content) ?>
					</div>
				</div>
				<div class="col-sm-2 col-lg-6">
					<div class="campus-gallery">
						<div class="row">
						<?php $additional_images = get_page_additional_images($page->id,'page_content'); ?>

						<?php if (!empty($additional_images)): ?>
							<?php foreach ($additional_images as $index => $value):
							if ($index < 1) {
								// first
								?>
								<div class="col-sm-12 mb-2">
									<img src="<?php echo base_url().(html_escape($value->image_big)); ?>" alt="" class="img-responsive mx-auto">
								</div>
								<?php
							} else {
								// last
								?>
								<div class="col-sm-6 mb-2">
									<img src="<?php echo base_url().(html_escape($value->image_big)); ?>" alt="" class="img-responsive mx-auto">
								</div>
								<?php
							}
							endforeach; ?>
						<?php endif; ?>


						</div>
					</div>
				</div>
			</div>
			<div class="feature7">
			</div>
		</div>

	</div>
</section>
