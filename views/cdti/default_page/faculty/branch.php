<section class="campus-box  mini-spacer p-b-0">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4   text-primary"><i class="far fa-newspaper"></i> สาขาที่เปิดสอน</h3>
				</div>
          <?php if($header == true):?>
      				<div class="col-sm-6 text-right">
      					<ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
                    <li class="breadcrumb-item active">สาขาที่เปิดสอน</li>
                </ol>
      				</div>
          <?php endif;?>
			</div>
		</div>
	</div>
</section>

<?php 

foreach ($page_branch as $key => $branch): ?>
    <div class="news mini-spacer feature22 <?php ($key%2 == 0)?'bg-light-info':'bg-white'?>">
        <div class="container">
            <div class="row wrap-feature-22">
            <div class="col-lg-6 order-lg-<?php echo ($key%2 > 0)?12:0?>">
                <img src="<?php echo base_url().(html_escape($branch->branch_cover_image)); ?>" class="rounded img-shadow img-responsive" alt="wrapkit" />
            </div>
            <div class="col-lg-6 order-lg-1">
                <div class="text-box">
                    <h3 class="font-light color-primary"><?php echo (html_escape($branch->title)); ?></h3>
                    <h3 class="font-light color-primary"><?php echo (html_escape($branch->branch_title_en)); ?></h3>
                    <hr class="hr-info">
                    <p class="text-dark">
                        <?php echo (html_escape($branch->branch_description)); ?>
                    </p>
                </div>
                    <a class="btn btn-custom-primary btn-md btn-arrow m-t-20" href="<?php echo base_url().$branch->slug?>"><span>ดูเพิ่มเติม<i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>