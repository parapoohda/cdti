<section class="campus-box  mini-spacer">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 text-primary"><i class="far fa-newspaper"></i> รู้จักคณะ</h3>
				</div>
				<div class="col-sm-6 text-right">
					<ol class="breadcrumb ">
              <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
              <li class="breadcrumb-item active">รู้จักคณะ</li>
          </ol>
				</div>
			</div>
		</div>
		<div class="campus-content">
			<div class="row m-b-40 m-t-40">
				<div class="col-sm-12 text-center">
					<h3 class="mb-0 color-primary">คณะเทคโนโลยีอุตสาหกรรม หลักสูตรเทคโนโลยีบัณฑิต (ทล.บ.) Bachelor of Technology </h3>
					<hr class="hr-primary">
				</div>
			</div>
			<div class="row m-b-40">
				<div class="col-sm-2 col-lg-6">
					<div class="campus-detail mb-2">
						<p><strong>ปริญญาและสาขาวิชา</strong></p>
						<p><strong>ชื่อเต็ม (ไทย) :</strong> เทคโนโลยีบัณฑิต (เทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์)</p>
						<p><strong>ชื่อย่อ (ไทย) :</strong> ทล.บ. (เทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์)</p>
						<p><strong>ชื่อเต็ม (อังกฤษ) :</strong> Bachelor of Technology (Electrical and Electronic Technology)</p>
						<p><strong>ชื่อย่อ (อังกฤษ) :</strong> B.Tech. (Electrical and Electronic Technology)</p>
					</div>
					<div class="campus-paragraph">
						<p>   เป็นหลักสูตรที่สร้างบัณฑิตที่เป็นคนดี มีฝีมือ มีความรู้ความสามารถ ทางวิชาชีพทางด้านเทคโนโลยีอุตสาหกรรม จัดการศึกษาตามแนวทาง “เรียนคู่งาน งานคู่เรียน” ให้มีสมรรถนะในการทำงาน เท่าทันต่อการเปลี่ยนแปลงทางด้านเทคโนโลยีของอุตสาหกรรมสมัยใหม่ เพื่อให้เป็นที่ยอมรับในระดับอาเซียน   </p>
						<h6>อุตสาหกรรม 4.0 กับ ความต้องการด้านแรงงาน</h6><br>
						<p>   อุตสาหกรรม 4.0 เป็นแนวนโยบายที่ท้าทายและชี้เป้าที่ต้องการปรับเปลี่ยนโครงสร้างอุตสาหกรรมของประเทศจากอุตสาหกรรมที่ใช้แรงงานเข้มข้นและเทคโนโลยีอย่างง่ายเพื่อผลิตสินค้าและบริการที่ไม่ซับซ้อนและมีมูลค่าเพิ่มต่ำ ไปสู่อุตสาหกรรมที่ขับเคลื่อนด้วยการพัฒนาด้านวิทยาศาสตร์ เทคโนโลยี วิจัย และนวัตกรรมเป็นสำคัญ เพื่อผลิตสินค้าและบริการที่ซับซ้อนและมีมูลค่าเพิ่มสูงขึ้นได้อย่างหลากหลายตามความต้องการเฉพาะของผู้บริโภค</p>
						<p>   องค์ความรู้ที่เกิดขึ้นจากภาคการศึกษาจะเป็นรากฐานสำคัญของการเพิ่มสมรรถนะ (Capability) ของภาคการผลิต ซึ่งจะนำไปสู่ความสำเร็จในการปฏิรูปครั้งใหม่ เพื่อให้สามารถผลิตแรงงานและนักเทคโนโลยีให้มีสมรรถนะตามมาตรฐานอาชีพที่ต้องการ</p>
						<h6>คณะเทคโนโลยีอุตสาหกรรม หลักสูตรเทคโนโลยีบัณฑิต </h6><br>
						<p>   เป็นหลักสูตรที่ตอบสนองต่อ ความต้องการแรงงานในการพัฒนาอุตสาหกรรมของประเทศไทย ดังนั้นจึงเป็นหน้าที่ของคณะเทคโนโลยี อุตสาหกรรมที่ต้องผลิตบัณฑิตที่มีความสามารถ เป็นผู้เชี่ยวชาญ ชำนาญงานในการประกอบวิชาชีพ รับผิดชอบต่อตนเองและสังคม ตลอดจนสื่อสารได้ดีทั้งภาษาไทยและต่างประเทศ สอดคล้องกับจุดมุ่งหมาย ของสถาบันเพื่อเป็นทรัพยากรบุคคลที่มีคุณค่าต่อการพัฒนาประเทศและสังคม</p>
						<br>
						<a href="<?php echo campus_link($page,$fac_link[2])?>" class="btn btn-custom-primary">ดูสาขาที่เปิดสอน</a>
					</div>
				</div>
				<div class="col-sm-2 col-lg-6">
					<div class="campus-gallery">
						<div class="row">
							<div class="col-sm-12 mb-2">
								<img src="<?php echo base_url(); ?>cdti_assets/images/news/13.jpg" alt="" class="img-responsive mx-auto">
							</div>
							<div class="col-sm-12 mb-2">
								<img src="<?php echo base_url(); ?>cdti_assets/images/news/10.jpg" alt="" class="img-responsive mx-auto">
							</div>
							<div class="col-sm-6 mb-2">
								<img src="<?php echo base_url(); ?>cdti_assets/images/news/12.jpg" alt="" class="img-responsive mx-auto">
							</div>
							<div class="col-sm-6 mb-2">
								<img src="<?php echo base_url(); ?>cdti_assets/images/news/8.jpg" alt="" class="img-responsive mx-auto">
							</div>
						</div>
					</div>
				</div>
			</div>			
			<div class="feature7">
        <?php if($fac_reward): ?>

        				<div class="row m-b-40">
        					<div class="col-sm-12 text-left m-b-20">
        						<h3 class="mb-0 color-primary">ผลงานและรางวัล</h3>
        						<hr class="hr-primary">
        					</div>
        					<div class="col-sm-12 campus-gallery">

        						<div class="row campus-gallery-row">
                        <?php foreach($fac_reward as $key => $post): ?>
                             <?php if($key == 0):?>
                                 <!-- <div class="col-sm-4 col-lg-5 wrap-feature7-box  wrap-feature7-box-small img-gallery">
                                    <a href="<?php echo fac_url('fac-reward/'.$page->slug,$post); ?>">
                                        <img src="<?php echo get_post_image($post,'default'); ?>" alt="<?php echo html_escape($post->title); ?>" class="img-responsive">
                                        <div class="img-overlay">
                                          <p><?php echo html_escape($post->title); ?></p>
                                        </div>
                                    </a>
                                 </div> -->
                                 <div class="col-sm-4 col-lg-5 wrap-feature7-box  wrap-feature7-box-small img-gallery">
                                   <a href="<?php echo fac_url('fac-reward/'.$page->slug,$post); ?>">
                                       <img src="<?php echo get_post_image($post,'default'); ?>" alt="<?php echo html_escape($post->title); ?>" class="img-responsive">
                                   </a>
                                 </div>
                                 <div class="col-sm-8 col-lg-7 ">
                                   <div class="row">
                                     <?php else:?>
                                       <div class="col-sm-4 mb-4 wrap-feature7-box  wrap-feature7-box-small">
                                         <a href="<?php echo fac_url('fac-reward/'.$page->slug,$post); ?>">
                                             <img src="<?php echo get_post_image($post,'default'); ?>" alt="<?php echo html_escape($post->title); ?>" class="img-responsive">
                                         </a>
                                       </div>
                                     <?php endif;?>
                            <?php endforeach;?>
                                  </div>
                                </div>
        						</div>
        					</div>
        				</div>
        <?php endif;?>


      <?php if($fac_gallery): ?>
    				<div class="row">
    					<div class="col-sm-12 text-left m-b-20">
    						<h3 class="mb-0 color-primary">ภาพบรรยากาศการเรียนการสอน</h3>
    						<hr class="hr-primary">
    					</div>
    					<div class="col-md-12">
    						<div class="row">
                  <?php foreach($fac_gallery as $key => $post): ?>
        							<div class="col-md-4 wrap-feature7-box  wrap-feature7-box-small">
        								<div class="row">
                            <div class="col-md-12 col-6">
                               <a href="<?php echo fac_url('fac-reward/'.$page->slug,$post); ?>">
                               	 <img class="rounded img-responsive m-b-10" src="<?php echo get_post_image($post,'mid'); ?>" alt="<?php echo html_escape($post->title); ?>" />
                               </a>
                            </div>
                            <div class="col-md-12 col-6">
                                <p class="text-dark">
                                	<a href=""><?php echo html_escape(character_limiter($post->title,50, '...')); ?></a>
                                </p>
                            </div>
                         </div>
        						   </div>
                   <?php endforeach;?>
    						</div>
    					</div>
    				</div>
          <?php endif;?>
			</div>
		</div>

	</div>
</section>

<div class="bg-white">
      	<?php $this->load->view("cdti/include/partner",["link" => $links])?>
</div>
