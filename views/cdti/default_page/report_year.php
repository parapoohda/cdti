<section class="campus-box mini-spacer">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> รายงานประจำปี</h3>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="campus-box">
	<div class="container">
		<div class="row m-b-40">
			<div class="card mr-3" style="width: 12rem;">
				<img class="img-fluid img-thumbnail" src="<?=base_url()?>/uploads/files/AnnualReport/img/AnnualReport_2562.jpg" alt="Card image cap">
				<div class="card-body">
					<a target="_blank" href="<?=base_url()?>/uploads/files/AnnualReport/AnnualReport_2562.pdf" class="btn btn-primary btn-lg btn-block">Download</a>
				</div>
			</div>

			<div class="card mr-3" style="width: 12rem;">
				<img class="img-fluid img-thumbnail" src="<?=base_url()?>/uploads/files/AnnualReport/img/AnnualReport_2563.jpg" alt="Card image cap">
				<div class="card-body">
					<a target="_blank" href="<?=base_url()?>/uploads/files/AnnualReport/AnnualReport_2563.pdf" class="btn btn-primary btn-lg btn-block">Download</a>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
