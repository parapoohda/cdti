 <section class="campus-box  mini-spacer p-b-0">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4   text-primary"><i class="far fa-newspaper"></i> สาขาที่เปิดสอน</h3>
				</div>
          <?php if($header == true):?>
      				<div class="col-sm-6 text-right">
      					<ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
                    <li class="breadcrumb-item active">สาขาที่เปิดสอน</li>
                </ol>
      				</div>
          <?php endif;?>
			</div>
		</div>
	</div>
</section>

<?php 

foreach ($page_branch as $key => $value): ?>
    <div class="news mini-spacer feature22 <?php ($key%2 == 0)?'bg-light-info':'bg-white'?>">
        <div class="container">
            <div class="row wrap-feature-22">
                <?php $this->load->view($value->page_content,['order' => $key%2,'branch' => $value]);?>
                    <a class="btn btn-custom-primary btn-md btn-arrow m-t-20" href="<?php echo base_url().$value->slug?>"><span>ดูเพิ่มเติม<i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>
