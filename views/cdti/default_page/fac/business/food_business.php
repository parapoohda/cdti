<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาการจัดการธุรกิจอาหาร</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาการจัดการธุรกิจอาหาร <br /> <small>(B.B.A. Food Business
                                Management)</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">
                            จากความได้เปรียบของประเทศในด้านทำเลที่ตั้งและภูมิอากาศที่เหมาะสมที่สร้างความอุดมสมบูรณ์ให้พืชผลทางการเกษตรและเนื้อสัตว์
                            รวมไปถึงศิลปวัฒนธรรมในการสร้างสรรค์อาหารไทยที่มีรสชาติถูกปากทั้งชาวไทยและต่างชาติ
                            จึงทำให้ธุรกิจอาหารเจริญเติบโตและสร้างรายได้ให้กับประเทศอยู่ในอันดับต้นๆ
                            และได้ถูกกำหนดให้เป็นธุรกิจยุทธศาสตร์ที่จะสร้างการเติบโตให้กับประเทศไทยได้อย่างแข็งแกร่งและยั่งยืน
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_main.jpg"
                        alt="" class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร? </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขาการจัดการธุรกิจอาหาร</h4>
                                        <ul>
                                            <li>
                                                เป็นหลักสูตรที่ผสมผสานระหว่างศาสตร์ทางด้านอาหารและศาสตร์ทางด้านการบริหารจัดการ
                                            </li>
                                            <li>
                                                เน้นการบูรณาการการเรียนการสอนที่มาจากอาจารย์ที่เชี่ยวชาญและเสริมด้วยประสบการณ์จากผู้บริหารจากองค์กรชั้นนำ
                                            </li>
                                            <li>
                                                เน้นการเรียนรู้โดยการนำเอาทฤษฎีและหลักการมาประยุกต์ใช้ในการปฏิบัติที่สอดคล้องกับการทำงานในสาขาที่เกี่ยวข้อง
                                            </li>
                                            <li>เน้นการฝึกปฏิบัติงานในสถานประกอบการชั้นนำของประเทศทั้งทางโรงงานอุตสาหกรรมอาหารและธุรกิจบริการอาหาร
                                            </li>
                                            <li>เป็นหลักสูตรที่ได้รับความร่วมมือกับธุรกิจที่หลากหลายในการรับนักศึกษาเข้าฝึกปฏิบัติงานและเข้าทำงาน
                                            </li>
                                        </ul>
                                        <br />
                                        <h4 class="text-primary">ทำไมต้องธุรกิจอาหาร?</h4>
                                        <p>อุตสาหกรรมอาหาร เป็นอุตสาหกรรมหนึ่งที่มีบทบาทสำคัญต่อการพัฒนาเศรษฐกิจ
                                            และถูกกำหนดให้เป็นหนึ่งในยุทธศาสตร์สำคัญที่จะสร้างความเข้มแข็งให้กับประเทศอย่างยั่งยืนในอนาคต
                                            การเติบโตของอุตสาหกรรมอาหารทำให้เกิดความต้องการบุคลากรเพิ่มสูงขึ้นอย่างต่อเนื่อง
                                            ทั้งในระดับปฏิบัติการ และ ระดับบริหารจัดการ โดยในปัจจุบัน
                                            บุคลากรทางด้านนี้ยังมีอยู่อย่างจำกัดและยังไม่เพียงพอต่อความต้องการ
                                            จึงเป็นที่มาของการพัฒนาหลักสูตรเพื่อช่วยผลิตบุคลากรที่มีทั้งความรู้ความสามารถ
                                            และมีจริยธรรมคุณธรรมที่ดี เพื่อรองรับการขยายตัวของอุตสาหกรรม
                                            ตลอดจนเป็นกำลังสำคัญในการขับเคลื่อนและพัฒนาอุตสาหกรรมอาหารของประเทศไทยต่อไปในอนาคต
                                        </p>
                                        <br />
                                        <h4 class="text-primary">ธุรกิจอาหาร: ยุทธศาสตร์แห่งการเติบโตของประเทศ</h4>
                                        <p>จากความได้เปรียบของประเทศในด้านทำเลที่ตั้งและภูมิอากาศที่เหมาะสมที่สร้างความอุดมสมบูรณ์ให้พืชผลทางการเกษตรและเนื้อสัตว์
                                            รวมไปถึงศิลปะวัฒนธรรมในการสร้างสรรค์อาหารไทยที่มีรสชาติถูกปากทั้งชาวไทยและต่างชาติ
                                            จึงทำให้ธุรกิจอาหารเจริญเติบโตและสร้างรายได้ให้กับประเทศอยู่ในอันดับต้นๆ
                                            และได้กำหนดให้เป็นธุรกิจยุทธศาสตร์ที่จะสร้างการเติบโตให้กับประเทศได้อย่างแข็งแกร่งและยั่งยืน
                                        </p>
                                        <br />
                                        <h4 class="text-primary">ทำไมต้อง FBM Chitralada?</h4>
                                        <p>
                                            1. เป็นหลักสูตรที่ผสมผสานระหว่างการเรียนรู้ทฤษฎี
                                            พร้อมกับการลงมือฝึกปฎิบัติงานจริง<br />
                                            2.
                                            มีกระบวนการเรียนการสอนที่ช่วยให้นักศึกษามีทักษะที่จำเป็นต่อการทำงานในโลกยุคใหม่<br />
                                            3. โอกาสที่จะได้ฝึกฝนและเรียนรู้งานจริงกับบริษัทชั้นนำในธุรกิจอาหาร<br />
                                            4.
                                            เป็นหลักสูตรที่ถ่ายทอดโดยอาจารย์ที่เชี่ยวชาญและเสริมด้วยประสบการณ์จริงของผู้บริหารในแวดวงธุรกิจ<br />
                                            5. เป็นหลักสูตรที่ส่งเสริมความเป็นนานาชาติทั้งทางด้านภาษาและ Mind Set

                                        </p>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_17.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ผู้เข้าศึกษาต้องสำเร็วการศึกษาไม่ต่ำกว่ามัธยมศึกษาตอนปลายหรือเทียบเท่า หรือ
                                            สำเร็จการศึกษาในระดับประกาศนียบัตรวิชาชีพชั้นสูง ( ปวส. )</p>
                                        <br />
                                        <h4 class="text-primary">การคัดเลือกผู้เข้าศึกษา?</h4>
                                        <p>ผู้เข้าศึกษาต้องสำเร็วการศึกษาไม่ต่ำกว่ามัธยมศึกษาตอนปลายหรือเทียบเท่า หรือ
                                            สำเร็จการศึกษาในระดับประกาศนียบัตรวิชาชีพชั้นสูง ( ปวส. )</p>
                                        <br />
                                        <h4 class="text-primary">ค่าใช้จ่ายในการศึกษา</h4>
                                        <p class="mb-0">ภาคการศึกษาละ 29,500 บาท* (เหมาจ่ายต่อภาคการศึกษา)</p>
                                        <p class="text-danger">*ทั้งนี้อาจมีการแปลงได้ในภายหลัง</p>
                                        <br />
                                        <h4 class="text-primary">การฝึกงาน (ระยะเวลาและหน่วยที่คู่สัญญา)</h4>
                                        <p>การฝึกงาน (รายวิชาสหกิจศึกษา) รวมตลอดหลักสูตรเป็นจำนวนไม่น้อยกว่า12 เดือน
                                            จำนวน 36
                                            หน่วยกิตโดยได้รับความร่วมมือกับสถานประกอบการชั้นนำของประเทศทั้งทางอุตสากรรมโรงงานและธุรกิจบริการอาหาร
                                        </p>
                                        <br />
                                        <h4 class="text-primary">ทุนการศึกษา</h4>
                                        <p>ทุนการศึกษาตามโครงการเรียนดีตลอดการศึกษา 4 ปี และทุนอื่นๆ หลายประเภทด้วยกัน
                                            อาทิ</p>
                                        <ul>
                                            <li>ทุนสนับสนุนการเรียน ค่าเทอม และค่าใช้จ่ายรายเดือน</li>
                                            <li>ทุนสำหรับนักกีฬา</li>
                                            <li>ทุนสำหรับนักศึกษาที่สร้างคุณงามความดี</li>
                                            <li>ทุนสำหรับนักศึกษาที่มีขาดแคลนทุนทรัพย์</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_18.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">ปริญญาและสาขาวิชา</h4>
                                        <p class="mb-0">ชื่อเต็ม (ไทย): บิรหารธุรกิจบัณฑิต (การจัดการธุรกิจอาหาร)</p>
                                        <p class="mb-0">ชื่อย่อ (ไทย): บธ.บ. (การจัดการธุรกิจอาหาร)</p>
                                        <p class="mb-0">ชื่อเต็ม (อังกฤษ): Bachelor of Business Adminitration(Food
                                            Business Management)</p>
                                        <p class="">ชื่อย่อ (อังกฤษ): B.B.A. (Food Business Management)</p>
                                        <h4 class="text-primary">สาขาวิชาเอก</h4>
                                        <p>การจัดการธุรกิจอาหาร</p>
                                        <h4 class="text-primary">กลุ่มวิชาที่เปิดสอน</h4>
                                        <p>ธุรกิจอุตสาหกรรมอาหาร ( Food Industry Business)</p>
                                        <p>ธุรกิจบริการอาหาร ( Food Service Business)</p>
                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>
                                        <p>โครงสร้างหลักสูตร แบ่งเป็นนหมวดวิชาที่สอดคล้องกับที่กําหนด
                                            ไว้ในเกณฑ์มาตรฐานหลักสูตรของกระทรวงศึกษาธิการดังนี้</p>
                                        <p>จำนวนหน่วยกิตรวมทั้งหลักสูตร 138 หน่วยกิต</p>
                                        <p>หมวดวิชาศึกษาทั่วไป 33 หน่วยกิต</p>
                                        <ul>
                                            <li>กลุ่มวิชาสังคมศาสตร์ 6 หน่วยกิต</li>
                                            <li>กลุ่มวิชามนุษยศาสตร์ 6 หน่วยกิต</li>
                                            <li>กลุ่มวิชาวิทยาศาสตร์และคณิตศาสตร์ 6 หน่วยกิต</li>
                                            <li>กลุ่มวิชาภาษา 15 หน่วยกิต</li>
                                        </ul>
                                        <p>หมวดวิชาชีพเฉพาะ 99 หน่วยกิต</p>
                                        <ul>
                                            <li>กลุ่มวิชาทางด้านบริหาร 33 หน่วยกิต</li>
                                            <li>กลุ่มวิชาทางวิทยาศาสตร์การอาหาร 54 หน่วยกิต <br /> <span
                                                    class="text-danger">(รวมการฝึกปฏิบัติงาน 36 หน่วยกิต)</span></li>
                                            <li>กลุ่มวิชาทางธุรกิจอุตสาหกรรมอาหาร 12 หน่วย กิต</li>
                                        </ul>
                                        <p>หมวดวิชาเลือกเสรี 6 หน่วยกิต</p>
                                        <p>เอกสาร</p>
                                        <a href=""><i class="fas fa-file-pdf"></i> โหลดหลักสูตรบริหารธุรกิจบัณฑิต
                                            สาขาการจัดการธุรกิจอาหาร</a>
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_14.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_16.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">หลักสูตรการจัดการธุรกิจอาหาร</h4>
                                        <p class="">เป้าหมายสำคัญในการผลิตบัณฑิต คณะบริหารธุรกิจ บริหารธุรกิจบัณฑิต
                                            (บธ.บ.) สถาบันเทคโนโลยีจิตรลดามีดังนี้</p>
                                        <ul>
                                            <li>ปฏิบัติงานได้ทันทีตามสาขาเฉพาะทาง</li>
                                            <li>มีบุคลิกภาพ วินัย ความคิดสร้างสรรค์ สอดคล้อง ตรงตามลักษณะงานตามสาขา</li>
                                            <li>มีคุณลักษณะใฝ่รู้และสู้งานหนัก</li>
                                            <li>มีสมรรถนะสื่อสารอย่างมีประสิทธิภาพ</li>
                                        </ul>
                                        <h4 class="text-primary">กลุ่มวิชาที่เปิดสอน</h4>
                                        <p>กลุ่มวิชาที่ 1 ธุรกิจอุตสาหกรรมอาหาร ( Food Industry Business)</p>
                                        <p>กลุ่มวิชาที่ 2 ธุรกิจบริการอาหาร ( Food Service Business)</p>
                                        <h4 class="text-primary">4 ปีในบ้าน CDTI</h4>
                                        <p>
                                            ประสบการณ์ในบ้าน CDTI 4ปี แบ่งเป็นดังนี้ <br />
                                            ปี1 : เรียนรู้โลกและสังคมรอบตัว
                                            ฝึกทักษะด้านการสื่อสารทั้งภาษาไทยและภาษาอังกฤษรวมทั้งเรียนรู้การสร้างสรรค์และชื่นชมงานศิลปะ<br />ปี2
                                            : เรียนรู้หลักการและแนวคิดในการทำธุรกิจ<br />
                                            ปี3 : เรียนรู้ทางด้านอาหารและโภชนการ<br />
                                            ปี4 : ฝึกปฏิบัติงานและทำโครงการพิเศษ
                                        </p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_11.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_12.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_13.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โอกาศทางธุรกิจหลังสำเร็จการศึกษา</h4>
                                        <ul>
                                            <li>ผู้ประกอบการธุรกิจส่วนตัว</li>
                                            <li>พนักงานหรือผู้บริหารในโรงงานอุตสาหกรรมที่ผลิตอาหาร ธุรกิจบริการอาหาร
                                                เช่น โรงแรม ภัตตาคาร ร้านอาหาร เป็นต้น</li>
                                            <li>เจ้าหน้าที่ในหน่วยงานหรือองค์กรของรัฐที่เกี่ยวข้องกับธุรกิจอาหาร</li>
                                        </ul>
                                        <h4 class="text-primary">
                                            เรียนรู้จริงได้ประสบการณ์จริงฝึกปฏิบัติงานกับสถานประกอบการชั้นนำ</h4>
                                        <p>ระบบการศึกษาที่เน้นการปฏิบัติงานในสถานประกอบการอย่างมีระบบ
                                            โดยจัดให้มีการเรียนในสถานศึกษาร่วมกับการจัดให้นักศึกษาไปปฏิบัติงานจริงในฐานะพนักงานชั่วคราว
                                            ณ
                                            สถานประกอบการที่ให้ความร่วมมือนักศึกษาจะได้เรียนรู้ประสบการณ์จริงจากการไปปฏิบัติงานในสถานประกอบการ
                                            ทำให้ทราบว่าวิชาความรู้ที่เรียนมานั้นสามารถนำไปใช้งานและประกอบอาชีพได้อย่างไร
                                            ได้เปิดโลกทัศน์และสัมผัสเทคโนโลยีใหม่ๆ ที่ใช้งานจริง
                                            นอกจากนี้ยังได้พัฒนาตนเองในการอยู่ร่วกับสังคมในที่ทำงาน
                                            เรียนรู้การปฏิบัติตนอย่างเหมาะสมต่อผู้ร่วมงาน ผู้บังคับบัญชา และผู้บริหาร
                                            เพื่อเตรียมตัวให้พร้อมสำหรับการเป็นบัณฑิตที่มีคุณภาพ
                                            และมีคุณสมบัติตรงตามที่สถานประกอบการต้องการ
                                            รวมทั้งมีวิสัยทัศน์มองเห็นแนวทางการประกอบอาชีพในอนาคต</p>
                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4> -->
                                        <!-- <div class="row">
	                                		<div class="col-sm-12">
	                                			<div id="partner-slider"  class="owl-carousel owl-theme">
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png" alt="">
				                                    </div>
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png" alt="">
				                                    </div>
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png" alt="">
				                                    </div>
				                                    <div class="item">
				                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png" alt="">
				                                    </div>
				                                </div>
	                                		</div>
	                                	</div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_10.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>