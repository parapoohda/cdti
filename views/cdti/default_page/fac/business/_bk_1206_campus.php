 <div class="banner-innerpage" style="background-image:url(<?php echo base_url().'/cdti_assets/images/banner_campus.png'?>)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
               <!--  <h1 class="title">Static Sliders</h1>
                <h6 class="subtitle op-8">You can put any slider in your page</h6> -->
            </div>
        </div>
    </div>
</div>
<div class="whats spacer">
	<div class="container">
		<div class="section-header text-center">
			<h3 class="section-title color-2">คณะบริหารธุรกิจ หลักสูตรบริหารธุรกิจบัณฑิต (บธ.บ.)</h3>
			<p class="color-2 mb-4">Bachelor of Business Adminitration</p>
			<p class="color-3"> หลักสูตรบริหารธุรกิจ สาขาวิชาการจัดการธุรกิจอาหาร (B.B.A. Food Business Management)</p>
			<p class="color-3">หลักสูตรนี้ถูกใช้เป็นกรอบในการพัฒนานักศึกษาให้มีคุณลักษณะของการเป็นผู้ประกอบการที่มีความรู้ด้านบริหารธุรกิจและศาสตร์ที่เกี่ยวข้องเป็นบัณฑิตที่มีคุณธรรม จริยธรรมทางธุรกิจ รวมถึงสามารถดำเนินธุรกิจหรือปฏิบัติงานภายใต้สภาวการณ์ทางเศรษฐกิจและสังคมที่เปลี่ยนแปลงอย่างรวดเร็ว</p>
		</div>
	</div>
</div>
<section id="" class="aboutus-innperpage aboutus-innperpage-business" style="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-lg-6 bg-custom-blue-light spacer">
                <div class="row">
                    <div class="col-sm-12 col-md-10 col-lg-8 ml-auto">
                        <div class="about-content  p-20	" >
                            <h3 class="about-title color-yellow mb-3">
                               คณะบริหารธุรกิจ
                            </h3>
                            <hr class="mb-3">

                            <div class="about-detail ">
                                <p class="text-white m-b-60">
                                    จากความเป็นไปได้เปรียบเชิงภูมิศาสตร์ ทำให้ประเทศไทยมีความอุดมสมบูรณ์ทางด้านการเกษตรและปศุสัตว์เป็นอย่างมาก ส่งผลให้อุตสาหกรรมอาหารกลายมาเป็นอุตสาหกรรมหนึ่งที่มีบทบาทสำคัญต่อการพัฒนาเศรษฐกิจ และกำหนดให้เป็นหนึ่งในยุทธศาสตร์สำคัญที่จะสร้างความเข็มแข็งให้กับประเทศอย่างยั่งยืนในอนาคต
                                </p>
                                <p class="text-white m-b-60">การเติบโตของอุตสาหกรรมอาหารทำให้เกิดความต้องการบุคลากรเพิ่มสูงขึ้นอย่างต่อเนื่องในแต่ละภาคส่วน ทั้งในระดับปฏิบัติการและระดับบริหารจัดการ โดยในปัจจุบัน บุคลากรทางด้านนี้ยังมีอยู่อย่างจำกัดและยังไม่เพียงพอต่อความต้องการ
                                </p>

                                <a href="<?php echo campus_link($page,$fac_link[1])?>" class="btn btn-custom-primary">รู้จักคณะ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
	<?php if($page_branch)$this->load->view(str_replace('.php','_branch.php',$page->link),['page_branch' => $page_branch,'header' => false]);?>
</section>

<div class="bg-white">
	<section id="news" class="mini-spacer feature7 news">
			<div class="container">
					<div class="row">
							<div class="col-lg-8 col-md-9">
									<?php $this->load->view('cdti/include/post/campus_post',['data' => $latest_posts]);?>
									<?php $this->load->view('cdti/include/post/channel_post',['data' => $channel_posts]);?>
							</div>
							<div class="col-lg-4 col-md-3">
									<div class="sidebar">
										<?php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
									</div>
							</div>
					</div>
			</div>
	</section>
</div>

<div class="bg-white">
      	<!-- <?php $this->load->view("cdti/include/gallery_cdti",["main_category1_cover" => $main_category1_cover])?>
      	<?php $this->load->view("cdti/include/partner",["link" => $links])?> -->
</div>
