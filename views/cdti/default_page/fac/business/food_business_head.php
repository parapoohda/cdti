<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/food_main.jpg"
        class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary"><?php echo (html_escape($branch->title)); ?></h3>
        <h3 class="font-light color-primary"><?php echo (html_escape($branch->branch_title_en)); ?></h3>
        <hr class="hr-info">
        <p class="text-dark">
            <?php echo (html_escape($branch->branch_description)); ?>
        </p>
    </div>