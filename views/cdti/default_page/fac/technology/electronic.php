<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาเทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาเทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์<br /> <small>(Electrical and
                                Electronic Technology)</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">เป็นหลักสูตรที่สร้างบัณฑิตที่เป็นคนดี มีฝีมือ มีความรู้ความสามารถ
                            ทางวิชาชีพทางด้านเทคโนโลยีอุตสาหกรรม จัดการศึกษาตามแนวทาง “เรียนคู่งาน งานคู่เรียน”
                            ให้มีสมรรถนะในการทำงาน เท่าทันต่อการเปลี่ยนแปลงทางด้านเทคโนโลยีของอุตสาหกรรมสมัยใหม่
                            เพื่อให้เป็นที่ยอมรับในระดับอาเซียน
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/news/11.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร ?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>
                                        <ul>
                                            <li>
                                                เน้นการเรียนรู้คู่ปฏิบัติงาน (Work and Study Integrated)
                                            </li>
                                            <li>
                                                เน้นบัณฑิต มีวินัย ใฝ่รู้ สู้งานหนักและสื่อสารเป็น
                                            </li>
                                            <li>
                                                จัดการศึกษาร่วมกันระหว่างสถาบันและสถานประกอบการ
                                            </li>
                                            <li>ผลิตบัณฑิตที่มีคุณภาพ ตามความต้องการของสถานประกอบการ
                                                สำเร็จการศึกษาแล้วทำงานได้ทันที</li>
                                            <li>ปฏิบัติงานร่วมกับบริษัทชั้นนำของประเทศที่มีสาขางานที่ครอบคลุม</li>
                                            <li>ส่งเสริมวิจัยและพัฒนานวัตกรรมร่วมกับสถานประกอบการ
                                                เพื่อให้นักศึกษามีประสบการณ์ตรง</li>
                                            <li>ถ่ายทอดความรู้โดยอาจารย์ที่เชี่ยวชาญและเสริมด้วยประสบการณ์จริงของผู้เชี่ยวชาญจากภาคอุตสาหกรรม
                                            </li>
                                        </ul>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_10.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_7.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_11.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>สำเร็จการศึกษาไม่ต่ำกว่ามัธยมศึกษาตอนปลายหรือ ปวช.</p>
                                        <br />
                                        <h4 class="text-primary">การคัดเลือกผู้เข้าศึกษา?</h4>
                                        <p>สอบสัมภาษณ์ พร้อมทดสอบ ทักษะความถนัดทางด้านช่าง</p>
                                        <br />
                                        <h4 class="text-primary">ค่าใช้จ่ายในการศึกษา</h4>
                                        <p class="mb-0">ภาคการศึกษาละ 31,500 บาท* (เหมาจ่ายต่อภาคการศึกษา) </p>
                                        <p class="text-danger">*ทั้งนี้อาจมีการแปลงได้ในภายหลัง</p>
                                        <br />
                                        <h4 class="text-primary">การฝึกงาน (ระยะเวลาและหน่วยที่คู่สัญญา)</h4>
                                        <p>การฝึกงาน (รายวิชาปฏิบัติงานวิชาชีพในสถานประกอบการ) รวมตลอดหลักสูตรเป็นจำนวน ๒ ภาคการศึกษา จำนวน ๑๒ หน่วยกิต โดยได้รับความร่วมมือจากสถานประกอบการชั้นนำของประเทศทั้งทางอุตสาหกรรมโรงงานและสถานประกอบการทางด้านอุตสาหกรรม

                                        </p>
                                        <br />
                                        <h4 class="text-primary">ทุนการศึกษา</h4>
                                        <p>ทุนการศึกษาตามโครงการเรียนดีตลอดการศึกษา 4 ปี และทุนอื่นๆ หลายประเภทด้วยกัน
                                            อาทิ</p>
                                        <ul>
                                            <li>ทุนสนับสนุนการเรียน ค่าเทอม และค่าใช้จ่ายรายเดือน</li>
                                            <li>ทุนสำหรับนักกีฬา</li>
                                            <li>ทุนสำหรับนักศึกษาที่สร้างคุณงามความดี</li>
                                            <li>ทุนสำหรับนักศึกษาที่มีขาดแคลนทุนทรัพย์</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_12.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_9.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_10.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">ปริญญาและสาขาวิชา</h4>
                                        <p class="mb-0">ชื่อเต็ม (ไทย): เทคโนโลยีบัณฑิต (เทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์)</p>
                                        <p class="mb-0">ชื่อย่อ (ไทย): ทล.บ. (เทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์)</p>
                                        <p class="mb-0">ชื่อเต็ม (อังกฤษ): Bachelor of Technology (Electrical and Electronic Technology)</p>
                                        <p class="">ชื่อย่อ (อังกฤษ): B.Tech. (Electrical and Electronic Technology)</p>
                                        <h4 class="text-primary">สาขาวิชาเอก</h4>
										<p>เทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์</p>
										<h4 class="text-primary">แขนง</h4>
										<ul>
                                            <li>เทคโนโลยีไฟฟ้าสำหรับระบบอาคารอัจฉริยะ</li>
                                            <li>เทคโนโลยีอิเล็กทรอนิกส์อุตสาหกรรม</li>
                                            <li>เทคโนโลยีระบบควบคุมอัตโนมัติ</li>
										</ul>
										
                                        <h4 class="text-primary">กลุ่มวิชาที่เปิดสอน</h4>
                                        <p>ธุรกิจอุตสาหกรรมอาหาร ( Food Industry Business)</p>
                                        <p>ธุรกิจบริการอาหาร ( Food Service Business)</p>
										<h4 class="text-primary">โครงสร้างหลักสูตร</h4>
										<h4 class="text-primary">หมวดวิชาศึกษาทั่วไป 33	หน่วยกิต</h4>
                                        
                                        <ul>
                                            <li>กลุ่มวิชาสังคมศาสตร์ 6 หน่วยกิต</li>
                                            <li>กลุ่มวิชามนุษยศาสตร์ 6 หน่วยกิต</li>
                                            <li>กลุ่มวิชาวิทยาศาสตร์และคณิตศาสตร์ 6 หน่วยกิต</li>
                                            <li>กลุ่มวิชาภาษา 15 หน่วยกิต</li>
                                        </ul>
                                        <h4 class="text-primary">หมวดวิชาชีพเฉพาะ 99 หน่วยกิต</h4>
                                        <ul>
                                            <li>กลุ่มวิชาแกนพื้นฐานทางเทคโนโลยี 10 หน่วยกิต</li>
                                            <li>กลุ่มวิชาแกนพื้นฐานเทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์ 32 หน่วยกิต</li>
											<li>กลุ่มวิชาชีพเฉพาะ 40 หน่วยกิต</li>
											<li>หมวดวิชาเลือกไม่น้อยกว่า 7 หน่วยกิต</li>
                                        </ul>
                                        
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_4.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_15.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_14.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">หลักสูตรเทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์</h4>
                                        <p class="">เป้าหมายสำคัญในการผลิตบัณฑิต คณะเทคโนโลยีอุตสาหกรรม สถาบันเทคโนโลยีจิตรลดามีดังนี้</p>
                                        <ul>
                                            <li>ปฏิบัติงานได้ทันทีตามสาขาเฉพาะทาง</li>
                                            <li>มีบุคลิกภาพ วินัย ความคิดสร้างสรรค์ สอดคล้อง ตรงตามลักษณะงานตามสาขา</li>
                                            <li>มีคุณลักษณะใฝ่รู้และสู้งานหนัก</li>
                                            <li>มีสมรรถนะสื่อสารอย่างมีประสิทธิภาพ</li>
                                        </ul>
                                        <h4 class="text-primary">แขนงที่เปิด</h4>
										<p class="text-primary">แขนงวิชาที่ 1 เทคโนโลยีไฟฟ้าสำหรับระบบอาคารอัจฉริยะ (Electrical Technology for Smart Building)</p>
										<p>เพื่อสร้างบัณฑิต ให้เชี่ยวชาญในการออกแบบ ติดตั้ง ตรวจสอบ ระบบต่างๆ  ในอาคาร อาทิ การส่งจ่ายกำลังไฟฟ้า ระบบความปลอดภัย ระบบสื่อสารเตือนภัย อินเตอร์เน็ต กล้องวงจรปิด ระบบทำความเย็นและปรับอากาศ ควบคุมการจ่ายไฟเพื่องานสาธารณูปโภค รวมทั้งระบบควบคุมอาคารอัตโนมัติ</p>
										
										<p class="text-primary">แขนงวิชาที่ 2 เทคโนโลยีอิเล็กทรอนิกส์อุตสาหกรรม (Industrial Electronic Technology)</p>
										<p>เพื่อสร้างบัณฑิต ให้เชี่ยวชาญในการซ่อมวงจรอิเล็กทรอนิกส์ เครื่องมือวัดทางไฟฟ้าและอิเล็กทรอนิกส์ ตรวจสอบและซ่อมบำรุง ชิ้นส่วนควบคุมทางอิเล็กทรอนิกส์ในสายการผลิตอุตสาหกรรม ออกแบบพัฒนาวงจรอิเล็กทรอนิกส์และระบบไมโครคอนโทรลเลอร์ในการควบคุมการผลิต เพื่อช่วยให้การทำงานในระบบอุตสาหกรรมมีเสถียรภาพมากขึ้น</p>
										
										<p class="text-primary">แขนงวิชาที่ 3 เทคโนโลยีระบบควบคุมอัตโนมัติ (Automatic Control System Technology)</p>
										<p> เพื่อสร้างบัณฑิต ให้เชี่ยวชาญในการออกแบบ ติดตั้ง ซ่อมบำรุง ใช้งานระบบอัตโนมัติในงานอุตสาหกรรม อาทิ ระบบสายพานการผลิต ระบบควบคุมเครื่องจักรทั้งแบบที่ควบคุมด้วยพีแอลซีและคอมพิวเตอร์ ระบบการผลิตอาหารแบบอัตโนมัติ หุ่นยนต์อุตสาหกรรม ระบบ DCS และ SCADA</p>
										
										<h4 class="text-primary">4 ปีในบ้าน CDTI</h4>
                                        <p>โดยใช้แนวทางการศึกษาระบบสหกิจศึกษา (Cooperative Education)  มีดังนี้</p>
										<ul>
                                            <li>เรียนรู้คู่กับการทำงาน (WIL ; Work Integrated Learning)</li>
                                            <li>เรียนที่สถาบัน 3 ปี ปฏิบัติงานอาชีพในสถานประกอบการ 1 ปี</li>
                                            <li>มีรายได้ระหว่างปฏิบัติงานอาชีพ</li>
											<li>เรียนภาษาอังกฤษและภาษาที่สาม (จีน ญี่ปุ่น และเยอรมัน) ตามมาตรฐานสากล</li>
											<li>มีปรับพื้นฐานทักษะทางช่างวิศวกรรม เพื่อให้ปฏิบัติงานได้ ทำงานเป็น</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_15.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_3.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_8.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">โอกาศทางธุรกิจหลังสำเร็จการศึกษา</h4>
                                        <ul>
                                            <li>ผู้ประกอบการอิสระ</li>
                                            <li>หัวหน้างานฝ่ายผลิต</li>
											<li>วิศวกรสายปฏิบัติการ</li>
											<li>ผู้ดูแลและบำรุงรักษาระบบอาคารสูง</li>
											<li>วิทยากรฝึกอบรมด้านวิชาชีพ</li>
											<li>นักวิจัยหรือนวัตกร ผู้สร้างสรรค์สิ่งประดิษฐ์</li>
											<li>วิศวกรฝ่ายขาย</li>
                                        </ul>
                                        
                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_2.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_16.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/bachelor/business_adminitration/techno_17.jpg" alt=""
                                                    class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>