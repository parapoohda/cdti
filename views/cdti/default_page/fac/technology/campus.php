<div class="banner-innerpage" style="background-image:url(<?php echo base_url().'/cdti_assets/images/banner_campus1.jpg'?>)">
   <div class="container">
       <div class="row justify-content-center spacer">
           <div class="col-md-6 align-self-center text-center spacer">
              <!--  <h1 class="title">Static Sliders</h1>
               <h6 class="subtitle op-8">You can put any slider in your page</h6> -->
           </div>
       </div>
   </div>
</div>
<div class="whats mini-spacer">
 <div class="container">
   <div class="section-header text-center">
     <h3 class="section-title color-2">คณะเทคโนโลยีอุตสาหกรรม หลักสูตรเทคโนโลยีบัณฑิต (ทล.บ.) </h3>
     <p class="color-2 mb-4">Bachelor of Technology Program in Electrical and Electronic Technology</p>
     <p class="color-3">หลักสูตรเทคโนโลยีบัณฑิต สาขาวิชาเทคโนโลยีไฟฟ้าและอิเล็กทรอนิกส์ B.Tech. (Electrical and Electronic Technology)</p>
     <p class="color-3">เป็นหลักสูตรที่สร้างบัณฑิตที่เป็นคนดี มีฝีมือ มีความรู้ความสามารถ ทางวิชาชีพทางด้านเทคโนโลยีอุตสาหกรรม จัดการศึกษาตามแนวทาง 
     “เรียนคู่งาน งานคู่เรียน” ให้มีสมรรถนะในการทำงาน เท่าทันต่อการเปลี่ยนแปลงทางด้านเทคโนโลยีของอุตสาหกรรมสมัยใหม่ เพื่อให้เป็นที่ยอมรับในระดับอาเซียน</p>
   </div>
 </div>
</div>
<section id="" class="aboutus-innperpage aboutus-innperpage-businessx" style="background: url(<?php echo base_url().'/cdti_assets/images/news/14.jpg'?>)"> 

   <div class="container-fluid">
       <div class="row">
           <div class="col-md-10 col-lg-6 bg-custom-blue-light spacer">
               <div class="row">
                   <div class="col-sm-12 col-md-10 col-lg-8 ml-auto">
                       <div class="about-content  p-20	" >
                           <h3 class="about-title color-yellow mb-3">
                           คณะเทคโนโลยีอุตสาหกรรม
                           </h3>
                           <hr class="mb-3">

                           <div class="about-detail ">
                               <p class="text-white m-b-20">   อุตสาหกรรม 4.0 เป็นแนวนโยบายที่ท้าทายและชี้เป้าที่ต้องการปรับเปลี่ยนโครงสร้างอุตสาหกรรมของประเทศจากอุตสาหกรรมที่ใช้แรงงานเข้มข้นและเทคโนโลยีอย่างง่ายเพื่อผลิตสินค้าและบริการที่ไม่ซับซ้อนและมีมูลค่าเพิ่มต่ำ ไปสู่อุตสาหกรรมที่ขับเคลื่อนด้วยการพัฒนาด้านวิทยาศาสตร์
                               เทคโนโลยี วิจัย และนวัตกรรมเป็นสำคัญ เพื่อผลิตสินค้าและบริการที่ซับซ้อนและมีมูลค่าเพิ่มสูงขึ้นได้อย่างหลากหลายตามความต้องการเฉพาะของผู้บริโภค
                               </p>
                               <p class="text-white m-b-60">   องค์ความรู้ที่เกิดขึ้นจากภาคการศึกษาจะเป็นรากฐานสำคัญของการเพิ่มสมรรถนะ (Capability) ของภาคการผลิต ซึ่งจะนำไปสู่ความสำเร็จในการปฏิรูปครั้งใหม่ 
                               เพื่อให้สามารถผลิตแรงงานและนักเทคโนโลยีให้มีสมรรถนะตามมาตรฐานอาชีพที่ต้องการ</p>

                               <a href="<?php echo campus_link($page,$fac_link[1])?>" class="btn btn-custom-primary">รู้จักคณะ</a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</section>
<section>
 <?php if($page_branch)$this->load->view(str_replace('.php','_branch.php',$page->link),['page_branch' => $page_branch,'header' => false]);?>
</section>

<div class="bg-white">
 <section id="news" class="mini-spacer feature7 news">
   <div class="container">
     <div class="row">
       <div class="col-lg-8 col-md-9">
         <?//php $this->load->view('cdti/include/post/campus_post',['data' => $fac_posts,'page' => $page]);?>
         <?//php $this->load->view('cdti/include/post/channel_post',['data' => $channel_posts,'page' => $page]);?>
       </div>
       <div class="col-lg-4 col-md-3">
         <div class="sidebar">
           <?//php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
         </div>
       </div>
     </div>
   </div>
 </section>
</div>

<div class="bg-white">
   <?php //$this->load->view("cdti/include/gallery_cdti",["main_category1_cover" => $main_category1_cover])?>
   <?php $this->load->view("cdti/include/partner",["link" => $links])?>
</div>
