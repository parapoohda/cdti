<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> ข้อมูลสถิติการให้บริการ</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container mb-5">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-1" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">1. การให้บริการเอกสารทางการศึกษา</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        

                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-01-2563-10.pdf">- เดือนตุลาคม 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-01-2563-11.pdf">- เดือนพฤศจิกายน 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-01-2563-12.pdf">- เดือนธันวาคม 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-01-2564-01.pdf">- เดือนมกราคม 2564</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-01-2564-02.pdf">- เดือนกุมภาพันธ์ 2564</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-01-2564-03.pdf">- เดือนมีนาคม 2564</a><br>

                    </p>
                </div>
            </div>
        </div>
      </section>
      
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-2" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">2. การให้บริการยืมและใช้บริการทรัพยากรสารสนเทศ</a>
                </h4>
            </div>
            <div id="panelBody-2" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-02-2563-10.pdf">- เดือนตุลาคม 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-02-2563-11.pdf">- เดือนพฤศจิกายน 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-02-2563-12.pdf">- เดือนธันวาคม 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-02-2564-01.pdf">- เดือนมกราคม 2564</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-02-2564-02.pdf">- เดือนกุมภาพันธ์ 2564</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O15-02-2564-03.pdf">- เดือนมีนาคม 2564</a><br>
                    </p>
                </div>
            </div>
        </div>
      </section>


      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-2" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">3. สรุปผลการสำรวจความพึงพอใจและความคิดเห็นต่อการดำเนินงานของสถาบัน</a>
                </h4>
            </div>
            <div id="panelBody-2" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/service_statistic/O16-2563.pdf">- ปีการศึกษา 2563</a><br>
                         
                    </p>
                </div>
            </div>
        </div>
      </section>
    </div>
</div>

</style>