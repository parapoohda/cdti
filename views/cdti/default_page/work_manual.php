<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> คู่มือการปฏิบัติงาน</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container mb-5">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-1" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">คู่มือการปฏิบัติงาน</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-0.pdf">- ส่วนนำ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13011.pdf">- ขั้นตอนการปฏิบัติงาน งานบริหารทั่วไปและสื่อสารองค์กร</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13012.pdf">- ขั้นตอนการปฏิบัติงาน งานยุทธศาสตร์ แผน และงบประมาณ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13013.pdf">- ขั้นตอนการปฏิบัติงาน งานการเงินและบัญชี</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13014.pdf">- ขั้นตอนการปฏิบัติงาน งานพัสดุ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13015.pdf">- ขั้นตอนการปฏิบัติงาน งานเทคโนโลยีสารสนเทศ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13016.pdf">- ขั้นตอนการปฏิบัติงาน งานระบบกายภาพและโสตทัศนูปกรณ์</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13017.pdf">- ขั้นตอนการปฏิบัติงาน งานบริหารทรัพยากรมนุษย์และนิติการ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13021.pdf">- ขั้นตอนการปฏิบัติงาน งานพัฒนาคุณภาพการศึกษา</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13022.pdf">- ขั้นตอนการปฏิบัติงาน งานวิจัยและวิรัชกิจ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13023.pdf">- ขั้นตอนการปฏิบัติงาน งานทะเบียนและวัดผล</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13024.pdf">- ขั้นตอนการปฏิบัติงาน งานวิทยทรัพยากร</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13025.pdf">- ขั้นตอนการปฏิบัติงาน งานนวัตกรรมและสื่อการเรียนการสอน</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13026.pdf">- ขั้นตอนการปฏิบัติงาน งานบริการวิชาการ</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13031.pdf">- ขั้นตอนการปฏิบัติงาน งานทุนการศึกษาและบริการนักเรียนนักศึกษา</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/work_manual/O13-13032.pdf">- ขั้นตอนการปฏิบัติงาน งานพัฒนาและติดตามผลนักเรียนนักศึกษา</a><br>
                            
                    </p>
                </div>
            </div>
        </div>
      </section>
      
    </div>
</div>

</style>