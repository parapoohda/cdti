<div class="col-lg-6 order-lg-<?php echo (isset($order) && $order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information6.jpg"
        class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาการตลาด </h3>
        <h3 class="font-light color-primary">Marketing</h3>
        <hr class="hr-info">
        <p class="text-dark">เรียนรู้แนวความคิดการบริหารธุรกิจ และการตลาดสมัยใหม่ ทั้งด้านการขาย การโฆษณา การเงิน
            การบัญชี การวางแผนการตลาด การเขียนแผนธุรกิจอย่างมีคุณภาพ
            เพื่อพัฒนาความสามารถทางด้านธุรกิจและการเป็นผู้ประกอบการมืออาชีพ
            การติดต่อสื่อสารอย่างมีประสิทธิภาพทั้งภาษาไทยและภาษาต่างประเทศทั้งภาษาอังกฤษ ภาษาจีนและภาษาญี่ปุ่น
            เพื่อตอบสนองความต้องการของสถานประกอบการ หรือประกอบกิจการส่วนตัว
            ให้สอดคล้องกับการเปลี่ยนแปลงทางเทคโนโลยียุคปัจจุบัน และสามารถศึกษาต่อในระดับสูงต่อไปได้</p>
    </div>