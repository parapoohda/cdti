<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาเทคโนโลยีสารสนเทศ</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาเทคโนโลยีสารสนเทศ<br /> <small>
                            </small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">
                            เน้นการศึกษาและปฎิบัติงานพื้นฐานอาชีพเทคโนโลยีสารสนเทศตามหลักการและกระบวนการ แสดงความรู้
                            หลักการ และกระบวนการของการเป็นผู้ให้บริการเทคโนโลยีสารสนเทศ
                            นักเรียนจะได้เรียนรู้และฝึกปฏิบัติจริงโดยเริ่มจาก
                            การเรียนงานคอมพิวเตอร์และสารสนเทศเพื่องานอาชีพ ซึ่งเป็นพื้นฐานในการเรียนเพื่อต่อยอดแขนงต่างๆ
                            ของเทคโนโลยีสารสนเทศ ได้แก่ งานเขียนโปรแกรมพัฒนาระบบงาน โปรแกรมประยุกต์ขนาดเล็ก
                            โปรแกรมจัดการงานคอมพิวเตอร์ แก้ไขปรับปรุงโปรแกรมให้เหมาะสมกับความต้องการของผู้ใช้งาน
                            นอกจากนี้ยังศึกษาและปฏิบัติเกี่ยวกับการออกแบบวางแผนการจัดการ ปรับปรุง พัฒนาระบบเครือข่าย
                            ระบบ และโปรแกรมในอุปกรณ์คอมพิวเตอร์พกพา ระบบในอุปกรณ์สมองกลฝังตัว การพัฒนาเกมและแอนนิเมชัน
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information2.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>
                                        <p>สาขาวิชาเทคโนโลยีสารสนเทศและการสื่อสาร โรงเรียนจิตรลดาวิชาชีพ
                                            มีความมุ่งมั่นจะสร้างบุคลากรที่มีความรู้ด้านเทคโนโลยีสารสนเทศควบคู่กันไปกับภาคปฏิบัติ
                                            ด้วยการเรียนการสอนที่เน้นองค์ประกอบ 2 ด้าน คือ ด้านระบบเครือข่าย (Network)
                                            และงานระบบ (System) ทั้งนี้เพื่อรองรับความต้องการขององค์กร
                                            และผู้ใช้งานในองค์กรด้านเทคโนโลยีสารสนเทศ
                                            ตลอดหลักสูตรการเรียนการสอนจะมุ่งเน้นการฝึกฝนทักษะสู่การปฏิบัติจริงโดยมีอุปกรณ์และผู้เชี่ยวชาญจากภาคธุรกิจในห้องปฏิบัติการที่ทันสมัย
                                            ผู้ที่สำเร็จการศึกษาจะมีความสามารถหลากหลายผ่านโครงการคอมพิวเตอร์ธุรกิจ
                                            พร้อมผสานความรู้และประสบการณ์ที่ได้รับจากการศึกษาเพื่อประโยชน์ขององค์กรโดยมุ่งเน้นการใช้เทคโนโลยีที่ทันสมัยในการใช้งานทางธุรกิจเช่น
                                            Business Intelligence (BI) ที่ใช้ในการวิเคราะห์ข้อมูลทางธุรกิจ
                                            การวางแผนทรัพยากรองค์กร (ERP) ใช้ในการวางแผนการจัดการธุรกิจขององค์กร</p>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ผู้เข้าเรียน ต้องมีพื้นความรู้ สําเร็จการศึกษาไม่ต่ำกว่าระดับมัธยมศึกษาปีที่ 3  หรือเทียบเท่าต้องมีอายุไม่ต่ำกว่า
15 ปีบริบูรณ์
</p>
                                        <br />

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">

                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>
                                        <p>---</p>


                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขานี้ ต้องเรียนอะไร</h4>
                                        <p class="">ช่วงแรกจะเป็นการเรียนที่คลอบคลุมในทุกแขนงของคอมพิวเตอร์โดยเริ่มจากวิชาพื้นฐานด้านคอมพิวเตอร์ ได้แก่คอมพิวเตอร์และสารสนเทศเพื่องานอาชีพ  ระบบปฏิบัติการและการใช้โปรแกรมอรรถประโยชน์  คณิตศาสตร์คอมพิวเตอร์  อาชีวอนามัยและความปลอดภัย  คอมพิวเตอร์กราฟิกและการสร้างภาพเคลื่อนไหว
เมื่อเรียนอยู่ในระดับที่สูงขึ้นจะเรียนวิชาเฉพาะทางด้านคอมพิวเตอร์มากขึ้น ได้แก่  เครือข่ายและระบบความปลอดภัยคอมพิวเตอร์  โปรแกรมด้านไมโครซอฟออฟฟิต  การเขียนโปรแกรมคอมพิวเตอร์  การจัดการฐานข้อมูลเบื้องต้น  การพัฒนาเว็บด้วยภาษาเอชทีเอ็มแอล  การสร้างเกมคอมพิวเตอร์เบื้องต้น
นอกจากนี้ยังมีเรียนวิชาทางด้านอิเล็กทรอนิกส์ เพื่อเสริมทักษะทางด้านช่างๆ ได้แก่ ไมโครคอนโทรลเลอร์เบื้องต้น  ดิจิตอลเบื้องต้น  งานไฟฟ้าและอิเล็กทรอนิกส์เบื้องต้น  เทคโนโลยีสมองกลฝังตัว  วงจรพัลส์และดิจิตอล  การผลิตสื่อดิจิตอลและระบบอินเทอร์เฟส

                                        </p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p class="">เมื่อเรียนจบสาขาเทคโนโลยีสารสนเทศแล้ว สามารถไปทำงานเกี่ยวกับ
</p>
                                        <ul>
                                            <li>นักวิชาการด้านเทคโนโลยีสารสนเทศ</li>
                                            <li>นักวิเคราะห์และออกแบบระบบงานด้านสารสนเทศ</li>
                                            <li>นักออกแบบและพัฒนาเว็บ (Web Designer)</li>
                                            <li>ผู้บริหารจัดการระบบฐานข้อมูล  
                                            </li>
                                            <li>ผู้ดูแลระบบสารสนเทศ (IT / Network Support)
 </li>
                                            <li>โปรแกรมเมอร์ (Programmer) </li>
                                            <li>นักข่าวหรือบรรณาธิการนิตยสารสายไอที</li>
                                            <li>คุณครูสอนวิชาคอมพิวเตอร์</li>
                                            
                                        </ul>

                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/Information5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>