 <section class="campus-box  mini-spacer p-b-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url(); ?>cdti_assets/images/seminar.png" alt=""> บุคลากร</h3>
						</div>
						<div class="col-sm-8 text-right">
							<ol class="breadcrumb">
			                   <li class="breadcrumb-item"><a href="<?php echo base_url().$page->slug?>"><?php echo $page->title?></a></li>
			                    <li class="breadcrumb-item active">บุคลากร</li>
			                </ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if($teams):?>
    <?php foreach ($teams as $key => $value):?>
          <div class="teambox mini-spacer <?php echo ($key%2 == 0)?'bg-white':'bg-light-info'?>">
            	<div class="container">
            		<div class="row">
            			<div class="col-md-5 col-lg-4">
            				<img src="<?php echo base_url().$value->path_big; ?>" alt="<?php echo html_escape($value->title); ?>" class="img-responsive img-fluid mx-auto">
            			</div>
            			<div class="col-md-7 col-lg-8">
            				<div class="team-header mb-4">
            					<h4 class="text-primary"><?php echo html_escape($value->title); ?></h4>
            					<p class="color-darkgray"><?php echo html_escape($value->description); ?></p>
            				</div>
            				<div class="team-body">
                          <?php echo $value->description2; ?>
            				</div>
            			</div>
            		</div>
            	</div>
            </div>
    <?php endforeach;?>
<?php endif;?>
 
