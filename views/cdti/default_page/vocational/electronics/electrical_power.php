<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาไฟฟ้า</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาช่างไฟฟ้ากำลัง<br /> <small>Electrical Power Technology
                                Course</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">สาขาที่เกี่ยวข้องกับระบบไฟฟ้า เช่น การเดินสายไฟฟ้าภายในบ้าน ภายในอากาศ
                            การติดตั้งเครื่องใช้ไฟฟ้าต่าง ๆ การตรวจเช็คระบบไฟฟ้า การวงระบบวงจรไฟฟ้า แก้ไขปัญหาต่าง ๆ
                            เกี่ยวกับไฟฟ้า เป็นต้น การจัดกิจกรรมการเรียนการสอนสาขาช่างไฟฟ้า
                            ให้ความสำคัญกับการสร้างความรู้พื้นฐานด้านระบบไฟฟ้า การติดตั้ง การเดินสายไฟฟ้า
                            การต่อวงจรไฟฟ้าในลักษณะต่าง ๆ การตรวจเช็คระบบไฟฟ้า การตรวจซ่อมอุปกรณ์ไฟฟ้า เป็นต้น
                            สาขาวิชาช่างไฟฟ้า ให้ความสำคัญกับการจัดการเรียนการสอนทั้งภาคทฤษฎีและภาคปฏิบัติควบคู่กัน</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician6.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>
                                        <p>เป็นสาขาที่ได้เรียนรู้เทคโนโลยีที่ทันสมัย  มีการพัฒนาอย่างต่อเนื่อง เพื่อรองรับเทคโนโลยีในอนาคต อุปกรณ์ในสาขามีได้รับการสนับสนุน จากสถานประกอบการทางด้านเทคโนโลยีไฟฟ้า ชั้นนำของประเทศ เครื่องใช้ไฟฟ้าต่าง ๆ เป็นเครื่องอำนวยความสะดวกที่จำเป็นต่อการดำรงชีวิต การใช้ไฟฟ้าการแก้ไขปัญหาต่าง ๆ จำเป็นต้องอาศัยผู้มีความชำนาญเฉพาะทาง เช่น ช่างไฟฟ้าช่วยแก้ไขปัญหาต่าง ๆ ที่เกิดขึ้นกับไฟฟ้า และที่สำคัญคือ ทุกบ้านมีไฟฟ้าใช้ในชีวิตประจำวัน จึงได้ศึกษาเรียนรู้กับอุปกรณ์และเครื่องมือที่ทันสมัย และ   ได้เข้าฝึกทักษะวิชาชีพ จากสถานประกอบการที่มีคุณภาพ และ สามารถสร้างโอกาสในการมีอาชีพ กับหน่วยงานที่มีมาตรฐาน</p>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ระดับ ปวช.   รับนักเรียนที่เรียนจบ ระดับชั้นมัธยมศึกษาชั้นปีที่  3  เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.50</p>
                                        <br />

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">

                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>
                                        <p>---</p>
                                       

                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician10.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชาไฟฟ้ากำลัง เรียนอะไร</h4>
                                        <p class="">เรียนรู้และฝึกฝนพัฒนาด้านวิชาช่างไฟฟ้ากำลัง  วิเคราะห์วงจรไฟฟ้า  จะเป็นการคำนวณหาค่ากระแสไฟฟ้า2 เฟส และ 3 เฟส ศึกษากฎของโอห์ม กำลังไฟฟ้า พลังงานไฟฟ้า วงจรความต้านทานแบบต่างๆ วงจรแบ่งกระแสไฟฟ้า  วงจรแบ่งแรงดันไฟฟ้า  การแปลงวงจรความต้านทานเดลต้า-สตาร์  ดีเทอร์มิแนนต์  เซลล์ไฟฟ้า  กฎของเคอร์ชอฟฟ์  วงจรบริดจ์  เมชเคอร์เรนท์  การติดตั้งไฟฟ้าในอาคารและโรงงาน การควบคุมเครื่องกลไฟฟ้า หม้อแปลงไฟฟ้าเครื่องมือวัด โปรแกรมและการควบคุมเครื่องกลไฟฟ้า  นิวเมติกส์ไฮดรอลิกเบื้องต้น  เครื่องทำความเย็นและเครื่องปรับอากาศ อิเล็กทรอนิกส์อุตสาหกรรม โดยเน้นให้มีคุณธรรมในวิชาชีพ   ใส่ใจอาชีวอนามัยและความปลอดภัยในการทำงาน   และสามารถศึกษาต่อในระดับสูงต่อไปได้</p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p class="">เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ  ในหลายด้านเช่น</p>
                                        <ul>
                                            <li>งานช่างไฟฟ้า ที่เกี่ยวของกับ การเดินสายไฟฟ้าภายในบ้าน ภายในอากาศ การติดตั้งเครื่องใช้ไฟฟ้าต่าง ๆ การตรวจเช็คระบบไฟฟ้า การวางระบบวงจรไฟฟ้า แก้ไขปัญหาต่าง ๆ</li>
                                            <li>ช่างซ่อมอุปกรณ์ไฟฟ้า</li>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน  ทำงานเกี่ยวกับ ผู้ช่วยวิศวกรไฟฟ้า หรือพนักงานตรวจซ่อมเครื่องใช้ไฟฟ้าประจำบริษัท</li>
                                            <li>ช่างติดตั้งอุปกรณ์ไฟฟ้าต่าง ๆ เกี่ยวกับ การดูแลซ่อมบำรุง การติดตั้งไฟฟ้าในอาคารและโรงงานติดตั้งเครื่องมือ  เครื่องจักรอัตโนมัติ</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>
                                            <li>บริหารงานหน่วยงานด้านช่างเทคนิคในองค์กรของรัฐและเอกชนที่มีมาตรฐาน  เช่น พนักงานการไฟฟ้าฝ่ายผลิต หรือการไฟฟ้าส่วนภูมิภาค  เป็นต้น</li>
                                            <li>เจ้าของกิจการซ่อมเครื่องใช้ไฟฟ้าและติดตั้งเครื่องใช้ไฟฟ้า</li>
                                            <li>เจ้าของกิจการจำหน่ายเครื่องใช้ไฟฟ้าและติดตั้งเครื่องใช้ไฟฟ้า</li>
                                            <li>งานด้านวิจัยพัฒนา ผลิตภัณฑ์ด้านเทคโนโลยีสมัยใหม่ เพื่อรองรับความต้องการของโลกในอนาคต</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>

                                        </ul>

                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician10.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>