<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาไฟฟ้า</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาอิเล็กทรอนิกส์<br /> <small>Electronic</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">การใช้ชีวิตในปัจจุบัน อุปกรณ์อิเล็กทรอนิกส์เข้ามามีบทบาท
                            กับการดำเนินชีวิตประจำวัน อุปกรณ์ที่ใช้อำนวยความสะดวก
                            ล้วนแต่ใช้เทคโนโลยีที่มีวงจรอิเล็กทรอนิกส์ในการควบคุม และเป็นส่วนประกอบแทบทั้งสิ้น เช่น
                            โทรทัศน์ โทรศัพท์มือถือ วิทยุสื่อสาร คอมพิวเตอร์
                            ระบบเซนเซอร์เพื่อควบคุมการทำงานแบบอัตโนมัติต่าง ๆ จึงถือได้ว่า
                            งานอิเล็กทรอนิกส์เป็นงานที่มีความสำคัญและมีความต้องการอย่างมากในวิชาชีพปัจจุบัน</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics2.jpg"
                        alt="" class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>
                                        <p>เป็นสาขาที่ได้เรียนรู้เทคโนโลยีที่ทันสมัย มีการพัฒนาอย่างต่อเนื่อง
                                            เพื่อรองรับเทคโนโลยีในอนาคต อุปกรณ์ในสาขามีได้รับการสนับสนุน
                                            จากสถานประกอบการทางด้านเทคโนโลยีอิเล็กทรอนิกส์ชั้นนำของประเทศ
                                            จึงได้ศึกษาเรียนรู้กับอุปกรณ์และเครื่องมือที่ทันสมัย และ
                                            ได้เข้าฝึกทักษะวิชาชีพ จากสถานประกอบการที่มีคุณภาพ และ
                                            สามารถสร้างโอกาสในการมีอาชีพ กับหน่วยงานที่มีมาตรฐาน</p>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ระดับ ปวช. รับนักเรียนที่เรียนจบ ระดับชั้นมัธยมศึกษาชั้นปีที่ 3
                                            เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.50</p>
                                        <p>ระดับ ปวส. รับนักเรียนที่เรียนจบ ระดับ ปวช. ชั้นปีที่ 3
                                            จากสาขาวิชาอิเล็กทรอนิกส์ สาขาวิชาไฟฟ้ากำลัง เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.75
                                        </p>
                                        <br />

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">

                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>
                                        <p>---</p>


                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชาอิเล็กทรอนิกส์ เรียนอะไร</h4>
                                        <p class="">เรียนรู้ความปลอดภัย และอาชีวอนามัยในการทำงาน วิชาพื้นฐานช่าง
                                            การคำนวนวงจรไฟฟ้า การวิเคราะห์วงจรอิเล็กทรอนิกส์
                                            การเขียนแบบวงจรอิเล็กทรอนิกส์ การใช้เครื่องมือวัดทางไฟฟ้าและอิเล็กทรอนิกส์
                                            ระบบสื่อสาร คลื่นสัญญาน ดิจิตอลเทคนิค ระบบภาพและเสียง ระบบโทรคมนาคม
                                            เครือข่ายคอมพิวเตอร์ ไมโครคอลโทรเลอร์ และ การเขียนโปรแกรมคอมพิวเตอร์</p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p class="">เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ
                                            ในหลายด้านเช่น</p>
                                        <ul>
                                            <li>งานเทคโนโลยีโทรคมนาคม ที่เกี่ยวของกับ ระบบโทรศัพท์  ระบบส่งสัญญานดาวเทียม  ระบบอินเตอร์เน็ต  กับบริษัท ด้านการสื่อสารโทรคมนาคมที่มีชื่อเสียง    งานด้านการสื่อสารวิทยุการบิน
</li>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน  ทำงานเกี่ยวกับ การดูแลซ่อมบำรุง ติดตั้งเครื่องมือ เครื่องจักรอัตโนมัติ  ระบบการควบคุมแบบรีโมตคอนโทรล    ระบบเซนเซอร์ และ การวัดค่าด้วยเครื่องมือวัดอิเล็กทรอนิกส์</li>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน ทำงานเกี่ยวกับ
                                                ผู้ช่วยวิศวกรไฟฟ้า หรือพนักงานตรวจซ่อมเครื่องใช้ไฟฟ้าประจำบริษัท</li>
                                            <li>งานด้านบันเทิง ทำงานเกี่ยวกับการควบคุมระบบภาพ ระบบเสียง ในสตูดิโอ ต่าง ๆ   การติดตั้ง ระบบแสงสีเสียง ในงานคอนเสิร์ต  และ งานพิธีการต่าง ๆ  การควบคุมโดรน โดยเป็นงานที่กำลังต้องการ ผู้มีความชำนาญเฉพาะทางด้านนี้อย่างมาก
                                            </li>
                                            <li>งานด้านวิจัยพัฒนา ผลิตภัณฑ์ด้านเทคโนโลยีสมัยใหม่ เพื่อรองรับความต้องการของโลกในอนาคต </li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>

                                        </ul>

                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electronics8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>