<div class="col-lg-6 order-lg-<?php echo ($order > 0)?12:0?>">
    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/electrician6.jpg" class="rounded img-shadow img-responsive" alt="wrapkit" />
</div>
<div class="col-lg-6 order-lg-1">
    <div class="text-box">
        <h3 class="font-light color-primary">สาขาวิชาช่างไฟฟ้ากำลัง </h3>
        <h3 class="font-light color-primary">Electrical Power Technology Course</h3>
        <hr class="hr-info">
        <p class="text-dark">สาขาที่เกี่ยวข้องกับระบบไฟฟ้า เช่น การเดินสายไฟฟ้าภายในบ้าน ภายในอากาศ การติดตั้งเครื่องใช้ไฟ
ฟ้าต่าง ๆ การตรวจเช็คระบบไฟฟ้า การวงระบบวงจรไฟฟ้า แก้ไขปัญหาต่าง ๆ เกี่ยวกับไฟฟ้า เป็นต้น
การจัดกิจกรรมการเรียนการสอนสาขาช่างไฟฟ้า ให้ความสำคัญกับการสร้างความรู้พื้นฐานด้านระบบไฟ
ฟ้า การติดตั้ง การเดินสายไฟฟ้า การต่อวงจรไฟฟ้าในลักษณะต่าง
ๆ การตรวจเช็คระบบไฟฟ้า การตรวจซ่อมอุปกรณ์ไฟฟ้า เป็นต้น
สาขาวิชาช่างไฟฟ้า ให้ความสำคัญกับการจัดการเรียนการสอนทั้งภาคทฤษฎีและภาคปฏิบัติควบคู่กัน</p>
    </div>