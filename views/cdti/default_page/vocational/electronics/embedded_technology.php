<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาไฟฟ้า</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาเทคโนโลยีระบบสมองกลฝังตัว<br /> <small>Embedded System
                                Course</small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">การนำระบบสมองกลฝังตัวมาประยุกต์ใช้ทุกวงการ
                            และมีบทบาทสำคัญในชีวิตประจำวันของเราอย่างกว้างขวาง เช่น ยานยนต์ สมาร์ทโฟน
                            เครื่องมือวัดทางการแพทย์ หม้อหุงข้าว เครื่องซักผ้า เตาไมโครเวฟ เครื่องเล่นเกม เป็นต้น
                            ซึ่งนับเป็นเทคโนโลยีที่ได้รับความสนใจจากภาคอุตสาหกรรมเพื่อนำมาพัฒนาสร้างสรรค์ผลิตภัณฑ์ให้ก้าวลํ้ามีคุณภาพมากขึ้น
                            ในอนาคตอันใกล้ ประเทศไทยจะก้าวเป็นศูนย์กลางของประชาคมเศรษฐกิจอาเซียน
                            เราจำเป็นที่จะต้องพัฒนาประเทศด้วยวิทยาศาสตร์ และเทคโนโลยีต่าง ๆ ดังเช่น ระบบสมองกลฝังตัว
                            ซึ่งเป็นประโยชน์ต่อการพัฒนานวัตกรรมใหม่ ๆ สร้างมูลค่าเพิ่มให้ผลิตภัณฑ์และอุตสาหกรรมของไทย
                            เสริมสร้างคุณภาพชีวิตประชาชน เศรษฐกิจ ที่ยั่งยืน และลดการนำเข้า
                            โดยจะต้องใช้เทคโนโลยีแบบไมโครคอนโทรล เลอร์ มิเตอร์อัจฉริยะ (Smart Meter)
                            การสื่อสารข้อมูลแบบเครือข่ายไร้สาย Zigbee การถ่ายโอนข้อมูลแบบ NFC และการควบคุมผ่าน สมาร์ทโฟน
                            ในการพัฒนาระบบได้ต่อไป</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology12.jpg"
                        alt="" class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>
                                        <p>เป็นสาขาที่ได้เรียนรู้เทคโนโลยีที่ทันสมัย มีการพัฒนาอย่างต่อเนื่อง
                                            เพื่อรองรับเทคโนโลยีในอนาคต อุปกรณ์ในสาขามีได้รับการสนับสนุน
                                            จากสถานประกอบการทางด้านเทคโนโลยีอิเล็กทรอนิกส์ ระบบสมองกลฝังตัว
                                            ชั้นนำของประเทศ จึงได้ศึกษาเรียนรู้กับอุปกรณ์และเครื่องมือที่ทันสมัย และ
                                            ได้เข้าฝึกทักษะวิชาชีพ จากสถานประกอบการที่มีคุณภาพ และ
                                            สามารถสร้างโอกาสในการมีอาชีพ กับหน่วยงานที่มีมาตรฐาน</p>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <ul>
                                            <li>ระดับ ปวช.   รับนักเรียนโควตาเรียนดี  ที่เรียนจบ ระดับชั้นมัธยมศึกษาชั้นปีที่  3  
                                            </li>
                                            <li>สัญชาติไทยและมีบัตรประจำตัวประชาชน</li>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน ทำงานเกี่ยวกับ
                                                ผู้ช่วยวิศวกรไฟฟ้า หรือพนักงานตรวจซ่อมเครื่องใช้ไฟฟ้าประจำบริษัท</li>
                                            <li>มีระดับผลการเรียนเฉลี่ยสะสมรวมทุกรายวิชา 5 ภาคเรียน  ไม่ต่ำกว่า 2.75
                                            </li>
                                            <li>มีระดับผลการเรียนเฉลี่ยรายวิชาคณิตศาสตร์ ไม่ต่ำกว่า 2.75</li>
                                            <li> มีระดับผลการเรียนเฉลี่ยรายวิชาวิทยาศาสตร์ ไม่ต่ำกว่า 2.75</li>
                                            <li>มีความสามารถในการสื่อสารภาษาต่างประเทศ (ภาษาอังกฤษ)</li>
                                            <li>มีความประพฤติเรียบร้อย</li>
                                            <li>ไม่เป็นโรคติดต่อร้ายแรง ซึ่งเป็นอุปสรรคต่อการศึกษา</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">

                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>
                                        <p>---</p>


                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชาเทคโนโลยีระบบสมองกลฝังตัว เรียนอะไร</h4>
                                        <p class="">เรียนรู้และฝึกฝนทักษะด้าน อิเล็กทรอนิกส์ และไฟฟ้า การออกแบบโครงสร้างและใช้เครื่องมือกลสร้างชิ้นส่วนและอุปกรณ์ การเขียนโปรแกรมคอมพิวเตอร์เพื่อควบคุมระบบสมองกลฝังตัว  การใช้งานแอพพลิเคชันเพื่อควบคุมสั่งการ  ผ่านระบบเครือข่ายคอมพิวเตอร์และระบบการสื่อสารควบคุมแบบไร้สาย  การสร้างสิ่งประดิษฐ์ที่ควบคุมด้วยระบบสมองกลฝังตัว ด้วยกระบวนการทางด้านวิทยาศาสตร์ เทคโนโลยี วิศวกรรมศาสตร์ และ คณิตศาสตร์ โดยเน้นให้มีคุณธรรมในวิชาชีพ   ใส่ใจอาชีวอนามัยและความปลอดภัยในการทำงาน  และสามารถศึกษาต่อในระดับสูงต่อไปได้</p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology10.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology11.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology12.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p class="">เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ  ในสาขาวิชาระบบสมองกลฝังตัว ซึ่งการเรียนจะเน้นตลาดด้านอุตสาหกรรมโดยเฉพาะ ซึ่งจะมีมาตรฐานสากลเป็นตัวกำหนดรายละเอียดในแต่ละระดับ ซึ่งกลุ่มประเภทอุตสาหกรรมที่เกี่ยวข้องในงานอัตโนมัติจะแบ่งได้เป็น 3 กลุ่มคือ เช่น</p>
                                        <ul>
                                            <li>1. Process Control เป็นอุตสาหกรรมที่มีวัตถุดิบจะมีการไหลอย่างต่อเนื่องในระบบและจะต้องมีการทำงานตลอดเวลา อุตสาหกรรมเล่านี้ ได้แก่ ผู้เชี่ยวชาญเฉพาะทางระบบสมองกลฝังตัว  โรงกลั่นน้ำมัน  โรงแยกก๊าซ โรงงานปิโตรเคมี  โรงงานผลิตปูนซีเมนต์โรงไฟฟ้า เป็นต้น 
                                            </li>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน ทำงานเกี่ยวกับ
                                                การดูแลซ่อมบำรุง ติดตั้งเครื่องมือ เครื่องจักรอัตโนมัติ
                                                ระบบการควบคุมแบบรีโมตคอนโทรล ระบบเซนเซอร์ และ
                                                การวัดค่าด้วยเครื่องมือวัดอิเล็กทรอนิกส์</li>
                                            <li>2. Manufacturing Process เป็นอุตสาหกรรมที่เน้นไปในลักษณะการประกอบชิ้นส่วน ซึ่งในขั้นตอนการผลิตนั้นอาจจะไม่ต่อเนื่อง หรือแยกส่วนกันทำได้ เช่น วิศวกรสมองกลฝังตัว วิศวกรซอฟต์แวร์ ในโรงงานประกอบรถยนต์  โรงงานผลิตเครื่องใช้ไฟฟ้า โรงงานผลิตชิ้นส่วนยานยนต์ เป็นต้น</li>
                                            <li>3. Batch Process เป็นอุตสาหกรรมที่กึ่ง Process Control และ Manufacturing Process โดยที่รูปแบบการผลิตสามารถปรับเปลี่ยนไปได้ตามผลิตภัณฑ์ เช่น นักพัฒนาซอฟต์แวร์    ผู้ช่วยนักวิจัยพัฒนาอุตสาหกรรมยา  อุตสาหกรรมอาหารและเครื่องดื่ม เป็นต้น

                                            </li>
                                            <li>ผู้บริหารงานด้านบันเทิง ทำงานเกี่ยวกับการควบคุมระบบภาพ ระบบเสียง ในสตูดิโอ ต่าง ๆ   การติดตั้ง ระบบแสงสีเสียง ในงานคอนเสิร์ต  และ งานพิธีการต่าง ๆ  </li>
                                            <li>สามารถศึกวิศวกรคอมพิวเตอร์ การควบคุมโดรน โดยเป็นงานที่กำลังต้องการ ผู้มีความชำนาญเฉพาะทางด้านนี้อย่างมากษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>
                                            <li>ประกอบกิจการในด้านเทคโนโลยีระบบสมองกลฝังตัวของตนเอง</li>
                                            <li>งานด้านนักวิจัยพัฒนา ผลิตภัณฑ์ด้านเทคโนโลยีสมัยใหม่ เพื่อรองรับความต้องการของโลกในอนาคต</li>
                                            <li>อาจารย์สังกัดหน่วยงานของรัฐและเอกชน</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>

                                        </ul>

                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology13.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology14.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/electronics/systemtechnology5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>