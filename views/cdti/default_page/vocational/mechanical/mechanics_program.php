<section class="campus-box mini-spacer pb-0">
    <div class="container">
        <div class="section-header campus-header mb-4">
            <div class="row">
                <div class="col-sm-4 align-middle">
                    <h3 class="campus-title mt-4 text-primary"><img
                            src="<?=base_url()?>/cdti_assets/images/icon/005-mortarboard-1.png" alt=""> หลักสูตร/สาขา
                    </h3>
                </div>
                <div class="col-sm-8 text-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="<?php echo base_url().$page_main->slug?>"><?php echo $page_main->title?></a></li>
                        <li class="breadcrumb-item"><a
                                href="<?php echo campus_link($page_main,$fac_link[2])?>">สาขาที่เปิดสอน</a></li>
                        <li class="breadcrumb-item active">สาขาวิชาช่างยนต์</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="campus-content p-t-20">
            <div class="row">
                <div class="col-md-6">
                    <div class="campus-detail">
                        <h4 class="text-primary">สาขาวิชาช่างยนต์<br /> <small>
                                </small></h4>
                        <hr class="hr-primary">
                        <p class="text-indent">สาขาวิชาช่างยนต์ คือ สาขาวิชาที่เกี่ยวข้องกับการซ่อมบำรุงรถยนต์
                            รถจักรยานยนต์ เครื่องยนต์ต่าง ๆ ที่นิยมใช้ในการเกษตร และอื่น ๆ
                            สาขาช่างยนต์มีการจัดการเรียนการสอนเน้นการฝึกทักษะ ประสบการณ์ และการทดลองสิ่งใหม่
                            การจัดกิจกรรมการเรียนรู้สาขาช่างยนต์ เช่น ความรู้พื้นฐานเกี่ยวกับเครื่องยนต์ต่าง ๆ
                            การใช้อุปกรณ์ในงานช่าง วัสดุเชื้อเพลิง การถอดประกอบเครื่องยนต์ การคำนวณต่าง ๆ
                            ที่จำเป็นต่องานช่าง วิชากลศาสตร์ วิชาเทคโนโลยีสมัยใหม่ เป็นต้น การเรียนสาขา
                            ช่างยนต์เป็นสาขาวิชาที่เน้นการฝึกปฏิบัติกับทฤษฎีควบคู่กันและมีการปรับเปลี่ยนเทคนิคการสอนและฝึกปฏิบัติตามเทคโนโลยีสมัยใหม่
                            ให้สอดคล้องกับการพัฒนาเทคโนโลยียานยนต์</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics11.jpg" alt=""
                        class="img-responsive img-responsive">
                </div>
            </div>
        </div>

    </div>

    <div class="campus-tabs ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills campus-tabs-nav nav-fill m-t-40">
                        <div class="slider"></div>
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab1.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จุดเด่นของสาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab2.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>คุณสมบัติของผู้เข้าศึกษา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab3.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>โครงสร้างหลักสูตร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab4.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>สาขานี้เรียนอะไร?</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <img src="<?php echo base_url(); ?>cdti_assets/images/icon/tab5.png" alt=""
                                    class="d-block mx-auto"> <br />
                                <p>จบแล้วมีงานทำ</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ligt-gray p-t-30 p-b-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content br-n pn p-t-30">
                            <div id="navpills-1" class="tab-pane fadeIn animated slow active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">จุดเด่นของสาขา</h4>
                                        <p>เป็นสาขาที่ได้เรียนรู้เทคโนโลยีที่ทันสมัย มีการพัฒนาอย่างต่อเนื่อง
                                            เพื่อรองรับเทคโนโลยีในอนาคต อุปกรณ์ในสาขามีได้รับการสนับสนุน
                                            จากสถานประกอบการทางด้านเครื่องยนต์
                                            ผู้มีความรู้ความชำนาญมีทักษะในเรื่องของเครื่องยนต์กลไกต่างๆ
                                            จัดเป็นอีกหนึ่งสาขาวิชาที่ชายหนุ่มหลายคนต้องการจะเข้าไปศึกษาต่อ
                                            อันเนื่องมาจากนิสัยโดยพื้นฐานของเด็กผู้ชายส่วนมาก มักจะชอบการสร้าง
                                            การดัดแปลง โดยเฉพาะในเรื่องของเครื่องยนต์กลไกอยู่แล้วก็ตาม บวกกับ
                                            ความหลงใหลในเรื่องของรถยนต์รุ่นเท่ๆ รวมทั้งมีตลาดแรงงานรองรับค่อนข้างสูง
                                            จึงทำให้‘ช่างยนต์’ เป็นอีกหนึ่งอาชีพที่ได้รับความนิยมเป็นอันดับต้นๆ
                                            และด้วยทุก ๆ วันนี้ แต่ละครอบครัวมีรถอย่างต่ำบ้านละ 1 คันขึ้นไป
                                            โดยส่วนใหญ่แล้วมีมากกว่า 1 คัน ซึ่งการใช้รถจะมีระยะเวลาบำรุงรักษาทุก ๆ รอบ
                                            เช่น 3 เดือนครั้ง หรือ 6 เดือนครั้ง หรือตามสภาพรถ
                                            โอกาสในความเจริญก้าวหน้าของช่างยนต์จึงมีอัตราการเจริญเติบโตสูงมาก
                                            และสามารถประกอบส่วนตัวได้</p>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics12.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics4.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-2" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-primary">คุณสมบัติของผู้เข้าศึกษา</h4>
                                        <p>ระดับ ปวช.   รับนักเรียนที่เรียนจบ ระดับชั้นมัธยมศึกษาชั้นปีที่  3  เกรดเฉลี่ยสะสมไม่ต่ำกว่า 2.50</p>
                                        <br />

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics10.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics2.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics9.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="navpills-3" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">

                                        <h4 class="text-primary">โครงสร้างหลักสูตร</h4>
                                        <p>---</p>


                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics6.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics7.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics8.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-4" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <h4 class="text-primary mb-2">สาขาวิชาไฟฟ้ากำลัง เรียนอะไร</h4>
                                        <p class="">เรียนรู้และฝึกฝนทักษะด้านงานยานยนต์ การอ่านแบบเขียนแบบยานยนต์ การบริการตรวจซ่อมเครื่องยนต์ ซ่อมบำรุงรถยนต์ รถจักรยานยนต์ ระบบส่งกำลังรถยนต์ เปลี่ยนอุปกรณ์ภายในรถหรือเครื่องยนต์ เครื่องล่างรถยนต์และไฟฟ้ารถยนต์ เปลี่ยนถ่ายน้ำมันต่าง ๆ เชื้อเพลิงและวัสดุหล่อลื่น เช็คระบบวงจร รถระบบปรับอากาศรถยนต์ นิวเมติกส์ไฮดรอลิกเบื้องต้น เคาะพ่นสี  ตั้งศูนย์ถ่วงล้อ แก้ไขปัญหาต่าง ๆ ที่เกี่ยวของกับเครื่องยนต์ เป็นต้น โดยเน้นให้มีคุณธรรมในวิชาชีพ   ใส่ใจอาชีวอนามัยและความปลอดภัยในการทำงาน และสามารถศึกษาต่อระดับสูงต่อไปได้
</p>

                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics1.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics3.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics5.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills-5" class="tab-pane fadeIn animated slow">
                                <div class="row">
                                    <div class="col-md-6 6 order-md-12">
                                        <h4 class="text-primary mb-2">จบแล้วมีงานทำ</h4>
                                        <p class="">เมื่อจบการศึกษาสามารถเข้าทำงานในสถานประกอบการ หรือ องค์กรของรัฐ  ในหลายด้านเช่น   </p>
                                        <ul>
                                            <li>ช่างยนต์</li>
                                            <li>นายช่างประจำศูนย์บริการ</li>
                                            <li>ในภาคอุตสาหกรรมการผลิตกับผู้ประกอบการระดับมาตรฐาน  ทำงานเกี่ยวกับ ผู้ช่วยวิศวกรโรงงาน หรือพนักงานตรวจซ่อมเครื่องกลประจำโรงงาน </li>
                                            <li>บริหารงานหน่วยงานด้านช่างเทคนิคในองค์กรของรัฐและเอกชนที่มีมาตรฐาน  เช่น พนักงานซ่อมบำรุงเครื่องยนต์ หรือเป็นผู้ประกอบกิจการในภาคอุตสาหกรรม  เป็นต้น

                                            </li>
                                            <li>เจ้าของกิจการซ่อมบำรุงเครื่องยนต์ </li>
                                            <li>เจ้าของกิจการคาร์แคร์</li>
                                            <li>เจ้าของกิจการอู่รถ</li>
                                            <li>เจ้าของกิจเต๊นรถ</li>
                                            <li>เจ้าของกิจการจำหน่ายอะไหล่รถ</li>
                                            <li>พนักงานทั่วไป</li>
                                            <li>พนักงานขนส่งสินค้า</li>
                                            <li>งานด้านวิจัยพัฒนา ผลิตภัณฑ์ด้านเทคโนโลยีสมัยใหม่ เพื่อรองรับความต้องการของโลกในอนาคต</li>
                                            <li>สามารถศึกษาต่อในระดับการศึกษาที่สูงขึ้นไปได้</li>

                                        </ul>

                                        <!-- <h4 class="text-primary">สถานประกอบการชั้นนำ</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="partner-slider" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/1.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/2.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/3.png"
                                                            alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo base_url(); ?>cdti_assets/images/partner/4.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6 6 order-md-1 ">
                                        <div class="row">
                                            <div class="col-sm-12 mb-4">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics14.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics16.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo base_url(); ?>cdti_assets/images/vocational/commercial/mechanics17.jpg"
                                                    alt="" class="img-responsive m-b-10">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".campus-tabs .nav-pills a").click(function() {
    var position = $(this).parent().position();
    var width = $(this).parent().width();
    $(".campus-tabs .slider").css({
        "left": +position.left,
        "width": width
    });
});
var actWidth = $(".campus-tabs .nav-pills").find(".active").parent("li").width();
var actPosition = $(".campus-tabs .nav-pills .active").position();
$(".campus-tabs .slider").css({
    "left": +actPosition.left,
    "width": actWidth
});
</script>