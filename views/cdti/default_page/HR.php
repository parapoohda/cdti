



wait for page annual_budget_plan<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<section class="campus-box campus-box-calendar p-t-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header">
					<div class="row">
						<div class="col-sm-8 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><img src="<?php echo base_url()?>/assets/images/002-newspaper.png" alt=""> การบริหารและพัฒนาทรัพยากรมนุษย์</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="container mb-5">

    <div id="accordion" class="panel-group">
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-1" class="accordion-toggle active" data-toggle="collapse"
                        data-parent="#accordion">1. นโยบายการบริหารทรัพยากรมนุษย์</a>
                </h4>
            </div>
            <div id="panelBody-1" class="panel-collapse collapse show">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/oit/oit_o25.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่องนโยบายการบริหารทรัพยากรบุคคลและการพัฒนาบุคลากรของสถาบัน</a><br>
                            
                    </p>
                </div>
            </div>
        </div>
      </section>
     
     
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-2" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">2. การดำเนินการตามนโยบายการบริหารทรัพยากรมนุษย์</a>
                </h4>
            </div>
            <div id="panelBody-2" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/oit/oit_o26.pdf">- ปีงบประมาณ พ.ศ. 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/HR/O26-2564.pdf">- ปีงบประมาณ พ.ศ. 2564</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/HR/O26-2565.pdf">- ปีงบประมาณ พ.ศ. 2565</a><br>
                            
                    </p>
                </div>
            </div>
        </div>
      </section>

      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-3" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">3. หลักเกณฑ์การบริหารและพัฒนาทรัพยากรมนุษย์</a>
                </h4>
            </div>
            <div id="panelBody-3" class="panel-collapse collapse">
                <div class="panel-body">
                  <h5 class="panel-title">การสรรหา คัดเลือก บรรจุ และแต่งตั้งบุคลากร</h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_01.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล พ.ศ. 2561</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_07.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยเทคโนโลยีจิตรลดาว่าด้วย ว่าด้วยการบริหารงานบุคคล (ฉบับที่ 2) พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_13.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการบริหารงานบุคคล (ฉบับที่ 3) พ.ศ. 2562</a><br>
                    
                  </p>
                  <h5 class="panel-title">การประเมินผลการปฏิบัติงาน</h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_08.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายวิชาการ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_09.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง มาตรฐานและเกณฑ์ภาระงานของผู้ปฏิบัติงานสายสนับสนุน พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_11.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง กำหนดเกณฑ์คะแนนทดสอบความสามารถทางภาษาอังกฤษของอาจารย์ประจำ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_21.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน พ.ศ. 2563</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_22.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง หลักเกณฑ์และวิธีการประเมินผลการปฏิบัติงานของผู้ปฏิบัติงานในสถาบัน (ฉบับที่ 2) พ.ศ. 2563</a><br>
                  </p>
                  <h5 class="panel-title">การพัฒนาบุคลากร</h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_18.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง ทุนการศึกษาต่อระดับปริญญาโทหรือปริญญาเอกแบบลาศึกษาบางเวลา พ.ศ. 2563</a><br> 
                  </p>
                  <h5 class="panel-title">การให้คุณให้โทษและสร้างขวัญกำลังใจ</h5>
                  <p class="pt10-pl30">
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_04.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง วันเวลาปฏิบัติงาน วันหยุดและวันลา พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_05.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยสวัสดิการและสิทธิประโยชน์ของบุคลากร พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_06.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_14.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยกองทุนสำรองเลี้ยงชีพ (ฉบับที่ 2) พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_20.pdf">- ประกาศสถาบันเทคโนโลยีจิตรลดา เรื่อง สวัสดิการและสิทธิประโยชน์ของบุคลากร พ.ศ. 2562</a><br>
                    <a target="_blank" href="<?=base_url()?>uploads/files/act/07_24.pdf">- ข้อบังคับสถาบันเทคโนโลยีจิตรลดาว่าด้วยค่าชดเชย พ.ศ. 2563</a><br>
                    
                  </p>
                  
                </div>
            </div>
            

        </div>
      </section>
     
      <section>
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a href="#panelBody-4" class="accordion-toggle collapsed" data-toggle="collapse"
                        data-parent="#accordion">4. รายงานผลการบริหารและพัฒนาทรัพยากรมนุษย์ประจำปี</a>
                </h4>
            </div>
            <div id="panelBody-4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="pt10-pl30">
                        <a target="_blank" href="<?=base_url()?>uploads/files/oit/oit_o28.pdf">- ปีงบประมาณ พ.ศ. 2562</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/HR/O28-2563.pdf">- ปีงบประมาณ พ.ศ. 2563</a><br>
                        <a target="_blank" href="<?=base_url()?>uploads/files/HR/O28-2564.pdf">- ปีงบประมาณ พ.ศ. 2564</a><br>
                            
                    </p>
                </div>
            </div>
        </div>
      </section>

    </div>
</div>

</style>