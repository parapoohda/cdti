<section id="speaker" class="speaker spacer">
    <div class="container">
       <div class="speaker-title ">
           <div class="row m-b-40">
               <div class="col-sm-3 col-10 mx-auto">
                   <img src="<?php echo base_url(); ?>cdti_assets//images/speaker/title_section.png" class="d-block mx-auto img-fluid" alt="">
               </div>
           </div>
           <div class="row">
               <div class="col-sm-10 mx-auto text-center">
                    <p class="speaker-subject m-b-40">
                       สมเด็จพระกนิษฐาธิราชเจ้า กรมสมเด็จพระเทพรัตนราชสุดา ฯ สยามบรมราชกุมารีในพิธีพระราชทานปริญญาบัตรแก่ผู้สำเร็จการศึกษา จากสถาบันเทคโนโลยีจิตรลดาเมื่อวันที่ 18 ตุลาคม 2561
                    </p>
                    <p class="speaker-head m-b-40">
                      ผู้เป็นบัณฑิตจึงมีหน้าที่หลายอย่างตามศักดิ์และสิทธิ์ที่มีอยู่
                    </p>
                    <p class="speaker-detail">
                        <?php /* อย่างหนึ่ง คือ จะต้องหมั่นศึกษาหาความรู้และฝึกฝนปฏิบัติ <br class="hidden-xs-down visible-sm-up hidden-md-up" /> เพิ่มเติมเพื่อให้มีกำลังความสามารถ เพิ่มพูนขึ้นอยู่เสมอ<br/>

                        อย่างหนึ่ง คือ จะต้องนำความรู้ความสามารถออกใช้ให้บังเกิด<br class="hidden-xs-down visible-sm-up hidden-md-up" />
                        ผลเป็นประโยชน์แก่ตนเองและ ประเทศชาติให้ใช้จริง<br/>

                        อย่างหนึ่ง คือ จะต้องประพฤติปฏิบัติตนให้ดี รู้ว่าสิ่งใดควรกระทำ<br class="hidden-xs-down visible-sm-up hidden-md-up" />
                        ก็มุ่งกระทำสิ่งใดควรละก็ละเพื่อ นำพาชีวิตไปในทางที่ดีที่เจริญ <br/> */ ?>
                      <strong>อย่างหนึ่ง</strong> คือ จะต้องหมั่นศึกษาหาความรู้และฝึกฝนปฏิบัติ <br class="hidden-xs-down visible-sm-up hidden-md-up" /> เพิ่มเติมเพื่อให้มีกำลังความสามารถ เพิ่มพูนขึ้นอยู่เสมอ<br/>

                      <strong>อย่างหนึ่ง</strong> คือ จะต้องนำความรู้ความสามารถออกใช้ให้บังเกิด<br class="hidden-xs-down visible-sm-up hidden-md-up" />
                      ผลเป็นประโยชน์แก่ตนเองและ ประเทศชาติให้ใช้จริง<br/>

                      <strong>อย่างหนึ่ง</strong> คือ จะต้องประพฤติปฏิบัติตนให้ดี รู้ว่าสิ่งใดควรกระทำ<br class="hidden-xs-down visible-sm-up hidden-md-up" />
                      ก็มุ่งกระทำสิ่งใดควรละก็ละเพื่อ นำพาชีวิตไปในทางที่ดีที่เจริญ <br/>
                    </p>
                    <p class="speaker-quot">
                       ปรัชญา  :  รักชาติ ศาสนา พระมหากษัตริย์ มีวินัย ใฝ่คุณธรรม นำความรู้ สู้งานหนัก
                    </p>
                    <p class="speaker-quot">
                       วิสัยทัศน์  :  สถาบันที่จัดการศึกษาตามแนวพระราชดำริ
                   </p>
                   <p class="speaker-heightlight">"เรียนคู่งาน งานคู่เรียน"</p>
                   <p class="speaker-quot">
                       สร้างคนดี มีฝีมือ มีวินัยและจรรยาบรรณวิชาชีพ
                   </p>
               </div>
           </div>
       </div>
    </div>
</section>
