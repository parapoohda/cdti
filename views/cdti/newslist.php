<?php include('views/layouts/header_inner.php'); ?>
<div class="bg-primary">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<p class="text-white m-t-20">
					ข่าวประชาสัมพันธ์
				</p>
			</div>
			<div class="col-4 ml-auto text-right">
				<ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
			</div>
		</div>
	</div>
</div>
<div class="bg-light">
	<section id="news-heighlight" class="mini-spacer feature7 news">
        <div class="container">
            <div class="section-header p-t-20 p-b-20 m-b-20">
                <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> ข่าวแนะนำ</h3>
                <!-- <a href="" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a> -->
            </div>
            <div class="row mb-4">
				<?php for ($i=0; $i < 12 ; $i++):?>
			    <div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
			        <div class="row">
			            <div class="col-md-12 col-6">
			                <img class="rounded img-responsive m-b-10" src="<?=$base_url?>assets/images/news/thumb.png" alt="news" />
			            </div>
			            <div class="col-md-12 col-6">
			                <p class="text-dark">นักศึกษารุ่นใหม่ต่อต้านคอรัปชั่น</p>
			                <p class="date text-muted"><span class="text-danger">ข่าวกิจกรรม</span> 14 ม.ค. 62</p>
			            </div>
			        </div>
			    </div>
				<?php endfor;?>
			</div>
			<div class="row mb-4">
				<div class="col-md-10 mx-auto text-center">                        
					<ul class="pagination d-inline-flex mx-auto">
                        <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">..</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
				</div>
			</div>
        </div>

    </section>
    <section class="news mini-spacer">
    	<div class="container">
    		<div class="section-header p-t-20 p-b-20 m-b-20">
	            <h3 class="mr-auto d-inline"><i class="fas fa-tags"></i> แท็กที่เกี่ยวข้อง</h3>
	            
	        </div>
	        <div class="row">
	        	<div class="col-12 col-sm-12">
	        		<div class="tag">
	                    <ul class="list-tags list-inline">
	                        <li>
	                            <p>ข่าวการศึกษา : </p>
	                        </li>
	                        <li>
	                            <a href="">ข่าวกิจกรรม</a>
	                        </li>
	                        <li>
	                            <a href="">ข่าวประชาสัมพันธ์</a>
	                        </li>
	                        <li>
	                            <a href="">ข่าวน่ารู้</a>
	                        </li>
	                        <li>
	                            <a href="">สมัครเรียน</a>
	                        </li>
	                        <li>
	                            <a href="">ภาพกจิกรรมการเรียน</a>
	                        </li>
	                         <li>
	                            <a href="">ภาพกจิกรรมนักศึกษา</a>
	                        </li>
	                    </ul>
	                </div>
	        	</div>
	        </div>
    	</div>
    </section>
</div>
<?php include('views/layouts/footer.php'); ?>