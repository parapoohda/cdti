<section id="parter" class="spacer partner">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 text-center">
                <div class="section-title m-b-40">
                    <h3 class="color-primary"><?php echo trans('home_links')?></h3>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="partner-slider"  class="slick p-10">
                            <?php if(isset($links)):?>
                              <?php foreach ($links as $key => $item): ?>
                                      <div class="item">
                                          <?php if($item->description != ""):?>
                                              <a href="<?php echo $item->description;?>" target="_blank" >
                                                <img src="<?php echo base_url() . html_escape($item->path_small); ?>" alt="<?php echo html_escape($item->title); ?>" >
                                              </a>
                                          <?php else:?>
                                                <img src="<?php echo base_url() . html_escape($item->path_small); ?>" alt="<?php echo html_escape($item->title); ?>" >
                                          <?php endif;?>
                                    </div>
                              <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
