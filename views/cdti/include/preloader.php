<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label"><?php echo $settings->application_name; ?></p>
    </div>
</div>
