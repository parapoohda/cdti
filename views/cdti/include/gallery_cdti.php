<section id="gallery" class="spacer gallery">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-12 text-center">
                <div class="section-title m-b-40">
                    <h3 class="color-primary">บรรยากาศครอบครัว CDTI</h3>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div id="gallery-slide" class="owl-carousel owl-theme">
                            <?php if(isset($main_category1_cover) and count($main_category1_cover) > 0):?>
                                  <?php foreach ($main_category1_cover as $key => $value): ?>
                                        <div class="item">
                                            <div class="card card-shadow card-gallery">
                                                <a href="#" class="img-ho"> <img src="<?php echo base_url().$value->path_small?>" alt="" class="img-responsive"></a>
                                                <div class="card-overlay">
                                                    <h5 class="font-medium m-b-0"><?php echo $value->category_name?></h5>
                                                    <p class="m-b-0 font-14"><?php echo $value->main_category_name?></p>
                                                    <a href="" class="btn btn-custom-primary m-t-20"><?php echo trans('view_more')?></a>
                                                </div>
                                            </div>
                                        </div>
                                  <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
