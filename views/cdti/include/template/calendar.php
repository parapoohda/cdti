<section class="campus-box campus-box-calendar">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><i class="far fa-calendar-alt"></i> ปฏิทินการศึกษา</h3>
						</div>
					</div>
				</div>
				
				<div class="section-body">
					<div class="row">
						<div class="col-md-12">
							<div class="timeline-box ">
                                <ul class="timeline">
									<?
									//for ($i=0; $i < 5; $i++) { 
										
									?>
                                    <!-- <li class="timeline-item current">
                                        <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                      		<div class="timeline-content">
                                        		<div class="row">
		                                        	
		                                        	<div class="col-sm-12">
		                                            <p class="timeline-title mb-0">กิจกรรมอุ่นไอรัก คลายความหนาว</p>
		                                            <p class="text-muted mb-0">
		                                                <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
		                                                <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> 
		                                                <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
		                                            </p>
		                                            <p class="text-dark font-14">เนื่องด้วยสำนักงานนวัตกรรมแห่งชาติ (องค์การมหาชน) หรือ สนช. กระทรวงวิทยาศาสตร์และเทคโนโลยี เป็นหน่วยงานที่ก่อตั้งมาพร้อมพันธกิจในการพัฒนานวัตกรรมภาครัฐ ภาคเอกชน และภาคสังคมโดยรวมอย่างเป็นระบบและยั่งยืน ภายใต้วิสัยทัศน์ “องค์กรหลักในการเสริมสร้างระบบนวัตกรรมแห่งชาติ  เพื่อเพิ่มคุณค่าที่ยั่งยืน” ขับเคลื่อนผ่านยุทธศาสตร์ของชาติ และงานที่จะเกิดขึ้นจะเป็นการผลักดันให้นวัตกรรมในระดับประเทศไทยก้าวไปสู่สากลอย่างแท้จริง</p>
		                                        </div>
                                        	</div>
                                        </div>
									</li> -->
									<?php
								//}
									?>
                                </ul>
                                <div class="row">
                                   <!--  <div class="col-12 text-center">
                                        <a href="" class="btn btn-custom-primary mx-auto">ดูทั้งหมด</a>
                                    </div> -->
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);

	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();

	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>
<script>
	$(document).ready(function(){
		getdate();
	});

	
	function getdate(){
		var carlendar = document.getElementById('event-calendar');
		var datenow = new Date();
		jsCalendar.new(carlendar, datenow);
	}
</script>