<section class="campus-box campus-box-calendar mini-spacer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-header campus-header mb-4">
					<div class="row">
						<div class="col-sm-4 align-middle">
							 <h3 class="campus-title mt-4 text-primary"><i class="far fa-calendar-alt"></i> ปฏิทินการศึกษา</h3>
						</div>
					</div>
				</div>
				<div class="tabs-year tabs-year-calendar">
					<ul class="nav nav-pills nav-fill nav-years nav-year-calendar">
						<div class="slider"></div>
					  	<li class="nav-item">
					    	<a class="nav-link active" href="#y2498">
						    	<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2499">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2500">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2501">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2504">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2507">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2511">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2526">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2528">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2547">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2557">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" href="#y2561">
					    		<p class="color-primary">2562</p>
								<p class="year color-primary">มกราคม</p>
					    	</a>
					  	</li>
					</ul>
				</div>
				<div class="section-body spacer">
					<h3 class="text-primary">
						กิจกรรมที่กำลังจะเริ่มขึ้น
					</h3>
					<hr class="hr-primary">
					<div class="event-coming-soon feature7">
						<div class="row">
							<div class="col-md-8">
								<div class="row">
									<div class="col-12 col-sm-6 col-lg-6  wrap-feature7-box  wrap-feature7-box-small">
		                                <div class="row">
		                                    <div class="col-md-12 col-6">
		                                        <img class="rounded img-responsive m-b-10" src="<?php echo base_url() ?>assets/images/news/thumb.png" alt="news" />
		                                    </div>
		                                    <div class="col-md-12 col-6">
		                                        <div class="row">
		                                        	<div class="col-2">
		                                        		<h4 class="feature7-date text-center text-primary">
		                                        			14<br/>
		                                        			<small>ม.ค.</small>
		                                        		</h4>
		                                        	</div>
		                                        	<div class="col-10">
		                                        		<p class="text-priamry">CDTI OPEN HOUSE 2019</p>
		                                        		<p class="text-dark"><small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small></p>
		                                        		<p class="text-dark"><small><i class="far fa-clock"></i> เวลา 09:00 น. - 12:00 น. </small></p>
		                                        		<p class="text-dark"><small><i class="fas fa-map-marker-alt"></i> ณ พระลานพระราชวังดุสิต </small></p>
		                                        	</div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-12 col-sm-6 col-lg-6  wrap-feature7-box  wrap-feature7-box-small">
		                                <div class="row">
		                                    <div class="col-md-12 col-6">
		                                        <img class="rounded img-responsive m-b-10" src="<?=base_url() ?>assets/images/news/thumb.png" alt="news" />
		                                    </div>
		                                    <div class="col-md-12 col-6">
		                                        <div class="row">
		                                        	<div class="col-2">
		                                        		<h4 class="feature7-date text-center text-orange">
		                                        			14<br/>
		                                        			<small>ม.ค.</small>
		                                        		</h4>
		                                        	</div>
		                                        	<div class="col-10">
		                                        		<p class="text-orange">CDTI OPEN HOUSE 2019</p>
		                                        		<p class="text-dark"><small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small></p>
		                                        		<p class="text-dark"><small><i class="far fa-clock"></i> เวลา 09:00 น. - 12:00 น. </small></p>
		                                        		<p class="text-dark"><small><i class="fas fa-map-marker-alt"></i> ณ พระลานพระราชวังดุสิต </small></p>
		                                        	</div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
								</div>
							</div>
							<div class="col-md-4">
								<p class="text-primary date-now text-center ">14 มกราคม 2562</p>
                                <div id="event-calendar" class="event-calendar mx-auto text-center"  data-language="th"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="timeline-box ">
                                <ul class="timeline">
                                    <li class="timeline-item current">
                                        <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                      		<div class="timeline-content">
                                        		<div class="row">
		                                        	<div class="col-sm-3">
		                                        		<img src="<?=base_url();?>assets/images/cp_1.png" alt="" class="img-responsive">
		                                        	</div>
		                                        	<div class="col-sm-9">
		                                            <p class="timeline-title mb-0">กิจกรรมอุ่นไอรัก คลายความหนาว</p>
		                                            <p class="text-muted mb-0">
		                                                <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
		                                                <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> 
		                                                <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
		                                            </p>
		                                            <p class="text-dark font-14">เนื่องด้วยสำนักงานนวัตกรรมแห่งชาติ (องค์การมหาชน) หรือ สนช. กระทรวงวิทยาศาสตร์และเทคโนโลยี เป็นหน่วยงานที่ก่อตั้งมาพร้อมพันธกิจในการพัฒนานวัตกรรมภาครัฐ ภาคเอกชน และภาคสังคมโดยรวมอย่างเป็นระบบและยั่งยืน ภายใต้วิสัยทัศน์ “องค์กรหลักในการเสริมสร้างระบบนวัตกรรมแห่งชาติ  เพื่อเพิ่มคุณค่าที่ยั่งยืน” ขับเคลื่อนผ่านยุทธศาสตร์ของชาติ และงานที่จะเกิดขึ้นจะเป็นการผลักดันให้นวัตกรรมในระดับประเทศไทยก้าวไปสู่สากลอย่างแท้จริง</p>
		                                        </div>
                                        	</div>
                                        </div>
                                    </li>
                                    <li class="timeline-item timeline-orange">
                                        <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                        <div class="timeline-content">
                                            <div class="row">
	                                        	<div class="col-sm-3">
	                                        		<img src="<?=base_url();?>assets/images/cp_1.png" alt="" class="img-responsive">
	                                        	</div>
	                                        	<div class="col-sm-9">
	                                            <p class="timeline-title text-orange mb-0">กิจกรรมอุ่นไอรัก คลายความหนาว</p>
	                                            <p class="text-muted mb-0">
	                                                <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
	                                                <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> 
	                                                <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
	                                            </p>
	                                            <p class="text-dark font-14">เนื่องด้วยสำนักงานนวัตกรรมแห่งชาติ (องค์การมหาชน) หรือ สนช. กระทรวงวิทยาศาสตร์และเทคโนโลยี เป็นหน่วยงานที่ก่อตั้งมาพร้อมพันธกิจในการพัฒนานวัตกรรมภาครัฐ ภาคเอกชน และภาคสังคมโดยรวมอย่างเป็นระบบและยั่งยืน ภายใต้วิสัยทัศน์ “องค์กรหลักในการเสริมสร้างระบบนวัตกรรมแห่งชาติ  เพื่อเพิ่มคุณค่าที่ยั่งยืน” ขับเคลื่อนผ่านยุทธศาสตร์ของชาติ และงานที่จะเกิดขึ้นจะเป็นการผลักดันให้นวัตกรรมในระดับประเทศไทยก้าวไปสู่สากลอย่างแท้จริง</p>
	                                        </div>
                                        </div>
                                    </li>
                                    <li class="timeline-item timeline-danger">
                                        <label for="" class="timeline-date text-center">14 <br/> ม.ค.</label>
                                        <div class="timeline-content">
                                            <div class="row">
	                                        	<div class="col-sm-3">
	                                        		<img src="<?=base_url();?>assets/images/cp_1.png" alt="" class="img-responsive">
	                                        	</div>
	                                        	<div class="col-sm-9">
	                                            <p class="timeline-title mb-0 text-danger">กิจกรรมอุ่นไอรัก คลายความหนาว</p>
	                                            <p class="text-muted mb-0">
	                                                <small>วันที่ 14 มกราคม 2562 - 30 มีนาคม 2562</small> <br/>
	                                                <small><i class="fas fa-stopwatch"></i> เวลา 09.00 น. - 12.00 น</small> 
	                                                <small><i class="fas fa-map-marker-alt"></i> ณ ลานพระราชวังดุสิต</small>
	                                            </p>
	                                            <p class="text-dark font-14">เนื่องด้วยสำนักงานนวัตกรรมแห่งชาติ (องค์การมหาชน) หรือ สนช. กระทรวงวิทยาศาสตร์และเทคโนโลยี เป็นหน่วยงานที่ก่อตั้งมาพร้อมพันธกิจในการพัฒนานวัตกรรมภาครัฐ ภาคเอกชน และภาคสังคมโดยรวมอย่างเป็นระบบและยั่งยืน ภายใต้วิสัยทัศน์ “องค์กรหลักในการเสริมสร้างระบบนวัตกรรมแห่งชาติ  เพื่อเพิ่มคุณค่าที่ยั่งยืน” ขับเคลื่อนผ่านยุทธศาสตร์ของชาติ และงานที่จะเกิดขึ้นจะเป็นการผลักดันให้นวัตกรรมในระดับประเทศไทยก้าวไปสู่สากลอย่างแท้จริง</p>
	                                        </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="" class="btn btn-custom-primary mx-auto">ดูทั้งหมด</a>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);

	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();

	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>
<script>
	$(document).ready(function(){
		getdate();
	});

	
	function getdate(){
		var carlendar = document.getElementById('event-calendar');
		var datenow = new Date();
		jsCalendar.new(carlendar, datenow);
	}
</script>