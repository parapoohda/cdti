<div class="row">
</div>
<style>
  .image-emagazine-container {
    height : 370px;
  }
  .text-align{
    text-align: center;
  }
  @media only screen and (max-width: 990px) {
    .image-emagazine-container {
    height : 480px;
  }
  .text-align{
    text-align: center;
  }
  @media only screen and (max-width: 450px) {
    .image-emagazine-container {
    height : 250px;
  }
  .text-align{
    text-align: left;
  }
}
</style>

<div class="row">
    <div class="col-md-12 col-6  mb-2">
      <div class="image-emagazine-container ">
      <a href="<?php echo base_url() . $data->post_type . '/' . $data->video_path; ?>" target="_blank">
              <?php $this->load->view("cdti/include/emagazine/_post_image", ["post" => $data, "icon_size" => "md", "bg_size" => "md", "image_size" => "mid", "class" => "lazyload"]); ?>
        </a>
      </div>
    </div>
    <div class="col-md-12 col-6 mb-2">
        <p class="text-dark text-align">
        <a href="<?php echo base_url() . $data->post_type . '/' . $data->video_path; ?>" target="_blank">
            <?php echo html_escape(character_limiter($data->title, 35, '...')); ?>
          </a>
        </p>
    </div>
</div>
