<div class="sidebar-title">
        <h3><i class="far fa-calendar-alt"></i> ปฎิทินการศึกษา</h3>
    </div>
    <div class="carlendar  text-center">
        <p class="text-muted date-now"><?php echo helper_date_format_month2(date("Y-m-d"))?></p>
        <div class="jsCalendar"  data-language="th"></div>
    </div>
    <div class="timeline-box t">
        <ul class="timeline">
            <div class="text-center timeline-spinner pb-4">
                <div class="spinner-grow text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="timeline-items">
            </div>
            <div class="row timeline-see-all-btn">
                <div class="col-12 text-center">
                    <a href="" class="btn btn-custom-primary mx-auto">ดูทั้งหมด</a>
                </div>
            </div>
        </ul>
    </div>
</div>

<!--
    <div class="timeline-box t">
        <ul class="timeline">
        <?php //if($event_calendar):?>
                <?php //foreach ($event_calendar as $key => $data):?>
                  <li class="timeline-item current">
                      <label for="" class="timeline-date text-center"><?php //echo helper_date_format_day($data->sdate_event)?><br/> <?php //echo helper_date_format_month_short($data->sdate_event)?></label>
                      <div class="timeline-content">
                          <p class="timeline-title mb-0">
                              <a href=""><?php //echo $data->title?></a>
                          </p>
                          <p class="text-muted">
                              <?php //if(helper_date_format_month2($data->sdate_event) == helper_date_format_month2($data->edate_event)):?>
                                  <small>วันที่ <?php //echo helper_date_format_month2($data->sdate_event)?></small> <br/>
                              <?php //else:?>
                                  <small>วันที่ <?php //echo helper_date_format_month2($data->sdate_event)?> - <?php //echo helper_date_format_month2($data->edate_event)?></small> <br/>
                              <?php //endif;?>
                              <small><i class="fas fa-stopwatch"></i> เวลา <?php //echo helper_date_format_time($data->sdate_event)?> น. - <?php //echo helper_date_format_time($data->edate_event)?> น</small> <br/>
                              <small><i class="fas fa-map-marker-alt"></i> <?php //echo $data->place?></small>
                          </p>
                      </div>
                  </li>
                <?php //endforeach;?>
              <div class="row">
                  <div class="col-12 text-center">
                      <a href="" class="btn btn-custom-primary mx-auto">ดูทั้งหมด</a>
                  </div>
              </div>
            <?php //endif;?> 
        </ul>
    </div>
-->