<div class="bg-light">
    <section id="slider-sec" class="slider2">
        <div id="slider2" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <?php if(isset($banners)):?>
                    <?php foreach ($banners as $key => $item): ?>
                        <div class="carousel-item <?php echo ($key==0)?'active':''?>">
                            <?php if($item->description != ""):?>
                                <a href="<?php echo $item->description;?>" target="_blank" >
                                   <?php /* <img src="<?php echo base_url() . html_escape($item->path_big); ?>" alt="<?php echo html_escape($item->title); ?>" alt="" class="slide-image" /> */ ?>
                                       <!-- Desktop banner -->
                                    <img src="<?php echo base_url() . html_escape($item->path_big); ?>" alt="<?php echo html_escape($item->title); ?>" class="slide-image hidden-sm-down" style="width: 100% !important;height: auto;" />

                                    <!-- Mobile banner -->
                                    <img src="<?php echo base_url() . html_escape($item->path_big); ?>" alt="<?php echo html_escape($item->title); ?>" class="slide-image visible-sm-down hidden-md-up" style="width: 100% !important;height: auto;"/>
                                </a>
                            <?php else:?>
                                <a href="<?php echo $item->description;?>" target="_blank" >
                                <img src="<?php echo base_url() . html_escape($item->path_big); ?>" alt="<?php echo html_escape($item->title); ?>" alt="<?php echo html_escape($item->title); ?>" class="slide-image hidden-sm-down"  style="width: 100% !important;height: auto;"/>

                                <img src="<?php echo base_url() . html_escape($item->path_big); ?>" alt="<?php echo html_escape($item->title); ?>" alt="<?php echo html_escape($item->title); ?>" class="slide-image visible-sm-down hidden-md-up" style="width: 100% !important;height: auto;"/>
                                </a>
                            <?php endif;?>
                        </div>
                    <?php endforeach;?>
                  <?php endif;?>

                <div class="slider-control">
                    <!-- Left Control -->
                    <a class="left carousel-control-prev font-14" href="#slider2" role="button" data-slide="prev"> <span class="ti-angle-left" aria-hidden="true"></span> <b class="sr-only font-normal">Previous</b> </a>
                    <!-- Right Control -->
                    <a class="right carousel-control-next font-14" href="#slider2" role="button" data-slide="next"> <span class="ti-angle-right" aria-hidden="true"></span> <b class="sr-only font-normal">Next</b> </a>
                </div>
                <!-- End of Slider Control -->
            </div>
        </div>
        <!-- End Slider -->
    </section>
</div>
