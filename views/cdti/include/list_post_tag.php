<?php if(count($categories)):?>
    <div class="tag">
        <ul class="list-tags list-inline">
            <li>
                <p>Tag : </p>
            </li>
            <?php foreach ($categories as $key => $value): ?>
                <li>
                    <a href="<?php echo lang_base_url(); ?>tag/<?php echo html_escape($value->tag_slug); ?>"><?php echo $value->tag?></a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>
