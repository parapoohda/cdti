<div class="sidebar-title">
    <h3><i class="far fa-newspaper"></i> ข่าวแนะนำ</h3>
</div>
<div class="feature7">
    <?php foreach ($data as $key => $value):?>
        <?php if($key == 0):?>
            <div class="row">
              <div class="col-md-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-large">
                  <?php $this->load->view('cdti/include/post/post_big',['data' => $value]);?>
              </div>
            </div>
            <div class="row">
        <?php else:?>
                  <div class="col-12 col-sm-12 col-lg-12  wrap-feature7-box  wrap-feature7-box-small">
                      <!-- <div class="row"> -->
                          <?php $this->load->view('cdti/include/post/post_small3',['data' => $value]);?>
                      <!-- </div> -->
                  </div>
        <?php endif;?>
    <?php endforeach;?>
        </div>
</div>
