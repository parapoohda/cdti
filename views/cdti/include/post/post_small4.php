<div class="image-container popup-gallery portfolio-box4" style="height: 170px">
  <a href="<?php echo get_post_image($data, $image_size,"big"); ?>">
      <?php $this->load->view("cdti/include/post/_post_image", ["post" => $data, "icon_size" => "md", "bg_size" => "md", "image_size" => "big", "class" => "lazyload"]); ?>
  </a>
</div>
<div class="m-t-10">
    <p class="text-dark">
      <a href="<?php echo post_url($data); ?>">
        <?php echo html_escape(character_limiter($data->title, 40, '...')); ?>
      </a>
    </p>
    <p class="date text-muted thumb-font-post-12px">
      <a href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($data->category_name_slug); ?>">
          <span class="text-danger thumb-font-post-12px"><?php echo $data->category_name?></span>
      </a>
      <span class="thumb-font-post-12px"><?php echo helper_date_format_thai_s($data->created_at)?></span>
    </p>
</div>
<link href="<?php echo base_url(); ?>cdti_assets/node_modules/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
<script src="<?php echo base_url()?>cdti_assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/magnific-popup/dist/jquery.magnific-popup.js"></script>

<script>
  

$('.popup-gallery').magnificPopup({
  delegate: 'a',
  type: 'image',
  tLoading: 'Loading image #%curr%...',
  mainClass: 'mfp-img-mobile',
  gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  },
  image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      titleSrc: function(item) {
          return item.el.attr('title');
      }
  }
});

</script>