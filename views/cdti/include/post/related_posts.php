<div class="section-header p-t-20 p-b-20 m-b-20">
    <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> ข่าวที่เกี่ยวข้อง</h3>
    <a href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($category->name_slug); ?>" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
</div>
<div class="row">
  <?php foreach ($data as $key => $value):?>
          <div class="col-12 col-sm-6 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
              <?php $this->load->view('cdti/include/post/post_small',['data' => $value, 'height' => 107]);?>
          </div>
  <?php endforeach;?>
</div>
