<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
  .over-img {
        position: absolute;
        top: 18%;
        left: 39%;
        width: 55px;
    }


</style>

<?php if ($post->post_type == "video"): ?>
    <img src="<?php echo base_url(); ?>assets/img/icon_play.svg" alt="icon" class="over-img post-icon post-icon-<?php echo $icon_size; ?>"/>
<?php endif; ?>
<?php if ($post->post_type == "audio"): ?>
    <img src="<?php echo base_url(); ?>assets/img/icon_music.svg" alt="icon" class="over-img  post-icon post-icon-<?php echo $icon_size; ?>"/>
<?php endif; ?>
<?php
$img_bg = "";
if ($bg_size == "sm"):
    $img_bg = $img_bg_sm;
elseif ($bg_size == "sl"):
    $img_bg = $img_bg_sl;
elseif ($bg_size == "lg"):
    $img_bg = $img_bg_lg;
elseif ($bg_size == "sm_footer"):
    $img_bg = $img_bg_sm_footer;
else:
    $img_bg = $img_bg_mid;
endif;
?>
<?php if (!empty($post->image_url)): ?>
    <img class="card-img-top img-responsive <?php echo $class?>" src="<?php echo get_post_image($post, "big"/*$image_size*/); ?>" alt="<?php echo html_escape($post->title); ?>">
<?php else: ?>
    <img class="card-img-top img-responsive <?php echo $class?>" src="<?php echo get_post_image($post, "big"/*$image_size*/); ?>" alt="<?php echo html_escape($post->title); ?>">
<?php endif; ?>
