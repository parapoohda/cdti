<div class="container">
    <div class="row ">
        <div class="col-lg-6 col-md-12">
            <div class="section-header  p-b-20 m-b-20">
                <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> ผลงานทางวิชาการ</h3>
                <a href="https://www.cdti.ac.th/tag/ผลงานทางวิชาการ" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
            </div>
            <div class="row  m-t-40">
                <?php 
                // dd($data);
                foreach ($activity_posts_2 as $key => $value):?>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4  wrap-feature7-box  wrap-feature7-box-small">
                                <?php $this->load->view('cdti/include/post/post_small',['data' => $value , 'height' => 107]);?>
                    </div>
                <?php endforeach;?>
             </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="section-header  p-b-20 m-b-20">
                <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> ผลงานนักศึกษา</h3>
                <a href="https://www.cdti.ac.th/tag/ผลงานนักศึกษา" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
            </div>
            <div class="row  m-t-40">
                <?php 
                // dd($data);
                foreach ($activity_posts_3 as $key => $value):?>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4  wrap-feature7-box  wrap-feature7-box-small">
                                <?php $this->load->view('cdti/include/post/post_small',['data' => $value , 'height' => 107]);?>
                    </div>
                <?php endforeach;?>
             </div>
        </div>


    </div>
</div>
