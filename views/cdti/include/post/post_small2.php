    <div class="image-container" style="height: 170px">
      <a href="<?php echo post_url($data); ?>">
          <?php $this->load->view("cdti/include/post/_post_image", ["post" => $data, "icon_size" => "md", "bg_size" => "md", "image_size" => "mid", "class" => "lazyload"]); ?>
      </a>
    </div>
    <div class="m-t-10">
        <p class="text-dark">
          <a href="<?php echo post_url($data); ?>">
            <?php echo html_escape(character_limiter($data->title, 40, '...')); ?>
          </a>
        </p>
        <p class="date text-muted thumb-font-post-12px">
          <a href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($data->category_name_slug); ?>">
             <span class="text-danger thumb-font-post-12px"><?php echo $data->category_name?></span>
          </a>
          <span class="thumb-font-post-12px"><?php echo helper_date_format_thai_s($data->created_at)?></span>
        </p>
    </div>
