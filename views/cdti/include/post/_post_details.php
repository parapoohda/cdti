
<?php if (!empty($post->image_url)): ?>
    <img class="img-responsive" src="<?php echo html_escape($post->image_url); ?>" alt="<?php echo html_escape($post->title); ?>">
<?php else: ?>
    <img class="img-responsive" src="<?php echo base_url() . html_escape($post->image_default); ?>" alt="<?php echo html_escape($post->title); ?>">
<?php endif; ?>
