
<div class="feature-box-campus">
    <a href="<?php echo post_url($data); ?>">
      <div class="card-img-thumbnail-">
         <?php $this->load->view("cdti/include/post/_post_image", ["post" => $data, "icon_size" => "md", "bg_size" => "md", "image_size" => "mid", "class" => "lazyload"]); ?>
      </div>
    </a>
    <div class="m-t-10">
      <p class="text-dark">
        <a href="<?php echo post_url($data); ?>">
          <?php echo html_escape(character_limiter($data->title,100, '...')); ?>
        </a>
      </p>
      <p class="date text-muted">
        <a href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($data->category_name_slug); ?>">
           <span class="text-danger thumb-font-post-14px"><?php echo $data->category_name?></span>
        </a>
        <span class="thumb-font-post-14px"><?php echo helper_date_format_thai($data->created_at)?></span>
      </p>
    </div>
</div>
