
<ul class="list-inline ml-auto social-list d-inline">
  <li>
      <a href="javascript:void(0)"
         onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo post_url($post); ?>', 'Share This Post', 'width=640,height=450');return false"
         class="social-icon social-icon-facebook">
          <i class="fab fa-facebook-f"></i>
      </a>
  </li>
  <li>
    <a href="javascript:void(0)"
    onclick="window.open('https://social-plugins.line.me/lineit/share?url=<?php echo post_url($post); ?>&amp;text=<?php echo html_escape($post->title); ?>', 'Share This Post', 'width=640,height=450');return false"
     class="social-icon social-icon-line">
      <i class="fab fa-line"></i>
    </a>
  </li>
  <li>
    <a href="javascript:void(0)"
       onclick="window.open('https://twitter.com/share?url=<?php echo post_url($post); ?>&amp;text=<?php echo html_escape($post->title); ?>', 'Share This Post', 'width=640,height=450');return false"
       class="social-icon social-icon-twitter">
        <i class="fab fa-twitter"></i>
    </a>
  </li>
</ul>
