
<div class="" data-aos="flip-left" data-aos-duration="1200">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <?php if ($this->post_file_model->get_post_additional_image_count($data->id)) : ?>
                  <?php foreach ($this->post_file_model->get_post_additional_images($data->id) as $image): ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $image->id?>" class=""></li>
                    <?php endforeach; ?>
            <?php endif; ?>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <a href="<?php echo post_url($data); ?>">
                    <?php $this->load->view("cdti/include/post/_post_image", ["post" => $data, "icon_size" => "md", "bg_size" => "md", "image_size" => "mid", "class" => "lazyload"]); ?>
                </a>
            </div>
            <?php if ($this->post_file_model->get_post_additional_image_count($data->id)) : ?>
                  <?php foreach ($this->post_file_model->get_post_additional_images($data->id) as $image): ?>
                           <div class="carousel-item">
                               <a href="<?php echo post_url($data); ?>">
                                  <img src="<?php echo base_url() . html_escape($image->image_default); ?>" class="mx-auto d-block img-responsive" alt="<?php echo html_escape($data->title); ?>"/>
                                 </a>
                           </div>
                    <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="m-t-10">
      <p class="text-dark">
        <a href="<?php echo post_url($data); ?>">
          <?php echo html_escape(character_limiter($data->title,150, '...')); ?>
        </a>
      </p>
      <p class="date text-muted thumb-font-post-14px">
        <a href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($data->category_name_slug); ?>">
           <span class="text-danger"><?php echo $data->category_name?></span>
        </a>
        <span class="thumb-font-post-14px"><?php echo helper_date_format_thai($data->created_at)?></span>
      </p>
    </div>
</div>
