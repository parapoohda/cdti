<div class="container">
    <div class="news-info">
        <div class="section-header p-t-20 p-b-20 m-b-20">
            <h3 class="mr-auto d-inline"><i class="fas fa-film"></i> CDTI Channel</h3>
            <a href="<?php echo base_url('category/cdti-channel')?>" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
        </div>
    </div>
    <div class="row m-b-20 m-t-40">
        <div class="col-lg-12">
            <div class="row">
                <?php foreach ($data as $key => $value):?>
                      <div class="col-6 col-sm-4 col-lg-3  wrap-feature7-box  wrap-feature7-box-small">
                          <?php $this->load->view('cdti/include/post/post_small4',['data' => $value]);?>
                      </div>
                  <?php endforeach;?>
              </div>
        </div>
    </div>
</div>