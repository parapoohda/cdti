
<div  class="container">
    <div class="section-header  p-b-20 m-b-20">
        <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> นิตยสาร</h3>
        <a href="https://www.cdti.ac.th/tag/นิตยสาร" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
    </div>
    <div class="row  m-t-40">
        <?php 
        // dd($data);
        foreach ($activity_posts_6 as $key => $value):?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-2  wrap-feature7-box  wrap-feature7-box-small">
                        <?php $this->load->view('cdti/include/post/post_small',['data' => $value , 'height' => 107]);?>
            </div>
        <?php endforeach;?>
                </div>
            </div>
    </div>
</div>