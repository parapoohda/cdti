<div class="news-info">
    <div class="section-header p-t-20 p-b-20 m-b-20">
        <h3 class="mr-auto d-inline"><i class="far fa-newspaper"></i> ข่าวประชาสัมพัมธ์</h3>
        <a href="<?php echo base_url('posts')?>" class="ml-auto d-inline readmore btn btn-custom-primary pull-right">ดูทั้งหมด</a>
    </div>
</div>
<div class="row m-b-20 m-t-40">
    <?php foreach ($data as $key => $value):?>
        <?php if($key == 0):?>
              <div class="col-md-6 col-lg-5  wrap-feature7-box  wrap-feature7-box-medium">
                  <?php $this->load->view('cdti/include/post/post_big',['data' => $value]);?>
              </div>
        <div class="col-lg-7">
          <div class="row">
        <?php else:?>
                <div class="col-12 col-sm-6 col-lg-6  wrap-feature7-box  wrap-feature7-box-small">
                    <?php $this->load->view('cdti/include/post/post_small',['data' => $value , 'height' => 107]);?>
                </div>
        <?php endif;?>
    <?php endforeach;?>
        </div>
    </div>
</div>
