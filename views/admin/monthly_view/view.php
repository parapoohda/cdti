
        <div class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                
                <span class="hidden-xs">เลือกปี <i class="fa fa-caret-down"></i> </span>
            </a>

            <ul class="dropdown-menu" role="menu" aria-labelledby="user-options">
                
                <li>

                <?php foreach ($options as $option): ?>
                    <a href=<?php echo $option->value+1 ?>><?php echo $option->text?></a>
                
            <?php endforeach; ?>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="table-responsive">
        
        <table class="table table-bordered table-striped dataTable" 
                >
            <thead>
            <tr role="row">
                <th><?php echo 'ปี เดือน' ?></th>
                <th><?php echo 'จำนวนคนดู' ?></th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($month_views as $month_view): ?>
                <?php if ($month_view->count!=0): ?>
                    <tr>
                        <td><?php echo date('Y/m',$month_view->id) ?></td>
                        <td><?php echo number_format($month_view->count) ?></td>
                        
                        <?php endif; ?>
                    </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
