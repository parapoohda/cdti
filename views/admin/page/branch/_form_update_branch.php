<div class="box">
    <div class="box-header with-border">
        <div class="left">
            <h3 class="box-title"><?php echo trans('post_details'); ?></h3>
        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <!-- include message block -->
        <?php $this->load->view('admin/includes/_messages'); ?>

        <div class="form-group">
            <label class="control-label">เกริ่นนำ</label>
            <textarea class="form-control text-area" name="branch_description" rows="10"
                      placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->branch_description); ?></textarea>
        
        </div>

        <div class="form-group">
            <label class="control-label">จุดเด่นของสาขา</label>
            <textarea class="form-control text-area" name="branch_highlight" rows="10"
                      placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->branch_highlight); ?></textarea>
        
        </div>

        <div class="form-group">
            <label class="control-label">คุณสมบัติของผู้เข้าศึกษา</label>
            <textarea class="form-control text-area" name="branch_qualification" rows="10"
                      placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->branch_qualification); ?></textarea>
        
        </div>

        <div class="form-group">
            <label class="control-label">โครงสร้างหลักสูตร</label>
            <textarea class="form-control text-area" name="branch_structure" rows="10"
                      placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->branch_structure); ?></textarea>
        
        </div>

        <div class="form-group">
            <label class="control-label">สาขานี้เรียนอะไร</label>
            <textarea class="form-control text-area" name="branch_study" rows="10"
                      placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->branch_study); ?></textarea>
        
        </div>

        <div class="form-group">
            <label class="control-label">จบแล้วมีงานทำ</label>
            <textarea class="form-control text-area" name="branch_jobs"
                      placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->branch_jobs); ?></textarea>
        
        </div>


    </div>
</div>
