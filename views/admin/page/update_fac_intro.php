<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-sm-12 col-xs-12">

        <div class="nav-tabs-custom">
            <!-- form start -->
            <?php echo form_open('page_controller/update_page_brach'); ?>
            <ul class="nav nav-tabs">
                <li class="<?php echo ($page->sflag == '')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/" >แก้ไขคณะและสาขา</a></li>
                <li class="<?php echo ($page->sflag == 'intro')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/intro" >เกริ่นนำ</a></li>
                <li class="<?php echo ($page->sflag == 'highlight')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/highlight" >จุดเด่นของสาขา</a></li>
                <li class="<?php echo ($page->sflag == 'qualification')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/qualification" >คุณสมบัติของผู้เข้าศึกษา</a></li>
                <li class="<?php echo ($page->sflag == 'structure')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/structure" >โครงสร้างหลักสูตร</a></li>
                <li class="<?php echo ($page->sflag == 'study')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/study" >สาขานี้เรียนอะไร</a></li>
                <li class="<?php echo ($page->sflag == 'jobs')?'active':'';?>"><a href="<?php echo admin_url(); ?>update-page/<?php echo html_escape($page->id); ?>/jobs" >จบแล้วมีงานทำ</a></li>
            </ul>
            <div class="tab-content settings-tab-content">
                <div id="tab_3" class="box box-primary tab-pane active">
                    <input type="hidden" name="id" value="<?php echo html_escape($page->id); ?>">
                    <input type="hidden" name="field" value="<?php echo html_escape($page->flag); ?>" >
                    <input type="hidden" name="sflag" value="<?php echo html_escape($page->sflag); ?>" >
                    <input type="hidden" name="redirect_url"
                        value="<?php echo admin_url()."update-page/".$page->id."/".$page->sflag; ?>">
                    
                    <input type="hidden" id="selected_image_type" value="image"> 
                    <div class="box-body">
                        <!-- include message block -->
                        <?php $this->load->view('admin/includes/_messages'); ?>
                
                        <div class="form-group">
                            <label class="control-label">เกริ่นนำ</label>
                            <textarea class="form-control text-area" name="content_text" rows="10"
                                    placeholder="" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>><?php echo html_escape($page->content); ?></textarea>
                        </div>
                        <?php if ($page->sflag == 'intro'): ?>
                            <div class="form-group m0"> <label class="control-label"><?php echo trans('main_image'); ?></label> <input
                                type="hidden" id="selected_image_type" value="image"> 
                                <div class="row">
                                <div class="col-sm-12"> <a class='btn btn-sm bg-purple' data-toggle="modal"
                                        data-target="#image_file_manager" onclick="$('#selected_image_type').val('image');">
                                        <?php echo trans('select_image'); ?> </a> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 m-t-15"> 
                                <input type="hidden" name="post_image_id"> 
                                <img id="selected_image_file" name="" src="<?php echo base_url() . $page->branch_cover_image; ?>" alt="" class="img-responsive" />
                                 <?php if (!empty($page->branch_cover_image)){ ?> 
                                    <a class="btn btn-danger btn-sm btn-delete-main-img" onclick="delete_page_main_image('<?php echo $page->id; ?>');"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div> 
                            <?php }else{ ?> 
                            <div class="row">
                                <div class="col-sm-12 m-t-15"> <img src="<?php echo $page->branch_cover_image; ?>" alt=""  class="img-responsive" /> </div>
                            </div> 
                            <?php } ?>
                            </div>
                        <?php else: ?> 


                        <div class="form-group m0">
                            <label class="control-label"><?php echo trans('additional_images'); ?></label>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a class='btn btn-sm bg-purple' data-toggle="modal" data-target="#image_file_manager" onclick="$('#selected_image_type').val('additional_image');">
                                        <?php echo trans('select_image'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m0">
                            <div class="row">
                                <div class="col-sm-12 m-b-15">
                                    <div class="additional-image-list">
                                        <?php $additional_images = get_page_additional_images($page->id,$page->flag); ?>
                                        <?php if (!empty($additional_images)): ?>
                                            <?php foreach ($additional_images as $image): ?>
                                                <div class="additional-item additional-item-<?php echo $image->id; ?>">
                                                    <img class="img-additional" src="<?php echo base_url() . $image->image_default; ?>" alt="">
                                                    <a class="btn btn-danger btn-sm btn-page-additional-image" data-value="<?php echo $image->id; ?>">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                        <?php endif; ?>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit"
                            class="btn btn-primary pull-right"><?php echo trans('save_changes'); ?></button>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->

            </div>
            <?php echo form_close(); ?>
        <!-- form end -->
        </div>
        
        <!-- /.tab -->
    </div>
</div>
<?php $this->load->view("admin/includes/_file_manager_image"); ?>