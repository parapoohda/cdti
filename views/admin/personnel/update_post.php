<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-sm-12">
        <!-- form start -->
        <?php echo form_open_multipart($controller.'/update_post_post'); ?>
        <div class="row">
            <div class="col-sm-12 form-header">
                <h1 class="form-title"><?php echo $title;?></h1>
                <a href="<?php echo admin_url().$type; ?>?lang_id=<?php echo $general_settings->site_lang;?>" class="btn btn-success btn-add-new pull-right">
                        <i class="fa fa-bars"></i>
                        <?php echo trans($type); ?>
                    </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-post">
                    <div class="form-post-left" style="display: none;">
                        <?php $this->load->view("admin/includes/_form_update_pdf_left"); ?>
                    </div>
                    <div class="form-post-right">
                        <?php $this->load->view('admin/includes/_messages'); ?>
                        <div class="row">
                            <?php if (empty($post->feed_id)): ?>
                                <div class="col-sm-12">
                                    <?php $this->load->view('admin/includes/_files_edit_box'); ?>
                                </div>
                            <?php endif; ?>
                                <button type="submit" name="publish" value="0" class="btn btn-primary pull-right"><?php echo trans('save_changes'); ?></button>
                            <?php if (!get_ft_tags()) {
                                exit();
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?><!-- form end -->
    </div>
</div>
