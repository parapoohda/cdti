<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-sm-12">
        <!-- form start -->
        <?php echo form_open_multipart($controller.'/add_post_post'); ?>
        <div class="row">
            <div class="col-sm-12 form-header">
                <h1 class="form-title"><?php echo $add_title;?></h1>
                <a href="<?php echo admin_url().$type; ?>?lang_id=<?php echo $general_settings->site_lang;?>" class="btn btn-success btn-add-new pull-right">
                        <i class="fa fa-bars"></i>
                        <?php echo $title; ?>
                    </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-post">
                    <div class="form-post-left">
                        <?php $this->load->view("admin/includes/_form_add_pdf_left"); ?>
                    </div>
                    <div class="form-post-right">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_pdf_upload_box'); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_sidebar_language_categories',['type' => $type]); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_post_publish_box',['type' => $type]); ?>
                            </div>
                            <?php if (!get_ft_tags()) {
                                exit();
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?><!-- form end -->
    </div>
</div>
