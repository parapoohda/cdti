<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


<style>
    .pl-5-px{
        padding-left: 5px
    }
.container {
    width: 1180px;
    margin-top: 3em;
}

#accordion .panel {
    border-radius: 0;
    border: 0;
    margin-top: 0px;
}

#accordion a {
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #5dacf5;
    text-decoration: none;
}
#accordion .panel-body a{
  display: contents;
}
#accordion .panel-body a:hover,
#accordion .panel-body a:focus {
    color: black;
}

#accordion .panel-heading a.collapsed:hover,
#accordion .panel-heading a.collapsed:focus {
    background-color: #5dacf5;
    color: white;
    transition: all 0.2s ease-in;
}

.borderone {
    border: solid 1px #BBB;
}
#accordion .panel-heading a.collapsed:hover::before,
#accordion .panel-heading a.collapsed:focus::before {
    color: white;
}
.pt10-pl30{
    padding-top: 10px;
    padding-left: 30px;
}
#accordion .panel-heading {
    padding: 0;
    border-radius: 0px;
    text-align: left;
}

#accordion .panel-heading a:not(.collapsed) {
    color: white;
    background-color: #5dacf5;
    transition: all 0.2s ease-in;
}

}
#accordion .panel .panel-collapse .panel-body p{
    padding-left: 10px;
}

/* Add Indicator fontawesome icon to the left */
/* #accordion .panel-heading .accordion-toggle::before {
    font-family: 'FontAwesome';
    content: '\f00d';
    float: left;
    color: white;
    font-weight: lighter;
    transform: rotate(0deg);
    transition: all 0.2s ease-in;
} */

#accordion .panel-heading .accordion-toggle.collapsed::before {
    color: #444;
    transform: rotate(-135deg);
    transition: all 0.2s ease-in;
}
</style>
<div  class="row">
    <div id="add-box-page"  class="col-md-12">

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo trans("add_oit"); ?></h3>
                    </div>
                    <!-- /.box-header -->


                    <!-- form start -->
                    <?php echo form_open('admin_controller/add_oit_put'); ?>

                    <div class="box-body">
                        <!-- include message block -->
                        <?php if (empty($this->session->flashdata("mes_menu_limit"))):
                            $this->load->view('admin/includes/_messages_form');
                        endif; ?>

                        <div class="form-group">
                            <label><?php echo trans("title"); ?></label>
                            <input type="text" class="form-control" name="title" placeholder="<?php echo trans("title"); ?>"
                                   value="<?php echo old('title'); ?>"
                                   maxlength="200" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?> required>
                        </div>

                        <div class="form-group">
                            <label><?php echo trans("link"); ?></label>
                            <input type="text" class="form-control" name="link" placeholder="<?php echo trans("link"); ?>"
                                   value="<?php echo old('link'); ?>" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>>
                        </div>
                        
                        
                        <input type="hidden" id="selected_pdf_type" value="pdf">

                        <div class="form-group">
                            <label class="control-label">เป็นหัวข้อย่อยของ</label>
                            <select id="parent_links" name="parent_index" class="form-control">
                                <option value=""><?php echo trans('none'); ?></option>
                                
                            <optgroup label="หัวข้อหลัก">
                                <?php foreach ($oit->oit as $indexP=>$item): ?>
                                    <option value="<?php echo $indexP; ?>"><?php echo $item->title; ?></option>
                                <?php endforeach; ?>
                            </optgroup>
                            <optgroup label="หัวข้อย่อยชั้นแรก">
                                <?php foreach ($oit->oit as $indexP=> $item): ?>
                                    <?php foreach ($item->children as $indexC1=>$firstChildren): ?>
                                        <option value="<?php echo $indexP; ?>/<?php echo $indexC1; ?>"><?php echo $firstChildren->title; ?></option>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </optgroup>
                            <optgroup label="หัวข้อย่อยชั้นที่ 2">
                                <?php foreach ($oit->oit as $indexP=>$item): ?>
                                    <?php foreach ($item->children as $indexC1=>$firstChildren): ?>
                                        <?php foreach ($firstChildren->children as $indexC2=>$secondLevelChild): ?>
                                        <option value="<?php echo $indexP; ?>/<?php echo $indexC1; ?>/<?php echo $indexC2; ?>"><?php echo $secondLevelChild->title; ?></option>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </optgroup>
                            </select>
                        </div>

                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success pull-right">เพิ่มข้อมูลสาธารณะ</button>
                    </div>
                    <!-- /.box-footer -->
                    <?php echo form_close(); ?><!-- form end -->
                </div>
            </div>

        </div>

    </div>
    <div id="edit-box-page" style="display: none;" class="col-md-12">

    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
            
                <div class="box-header with-border"  onclick="showAddOITForm()" >
                    <h3 class="box-title "><?php echo trans("add_oit"); ?>  <i class="fa fa-plus"></i></h3>
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo trans("update_oit"); ?></h3>
                </div>
                <!-- /.box-header -->

                <!-- form start -->
                <?php echo form_open('admin_controller/update_oit_put'); ?>
                <div class="box-body">
                    <!-- include message block -->
                    <?php if (empty($this->session->flashdata("mes_menu_limit"))):
                        $this->load->view('admin/includes/_messages_form');
                    endif; ?>

                    <div class="form-group">
                        <label><?php echo trans("title"); ?></label>
                        <input id="title_edit" type="text" class="form-control" name="title" placeholder="<?php echo trans("title"); ?>"
                            value="<?php echo old('title'); ?>"
                            maxlength="200" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?> required>
                    </div>

                    <div class="form-group" >
                        <label><?php echo trans("link"); ?></label>
                        <input id="link_edit"  type="text" class="form-control" name="link" placeholder="<?php echo trans("link"); ?>"
                            value="<?php echo old('link'); ?>" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>>
                    </div>
                    <div class="form-group" type="hidden">
                        <input id="index_hidden_edit" type="hidden" class="form-control" name="index_hidden_edit" >
                    </div>
                    <div class="form-group" type="hidden">
                        <input id="parent_index_hidden_edit"  type="hidden" class="form-control" name="parent_index_hidden_edit" >
                    </div>
                    
                    
                    <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_post_pdf_upload_box'); ?>
                            </div>

                    <div class="form-group">
                        <label class="control-label">เป็นหัวข้อย่อยของ</label>
                        <select id="parent_index_edit" name="parent_index_edit" class="form-control">
                            <option value=""><?php echo trans('none'); ?></option>
                            
                        <optgroup label="หัวข้อหลัก">
                            <?php foreach ($oit->oit as $indexP=>$item): ?>
                                <option value="<?php echo $indexP; ?>"><?php echo $item->title; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                        <optgroup label="หัวข้อย่อยชั้นแรก">
                            <?php foreach ($oit->oit as$indexP=> $item): ?>
                                <?php foreach ($item->children as $indexC1=>$firstChildren): ?>
                                    <option value="<?php echo $indexP; ?>/<?php echo $indexC1; ?>"><?php echo $firstChildren->title; ?></option>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </optgroup>
                        <optgroup label="หัวข้อย่อยชั้นที่ 2">
                            <?php foreach ($oit->oit as $indexP=>$item): ?>
                                <?php foreach ($item->children as $indexC1=>$firstChildren): ?>
                                    <?php foreach ($firstChildren->children as $indexC2=>$secondLevelChild): ?>
                                    <option value="<?php echo $indexP; ?>/<?php echo $indexC1; ?>/<?php echo $indexC2; ?>"><?php echo $secondLevelChild->title; ?></option>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </optgroup>
                        </select>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit"  class="btn btn-warning pull-right">  แก้ไขข้อมูลสาธารณะ</button>

                </div>
                <!-- /.box-footer -->
                <?php echo form_close(); ?><!-- form end -->
            </div>
        </div>

    </div>

    </div>

    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="pull-left">
                    <h3 class="box-title"><?php echo trans('oit'); ?></h3>
                </div>
            </div><!-- /.box-header -->
            <div class="container">
                <div id="accordion" class="panel-group" style="padding-bottom: 100px;">
                
                <?php foreach ($oit->oit as $indexP=>$oit_item): ?>
                    <section>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#<?php echo $indexP; ?>" class="accordion-toggle collapsed" data-toggle="collapse"
                                        data-parent="#accordion"><?php echo $oit_item->title;?>
                                        <button class="btn btn-outline-primary" 
                                        onclick="editButtonClick('<?php echo $indexP; ?>','', '<?php echo $oit_item->title; ?>','<?php echo $oit_item->link; ?>')"><i class="fa fa-wrench"></i></button>
                                        
                                        <button class="btn btn-outline-primary" 
                                        onclick="deleteButtonClick('<?php echo $indexP; ?>','');"><i class="fa fa-trash"></i></button>
                                        </a>
                                        
                                </h4>
                            </div>
                            <div id="<?php echo $indexP; ?>" class="panel-collapse collapse" >
                                <div class="panel-body">
                                    <div class="pt10-pl30"><?php foreach ($oit_item->children as $indexC1=>$firstLevelChild): ?>
                                        <a href="#panel<?php echo $indexP?>_<?php echo $indexC1 ?>" class="accordion-toggle" data-toggle="collapse"
                                            data-parent="#<?php echo $indexP?>_<?php echo $indexC1 ?>"><?php echo $firstLevelChild->title;?></a><a href="#" ><i 
                                            onclick="editButtonClick('<?php echo $indexC1 ?>','<?php echo $indexP?>', '<?php echo $firstLevelChild->title; ?>','<?php echo $firstLevelChild->link; ?>')" class="fa fa-wrench fa-xs pl-5-px"></i>
                                            </a>
                                            <a href="#" ><i 
                                            onclick="deleteButtonClick('<?php echo $indexC1 ?>','<?php echo $indexP?>')" class="fa fa-trash fa-xs pl-5-px"></i>
                                            </a>
                                            <br>
                                            
                                            <?php if($firstLevelChild->children!=null):?>
                                                    
                                            <div id="<?php echo $indexP?>_<?php echo $indexC1 ?>" style="padding-left: 40px;">
                                            <div id="panel<?php echo $indexP?>_<?php echo $indexC1 ?>" class="panel-collapse collapse">
                                                <div class="panel-body" style="padding-left: 15px;">
                                                
                                                    <?php foreach($firstLevelChild->children as $indexC2=>$secondLevelChild): ?>
                                                    <p><strong> <a target="_blank" ><?php echo $secondLevelChild->title;?></a></strong>
                                                    <a href="#" ><i onclick="editButtonClick('<?php echo $indexC2 ?>','<?php echo $indexP?>/<?php echo $indexC1 ?>','<?php echo $secondLevelChild->title; ?>','<?php echo $secondLevelChild->link; ?>')" class="fa fa-wrench fa-xs pl-5-px"></i></a>
                                                     
                                                    <a href="#" ><i onclick="deleteButtonClick('<?php echo $indexC2 ?>','<?php echo $indexP?>/<?php echo $indexC1 ?>')" 
                                                    class="fa fa-trash fa-xs pl-5-px"></i></a>
                                                     <br></p></a>  
                                                    <?php if($secondLevelChild->children!=null):?>
                                                    
                                                        <?php foreach($secondLevelChild->children as $indexC3=>$thirdLevelChild): ?>
                                                            <a target="_blank"><?php echo $thirdLevelChild->title;?></a>
                                                            <a href="#" ><i onclick="editButtonClick('<?php echo $indexC3 ?>','<?php echo $indexP?>/<?php echo $indexC1 ?>/<?php echo $indexC2 ?>', '<?php echo $thirdLevelChild->title; ?>','<?php echo $thirdLevelChild->link; ?>')" 
                                                            class="fa fa-wrench fa-xs pl-5-px"></i></a>
                                                            <a href="#" ><i onclick="deleteButtonClick('<?php echo $indexC3 ?>','<?php echo $indexP?>/<?php echo $indexC1 ?>/<?php echo $indexC2 ?>')" 
                                                            class="fa fa-trash fa-xs pl-5-px"></i></a>
                                            </a><br></p>
                                           <br>
                                                        <?php endforeach;?>

                                                    <?php  endif; ?>
                                                        
                                                    <?php endforeach;?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  endif; ?>
                                        
                                        <?php endforeach ?>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </section>
                <?php endforeach ?>
                </div>
                </div>
        </div>
    </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type='text/javascript'>
    
    function deleteButtonClick(index,parentIndex ) {
        //alert(1324523)
        swal({
            text: "คุณแน่ใจว่าต้องการลบข้อมูลสาธารณะส่วนนี้? หัวข้อข้างในนี้จะถูกลบด้วย",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (willDelete) {
            /*swal({
                text: "sddsfds",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            alert("aaaa");
            */
           if (willDelete) {
                var data = {
                    'index': index,
                    'parent_index': parentIndex,
                };
                data[csfr_token_name] = $.cookie(csfr_cookie_name);

                //alert("success");
                $.ajax({
                    type: "POST",
                    url: 'delete_oit',
                    data: data,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        });
    }
    function showAddOITForm(){
        
        $("#edit-box-page").hide(500);
        $("#add-box-page").show(500);
    }
    /*function deleteButtonClick(index,parent_index){
        alert(parent_index)
        alert(index)
    }*/
   
    function editButtonClick(index,parent_index,title,link)
    {
        $("#title_edit").val(title);
        $("#link_edit").val(link);
        $("#index_hidden_edit").val(index);
        $("#parent_index_hidden_edit").val(parent_index);
        $("#parent_index_edit").val(parent_index);
        $("#edit-box-page").show(500);
        $("#add-box-page").hide(500);
        
       //alert(index)
    }
    function test(){
        e.preventDefault();
        return false;
    }
    
    function summitEdit() {
        //var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    //csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
        //var csrf_test_name = $("input[name=csrf_test_name]").val();
        var datastring = {
            title : $("#title_edit").val()
            , link : $("#link_edit").val()
            , index_hidden : $("#index_hidden_edit").val()
            , parent_index_hidden : $("#parent_index_hidden_edit").val()
            , parent_index : $("#parent_index_edit").val()
        };
        //e.preventDefault();
        $.ajax({
            url: 'update_oit_put',
            type: 'POST',
            data:datastring,
            error: function() {
                alert('Something is wrong');
            },
            success: function(data) {
                alert('success');
            }
        });
       //alert(title)
       //alert(link)
       //alert(index_hidden)
       //alert(parent_index_hidden)
       //alert(parent_index)
    }
/*
    $(function(){
    $("#comment").submit(function() {
        
        //e.preventDefault();
       datastring = $("#comment").serialize();
       $.ajax({
           url: 'update_oit_put',
           type: 'POST',
           data:datastring ,
           error: function() {
                alert('Something is wrong');
           },
           success: function(data) {
                alert('success');
           }
        });
       //alert(title)
       //alert(link)
       //alert(index_hidden)
       //alert(parent_index_hidden)
       //alert(parent_index)
       return false;
    });
});*/
    $(document).ready(function()
    {
        $('#button_1').click(function()
        {
            alert("test");
        });
    });
</script>