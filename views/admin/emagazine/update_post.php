<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-sm-12">
        <!-- form start -->
        <?php echo form_open_multipart($controller . '/update_post_post'); ?>
        <div class="row">
            <div class="col-sm-12 form-header">
                <h1 class="form-title"><?php echo $title; ?></h1>
                <a href="<?php echo admin_url() . $type; ?>?lang_id=<?php echo $general_settings->site_lang; ?>" class="btn btn-success btn-add-new pull-right">
                    <i class="fa fa-bars"></i>
                    <?php echo trans($type); ?>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-post">
                    <div class="form-post-left">
                        <?php $this->load->view("admin/includes/_form_update_pdf_left"); ?>
                    </div>
                    <div class="form-post-right">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_post_image_edit_box'); ?>
                            </div>
                            <?php if (empty($post->feed_id)) : ?>
                                <div class="col-sm-12">
                                    <div class="box">
                                        <div class="box-header with-border">
                                            <div class="left">
                                                <h3 class="box-title">อัพโหลด e-magazine</h3>
                                            </div>
                                        </div>

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="form-group m0">
                                                <label class="control-label text-danger"><b>ไม่สามารถแก้ไข ไฟล์ e-magazine<b></label>
                                                <label class="control-label">เนื่องจากระบบได้ทำการ ติดตั้ง e-mazine เรียบร้อยแล้ว</label>
                                                <div class="control-label text-primary text-center">
                                                    <a class=" btn btn-primary btn-lg btn-block" href="<?php echo base_url() . $post->post_type . '/' . $post->video_path; ?>" target="_blank" class="table-user-link">
                                                        <strong>open e-magazine : <?= $post->video_path ?></strong>
                                                    </a>
                                                </div>
                                                <label class="control-label  text-danger">หากต้องการแก้ไข ต้องลบ Post นี้และสร้างใหม่</label>
                                            </div>
                                            <div class="form-group m0">
                                                <div class="row">
                                                    <div id="post_selected_pdf" class="col-sm-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_sidebar_language_categories_edit'); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_post_publish_edit_box'); ?>
                            </div>
                            <?php if (!get_ft_tags()) {
                                exit();
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- form end -->
    </div>
</div>