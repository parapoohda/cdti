<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-sm-12">
        <!-- form start -->
        <?php echo form_open_multipart($controller . '/add_post_post', 'id="submit-form"'); ?>
        <div class="row">
            <div class="col-sm-12 form-header">
                <h1 class="form-title"><?php echo $add_title; ?></h1>
                <a href="<?php echo admin_url() . $type; ?>?lang_id=<?php echo $general_settings->site_lang; ?>" class="btn btn-success btn-add-new pull-right">
                    <i class="fa fa-bars"></i>
                    <?php echo $title; ?>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-post">
                    <div class="form-post-left">
                        <?php $this->load->view("admin/includes/_form_add_pdf_left"); ?>
                    </div>
                    <div class="form-post-right">
                        <div class="row">
                             <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_post_image_upload_box'); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_emagazine_upload_box'); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_sidebar_language_categories', ['type' => $type]); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/includes/_post_publish_emagazine_box', ['type' => $type]); ?>
                            </div>
                            <?php if (!get_ft_tags()) {
                                exit();
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- form end -->
    </div>
    <script type="text/javascript">
        function openLoading() {
            $('#exampleModal').modal('show');
        }
    </script>
    <!-- Modal -->
    <div class="modal fade" data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">ระบบกำลังติดตั้ง E-magazine</h4>
                </div>
                <div class="modal-body">
                    <h3><i class="fa fa-circle-o-notch fa-spin"></i> loading ... </h3>
                    <p class="text.danger">อาจใช้เวลานานกว่า 10-15 นาที เนื่องจากไฟล์มี ขนาดใหญ่</p>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>