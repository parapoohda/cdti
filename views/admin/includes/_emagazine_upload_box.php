<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="box">
    <div class="box-header with-border">
        <div class="left">
        <h3 class="box-title">อัพโหลด e-magazine</h3>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group m0">
            <label class="control-label">อัพโหลดไฟล์ zip ขนาดไม่เกิน 36 mb เท่านั้น</label>

            <div class="row">
                <div class="col-sm-12">
                    <input type="file" class="form-control-file" id="eMagazineFile" name="eMagazineFile" >
             
                </div>
            </div>
        </div>
        <div class="form-group m0">
            <div class="row">
                <div id="post_selected_pdf" class="col-sm-12">
                </div>
            </div>
        </div>
    </div>
</div>
