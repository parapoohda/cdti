<div class="box">
      <div class="box-header with-border">
          <div class="left">
              <h3 class="box-title"><?php echo trans('file'); ?></h3>
          </div>
      </div><!-- /.box-header -->

      <div class="box-body">
          <div class="form-group m0">
            <label class="control-label"><?php echo trans('additional_file'); ?></label>

            <div class="row">
                <div class="col-sm-12">
                    <a class='btn btn-sm bg-purple' data-toggle="modal" data-target="#1pdf_file_manager" onclick="$('#selected_pdf_type').val('additional_file');">
                        <?php echo trans('select_file'); ?>
                    </a>
                </div>
            </div>
        </div>
          <div class="form-group">
              <div class="row">
                  <div class="col-sm-12">
                      <div id="post_selected_pdf">
                          <?php $files = get_post_files($post->id); ?>
                          <?php if (!empty($files)): ?>
                              <?php foreach ($files as $file): ?>
                                  <p class="play-list-item file-list-item-<?php echo $file->files_id; ?>">
                                      <i class="fa fa-file"></i>&nbsp; <?php echo $file->file_name; ?>
                                      <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right btn-delete-files-database" data-value="<?php echo $file->files_id; ?>" data-post-id="<?php echo $post->id; ?>">
                                          <?php echo trans("delete"); ?>
                                      </a>
                                  </p>
                              <?php endforeach; ?>
                          <?php else: ?>
                              <span class="play-list-empty"><?php echo trans('play_list_empty'); ?></span>
                          <?php endif; ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>



  <?php $this->load->view("admin/includes/_file_manager_pdf1"); ?>
