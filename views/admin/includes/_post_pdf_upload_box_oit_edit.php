<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="box">
    <div class="box-header with-border">
        <div class="left">
            <h3 class="box-title"><?php echo trans('file'); ?></h3>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group m0">
            <label class="control-label"><?php echo trans('additional_file'); ?></label>

            <div class="row">
                <div class="col-sm-12">
                    <a class='btn btn-sm bg-purple' data-toggle="modal" data-target="#1pdf_file_manager_oit_edit" onclick="$('#selected_pdf_type').val('additional_file');">
                        <?php echo trans('select_file'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group m0">
            <div class="row">
                <div id="post_selected_pdf" class="col-sm-12">

                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="1pdf_file_manager_oit_edit" class="modal fade modal-file-manager" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo trans('file_manager'); ?></h4>
            </div>
            <div class="modal-body">

                <div class="file-manager">

                    <div class="file-manager-left">

                        <div id="add_pdf_form">
                            <div class="form-group">
                                <label class="control-label"><?php echo trans('pdf_name'); ?></label>
                                <input type="text" id="pdf_name" class="form-control" placeholder="pdf Name" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>>
                            </div>

                            <div class="form-group">
                                <label class="control-label"><?php echo trans('pdf_file'); ?></label>
                                <div class="row">
                                    <div class="col-sm-12 m-b-10">
                                        <a class='btn btn-sm bg-olive'>
                                            <?php echo trans('select_file'); ?>
                                            <input type="file" id="pdf_file_input" name="file" class="upload-file-input input-post-image-file" accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="$('#input_pdf_file_label').html($(this).val());">
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input-file-label" id="input_pdf_file_label"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <a id="btn_pdf_upload_oit_edit" class='btn btn-lg bg-purple btn-upload'>
                                        <i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;
                                        <?php echo trans('upload'); ?>
                                    </a>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="loader-file-manager m-t-15">
                                        <img src="<?php echo base_url(); ?>assets/admin/img/loader.gif" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="file-manager-right">
                        <div class="file-manager-content">
                            <div id="pdf_file_upload_response">
                                <?php foreach ($pdfs as $pdf): ?>
                                    <div class="col-sm-2 col-file-manager" id="pdf_col_id_<?php echo $pdf->id; ?>">
                                        <div class="file-box" data-file-id="<?php echo $pdf->id; ?>">
                                            <img src="<?php echo base_url(); ?>assets/admin/img/pdf-file.png" alt="" class="img-responsive file-icon">
                                            <p class="file-manager-list-item-name"><?php echo character_limiter($pdf->file_name, 18, '...'); ?></p>
                                        </div>
                                    </div>
                                    <?php $_SESSION["fm_last_pdf_id"] = $pdf->id; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" id="selected_pdf_file_id">

                </div>

            </div>


            <div class="modal-footer">

                <div class="file-manager-footer">
                    <button type="button" id="btn_pdf_delete_edit" class="btn btn-danger pull-left btn-file-delete"><i class="fa fa-trash"></i>&nbsp;&nbsp;<?php echo trans('delete'); ?></button>
                    <button type="button" id="btn_pdf_select_edit" class="btn bg-olive btn-file-select"><i class="fa fa-check"></i>&nbsp;&nbsp;<?php echo trans('select_pdf'); ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('close'); ?></button>
                </div>


            </div>

        </div>

    </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type='text/javascript'>
$(document).on('click', '#1pdf_file_manager_oit_edit #btn_pdf_select_edit', function () {
    select_1pdf();
});
$(document).on('dblclick', '#1pdf_file_manager_oit_edit .file-box', function () {
    select_1pdf();
});
$(document).on('click', '#1pdf_file_manager_oit_edit .file-box', function () {
    $('#pdf_file_manager .file-box').removeClass('selected');
    $(this).addClass('selected');
    var val = $(this).attr('data-file-id');
    $('#selected_pdf_file_id').val(val);

    $('#btn_pdf_delete').show();
            $('#btn_pdf_select').show();
    $('#btn_pdf_delete_edit').show();
    $('#btn_pdf_select_edit').show();
});

$(document).on('click', '#1pdf_file_manager_oit_edit .file-box', function () {
    $('#1pdf_file_manager_oit_edit .file-box').removeClass('selected');
    $(this).addClass('selected');
    var val = $(this).attr('data-file-id');
    $('#selected_pdf_file_id').val(val);

            $('#btn_pdf_delete').show();
            $('#btn_pdf_select').show();
    $('#btn_pdf_delete_edit').show();
    $('#btn_pdf_select_edit').show();
});


//upload pdf
$(document).on('click', '#btn_pdf_upload_oit_edit', function () {

    if ($('#add_pdf_form #pdf_name').val() == '') {
        $('#add_pdf_form #pdf_name').css("border-color", "#A3122F");
        return;
    } else {
        $('#add_pdf_form #pdf_name').css("border-color", "#d2d6de");
    }
    if ($('#add_pdf_form #pdf_file_input').prop('files')[0]) {

        $(".loader-file-manager").show();
        $("#btn_pdf_upload_oit_edit").prop("disabled", true);

        var form_data = new FormData();
        form_data.append('pdf_file', $('#add_pdf_form #pdf_file_input').prop('files')[0]);
        form_data.append('pdf_name', $('#add_pdf_form #pdf_name').val());
        form_data.append(csfr_token_name, $.cookie(csfr_cookie_name));

        $.ajax({
            method: 'POST',
            url: base_url + "file_controller/upload_pdf_file",
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                document.getElementById("pdf_file_upload_response").innerHTML = response;
                $(".loader-file-manager").hide();
                $("#btn_pdf_upload_oit_edit").prop("disabled", false);
                $('#add_pdf_form #pdf_name').val('');
                $('#add_pdf_form #pdf_file_input').val('');
                $('#input_pdf_file_label').html('');
            },
            error: function (response) {
            }
        });
    }

});


//delete pdf file
$(document).on('click', '#pdf_file_manager #btn_pdf_delete_edit', function () {

    var file_id = $('#selected_pdf_file_id').val();

    $('#pdf_col_id_' + file_id).remove();

    var data = {
        "file_id": file_id
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "file_controller/delete_pdf_file",
        data: data,
        success: function (response) {
            $('#btn_pdf_delete_edit').hide();
            $('#btn_pdf_select_edit').hide();
            $('#btn_pdf_delete').hide();
            $('#btn_pdf_select').hide();
        }
    });

});

//select pdf file
$(document).on('click', '#pdf_file_manager #btn_pdf_select', function () {
    select_pdf();
});

//select pdf file on double click
$(document).on('dblclick', '#pdf_file_manager .file-box', function () {
    select_pdf();
});

//select pdf file
$(document).on('click', '#1pdf_file_manager_oit_edit #btn_pdf_select', function () {
    select_1pdf_edit();
});

//select pdf file on double click
$(document).on('dblclick', '#1pdf_file_manager_oit_edit .file-box', function () {
    select_1pdf_edit();
});

//select pdf file
function select_pdf() {
    $('#pdf_file_manager').modal('toggle');

    var file_id = $('#selected_pdf_file_id').val();

    var data = {
        "file_id": file_id
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "file_controller/select_pdf_file",
        data: data,
        success: function (response) {
            // document.getElementById("post_selected_pdf").innerHTML = response;
            $('#post_selected_pdf').append(response);
            $('#pdf_file_manager .file-box').removeClass('selected');
            
            $('#btn_pdf_delete_edit').hide();
            $('#btn_pdf_select_edit').hide();
            $('#btn_pdf_delete').hide();
            $('#btn_pdf_select').hide();
        }
    });

};

function select_1pdf_edit() {
    
    $('#1pdf_file_manager_oit_edit').modal('toggle');

    var file_id = $('#selected_pdf_file_id').val();
    $('#selected_pdf_id_add').val(file_id);
    $('#selected_pdf_id_edit').val(file_id);
    
    var data = {
        "file_id": file_id
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "file_controller/select_pdf_file",
        data: data,
        success: function (response) {
            $.each($('#post_selected_pdf a'), (index, a) => {
                console.log(a);
                var item_id = $(a).attr("data-value");
                var post_id = $(a).attr("data-post-id");
                delete_post_file(item_id, post_id);
            });

            $('#post_selected_pdf').empty();
            // document.getElementById("post_selected_pdf").innerHTML = response;
            $('#post_selected_pdf').append(response);
            $('#1pdf_file_manager_oit_edit .file-box').removeClass('selected');
            
            $('#btn_pdf_delete_edit').hide();
            $('#btn_pdf_select_edit').hide();
            $('#btn_pdf_delete').hide();
            $('#btn_pdf_select').hide();
            /*$.each($('#post_selected_pdf_edit a'), (index, a) => {
                console.log(a);
                var item_id = $(a).attr("data-value");
                var post_id = $(a).attr("data-post-id");
                delete_post_file(item_id, post_id);
            });

            $('#post_selected_pdf_edit').empty();
            // document.getElementById("post_selected_pdf_edit").innerHTML = response;
            $('#post_selected_pdf_edit').append(response);
            $('#1pdf_file_manager_oit_edit .file-box').removeClass('selected');
            
            $('#btn_pdf_delete_edit').hide();
            $('#btn_pdf_select_edit').hide();
            $('#btn_pdf_delete').hide();
            $('#btn_pdf_select').hide();
        */
       }
    });

};
//load more pdfs
jQuery(function ($) {
    $('#pdf_file_manager .file-manager-content').on('scroll', function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            var data = {};
            data[csfr_token_name] = $.cookie(csfr_cookie_name);

            $.ajax({
                type: "POST",
                url: base_url + "file_controller/load_more_pdfs",
                data: data,
                success: function (response) {
                    $("#pdf_file_upload_response").append(response);
                }
            });
        }
    })
    $('#1pdf_file_manager_oit_edit .file-manager-content').on('scroll', function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            var data = {};
            data[csfr_token_name] = $.cookie(csfr_cookie_name);

            $.ajax({
                type: "POST",
                url: base_url + "file_controller/load_more_pdfs",
                data: data,
                success: function (response) {
                    $("#pdf_file_upload_response").append(response);
                }
            });
        }
    })
});

</script>