<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="box">
    <div class="box-header with-border">
        <div class="left">
            <h3 class="box-title"><?php echo trans('file'); ?></h3>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group m0">
            <label class="control-label"><?php echo trans('additional_file'); ?></label>

            <div class="row">
                <div class="col-sm-12">
                    <a class='btn btn-sm bg-purple' data-toggle="modal" data-target="#pdf_file_manager" onclick="$('#selected_pdf_type').val('additional_file');">
                        <?php echo trans('select_file'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group m0">
            <div class="row">
                <div id="post_selected_pdf" class="col-sm-12">

                </div>
            </div>
        </div>
    </div>
</div>






<?php $this->load->view("admin/includes/_file_manager_pdf"); ?>
