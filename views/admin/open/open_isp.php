<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
  <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="pull-left">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="row">
                    <!-- include message block -->
                    <div class="col-sm-12">
                        <?php $this->load->view('admin/includes/_messages'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped dataTable" id="cs_datatable"
                                   role="grid"
                                   aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th style="width: 5%;">รหัส</th>
                                    <th>ชื่อ </th>
                                    <th>บริษัท </th>
                                    <th>โทรศัพท์</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                <tbody>
                                   <?php foreach ($data as $key => $item): ?>
                                          <tr >
                                              <td><?php echo $item->OldRegisterID?></td>
                                              <td><?php echo $item->Prefix.' '.$item->FirstName.' '.$item->LastName?></td>
                                              <td><?php echo $item->Org?></td>
                                              <td><?php echo $item->Mobile?></td>
                                              <td><?php echo $item->Email?></td>
                                          </tr>
                                   <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div>
    </div>
</div>
