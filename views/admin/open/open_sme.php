<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
  <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="pull-left">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="row">
                    <!-- include message block -->
                    <div class="col-sm-12">
                        <?php $this->load->view('admin/includes/_messages'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped dataTable" id="cs_datatable"
                                   role="grid"
                                   aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th style="width: 5%;">รหัส</th>
                                    <th>ประเภท</th>
                                    <th style="width: 30%;">ชื่อ SME</th>
                                    <th style="width: 30%;">ชื่อ ผู้ประกอบการ</th>
                                    <th>โทรศัพท์</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                <tbody>
                                   <?php foreach ($data as $key => $item): ?>
                                          <tr >
                                              <td><?php echo $item->Gen_ID?></td>
                                              <td><?php echo ($item->genType == 1)?'SME':'STARTUP'?></td>
                                              <td><?php echo $item->SMEs_Name?></td>
                                              <td><?php echo $item->First_Name.' '.$item->Last_Name?></td>
                                              <td><?php echo $item->Mobile?></td>
                                              <td><?php echo $item->Email?></td>
                                          </tr>
                                   <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div>
    </div>
</div>
