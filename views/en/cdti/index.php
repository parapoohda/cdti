 <?php $this->load->view('cdti/include/banner',['banners' => $banners])?>

<div class="bg-white">
    <section id="news-heighlight" class="mini-spacer feature7 news">
        <?php $this->load->view('cdti/include/post/main_featured_post',['data' => $featured_posts]);?>
    </section>

    <section id="news-heighlight" class="mini-spacer feature7 news">
        <?php $this->load->view('cdti/include/post/main_activity_post',['activity_posts' => $activity_posts]);?>
    </section>

    <!--<section id="news-heighlight" class="mini-spacer feature7 news">
        <?php /*$this->load->view('cdti/include/post/main_activity_post_6',['activity_posts_6' => $activity_posts_6]);*/?>
    </section>
-->
    <section id="news-heighlight" class="mini-spacer feature7 news">
        <?php $this->load->view('cdti/include/post/main_activity_post_2',['activity_posts_2' => $activity_posts_2,'activity_posts_3' => $activity_posts_3]);?>
    </section>

    <section id="news-heighlight" class="mini-spacer feature7 news">
        <?php $this->load->view('cdti/include/post/main_activity_post_3',['activity_posts_6' => $activity_posts_6,'activity_posts_5' => $activity_posts_5]);?>
    </section>
<div>

<?php //$this->load->view('cdti/include/template/calendar.php'); ?>

<section id="news" class="mini-spacer feature7 news hide">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                
                <?php // $this->load->view('cdti/include/post/main_post',['data' => $latest_posts]);?>

                <?php $this->load->view('cdti/include/post/channel_post',['data' => $channel_posts]);?>
            </div>
            <?php /*
            <div class="col-lg-4 col-md-3">
                <div class="sidebar">
                  <?php $this->load->view("cdti/include/event_calendar",['data' => $event_calendar]);?>
                </div>
            </div> */?>
        </div>
    </div>
</section>

<div class="bg-white">
      	<?php //$this->load->view("cdti/include/gallery_cdti",["main_category1_cover" => $main_category1_cover])?>
      	<?php $this->load->view("cdti/include/partner",["link" => $links])?>
</div>

<div class="container">
    <?php $this->load->view('cdti/include/list_tag',['categories' => $categories]);?>
</div>
<script src="<?php echo base_url() ?>cdti_assets/plugin/slick/slick.min.js"></script>
<script>
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    centerPadding: '40px',
    focusOnSelect: true,
    arrows: true,
  });
</script>