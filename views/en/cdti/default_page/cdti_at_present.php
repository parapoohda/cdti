
<div class="container-fluid">
          <section class="campus-box mini-spacer">
	<div class="container">
        
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> CDTI at present</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section  class="banner-history" style="background-image:url(<?php echo base_url()?>/cdti_assets/images/bg_history.png)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center  align-self-center">
            <!-- Column -->
            <div class="col-md-4 text-center  bg-yellow">
               <div class="spacer">
               		<img src="<?php echo base_url()?>cdti_assets/images/about-logo.png" alt="" class="img-responsive ">
               </div>
            </div>
            <div class="col-md-6 align-self-center text-center aos-init aos-animate static-slider10" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">Cdti at present</h1>
            </div>
            <!-- Column -->
        </div>
    </div>
</section>
<section id="history-slide" class="history-slide mini-spacer">
	<div class="container">
		<div class="row mb-4">
			<div class="col-sm-12 text-center">
				<h3 class="color-primary history-slide-title">Cdti at Present</h3>
			</div>
		</div>
    </div>
    <div class="container">
        
        <
<div class="mini-spacer">
            <p class="color-primary font-18">
            CDTI at Present is the educational institute with the concept from the royal initiative “Study and Work – <span class = ''>Work and Study” to foster professional skills, self-discipline, and ethics in each individual.</span>

CDTI offers three levels of education, including vocational certificates, diplomas or high vocational certificates, and bachelor degrees, with a work-integrated learning and instruction approach in various areas of technology both within the institute and in the real workplaces of more than 80-CDTI networking well-known Thai companies.

The certificate and diploma programs are comprised of electronic technology, mechatronics, embedded systems, marketing, information and communication technology, food industry, Thai and international chef, Thai musical instrument craftsmanship, innovative agriculture, and railway engineering technologies. The bachelor’s degree programs cover the field of food business management, electrical engineering technology, and digital technology.

CDTI has been engaging in helping students reach their highest potential, be able to create success for themselves, and take an active part in building a better society and a better world. In doing so, thereby, CDTI embraces the moral philosophy of devoting self to nation, religion and the monarch, having disciplines and morals to guide learning, and embracing hard work.

The teaching and learning methods combine theoretical knowledge with vocational content, allowing students to demonstrate multi-dimensional abilities such as critical thinking, creative thinking, and practical skills. Students will also be encouraged to have a moral sense of duty, a passion for their profession, personal and civic responsibility, and a willingness to volunteer. These are important characteristics for CDTI graduates.

            </p>
        </div>
    </div>
	<div  class="mini-spacer" style="text-align: center;">
			<video width="800" controls>
		<source src="<?php echo base_url()?>cdti_assets/videos/สถาบันเทคโนโลยีจิตรลดา เรียนรู้คู่ทำงาน(ฉบับEng).mp4" type="video/mp4">
		Your browser does not support HTML video.
		</video>
	</div>	
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>

