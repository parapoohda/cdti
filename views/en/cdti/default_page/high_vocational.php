<!--red color custom  color-->
<div class="container-fluid">
<section class="campus-box mini-spacer">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i>CHITRALADA VOCATIONAL SCHOOL </h3>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="banner-innerpage  mb-4" style="background-image:url(<?php echo  base_url() ?>cdti_assets/images/banner-high-vocational-eng.jpg)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<section class="campus-box">
    <div class="container">
        
		<div>
			<p class = " font-20">
                Chitralada Vocational School, which operates under the auspices of Chitralada Technology Institute, provides vocational and high vocational certificate levels in a variety of study programs such as agriculture, business administration, entertainment and music industry, home economics, industry, and information and communication technology.  The school's customized programs in integrated state-of-the-art teaching and learning methodologies, as well as rigorous practical training programs in the country's leading companies, distinguish the school and increase students' professional skills and quality, as well as their potential for taking next steps in the real society of careers and professions.
            </p>
			
		</div>
		<div class ="row">
		<div class="col-lg-6 align-middle">
		<img id="selected_image_file" name="" src="<?php echo base_url() ?>uploads/images/image_750x_5fc9fd275f096.jpg" alt="" class="img-responsive" />
		</div>
		<div>
			
            <h3 class = "font-light color-primary">
                High Vocational Certificate (High Voc. Cert.)
 
			</h3>
			<h3 class = "font-light color-primary">
                Highlights of the program
			</h3>
			<hr class="hr-info">
			<p>
                Chitralada Vocational School provides students with the opportunity to participate actively in shaping their educational plans, to broaden their experiences, and to study deeply. Students can enroll in the following fields of study and programs to suit their interests and potentials:
            </p>
			<ul>
				<li>
                    <span class =  "color-primary">Regular Education Program</span><br>
                    This program is designed for students to gradually acquire knowledge and process the core and functional competencies in their professions.  Students are required to attend classes for 6 semesters in vocational certificate level and 4 semesters in high vocational certificate level.  Before graduation, they have to participate in practical trainings at real workplaces in more than 80 leading companies throughout the country during summer semesters.
                </li>
				
				<li>
                    <span class =  "color-primary">Dual Vocational Education Program</span><br>
                    The school, in collaboration with networking leading companies, sets up the professional practice scheme for high vocational students.  Students obtain fundamental professional knowledge and life skills by taking courses with expert teachers at special designed workshops on campus during the first year, followed by another year of practical training in networking workplaces.  Upon graduation, students are offered appropriate occupations by these companies.
                </li>
                
				<li>
                    <span class =  "color-primary">Dual Degree Program</span><br>
                    Based on the cooperation with partner vocational and technical colleges in China, the school has initiated 2 dual degree programs in high vocational education level in mechatronics and robotics with Tianjin Bohai Vocational Technical College, and embedded system software with Guizhou Light Industry Technical College.  Students are granted 2 high vocational certificates from the colleges in China and CDTI upon the completion of their study programs.
                    The school also organized short-term programs in food and nutrition with Leshan Vocational and Technical College for over the past 10 years.
                </li>
            </ul>
		</div>
        <div>
            <h3 class = "font-light color-primary">
                Fields of study
            </h3>
            <hr class="hr-info">
            <ul>
                <li>
                    Automotive Body and Painting (dual vocational education program)
                </li>

                <li>
                    Automotive Parts Production (dual vocational education program)
                </li>
                
                <li>
                    Automotive Technology (regular/dual vocational education programs)
                </li>
                
                <li>
                    Control and Maintenance Techniques for Transportation Railway System 
                </li>
                <li>
                    Embedded System Software (2-3 years dual degree program) 

                </li>
                
                <li>
                    Food Industry
                </li>
                
                <li>
                    Industrial Electronics (regular/dual vocational education programs)

                </li>
                
                <li>
                    International Food Chef (dual vocational education program)

                </li>
                
                <li>
                    International Food and Nutrition
                </li>
                
                <li>
                    Information Technology
                </li>
                
                <li>
                    Marketing
                </li>
                
                <li>
                    Mechatronics and Robotics (dual vocational education/dual degree programs)
                </li>
                
                <li>
                    Power Electrics (regular/dual vocational education programs)
                </li>
                
                <li>
                    Thai Food Chef
                </li>
                
                <li>
                    Thai Musical Instrument Craftsmanship Technology

                </li>
            </ul>
		</div>

		<div>
			<h3 class = "font-light color-primary">
				Admission requirements
			</h3>
			<hr class="hr-info">
			<p>
                An applicant must have completed a higher secondary education level, or hold a vocational certificate in allied fields of study, or equivalent from a school or institution accredited by the Ministry of Education.
            </p>
		</div>
		
		<div class ="col-12">
			<h3 class = "font-light color-primary">For inquiries</h3>
			<hr class="hr-info">Chitralada Vocational School<br>
                Tel: +662-282-6808, +662-282-6782, +662-282-6786 during office hours<br>
                Email: info@cdti.ac.th

		</div>


	</div>
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>

