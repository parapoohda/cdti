
<div class="container-fluid">
<section class="campus-box mini-spacer">
	<div class="container">
        
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> Business Admin</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="banner-innerpage  mb-4" style="background-image:url(https://www.cdti.ac.th//cdti_assets/images/banner_campus_53.jpg)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<section id="history-slide" class="history-slide mini-spacer">
    <div class="container">
        
        <div>
            <p class="font-20">
            Thailand's advantage in appropriate location and climate for cultivation reflects the resourcefulness of agricultural and husbandry products.  With the combination of Thai arts and culture in creating the unique Thai foods with exotic tastes which is popular among Thai people and foreigners, food business in Thailand is placed at a high rank of the most prolific business as well as generates large amount of revenue for the country.  Food business is, therefore, set as a core business in Thailand’s strategic plan for the national sustainable development.
Due to the growth of food business, the demands for qualified operations and management personnel in food industry and food service businesses are continuously increased.  In order to meet business requirements and serve the country’s need, the institute has set up a curriculum to produce potential graduates with high competence and performance in food business management.
            </p>
        </div>
		<div>
			<p class="color-darkgray font-20">
				Reasons to study FBM at CDTI
			</p>
			<p class="color-primary font-18">

				1. The course incorporates both theoretical and practical learnings.<br>
				2. The educational process equips students with the skills they need to succeed in today's workplace.<br>
				3. The course provides an opportunity to work with leading food companies and gain real-world experience.<br>
				4. The course is delivered by an expert teacher and supplemented by the real-world experience of business executives.<br>
				5. The course encourages internationalization in terms of both language and mindset.
			</p>
		</div>
		<div >
			<p class="color-darkgray font-20">
				Highlights of the program
			</p>
			<ul>
				<li>Combining art and science of management and food production/food service</li>			
				<li>Combining theoretical studies and hands-on practices</li>			
				<li>Teaching by expert lecturers and experienced business executives </li>			
				<li>Collaborating with renowned companies for practical training, internship and employment opportunities</li>			
			</ul>
			
		</div>
		<div>
			<p class="color-darkgray font-20">
				Admission requirements

			</p>
			<p class="color-primary font-18">

				An applicant must have a high school diploma, a vocational certificate, or equivalent from a school or institution accredited by the Ministry of Education.
			</p>
		</div>
		
		<div>
			<p class="color-darkgray font-20">
				Program structure (138 total credit requirement)

			</p>
			<p class="color-primary font-18">

				General Education Courses 	33 credits
			</p>
			
			<div class = "row">
				<div class = "col">
					<ul>
						<li>Social Sciences</li>			
						<li>Humanities</li>			
						<li>Science and Mathematics</li>			
						<li>Language
						</li>			
					</ul>
				</div>
				<div class = "col">
					<ul>
						6 credits<br>		
						6 credits	<br>	
						6 credits		<br>
						15 credits<br>
						</li>			
					</ul>
				</div>
			</div>

		</div>
		<div>
			<p class="color-primary font-18">

			Specific Required Courses	99 credits

			</p>
			
			<div class = "row">
				<div class = "col">
				<ul>
					<li>Core courses</li>			
					<li>Major required courses</li>			
					<li>Major elective courses</li>			
					<li>Free Elective Courses
				</li>			
							</ul>
				</div>
					<div class = "col">
						<ul>
							33 credits<br>			
							54 credits	<br>		
							12 credits		<br>	
							6 credits<br>
										
						</ul>
					</div>
				</div>

				<div >
					<p class="color-darkgray font-20">
					Core courses
					</p>
					<ul>
						<li>Business Analysis and Planning</li>					
						<li>Business Finance</li>					
						<li>Business Statistics</li>					
						<li>Economics for Management</li>					
						<li>Human Resources Management</li>					
						<li>Logistics and Supply-chain Management</li>					
						<li>Operations Management</li>					
						<li>Principles of Accounting</li>					
						<li>Principles of Management </li>					
						<li>Principles of Marketing</li>				
					</ul>
					
				</div>

				<div >
					<p class="color-darkgray font-20">
					Major required courses
					</p>
					<ul>
						<li>Food Microbiology</li>							
						<li>Food Quality Control and Shelf-life Evaluation</li>							
						<li>Food Safety Management and Food Laws</li>							
						<li>Introduction to Food Science and Technology</li>							
						<li>Nutrition</li>							
						<li>Occupational Health and Safety</li>							
					</ul>
					
				</div>
				
				<div >
					
				<p class="color-darkgray font-20">
					Major elective courses
					</p>
					<p class="color-darkgray font-20">
						Food Industry Business:	
					</p>
					<ul>
						<li>Food Chemistry and Food Analysis</li>				
						<li>Food Packaging Technology and Design</li>				
						<li>Food Processing</li>				
						<li>Special Project in Food Industry Business</li>				
					</ul>
					<p class="color-darkgray font-20">
					Major required courses
					</p>
					<ul>
						<li>Basic Cooking Techniques </li>					
						<li>Cooking and Beverages Management</li>					
						<li>Restaurant Operation Management</li>					
						<li>Special Project in Food Service Business</li>					
					</ul>
				</div>
			</div>
			<div>
				<p class="color-darkgray font-20">
					Program structure (138 total credit requirement)

				</p>
				<p class="color-primary font-18">

					Internship and Practical Training
				</p>
				
				<div class = "row">
					<div class = "col">
					Year 2/Summer: Internship 1 	<br>
Year 3/Summer: Internship 2	<br>
Year 4/Semester 1: Practical Traning 1	<br>
Year 4/Semester 2: Practical Training 	<br>
						</p>
					</div>
				</div>

			</div>
			
			<div >
				<p class="color-darkgray font-20">
				Career opportunities
				</p>
				<ul>
					<li>Entrepreneurs in food business</li>			
					<li>Personnel and executives in food production industries/food service companies</li>			
					<li>Government officials dealing with food business </li>					
				</ul>
				
			</div>
			
			<div >
				<p class="color-darkgray font-20">
				For inquiries
				</p>
				
				<p class="color-darkgray font-18">
				Faculty of Business Administration<br>
Tel: +662-280-0551 (ext. 3263 or 3267) during office hours<br>
Email: ba@cdti.ac.th
				</p>
				
			</div>

		</div>
	</div>
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>

