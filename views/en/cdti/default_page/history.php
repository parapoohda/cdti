
<div class="container-fluid">
          <section class="campus-box mini-spacer">
	<div class="container">
        
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i> History</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section  class="banner-history" style="background-image:url(<?php echo base_url()?>/cdti_assets/images/bg_history.png)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center  align-self-center">
            <!-- Column -->
            <div class="col-md-4 text-center  bg-yellow">
               <div class="spacer">
               		<img src="<?php echo base_url()?>/cdti_assets/images/logo.png" alt="" class="img-responsive ">
               </div>
            </div>
            <div class="col-md-6 align-self-center text-center aos-init aos-animate  static-slider10" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">History</h1>
            </div>
            <!-- Column -->
        </div>
    </div>
</section>
<section id="history-slide" class="history-slide mini-spacer">
	<div class="container">
		<div class="row mb-4">
			<div class="col-sm-12 text-center">
				<h3 class="color-primary history-slide-title">History</h3>
			</div>
		</div>
    </div>
    <div class="container">
        
        <div class="mini-spacer">
            <p class="color-primary font-20">
            <span class ="color-darkgray font-18">Chitralada Technology Institute promotes the same vision graciously conveyed from His Majesty King Bhumibol Adulyadej to establish Chitralada School since January 10, 1955, when His Majesty the King benevolently granted an education in the kindergarten level at Amphorn Sathan Residential Hall, Dusit Palace.

Later, His Majesty the King considerately had a permanent school building constructed in the area of Chitralada Royal Villa, Dusit Palace, and named “Chitralada School.” Thereafter, the school had expanded and provided education in all levels from kindergarten to secondary.

In order to further His Majesty King Bhumibol Adulyadej's vision, the Chitralada Education System was expanded to include vocational certificate and high vocational certificate programs, as well as the establishment of </span>Chitralada Vocational School <span  class ="color-darkgray font-18">in 2004. 

With the determination of HRH Princess Maha Chakri Sirindhorn to extend the vocational programmes to bachelor degree level, </span>“Chitralada Technology College”<span class ="color-darkgray font-18"> was established in 2014. Chitralada Technology College was a private educational institution for bachelor degree level, with the establishment announced in the Royal Government Gazette on February 24, 2014.  The college was legally supported by Chitralada School Foundation. Since the college was initiated by Her Royal Highness Princess Maha Chakri Sirindhorn, who has been the president of the College Council, the 2nd of April 2014, as HRH Princess Maha Chakri Sirindhorn’s 59th birthday, was considered the College Foundation Day. 

In 2018, Chitralada Vocational School and Chitralada Technology College were merged to form</span> Chitralada Technology Institute (CDTI) <span class ="color-darkgray font-18">under the Chitralada Technology Institute Act of 2018, on August 13, 2018.
</span>
            </p>
        </div>
    </div>
		
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>

