<!--red color custom  color-->
<div class="container-fluid">
<section class="campus-box mini-spacer">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i>Technology</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="banner-innerpage  mb-4" style="background-image:url(https://www.cdti.ac.th//cdti_assets/images/banner_campus_134.jpg)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<section class="campus-box">
    <div class="container">
        
		<div>
			<p class = " font-20">
				The Faculty of Digital Technology aspires to produce graduates to think analytically and systematically. We also focus on lifelong learning processes under the royal initiative “Study and Work – Work and Study” conceptual framework.
			</p>
			
		</div>
		<div class ="row">
		<div class="col-lg-6 align-middle">
		<img id="selected_image_file" name="" src="<?php echo base_url() ?>uploads/images/image_750x_5fc9fd275f096.jpg" alt="" class="img-responsive" />
		</div>
		
		<div class="col-lg-6 align-middle mini-spacer">
			
			<h3 class = "font-light color-primary">
				Bachelor of Engineering in Computer Engineering 
			</h3>
			<h3 class = "font-light color-primary">
				B. Eng. (Computer Engineering)
			</h3>
			<hr class="hr-info">
			<p>
				The computer engineering program teaches the fundamentals of the computer field through a series of theory and practical classes in basic engineering and computer engineering, such as electronics for I/O interfacing, computer organization, embedded systems, operating systems, and networking, etc.
				The emphasis is not only on computer concepts and theories, but also on practical working skills. There are two specializations available in the program. Students can pursue specializations in either Internet of Things (IoT) or Data Engineering after two years, depending on their interests. As electives, students can gain current knowledge and skill sets in Artificial Intelligence (AI), Cloud Computing, Big Data, and Smart Cities.
			</p>
		</div>
		<div class ="mini-spacer">
			
			<h3 class = "font-light color-primary">
				Highlights of the program
			</h3>
			<hr class="hr-info">
			<ol>
				<li>
					Understand the essence of various subsystems in order to handle the complexity of large systems

				</li>
				<li>
					Understand the concept of digital technology and computer system, hardware and software design, system requirements, system development and performance evaluation, and computer system maintenance
				</li>
				<li>
					Possess a broad knowledge and basic skills in engineering fields and have in-depth expertise in computer engineering subjects
				</li>
				<li>
					Gain experiences in hardware and software design, requirements gathering, planning, requirements specification, and development to conduct at least 1 capstone project
				</li>
				<li>
					Acquire skills in using various tools in engineering, hardware and software development, and effective assessment program
				</li>
				<li>
					Understand the responsibilities of professional engineers in terms of way of living, communication, professional ethics, professional councils and relevant laws, and the impact of the profession to the society and the environment
				</li>
				<li>
					Exhibit skills to present and write technical reports as well as analyze and review other’s works
				</li>
			</ol>
		</div>

		<div>
			<h3 class = "font-light color-primary">
				Admission requirements
			</h3>
			<hr class="hr-info">
			<p>
				An applicant must have a high school diploma with at least 30 credits in science and mathematics, or a vocational certificate with an emphasis on industrial, electronics, telecommunications, or computer technology, or an equivalent from a school or institution accredited by the Ministry of Education.
			</p>
		</div>
		<div class ="mini-spacer col-12">
			<h3 class = "font-light color-primary">
				Program structure (127 total credit requirement)
			</h3>
			<hr class="hr-info">
			<ul>
				<li>
					<div class="row">
						
						
						<div class = "col-lg-6">
						General Education Courses<br>
							Social Science<br>
							Humanities<br>
							Science and Mathematics<br>
							Language<br>

						</div>
						
						<div class = "col-lg-6">
							33 credits<br>
							6 credits<br>
							6 credits<br>
							6 credits<br>
							15 credits<br>

						</div>
					</div>
				</li>
			</ul>
			<ul>
				<li>
					<div class="row">
						<div class = "col-lg-6">
						Specific Required Courses<br>
						Core courses<br>
						Major required courses<br>
						Major elective courses<br>

						</div>
						
						<div class = "col-lg-6">
							88 credits<br>
							30 credits<br>
							43 credits<br>
							15 credits<br>

						</div>
					</div>
				</li>
				<li>
					<div class="row">
						<div class = "col-lg-6">
							Free Elective Courses
						</div>
						
						<div class = "col-lg-6">
							6 credits<br>

						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class ="mini-spacer col-12">
			<h3 class = "font-light color-primary" >
			Interesting Elective Courses
			</h3>
			<hr class="hr-info">
			<p>Artificial Intelligence<br>
				Asynchronous Programming Development<br>
				Computation Structure <br>
				Cloud Computing<br>
				Cyber-Physical Systems<br>
				Embedded Systems and IoT <br>
				High Level Design for Digital Systems <br>
				Knowledge Engineering and Data Mining<br>
				Technologies for Smart City<br>
				Web Application Development
			</p>
		</div>
		
		<div class ="mini-spacer col-12">
			<h3 class = "font-light color-primary" >
				Career Opportunities
			</h3>
			<hr class="hr-info">
			
			<p>Big data system engineer<br>
				Computer business owner/entrepreneur<br>
				Computer engineer <br>
				Computer researcher/academic assistant<br>
				Educator<br>
				Embedded system developer<br>
				Integrated computer network and information system staff<br>
				Software developer

			</p>
		</div>
		<div class ="col-12">
			<h3 class = "font-light color-primary">For inquiries</h3>
			<hr class="hr-info">
				Faculty of Digital Technology<br>
				Tel: +662-121-3700 (ext. 5001) during office hours<br>
				Email: fdt@cdti.ac.th
		</div>


	</div>
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>

