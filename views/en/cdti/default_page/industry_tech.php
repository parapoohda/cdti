<!--red color custom  color-->
<div class="container-fluid">
<section class="campus-box mini-spacer">
	<div class="container">
		<div class="section-header campus-header">
			<div class="row">
				<div class="col-sm-6 align-middle">
					 <h3 class="campus-title mt-4 color-primary"><i class="far fa-newspaper"></i>Technology</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="banner-innerpage  mb-4" style="background-image:url(https://www.cdti.ac.th//cdti_assets/images/banner_campus_62.jpg)">
    <div class="container">
        <div class="row justify-content-center spacer">
            <div class="col-md-6 align-self-center text-center spacer">
            </div>
        </div>
    </div>
</div>
<section class="campus-box">
    <div class="container">
        
		<div>
			<p class = " font-20">
				The faculty aims to produce good and highly competent graduates equipped with knowledge and skills related to industrial technology. The curricula have been prudently organized in accordance with the royal initiative “Study and Work – Work and Study” conceptual framework to encourage work efficiency and to correspond with the current trends of modern industrial technology. 
			</p>
			
		</div>
		<div class ="row">
		<div class="col-lg-6 align-middle">
		<img id="selected_image_file" name="" src="<?php echo base_url() ?>uploads/images/image_750x_5e3bd42f583f9.jpg" alt="" class="img-responsive" />
		</div>
		
			<div class="col-lg-6 align-middle mini-spacer">
				
				<h3 class = "font-light color-primary">
					Bachelor of Technology Program in Electrical Engineering Technology (4-year program)
				</h3>
				<h3 class = "font-light color-primary">
					B.Tech. (Electrical Engineering Technology)
				</h3>
				<hr class="hr-info">
				<p>
					The curriculum, which is organized around a Study and Work strategy, is designed to produce and supply highly competent graduates who can meet the demands of the engineering industry. The program is divided to 3 fields:

				</p>
				<ul>
					<li>
						Engineering Technology for Smart Building System: To produce highly-skilled graduates for the line of work related to supervising, design, inspection, installation, and maintenance of electrical systems in buildings associated with smart buildings, fire alarm system, air-conditioning system, and renewable energy and energy management system.
					</li>
					<li>
						Engineering Technology for Electric Vehicle: To supply highly-skilled graduates in areas of analyzing, designing, and maintaining electric vehicle systems, control units, and charging systems, battery technology, and automotive.
					</li>
					<li>
						Engineering Technology for Automatic Industry System: To produce highly-skilled graduates for the line of work related to production development, Supervisory Control and Data Acquisition (SCADA) system development, information management system, and industrial process control, application and development of industrial robots, real-time monitor system development, and Internet of Things (IoT) system.
					</li>
				</ul>
			</div>
			<div class ="mini-spacer">
				
				<h3 class = "font-light color-primary">
					Highlights of the program
				</h3>
				<hr class="hr-info">
				<ul>
					<li>
						Focusing on learning by doing
					</li>
					<li>
						Focusing on producing good disciplinary, studious, hard-working, and proficient graduates 
					</li>
					<li>
						Offering collaborative classes between the faculty and entrepreneurs 
					</li>
					<li>
						Supplying highly competent graduates to meet industry demands and providing career opportunities upon graduation
					</li>
					<li>
						Collaborating with leading Thai companies that have extensive coverage work units
					</li>
					<li>
						Promoting research and innovative developments in collaboration with entrepreneurs in order to provide hands-on experience
					</li>
					<li>
						Professional instruction and training
					</li>
				</ul>
			</div>
	
			<div>
				<h3 class = "font-light color-primary">
					Admission requirements
				</h3>
				<hr class="hr-info">
				<p>An applicant must have a high school diploma, vocational certificate, or equivalent from a school or institution accredited by the Ministry of Education.
</p>
			</div>
			<div class ="mini-spacer col-12">
				<h3 class = "font-light color-primary">
					Program structure (138 total credit requirement)
				</h3>
				<hr class="hr-info">
				<ul>
					<li>
						<div class="row">
							
							
							<div class = "col-lg-6">
							General Education Courses<br>
								Social Science<br>
								Humanities<br>
								Science and Mathematics<br>
								Language<br>

							</div>
							
							<div class = "col-lg-6">
								33 credits<br>
								6 credits<br>
								6 credits<br>
								6 credits<br>
								15 credits<br>

							</div>
						</div>
					</li>
				</ul>
				<ul>
					<li>
						<div class="row">
							<div class = "col-lg-6">
							Specific Required Courses<br>
								Science and Mathematics<br>
								Technology<br>
								Electrical Engineer<br>
								Major elective courses<br>
								Final project and practical training<br>

							</div>
							
							<div class = "col-lg-6">
								99 credits<br>
								12 credits<br>
								18 credits<br>
								21 credits<br>
								25 credits<br>
								17 credits<br>

							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<div class = "col-lg-6">
								Free Elective Courses
							</div>
							
							<div class = "col-lg-6">
								6 credits<br>

							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class ="mini-spacer col-12">
				<h3 class = "font-light color-primary" >
					The Cooperative Education Guidelines
				</h3>
				<hr class="hr-info">
				<ul>
					<li>
						working-learning integration
					</li>
					<li>
						three-year studying on campus and one-year training at networking companies
					</li>
					<li>
						earning while training
					</li>
					<li>
						studying English and third language (Chinese, Japanese, and German)
					</li>
					<li>
						improving fundamental engineering skills and preparing for real-life working situations
					</li>
				</ul>
			</div>
			
			<div class ="mini-spacer col-12">
				<h3 class = "font-light color-primary" >
					Career Opportunities
				</h3>
				<hr class="hr-info">
				<ul>
					<li>
						Production Supervisor
					</li>
					<li>
						Engineer: Sales Engineer, Production Engineer, Research and Innovation Engineer
					</li>
					<li>
						Maintenance Supervisor for High-rise Building Systems
					</li>
					<li>
						Engineering and Technology Trainer
					</li>
				</ul>
			</div>



			<div class ="row  mini-spacer">
				<div class="col-lg-6 align-middle">
				<img id="selected_image_file" name="" src="<?php echo base_url() ?>uploads/images/image_750x_5e3bc93774c6e.jpg" alt="" class="img-responsive" />
				</div>
				
					<div class="col-lg-6 align-middle">
						
						<h3 class = "font-light color-primary">
							Bachelor of Technology Program in Industrial Technology (2-year program)
						</h3>
						<h3 class = "font-light color-primary">
							B.Tech. (Industrial Technology)
						</h3>
						<hr class="hr-info">
						<p>
							The curriculum, which is structured with Work and Study strategy, focuses on strengthening personnel with advanced engineering skills to be adaptive to changes and to accommodate future production and service in the form of START UP. This meets the requirements of the current workforce.
						</p>
						<h3 class = "font-light color-primary">
							Highlight of program
						</h3>
						<hr class="hr-info">
						<ul>
							<li>
								Engineering Technology for Smart Building System: To produce highly-skilled graduates for the line of work related to supervising, design, inspection, installation, and maintenance of electrical systems in buildings associated with smart buildings, fire alarm system, air-conditioning system, and renewable energy and energy management system.
							</li>
							<li>
								Engineering Technology for Electric Vehicle: To supply highly-skilled graduates in areas of analyzing, designing, and maintaining electric vehicle systems, control units, and charging systems, battery technology, and automotive.
							</li>
							<li>
								Engineering Technology for Automatic Industry System: To produce highly-skilled graduates for the line of work related to production development, Supervisory Control and Data Acquisition (SCADA) system development, information management system, and industrial process control, application and development of industrial robots, real-time monitor system development, and Internet of Things (IoT) system.
							</li>
						</ul>
					</div>
					<div>
						
						<h3 class = "font-light color-primary">
							Highlights of the program
						</h3>
						<hr class="hr-info">
						<ul>
							<li>
								studying-working integration
							</li>
							<li>
								performing regular duties on weekdays: strengthening knowledge and analyzing current projects
							</li>
							<li>
								studying on weekends: improving work-related subjects (Thai, English, Math, Social), gaining new knowledge (AI, SCADA, IOT, etc.), and experimenting modern technology
							</li>
						</ul>
					</div>
			
					<div>
						<h3 class = "font-light color-primary">
							Admission requirements
						</h3>
						<hr class="hr-info">
						<p>
							An applicant must have a vocational certificate in the field of industrial engineering, at least one year of work experience, and currently hold a permanent position in the field of industrial engineering.
						</p>
					</div>
					<div class ="col-12">
						<h3 class = "font-light color-primary">
							Program structure (75 total credit requirement)
						</h3>
						<hr class="hr-info">
						<ul>
							<li>
								<div class="row">
									
									
									<div class = "col-lg-6">
									General Education Courses<br>
									Social Science and Humanities <br>
									Science and Mathematics<br>
									Language<br>

									</div>
									
									<div class = "col-lg-6">
										51 credits<br>
										24 credits<br>
										27 credits<br>
										6 credits<br>
										
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									
									
									<div class = "col-lg-6">
										Specific Required Courses<br>
										Foundation courses<br>
										Major courses<br>

									</div>
									
									<div class = "col-lg-6">
										51 credits<br>
										24 credits<br>
										27 credits<br>
										
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									
									
									<div class = "col-lg-6">
									Free Elective Courses<br>

									</div>
									
									<div class = "col-lg-6">
										6 credits<br>
										
									</div>
								</div>
							</li>
						</ul>
						
					</div>
					<div class ="col-12">
						<h3 class = "font-light color-primary" >
						The Module Learning Method

						</h3>
						<hr class="hr-info">
						Majors:
						<ul>
							<li>
								Industrial: Safety and health, Quality control, Maintenance technology, Material handling systems, Information systems for industrial management
							</li>
							<li>
								Management: Project planning and management, Innovation and design, Industrial technology seminar, Coaching, Entrepreneurship
							</li>
						</ul>
						<table>
							<tbody>
								<tr>
									<td class = " no-wrap-top">
										Year 1
										
									</td>
									<td class = " center no-wrap-top">
										:
									</td>
									<td class = "">
										students take fundamental courses in innovative technology to build essential skills in planning, quality controlling, thinking and analyzing, and designing preliminary-structure projects.
									</td>
								</tr>
								<tr>
									<td class = "no-wrap-top">
										Year 2
									</td>
									
									<td class = "center no-wrap-top">
										:
									</td>

									<td class = "">
										students examine and apply technology to gain abilities in communicating, instructing, managing, and becoming an entrepreneur.
									</td>
								</tr>
							</tbody>
						</table>
						

					</div>
				</div>
				
				<div  class ="col-12">
					<h3 class = "font-light color-primary" >
						Career Opportunities
					</h3>
					<hr class="hr-info">
					<ul>
						<li>
							Entrepreneur and Business Owner
						</li>
						<li>
							Production Administrative Office
						</li>
						<li>
							Quality Control Manager
						</li>
						<li>
							New Product Design Manager
						</li>
						<li>
							Project Manager
						</li>
						<li>
							System Analyst and Designer
						</li>
						<li>
							Engineering Designer
						</li>
						<li>
							Automatic Control System Developer
						</li>
						<li>
							Government Officer and Teacher-Lecturer
						</li>
					</ul>
				</div>
				
				<div class ="col-12">
					<h3 class = "font-light color-primary">For inquiries</h3>
					<hr class="hr-info">
					<p>Faculty of Industrial Technology<br>
Tel: +662-280-0551 (ext. 3245) during office hours<br>
Email: ind.tech@cdti.ac.th
</p>
				</div>



		</div>
	</div>
</section>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
<script src="<?php echo base_url()?>cdti_assets/node_modules/owl.carousel/dist/owl.linked.js"></script>

<script>
	$('.history-slider').owlCarousel({
		loop:true,
		margin:10,
		nav: false,
		dots: false,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoHeight:false,
		smartSpeed: 300,
		items:1,
		URLhashListener:true,
		startPosition: 'URLHash',
		// linked: '.years-slider',
		onDragged: callback,
        responsive : {
            0 : {
            },
            480 : {
            },
            768 : {

            },
            992 : {

            }
        }
  	})
  	// $('.history-slider').on('dragged.owl.carousel', function(e){
  	// 	 callback();
  	// });
  	function callback(event){
  		var namespace = event.namespace;
  		var element   = event.target;
  		var item      = (event.item.index - 6);

  		$('.nav-years').find('.nav-link.active').removeClass('active')
  		$('#navy'+item).trigger('click').addClass('active');
  	}

	$(".tabs-year .nav-pills a").click(function() {
	  	var position = $(this).parent().position();
	  	var width = $(this).parent().width();
	  	var height = $(this).parent().height();
	  	var top = $(this).parent().position().top;
	  	var positionTo = (height + top);


	   	$(".tabs-year .slider").css({"left":+ position.left,"width":width, "top":positionTo});
	   	$('.nav-years').find('.nav-link.active').removeClass('active')
	   	$(this).addClass('active');
	});
	var actWidth = $(".tabs-year .nav-pills").find(".active").parent("li").width();
	var actHeight = $(".tabs-year .nav-pills").find(".active").parent("li").height();
	var actTop = $(".tabs-year .nav-pills").find(".active").parent("li").position().top;
	var actPositionTo = (actHeight + actTop);
	var actPosition = $(".tabs-year .nav-pills .active").position();
	$(".tabs-year .slider").css({"left":+ actPosition.left,"width": actWidth, "top":actPositionTo});

</script>

