        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Back to top -->
        <!-- ============================================================== -->
        <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
      </div>
      <!-- ============================================================== -->
      <!-- End Page wrapper  -->
      <!-- footer -->
      <!-- ============================================================== -->
      <?php /*<div class="footer4  b-t">
          <div class="container">
              <div class="row mini-spacer">
                  <div class="col-lg-3 col-md-6 m-b-30">
                      <h5 class="m-b-20">เมนูลัด</h5>
                      <ul class="list-unstyled">
                          <li>
                              <a href="https://reg.cdti.ac.th/registrar/apphome.asp">สมัครเรียน</a>
                          </li>
                          <li>
                              <a href="">ทุนการศึกษา</a>
                          </li>

                      </ul><br>
                      <h5  class="m-b-20">จำนวนผู้เยี่ยมชมเว็บไซต์</h5>

                      <table style="width:100%; color: white; font-size : 14px;">
				<tr>
					<th style=" font-size : 14px;width : 90px">รวมทั้งหมด</th>
					<th  class="center no-wrap-top" width="20px" style=" font-size : 14px;">: </th>
					<th style=" font-size : 14px;"><p style="margin:0px" ><?php echo $all_visit?></p></th>
					
				</tr>
				<tr height="10px"></tr>
				<tr>
					<td  style=" font-size : 14px;">วันนี้</td>
					<td  style=" font-size : 14px;">: </td>
					<td style=" font-size : 14px;"><p style="margin:0px" ><?php echo $today_visit?></p></td>
				</tr>
				
				<tr height="10px"></tr>
				<tr>
					<td  style=" font-size : 14px;">เมื่อวาน</td>
					<td style=" font-size : 14px;">:</td>
					<td style=" font-size : 14px;"><p style="margin:0px" ><?php echo $yesterday_visit?></p></td>
				</tr>
				
				<tr height="10px"></tr>
				<tr>
					<td style=" font-size : 14px;">30 วันก่อน</td>
					<td style=" font-size : 14px;">: </td>
					<td style=" font-size : 14px;"><p style="margin:0px"><?php echo $last_30_days_visit?></p></td>
				</tr>
				</table>

                    
                  </div>
                      
                  <div class="col-lg-3 col-md-6 m-b-30">
                      <h5 class="m-b-20">คณะ / สาขา</h5>

                      <h6 class="m-b-20">ปริญญาตรี</h6>
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?php echo base_url(); ?>คณะบริหารธุรกิจ">คณะบริหารธุรกิจ (บธ.บ.)</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>คณะเทคโนโลยีอุตสาหกรรม">คณะเทคโนโลยีอุตสาหกรรม (ทล.บ.)</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>คณะเทคโนโลยีดิจิทัล">คณะเทคโนโลยีดิจิทัล (วศ.บ.)</a>
                          </li>
                      </ul>
                      <h6 class="m-b-20">ระดับประกาศนียบัตรวิชาชีพ (ปวส.)</h6>
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาอุตสาหกรรม(ปวส.)">ประเภทวิชาอุตสาหกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาบริหารธุรกิจ(ปวส.)">ประเภทวิชาบริหารธุรกิจ</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาคหกรรม(ปวส.)">ประเภทวิชาวิชาคหกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาเทคโนโลยีสารสนเทศและการสื่อสาร(ปวส.)">ประเภทวิชาเทคโนโลยีสารสนเทศและการสื่อสาร</a>
                          </li>
                      </ul>
                      <h6 class="m-b-20">ระดับประกาศนียบัตรวิชาชีพ (ปวช.)</h6>
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาอุตสาหกรรม(ปวช.)">ประเภทวิชาอุตสาหกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาพาณิชยกรรม(ปวช.)">ประเภทวิชาพาณิชยกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาคหกรรม(ปวช.)">ประเภทวิชาคหกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาเกษตรกรรม(ปวช.)">ประเภทวิชาเกษตรกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาเทคโนโลยีสารสนเทศและการสื่อสาร(ปวช.)">ประเภทวิชาเทคโนโลยีสารสนเทศและการสื่อสาร</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ประเภทวิชาอุตสาหกรรมบันเทิงและดนตรี(ปวช.)">ประเภทวิชาอุตสาหกรรมบันเทิงและดนตรี</a>
                          </li>
                      </ul>
                  </div>
                  <div class="col-lg-3 col-md-6 m-b-30">
                      <h5 class="m-b-20">E-service</h5>
                      <ul class="list-unstyled">
                          <li>
                              <a target="_blank" href="//reg.cdti.ac.th">ระบบบริการการศึกษา (REG)</a>
                          </li>
                          <li>
                              <a target="_blank" href="//lib.cdti.ac.th">ห้องสมุด (Library Catalogs - OPAC) </a>
                          </li>
                          <li>
                              <a target="_blank" href="//elibrary.cdti.ac.th/">ห้องสมุดดิจิทัล (E-Library)</a>
                          </li>
                          <!--<li>
                                <a target="_blank" href="<?php echo base_url(); ?>webboard">webboard</a>
                          </li>-->
                          <li>
                              <a target="_blank" href="https://edoc.cdti.ac.th/DocClient/">e-Document</a>
                          </li>
                          <li>
                              <a target="_blank" href="https://hr.cdti.ac.th/misweb/Main/Home.aspx">ระบบแสดงผลออนไลน์ของบุคลากร (HR)</a>
                          </li>
                          <li>
                                <a target="_blank" href="https://my.matterport.com/show/?m=UDtHNR6k1Vq">นิทรรศการออนไลน์ CDTI 2020</a>
                          </li>

                      </ul>
                      <!--<h5 class="m-b-20">ธรรมาภิบาล</h5>
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?php echo base_url(); ?>พรบ">พระราชบัญญัติ/ข้อบังคับ/ระเบียบ/ฯลฯ</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>ข้อมูลสาธารณะ-(OIT)">การเปิดเผยข้อมูลสาธารณะ (OIT)</a>
                          </li>
                          <li>
                              <a target="_blank" href="//docs.google.com/forms/d/e/1FAIpQLSeVO6e89UGxURAVTjai-eY1s3oi0hp4edFPakwgt_r0-9gmgQ/viewform?usp=sf_link">ช่องทางร้องเรียน</a>
                          </li>
                          <li>
                              <a target="_blank" href="//docs.google.com/forms/d/e/1FAIpQLSe1BSyKHQeR4Y250__kNBQmEjHRwVjqABywHK3wIwtXm1YljA/viewform?usp=sf_link">ช่องทางเสนอแนะ</a>
                          </li>
                          <li>
                                <a target="_blank" href="<?php echo base_url(); ?>webboard">Q&A</a>
                          </li>
                      </ul>
                      -->
                      <h5 class="m-b-20">งานพัฒนาคุณภาพการศึกษา</h5>
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?php echo base_url(); ?>report_qa">รายงานผลการประเมินคุณภาพการศึกษา</a>
                          </li>
                      </ul>
                       <h5 class="m-b-20">ติดต่อเรา</h5>
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?php echo base_url(); ?>office/สำนักงานสถาบันฯ/contact">สำนักงานสถาบันฯ</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>general/สำนักวิชาศึกษาทั่วไป/contact">สำนักวิชาศึกษาทั่วไป</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>fac/คณะบริหารธุรกิจ/contact">คณะบริหารธุรกิจ</a>
                          </li>
                          <li>
                                <a href="<?php echo base_url(); ?>fac/คณะเทคโนโลยีอุตสาหกรรม/contact">คณะเทคโนโลยีอุตสาหกรรม</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>school/คณะเทคโนโลยีดิจิทัล/contact">คณะเทคโนโลยีดิจิทัล</a>
                          </li>
                          <li>
                              <a href="<?php echo base_url(); ?>fac/โรงเรียนจิตรลดาวิชาชีพ/contact">โรงเรียนจิตรลดาวิชาชีพ</a>
                          </li>
                      </ul>
                  </div>
                  <div class="col-lg-3 col-md-6">
                     
                      <h6 class="m-b-20">เบอร์ติดต่อ</h6>
                      <p class="m-t-10 m-b-10 font-14">
                          02-280-0551 ( โทรสาร 02-280-0552 )
                      </p>
                      <h6 class="m-b-20">เวลาทำการ</h6>
                      <p class="m-t-10 m-b-10 font-14">
                          ทุกวัน จันทร์-ศุกร์ 8.30 น. - 16.30 น. <br />(ยกเว้น วันหยุดนักขัตฤกษ์)
                      </p>
                      <h6 class="m-b-20">E-mail</h6>
                      <p class="m-t-10 m-b-10 font-14">
                          office@cdti.ac.th
                      </p>
                      <ul class="list-inline">
                          <?php if ($settings->facebook_url): ?>
                            <li>
                                <a href="<?php echo $settings->facebook_url;?>" class="font-20 text-white">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                          <?php endif;?>
                          <?php if ($settings->pinterest_url): ?>
                            <li>
                                <a href="<?php echo $settings->pinterest_url;?>" class="font-20 text-white">
                                    <i class="fab fa-line"></i>
                                </a>
                            </li>
                          <?php endif;?>
                          <?php if ($settings->twitter_url): ?>
                            <li>
                                <a href="<?php echo $settings->twitter_url;?>" class="font-20 text-white">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                          <?php endif;?>
                          <?php if ($settings->google_url): ?>
                            <li>
                                <a href="<?php echo $settings->google_url;?>" class="font-20 text-white">
                                    <i class="fab fa-google"></i>
                                </a>
                            </li>
                          <?php endif;?>
                          <?php if ($settings->instagram_url): ?>
                            <li>
                                <a href="<?php echo $settings->instagram_url;?>" class="font-20 text-white">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                          <?php endif;?>
                          <?php if ($settings->youtube_url): ?>
                            <li>
                                <a href="<?php echo $settings->youtube_url;?>" class="font-20 text-white">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                          <?php endif;?>
                        </ul>
                        <a target="_blank" href="http://omni-recipes.com/">
                            <img style="width: 160px;background-color: #fff;" src="<?php echo base_url(); ?>cdti_assets/images/logo-omni2.png">
                        </a>
                    </div>
              </div>

          </div>

          <div class="f4-bottom-bar">
              <div class="container">
                  <div class="row">
                      <div class="col-3 col-sm-2 col-md-1">
                          <img src="<?php echo get_logo_footer($vsettings); ?>" alt="" class="footer-logo img-responsive d-block mx-auto">
                      </div>
                      <div class="col-9 col-sm-10 col-md-11">

                          <div class="links text-right ml-auto m-t-10 m-b-10 hidden-sm-down visible-md-up">
                              <a href="#" class="p-10 p-l-0">หน้าแรก</a>
                              <a href="#" class="p-10">สมัครเรียน</a>
                              <a href="#" class="p-10">เกี่ยวกับสถาบัน</a>
                              <a href="#" class="p-10">คณะและหลักสูตร</a>
                              <a href="#" class="p-10">ศิษย์เก่า</a>
                              <a href="#" class="p-10">นักศึกษาปัจจุบัน</a>
                              <a href="#" class="p-10">บุคคลทั่วไป</a>
                          </div>
                          <hr class="hidden-sm-down visible-md-up">
                          <p class="text-sm-left text-xs-left text-md-right">
                             <small> <?php echo html_escape($settings->about_footer); ?> </small>
                          </p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      */  ?>
      <!-- ============================================================== -->
      <!-- End footer -->
      <!-- ============================================================== -->

  </div>
  
  <!-- ============================================================== -->
  <!-- End Wrapper -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- All Jquery -->
  <!-- ============================================================== -->
  <!-- Bootstrap popper Core JavaScript -->
  <!--<script src="<?php echo base_url(); ?>cdti_assets/node_modules/popper/dist/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>cdti_assets/node_modules/bootstrap/dist/js/bootstrap.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <!-- This is for the animation -->
  <script src="<?php echo base_url(); ?>cdti_assets/node_modules/aos/dist/aos.js"></script>
  <script src="<?php echo base_url(); ?>cdti_assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.js"></script>
  <!--Jquery touchSwipe -->
  <script src="<?php echo base_url(); ?>cdti_assets/js/jquery.touchSwipe.js"></script>


  <script src="<?php echo base_url(); ?>cdti_assets/plugin/jsCalendar/source/jsCalendar.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/myJsCalendar.js"></script>
  <script src="<?php echo base_url(); ?>cdti_assets/plugin/jsCalendar/source/jsCalendar.lang.template.js"></script>

  <script src="<?php echo base_url(); ?>cdti_assets/node_modules/owl.carousel/dist/owl.carousel.js"></script>
  <script src="<?php echo base_url(); ?>cdti_assets/plugin/slick/slick.min.js"></script>

  <script src="<?php echo base_url(); ?>cdti_assets/js/owl.rows.js"></script>
  <!--Custom JavaScript -->
  <script src="<?php echo base_url(); ?>cdti_assets/js/custom.js"></script>
  <script src="<?php echo base_url(); ?>cdti_assets/node_modules/prism/prism.js"></script>
  <!-- ============================================================== -->
  <!-- This page plugins -->
  <!-- ============================================================== -->
  <script src="<?php echo base_url(); ?>cdti_assets/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.js"></script>

  <script type="text/javascript">
      $('#slider2').bsTouchSlider();
      $(".carousel .carousel-inner").swipe({
          swipeLeft: function(event, direction, distance, duration, fingerCount) {
              this.parent().carousel('next');
          },
          swipeRight: function() {
              this.parent().carousel('prev');
          },
          threshold: 0
      });
  </script>
  
  <script>
       $(document).ready(function(){
          $('#gallery-slide').owlCarousel({
              loop: true
              , margin: 0
              , nav: false
              , dots: true
              , autoplay:true
              , merge:true
              , center: true
              , responsiveClass: true
              , responsive: {
                  0: {
                      items: 1
                      , nav: false
                      , merge:true
                      , center: true
                      , margin: 0
                  }
                  , 768:{
                      items: 1
                  }
                  , 992:{
                      items: 3
                      , center: true
                      , margin: 0
                  }
                  , 1170: {
                      items: 4
                      , center: true
                      , margin: 0
                       // , merge:true
                       // ,
                  }
                  , 1300: {
                      items: 4
                      , center: true
                      , margin: 0
                       // , merge:true
                       // ,
                  }
                  , 1440: {
                    items: 5
                  }
              }
          });
          $('#height-slide').owlCarousel({
              loop:true,
              margin:0,
              nav:true,
              items:1,
          });
          //  $('#partner-slider').owlCarousel({
          //     loop: true
          //     , margin: 0
          //     , nav: true
          //     , dots: true
          //     , autoplay:true
          //     , merge:true
          //     , responsiveClass: true
          //     , rows: true
          //     , rowsCount: 2
          //     , responsive: {
          //         0: {
          //             items: 2
          //             , nav: false
          //             , merge:true
          //             , margin: 10
          //         }
          //         , 1170: {
          //             items: 4
          //             , merge:true

          //             , nav: true
          //             , dots: true
          //         }
          //     }
          // })
      });

  </script>
  <script>
    $('#partner-slider').slick({
        slidesToShow: 4,
        // slidesToScroll: 1,
        arrows:true,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        rows: 1,
    })
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155221467-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-155221467-1');
</script>
<script>
var sessionLastVisit = "<?php echo $this->session->userdata('last_visit') ;?>";
var today = "<?php echo date('Y-m-d') ;?>";
//document.getElementById("visitorToday").innerHTML=now==sessionValue?"true":"false";

if(sessionLastVisit!=today){
    
    //document.getElementById("visitorToday").innerHTML=now==sessionValue?"true":"false";
    //document.getElementById("visitorYesterday").innerHTML=sessionValue;
    $.ajax({
                url: 'visitor/add',
                type: 'GET',
                error: function() {
                    //alert('Something is wrong');
                },
                success: function(data) {
                    //alert(dt.toLocaleDateString());
                    //alert('successxxxxx');
                    /*$visitor = jQuery.parseJSON(data);
                    //alert(data);   
                    //alert($visitor['today']);
                    document.getElementById("visitorToday").innerHTML=$visitor['today'];
                    document.getElementById("visitorYesterday").innerHTML=$visitor['yesterday'];
                    document.getElementById("visitorLastMonth").innerHTML=$visitor['lastMonth'];
                    document.getElementById("visitorTotal").innerHTML=$visitor['total'];
            */
                }
        });   
}
/*
$.ajax({
            url:'//www.cdti.ac.th/visitor/get',
            type: 'GET',
            error: function() {
                //alert('Something is wrong');
            },
            success: function(data) {
                //alert('successxxxxx');
                $visitor = jQuery.parseJSON(data);
                //alert(data);   
                //alert($visitor['today']);
                
                document.getElementById("visitorToday").innerHTML=$visitor['today'];
                document.getElementById("visitorYesterday").innerHTML=$visitor['yesterday'];
                document.getElementById("visitorLastMonth").innerHTML=$visitor['lastMonth'];
                document.getElementById("visitorTotal").innerHTML=$visitor['total'];
            
            }
    });
*/
/*$(document).ready(function(){
});*/
</script>

</script>
</body>

</html>
