<style>
  .no-wrap{
    white-space: nowrap
}

a.dropdown-toggle.dropdown-item{
    margin-left
}
</style>
<div class="header11 ">
    <div class="container">
        <!-- Header 1 code -->
        <nav class="navbar navbar-expand-lg h11-nav">
            <a class="navbar-brand" href="<?php echo lang_base_url() ?>"><img src="<?php echo get_logo($vsettings); ?>" alt="<?php echo $settings->application_name; ?>" /></a>
            <button style="margin-left: auto;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header11" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
            <div class="hidden-md-down visible-lg-up" style="margin-left: auto;">
              <div class="collapse navbar-collapse hover-dropdown flex-column" id="header11" >
                <a href="javascript:void(0);" class="nav-close" data-toggle="collapse" data-target="#header11" aria-controls="navbarTogglerDemo02"><i class="fa fa-times"></i></a>
                  <img src="<?php echo get_logo($vsettings); ?>" alt="<?php echo $settings->application_name; ?>" class="visible-sm-down hidden-md-up img-responsive ">
                  <div class="ml-auto h11-topbar hidden-sm-down visible-md-up">
                      <ul class="list-inline ">
                          
                    <li >  
                    <?php $this->load->view('cdti/layout/navbar_search'); ?>

                      </li>
                      <li> 
                        <table>
                          <tr>
                            <th><a class="nav-call"><img src="<?php echo base_url()?>cdti_assets/images/icon/001-call-answer.png" alt="">  สถาบันเทคโนโลยีจิตรลดา </a></th>
                            <th><a class="nav-call"> 0-2280-0551</a></th>
                          </tr>

                          <tr>
                              <th><a class="nav-call"><img src="<?php echo base_url()?>cdti_assets/images/icon/001-call-answer.png" alt="">   รร.จิตรลดาวิชาชีพ  </a></li>
                              </th>
                              <th><a class="nav-call"> 0-2282-6808 ต่อ 3054, 0-2282-6781</a></li>
                              </th>
                          </tr>
                        </table>
                      </li>
                      <?php if ($settings->facebook_url): ?>
                              <li>
                                <a target="_blank" href="<?php echo $settings->facebook_url;?>">
                                  <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-facebook.png" alt="">
                                </a>
                              </li>
                            <?php endif;?>
                            <?php if ($settings->pinterest_url): ?>
                              <li>
                                  <a target="_blank" href="<?php echo $settings->pinterest_url;?>">
                                    <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-line.png" alt="">
                                  </a>
                              </li>
                            <?php endif;?>
                            <?php if ($settings->twitter_url): ?>
                              <li>
                                  <a target="_blank" href="<?php echo $settings->twitter_url;?>">
                                    <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-twitter.png" alt="">
                                  </a>
                              </li>
                            <?php endif;?>
                            <?php if ($settings->google_url): ?>
                              <li>
                                  <a target="_blank" href="<?php echo $settings->google_url;?>">
                                    <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-twitter.png" alt="">
                                  </a>
                              </li>
                            <?php endif;?>
                            <?php if ($settings->instagram_url): ?>
                              <li>
                                <a target="_blank" href="<?php echo $settings->instagram_url;?>">
                                  <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-intagram.png" alt="">
                                </a>
                              </li>
                            <?php endif;?>
                            <?php if ($settings->youtube_url): ?>
                              <li>
                                  <a target="_blank" href="<?php echo $settings->youtube_url;?>">
                                    <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-youtube1.png" alt="">
                                  </a>
                              </li>
                            <?php endif;?>
                            <li class="b-l-2 pl-2 select-lang">
                                <?php
                                  foreach ($languages as $language):
                                    $lang_url = base_url() . $language->name . "/";
                                    if ($language->id == $this->general_settings->site_lang) {
                                        $lang_url = base_url();
                                    } ?>
                                    <a href="<?php echo $lang_url; ?>" class="d-inlinex p-0 <?php echo ($language->id == 1) ? 'selected' : ''; ?>">
                                    <img src="<?php echo base_url();  ?>cdti_assets/images/<?php echo $language->short_form?>.png" alt="">
                                    </a>
                                <?php endforeach; ?>
                            </li>
                          <li>
                          <?php if(date("Y-m-d H:i:s")  <= $vsettings->regis_date):?>
                            <a class="btn btn-custom-primary btn-register-nav"><?php echo trans('enroll')?></a>
                          <?php endif; ?>
                          </li>
                      </ul>
                  </div>
                  <ul class="navbar-nav ml-auto font-13">
                      <li class="nav-item active"><a class="nav-link no-wrap" href="<?php echo lang_base_url() ?>/English">Home</a></li>
                      <?php if (!empty($this->menu_links)):
                              foreach ($this->menu_links as $item):
                                  if ($item['visibility'] == 1 && $item['lang_id'] == 1 && $item['location'] != "none" && $item['parent_id'] == "0"):
                                    if($item['id'] == 185):
                                        $pages_fac_bachelor= $this->page_model->get_all_pages_fac(true,51);
                                        $pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,52);
                                        $pages_fac_vocational= $this->page_model->get_all_pages_fac(true,53);
                                        // dd($pages_fac);exit;
                                      ?>
                                              <li class="nav-item dropdown">
                                                  <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo html_escape($item['title']) ?> <i class="fa fa-angle-down m-l-5"></i>
                                                  </a>
                                                  <ul class="b-none dropdown-menu animated fadeInUp">
                                                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown">Bachelor's Degree <i class="fa fa-angle-right ml-auto"></i></a>
                                                          <ul class="dropdown-menu b-none menu-right">
                                                              <?php
                                                              foreach ($pages_fac_bachelor as  $value) {
                                                                ?>
                                                                <li><a class="dropdown-item" href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></li>
                                                                <?php
                                                              }
                                                              ?>

                                                          </ul>
                                                      </li>
                                                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="high-vocational" >High Vocational</a>
                                                          
                                                      </li>
                                                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="vocational" >Vocational</a>
                                                         
                                                      </li>
                                                  </ul>
                                              </li>
                                      
                                    <?php else:
                                        $sub_links = helper_get_sub_menu_links($item['id'], $item['type']);
                                        if (!empty($sub_links)):  ?>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <?php echo html_escape($item['title']) ?>
                                                    <i class="fa fa-angle-down m-l-5"></i>
                                                </a>
                                                <ul class="b-none dropdown-menu animated fadeInUp">
                                                    <?php if ($item['type'] == "category"): ?>
                                                        <li class="nav-item" >
                                                            <a class="dropdown-item"  href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($item['slug']) ?>"><?php echo trans("all"); ?></a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <?php foreach ($sub_links as $sub):
                                                    //130
                                                      if($sub['id'] == 106 || $sub['id'] == 116||$sub['id']==139||$sub['id']==156||$sub['id']==161||$sub['id']==167):
                                                        $pages_category= $this->page_model->get_page_category($sub['id'],$this->general_settings->site_lang);
                                                        ?>
                                                        <li class="<?php if($sub['id']==139||$sub['id']==156||$sub['id']==161||$sub['id']==167) echo "nav-item"?> dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown"><?php echo html_escape($sub['title']) ?> <i class="fa fa-angle-right ml-auto"></i></a>
                                                            <ul class="dropdown-menu b-none menu-right">
                                                                <?php
                                                                  foreach ($pages_category as  $value) {
                                                                    if($value->page_type == "link"){
                                                                      $url_link = $value->link;
                                                                    }else{
                                                                      $url_link = base_url().$value->slug;
                                                                    }
                                                                    ?>
                                                                    <li><a class="dropdown-item <?=$value->id?>" href="<?php echo $url_link;?>"><?php //echo $value->page_type;?><?php echo $value->title;?></a></li>
                                                                    <?php
                                                                  }
                                                                ?>
                                                            </ul>
                                                        </li>
                                                        <?php
                                                      else:
                                                      ?>
                                                        <li class="nav-item <?=$sub['id']?>" >
                                                            <a class="dropdown-item"  href="<?php echo $sub['link']; ?>"><?php echo html_escape($sub['title']) ?>
                                                            </a>
                                                        </li>
                                                    <?php
                                                    endif;
                                                  endforeach; ?>
                                                </ul>
                                            </li>
                                        <?php else: ?>
                                            <li class="nav-item" >
                                                <a  class="nav-link"  href="<?php echo $item['link']; ?>">
                                                    <?php echo html_escape($item['title']); ?>
                                                </a>
                                            </li>
                                        <?php endif;
                                      endif;
                                endif;
                            endforeach;
                        endif; ?>
                    <?php /*<li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ธรรมาภิบาล<i class="fa fa-angle-down m-l-5"></i>
                      </a>
                      <ul class="b-none dropdown-menu animated fadeInUp">
                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="<?php echo base_url(); ?>พรบ">พระราชบัญญัติ/ข้อบังคับ/ระเบียบ/ฯลฯ</a>
                      </li>
                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="<?php echo base_url(); ?>ข้อมูลสาธารณะ-(OIT)" >การเปิดเผยข้อมูลสาธารณะ (OIT)</a>
                      </li>
                      </ul>
                    </li>*/?>
                    <!--<li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ติดต่อเรา<i class="fa fa-angle-down m-l-5"></i>
                      </a>
                      <ul class="b-none dropdown-menu animated fadeInUp">
                      
                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="<?php echo base_url(); ?>/contact-us" >ที่อยู่ การติดต่อ</a>
                      </li>
                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="//docs.google.com/forms/d/e/1FAIpQLSeVO6e89UGxURAVTjai-eY1s3oi0hp4edFPakwgt_r0-9gmgQ/viewform?usp=sf_link">ช่องทางร้องเรียน</a>
                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="//docs.google.com/forms/d/e/1FAIpQLSe1BSyKHQeR4Y250__kNBQmEjHRwVjqABywHK3wIwtXm1YljA/viewform?usp=sf_link">ช่องทางเสนอแนะ</a>
                      </li>
                      <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="<?php echo base_url(); ?>webboard" >Q&A</a>
                      </li>
                      </ul>
                    </li>-->
                  </ul>
                  <div class="ml-auto h11-topbar b-d-1  visible-sm-down hidden-md-up">
                      <ul class="list-unstyled nav-social-mobile">
                          
                      <li> 
                        <table>
                          <tr>
                            <th><a class="nav-call"><img src="<?php echo base_url()?>cdti_assets/images/icon/001-call-answer.png" alt="">  สถาบันเทคโนโลยีจิตรลดา </a></th>
                            <th><a class="nav-call"> 0-2280-0551</a></th>
                          </tr>

                          <tr>
                              <th><a class="nav-call"><img src="<?php echo base_url()?>cdti_assets/images/icon/001-call-answer.png" alt="">   รร.จิตรลดาวิชาชีพ  </a></li>
                              </th>
                              <th><a class="nav-call"> 0-2282-6808 ต่อ 3054, 0-2282-6781</a></li>
                              </th>
                          </tr>
                        </table>
                      </li>
                    
                          <li class="nav-social">
                            <ul class="list-inline">

                              <?php if ($settings->facebook_url): ?>
                                <li>
                                  <a href="<?php echo $settings->facebook_url;?>">
                                    <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-facebook.png" alt="">
                                  </a>
                                </li>
                              <?php endif;?>
                              <?php if ($settings->pinterest_url): ?>
                                <li>
                                    <a href="<?php echo $settings->pinterest_url;?>">
                                      <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-line.png" alt="">
                                    </a>
                                </li>
                              <?php endif;?>
                              <?php if ($settings->twitter_url): ?>
                                <li>
                                    <a href="<?php echo $settings->twitter_url;?>">
                                      <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-twitter.png" alt="">
                                    </a>
                                </li>
                              <?php endif;?>
                              <?php if ($settings->google_url): ?>
                                <li>
                                    <a href="<?php echo $settings->google_url;?>">
                                      <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-twitter.png" alt="">
                                    </a>
                                </li>
                              <?php endif;?>
                              <?php if ($settings->instagram_url): ?>
                                <li>
                                  <a href="<?php echo $settings->instagram_url;?>">
                                    <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-intagram.png" alt="">
                                  </a>
                                </li>
                              <?php endif;?>
                              <?php if ($settings->youtube_url): ?>
                                <li>
                                    <a href="<?php echo $settings->youtube_url;?>">
                                      <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-youtube1.png" alt="">
                                    </a>
                                </li>
                              <?php endif;?>

                            </ul>
                          </li>


                          <li class="b-l-1 pl-2 select-lang mb-4">
                              <?php
                                foreach ($languages as $language):
                                  $lang_url = base_url() . $language->name . "/";
                                  if ($language->id == $this->general_settings->site_lang) {
                                      $lang_url = base_url();
                                  } ?>
                                  <a href="<?php echo $lang_url; ?>" class="d-inlinex <?php echo ($language->id == $selected_lang->id) ? 'selected' : ''; ?>">
                                      <img src="<?php echo base_url();  ?>cdti_assets/images/<?php echo $language->short_form?>.png" alt="">
                                  </a>
                              <?php endforeach; ?>
                          </li>
                          <li>
                          <?php if(date("Y-m-d H:i:s")  <= $vsettings->regis_date):?>
                            <a class="btn btn-custom-primary btn-register-nav"><?php echo trans('enroll')?></a>
                          <?php endif; ?>
                          </li>
                      </ul>
                  </div>
              </div>
            </div>
            <div class="visible-md-down hidden-lg-up">
              <div class="collapse navbar-collapse" id="header11"  style="overflow-y: auto;width: 100%;background-color: #fff0;padding:0">
                <a href="javascript:void(0);" class="nav-close" data-toggle="collapse" data-target="#header11" aria-controls="navbarTogglerDemo02" style="
    position: absolute;
    left: 10%;
"><i class="fa fa-times"></i></a>
                  <div class="hover-dropdown flex-column" style="
    width: 80%;
    float: right;
    background-color: #fff;
    padding: 15px;
">
                      <img src="<?php echo get_logo($vsettings); ?>" alt="<?php echo $settings->application_name; ?>" class="img-responsive ">
                      <hr class="img-responsive">
                      <ul class="navbar-nav ml-auto font-13" style="
    overflow-y: hidden;
">

                          <li class="nav-item active"><a class="nav-link" href="<?php echo lang_base_url() ?>">หน้าแรก</a></li>
                          <?php if (!empty($this->menu_links)):
                                  foreach ($this->menu_links as $item):
                                      if ($item['visibility'] == 1 && $item['lang_id'] == 1&& $item['location'] != "none" && $item['parent_id'] == "0"):


                                        if($item['id'] == 185):
                                            $pages_fac_bachelor= $this->page_model->get_all_pages_fac(true,51);
                                            $pages_fac_high_vocational= $this->page_model->get_all_pages_fac(true,52);
                                            $pages_fac_vocational= $this->page_model->get_all_pages_fac(true,53);
                                            // dd($pages_fac);exit;
                                          ?>
                                                  <li class="nav-item dropdown">
                                                      <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo html_escape($item['title']) ?> <i class="fa fa-angle-down m-l-5"></i>
                                                      </a>
                                                      <ul class="b-none dropdown-menu animated fadeInUp">
                                                          <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown">Bachelor's Degree <i class="fa fa-angle-right ml-auto"></i></a>
                                                              <ul class="dropdown-menu b-none menu-right">
                                                                  <?php
                                                                  foreach ($pages_fac_bachelor as  $value) {
                                                                    ?>
                                                                    <li><a class="dropdown-item" href="<?php echo base_url().$value->slug;?>"><?php echo $value->title;?></a></li>
                                                                    <?php
                                                                  }
                                                                  ?>

                                                              </ul>
                                                          </li>
                                                          <li class="submenu"> <a class="dropdown-item" href="high-vocational" data-toggle="dropdown">High Vocational </a>
                                                              
                                                                 
                                                          </li>
                                                          <li class="submenu"> <a class="dropdown-item"  href="vocational" data-toggle="dropdown">Vocational </a>
                                                              
                                                          </li>
                                                      </ul>
                                                  </li>
                                          <?php else:
                                                  $sub_links = helper_get_sub_menu_links($item['id'], $item['type']);
                                                  if (!empty($sub_links)):  ?>
                                                      <li class="nav-item dropdown">
                                                          <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                              <?php echo html_escape($item['title']) ?>
                                                              <i class="fa fa-angle-down m-l-5"></i>
                                                          </a>
                                                          <ul class="b-none dropdown-menu animated fadeInUp">
                                                              <?php if ($item['type'] == "category"): ?>
                                                                  <li class="nav-item" >
                                                                      <a class="dropdown-item"  href="<?php echo lang_base_url(); ?>category/<?php echo html_escape($item['slug']) ?>"><?php echo trans("all"); ?></a>
                                                                  </li>
                                                              <?php endif; ?>
                                                              <?php foreach ($sub_links as $sub):
                                                                if($sub['id'] == 106 || $sub['id'] == 116||$sub['id']==139||$sub['id']==156||$sub['id']==161||$sub['id']==167):
                                                                  $pages_category= $this->page_model->get_page_category($sub['id'],$this->general_settings->site_lang);
                                                                  ?>
                                                                  <li class="<?php  /* if($sub['id']==156||$sub['id']==161||$sub['id']==167) echo "nav-item"*/?>  dropdown-submenu"> <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#" data-toggle="dropdown"><?php echo html_escape($sub['title']) ?> <i class="fa fa-angle-right ml-auto"></i></a>
                                                                      <ul class=" dropdown-menu b-none menu-right">
                                                                          <?php
                                                                            foreach ($pages_category as  $value) {
                                                                              if($value->page_type == "link"){
                                                                                $url_link = $value->link;
                                                                              }else{
                                                                                $url_link = base_url().$value->slug;
                                                                              }
                                                                              ?>
                                                                              <li><a class="dropdown-item <?=$value->id?>" href="<?php echo $url_link;?>"><?php //echo $value->page_type;?><?php echo $value->title;?></a></li>
                                                                              <?php
                                                                            }
                                                                          ?>
                                                                      </ul>
                                                                  </li>
                                                                  <?php
                                                                else:
                                                                ?>
                                                                  <li class="nav-item <?=$sub['id']?>" >
                                                                      <a class="dropdown-item"  href="<?php echo $sub['link']; ?>"><?php echo html_escape($sub['title']) ?>
                                                                      </a>
                                                                  </li>
                                                              <?php
                                                              endif;

                                                            endforeach; ?>
                                                          </ul>
                                                      </li>
                                                  <?php else: ?>
                                                      <li class="nav-item" >
                                                          <a  class="nav-link"  href="<?php echo $item['link']; ?>">
                                                              <?php echo html_escape($item['title']); ?>
                                                          </a>
                                                      </li>
                                                  <?php endif;
                                          endif;
                                      endif;
                                  endforeach;
                              endif; ?>
                              <?php
                              /*
                               <li class="nav-item dropdown">
                                  <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ธรรมาภิบาล<i class="fa fa-angle-down m-l-5"></i>
                                  </a>
                                  <ul class="b-none dropdown-menu animated fadeInUp">
                                      <li class="dropdown-item"> <a class="dropdown-toggle dropdown-item"  href="<?php echo base_url(); ?>พรบ">พระราชบัญญัติ/ข้อบังคับ/ระเบียบ/ฯลฯ</a></li>
                                      <li class="dropdown-item"> <a class="dropdown-toggle dropdown-item"  href="<?php echo base_url()?>ข้อมูลสาธารณะ-(OIT)" >การเปิดเผยข้อมูลสาธารณะ (OIT)</a></li>
                                  </ul>
                              </li>*/
                              ?>
                              <!--<li class="nav-item dropdown">
                                  <a class="nav-link dropdown-toggle" href="#" id="h11-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ติดต่อเรา<i class="fa fa-angle-down m-l-5"></i>
                                  </a>
                                  <ul class="b-none dropdown-menu animated fadeInUp">
                                      <li class="dropdown-item"> <a class="dropdown-toggle dropdown-item"  href="<?php echo base_url()?>contact-us">ที่อยู่ การติดต่อ </a></li>
                                      <li class="dropdown-item"> <a class="dropdown-toggle dropdown-item"  href="//docs.google.com/forms/d/e/1FAIpQLSeVO6e89UGxURAVTjai-eY1s3oi0hp4edFPakwgt_r0-9gmgQ/viewform?usp=sf_link" >ช่องทางร้องเรียน</a></li>
                                      <li class="dropdown-item"> <a class="dropdown-toggle dropdown-item"  href="//docs.google.com/forms/d/e/1FAIpQLSe1BSyKHQeR4Y250__kNBQmEjHRwVjqABywHK3wIwtXm1YljA/viewform?usp=sf_link" >ช่องทางเสนอแนะ</a></li>
                                      <li class="dropdown-item"> <a class="dropdown-toggle dropdown-item"  href="<?php echo base_url()?>webboard" >Q&A</a></li>
                                  </ul>
                              </li>-->
                      </ul>
                      <div class="ml-auto h11-topbar b-d-1">
                          <ul class="list-unstyled nav-social-mobile">
                          <li> 
                            <table>
                              <tr>
                                <th><a class="nav-call"><img src="<?php echo base_url()?>cdti_assets/images/icon/001-call-answer.png" alt="">  สถาบันเทคโนโลยีจิตรลดา </a></th>
                                <th><a class="nav-call"> 0-2280-0551</a></th>
                              </tr>

                              <tr>
                                  <th><a class="nav-call"><img src="<?php echo base_url()?>cdti_assets/images/icon/001-call-answer.png" alt="">   รร.จิตรลดาวิชาชีพ  </a></li>
                                  </th>
                                  <th><a class="nav-call"> 0-2282-6808 ต่อ 3054, 0-2282-6781</a></li>
                                  </th>
                              </tr>
                            </table>
                          </li>
                    
                              <li class="nav-social">
                                <ul class="list-inline">

                                  <?php if ($settings->facebook_url): ?>
                                    <li>
                                      <a href="<?php echo $settings->facebook_url;?>">
                                        <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-facebook.png" alt="">
                                      </a>
                                    </li>
                                  <?php endif;?>
                                  <?php if ($settings->pinterest_url): ?>
                                    <li>
                                        <a href="<?php echo $settings->pinterest_url;?>">
                                          <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-line.png" alt="">
                                        </a>
                                    </li>
                                  <?php endif;?>
                                  <?php if ($settings->twitter_url): ?>
                                    <li>
                                        <a href="<?php echo $settings->twitter_url;?>">
                                          <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-twitter.png" alt="">
                                        </a>
                                    </li>
                                  <?php endif;?>
                                  <?php if ($settings->google_url): ?>
                                    <li>
                                        <a href="<?php echo $settings->google_url;?>">
                                          <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-twitter.png" alt="">
                                        </a>
                                    </li>
                                  <?php endif;?>
                                  <?php if ($settings->instagram_url): ?>
                                    <li>
                                      <a href="<?php echo $settings->instagram_url;?>">
                                        <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-intagram.png" alt="">
                                      </a>
                                    </li>
                                  <?php endif;?>
                                  <?php if ($settings->youtube_url): ?>
                                    <li>
                                        <a href="<?php echo $settings->youtube_url;?>">
                                          <img src="<?php echo base_url()?>cdti_assets/images/icon/socail-youtube1.png" alt="">
                                        </a>
                                    </li>
                                  <?php endif;?>

                                </ul>
                              </li>


                              <li class="b-l-1 pl-2 select-lang mb-4">
                                  <?php
                                    foreach ($languages as $language):
                                      $lang_url = base_url() . $language->name . "/";
                                      if ($language->id == $this->general_settings->site_lang) {
                                          $lang_url = base_url();
                                      } ?>
                                      <a href="<?php echo $lang_url; ?>" class="d-inlinex <?php echo ($language->id == $selected_lang->id) ? 'selected' : ''; ?>">
                                          <img src="<?php echo base_url();  ?>cdti_assets/images/<?php echo $language->short_form?>.png" alt="">
                                      </a>
                                  <?php endforeach; ?>
                              </li>
                              <li>
                              <?php if(date("Y-m-d H:i:s")  <= $vsettings->regis_date):?>
                                <a class="btn btn-custom-primary btn-register-nav"><?php echo trans('enroll')?></a>
                              <?php endif; ?>
                              </li>
                          </ul>
                      </div>
                  </div>       
              </div>     
            </div>
        </nav>

    </div>
</div>
