<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thematic_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['title'] = trans("index");

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function member()
    {

        $data['title'] = 'ผู้ยื่นโครงการ';
        $data['data']  = $this->thematic_model->get_member();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/thematic/member', $data);
        $this->load->view('admin/includes/_footer');
    }
    public function project()
    {

        $data['title'] = 'โครงการ';
        $data['data']  = $this->thematic_model->get_project();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/thematic/project', $data);
        $this->load->view('admin/includes/_footer');
    }

}
