<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        check_permission('settings');
    }


    public function faq_setting()
    {
        $data['title'] = trans("faq_setting");
        $data['categories'] = $this->faq_model->get_all_faqs();
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/faq/faqs', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_faq_post(){
        $this->form_validation->set_rules('name', trans("faq_name"), 'required|xss_clean|max_length[200]');
        // $this->form_validation->set_rules('color', trans("faq_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->faq_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->faq_model->add_faq()) {
                $last_id = $this->db->insert_id();
                $this->faq_model->update_slug($last_id);
                $this->session->set_flashdata('success_form', trans("faq-settings") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->faq_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_faq($id)
    {
        $data['title'] = trans("update_faq");
        //get category
        $data['category'] = $this->faq_model->get_faq($id);

        if (empty($data['category'])) {
            redirect($this->agent->referrer());
        }

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/faq/update_faq', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Category Post
     */
    public function update_faq_post()
    {
        $this->form_validation->set_rules('name', trans("faq_name"), 'required|xss_clean|max_length[200]');
      // $this->form_validation->set_rules('color', trans("faq_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->faq_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //category id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);
            if ($this->faq_model->update_faq($id)) {

                //update slug
                $this->faq_model->update_slug($id);
                $this->session->set_flashdata('success', trans("faq") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'faq-settings');
                }

            } else {
                $this->session->set_flashdata('form_data', $this->faq_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function delete_faq_post()
    {
        $id = $this->input->post('id', true);
        if ($this->faq_model->delete_faq($id)) {
            $this->session->set_flashdata('success', trans("faq") . " " . trans("msg_suc_deleted"));
            reset_cache_data_on_change();
        } else {
            $this->session->set_flashdata('error', trans("msg_error"));
        }
    }
}
