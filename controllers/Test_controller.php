<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test_controller extends Home_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('bcrypt');
        $this->load->helper('directory');
        $this->post_load_more_count = 6;

    }

    //teacher.xml
    public function updateteacher()
    {
        exit;
        $xml = simplexml_load_file("teacher.xml");
        
        $check = [];
        $count = 0;
        $count2 = 0;
        foreach ($xml->Worksheet->Table->Row as $key => $value) {
            // echo $key;
            $count++;
            
            if ($count == 1 or $count == 2) {
                continue;
            }
            $post_data['title'] = @trim($value->Cell[1]->Data);
            $post_data['description'] = @trim($value->Cell[0]->Data);
            
            $post_data['lang_id'] = 2;
            $post_data['category_id'] = 3;
            $post_data['fac_category_id'] = 9;
            $post_data['type'] = 5;
            $post_data['gallery_order'] = @trim($value->Cell[3]->Data);
            $post_data['gallery_level'] = @trim($value->Cell[2]->Data);
            $post_data['path_big'] = "uploads/gallery/image_1920x_avatar.jpg";
            $post_data['path_small'] = "uploads/gallery/image_1920x_avatar.jpg";
            $post_data['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert("gallery", $post_data);
            //$post_id = $this->db->insert_id();
            dd($post_data);
        }
    }

    public function updatenews()
    {
        exit;
        $xml = simplexml_load_file("tb_new_22-01-2020.xml");
        // dd($xml);
        $check = [];
        $count = 0;
        $count2 = 0;
        foreach ($xml->Worksheet->Table->Row as $key => $value) {
            // echo $key;
            $count++;
            
            if ($count == 1) {
                // dd($value);
                continue;
            }


            






            if ((@trim($value->Cell[0]->Data)) ) {
                $content_id = @trim($value->Cell[0]->Data);
                echo $content_id."<br>";

                $rs = $this->db->query("SELECT * FROM `posts` WHERE image_big LIKE '%/{$content_id}/%' AND id <=88 ")->row();
                if($rs){
                    dd($rs);
                    // exit;
                    $count2++;
                    // continue;
                }

                // exit;
                $dir = "./news_old/{$content_id}/";
                // echo $dir;
                // echo @trim($value->Cell[2]->Data);
                // echo "<br>";
                $date_create = date("Y-m-d H:i:s");
                if(@trim($value->Cell[2]->Data)){
                    list($date,$time) = explode('T',$value->Cell[2]->Data);
                    $date_create = $date." ".str_replace(".000","",$time);
                }
                // echo $date_create;
                // exit;
                
                if (is_dir($dir)) {
                    $map = directory_map($dir, false, true);
                    
                    $post_data['title'] = @trim($value->Cell[1]->Data);
                    $post_data['title_slug'] = str_slug(@trim($value->Cell[1]->Data));
                    $post_data['keywords'] = $data->keyword;
                    $post_data['lang_id'] = 2;
                    $post_data['summary'] = @trim($value->Cell[4]->Data);
                    $post_data['content'] = @trim($value->Cell[4]->Data);
                    $post_data['category_id'] = 11;
                    $post_data['subcategory_id'] = 0;
                    $post_data['image_big'] = "news_old/{$content_id}/{$map[0]}";
                    $post_data['image_default'] = "news_old/{$content_id}/{$map[0]}";
                    $post_data['image_small'] = "news_old/{$content_id}/{$map[0]}";
                    $post_data['image_slider'] = "news_old/{$content_id}/{$map[0]}";
                    $post_data['image_mid'] = "news_old/{$content_id}/{$map[0]}";
                    $post_data['visibility'] = 1;
                    $post_data['show_right_column'] = 1;
                    $post_data['post_type'] = 'post';
                    $post_data['user_id'] = 1;
                    $post_data['created_at'] = $date_create;
                    // dd($post_data);
                    //$this->db->insert("posts", $post_data);
                    //$post_id = $this->db->insert_id();

                    if (count($map) > 1) {
                        foreach ($map as $key => $value) {
                            if ($key == 0 or strpos($value, "{$content_id}_") !== false) {
                                continue;
                            }
                            $post_images = [];
                            $post_images['post_id'] = $post_id;
                            $post_images['image_big'] = "news_old/{$content_id}/{$value}";
                            $post_images['image_default'] = "news_old/{$content_id}/{$value}";
                            $post_images['created_at'] = $date_create;
                            //$this->db->insert("post_images", $post_images);
                        }
                    }
                }
                // exit;

            }
        }

    }
    public function index()
    {
        exit;
        $xml = simplexml_load_file("tb_new_22-01-2020.xml");
        // dd($xml);
        $check = [];
        $count = 0;
        $count2 = 0;
        foreach ($xml->Worksheet->Table->Row as $key => $value) {
            // echo $key;
            $count++;
            
            if ($count == 1) {
                // dd($value);
                continue;
            }
            // exit;
            // dd($value);
            if ((@trim($value->Cell[0]->Data))) {
                $content_id = @trim($value->Cell[0]->Data);
                // $rs = $this->db->query("SELECT * FROM `posts` WHERE image_big LIKE '%/{$content_id}/%' ")->row();
                // if($rs){
                //     // dd($rs);
                //     // exit;
                //     $count2++;
                //     continue;
                // }

                // exit;
                $dir = "./photo_gallery/{$content_id}/";
                // echo $dir;
                // echo @trim($value->Cell[2]->Data);
                // echo "<br>";
                $date_create = date("Y-m-d H:i:s");
                if(@trim($value->Cell[2]->Data)){
                    list($date,$time) = explode('T',$value->Cell[2]->Data);
                    $date_create = $date." ".str_replace(".000","",$time);
                }
                // echo $date_create;
                // exit;
                
                if (is_dir($dir)) {
                    $map = directory_map($dir, false, true);
                    
                    $post_data['title'] = @trim($value->Cell[1]->Data);
                    $post_data['title_slug'] = str_slug(@trim($value->Cell[1]->Data));
                    $post_data['keywords'] = $data->keyword;
                    $post_data['lang_id'] = 2;
                    $post_data['summary'] = @trim($value->Cell[4]->Data);
                    $post_data['content'] = @trim($value->Cell[4]->Data);
                    $post_data['category_id'] = 11;
                    $post_data['subcategory_id'] = 0;
                    $post_data['image_big'] = "photo_gallery/{$content_id}/{$map[0]}";
                    $post_data['image_default'] = "photo_gallery/{$content_id}/{$map[0]}";
                    $post_data['image_small'] = "photo_gallery/{$content_id}/{$map[0]}";
                    $post_data['image_slider'] = "photo_gallery/{$content_id}/{$map[0]}";
                    $post_data['image_mid'] = "photo_gallery/{$content_id}/{$map[0]}";
                    $post_data['visibility'] = 1;
                    $post_data['show_right_column'] = 1;
                    $post_data['post_type'] = 'post';
                    $post_data['user_id'] = 1;
                    $post_data['created_at'] = $date_create;
                    // dd($post_data);
                    $this->db->insert("posts_tmp", $post_data);
                    $post_id = $this->db->insert_id();

                    if (count($map) > 1) {
                        foreach ($map as $key => $value) {
                            if ($key == 0 or strpos($value, "{$content_id}_") !== false) {
                                continue;
                            }
                            $post_images = [];
                            $post_images['post_id'] = $post_id;
                            $post_images['image_big'] = "photo_gallery/{$content_id}/{$value}";
                            $post_images['image_default'] = "photo_gallery/{$content_id}/{$value}";
                            $post_images['created_at'] = $date_create;
                            $this->db->insert("post_images_tmp", $post_images);
                        }
                    }
                }
                // exit;

            }
            // dd($value);
        }
        echo $count2;
    }
    public function index_bk()
    {
        echo 22;exit;
        // $rs = $this->db->query("SELECT * FROM `posts` WHERE CHAR_LENGTH(title_slug) > 90")->result();
        // foreach ($rs as $key => $value) {
        //       dd($value);
        // }
        // exit;
        /////////////////////////
        // $query = $this->db->get('import_table');
        // $rs    = $query->result();
        // foreach ($rs as $key => $value) {
        //       if($value->show_management == TRUE){
        //           $insert = [];
        //           $insert['post_id']    = $value->post_id;
        //           $insert['tag']        = 'คณะบริหารธุรกิจ';
        //           $insert['tag_slug']   = 'คณะบริหารธุรกิจ';
        //           $insert['created_at'] = date("Y-m-d H:i:s");
        //           $this->db->insert('tags',$insert);
        //       }

        //       if($value->show_techno == TRUE){
        //           $insert = [];
        //           $insert['post_id']    = $value->post_id;
        //           $insert['tag']        = 'คณะเทคโนโลยีอุตสาหกรรม';
        //           $insert['tag_slug']   = 'คณะเทคโนโลยีอุตสาหกรรม';
        //           $insert['created_at'] = date("Y-m-d H:i:s");
        //           $this->db->insert('tags',$insert);
        //       }

        // }

        /////////////////////////
        // exit;
        $dir = './news_old';
        $map = directory_map($dir, false, true);
        //
        foreach ($map as $key => $value) {
            if (count($value)) {
                $content_id = str_replace("/", "", $key);
                $data = $this->get($content_id);
                // print_r($data);exit;

                $post_data = [];
                if ($data) {
                    $post_data['title'] = $data->subject;
                    $post_data['title_slug'] = str_slug(trim($data->subject));
                    $post_data['keywords'] = $data->keyword;
                    $post_data['lang_id'] = 2;
                    $post_data['summary'] = $data->short_detail;
                    $post_data['content'] = $data->short_detail;
                    $post_data['category_id'] = 11;
                    $post_data['subcategory_id'] = 0;
                    $post_data['image_big'] = "news_old/{$content_id}/{$value[0]}";
                    $post_data['image_default'] = "news_old/{$content_id}/{$value[0]}";
                    $post_data['image_small'] = "news_old/{$content_id}/{$value[0]}";
                    $post_data['image_slider'] = "news_old/{$content_id}/{$value[0]}";
                    $post_data['image_mid'] = "news_old/{$content_id}/{$value[0]}";
                    $post_data['visibility'] = 1;
                    $post_data['show_right_column'] = 1;
                    $post_data['post_type'] = 'post';
                    $post_data['user_id'] = 1;
                    $post_data['created_at'] = date("Y-m-d H:i:s");
                    $this->db->insert("posts", $post_data);
                    $post_id = $this->db->insert_id();

                    if (count($value) > 1) {
                        foreach ($value as $key => $value) {
                            if ($key == 0) {
                                continue;
                            }

                            $post_images = [];
                            $post_images['post_id'] = $post_id;
                            $post_images['image_big'] = "news_old/{$content_id}/{$value}";
                            $post_images['image_default'] = "news_old/{$content_id}/{$value}";
                            $post_images['created_at'] = date("Y-m-d H:i:s");
                            $this->db->insert("post_images", $post_images);
                        }
                    }
                } else {
                    echo $content_id . '    xxxxxxxxxxxx';
                }
            }
        }
    }

    public function get($id)
    {
        $this->db->where('content_id', $id);
        $query = $this->db->get('import_table');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function any($slug)
    {
        $slug = $this->security->xss_clean($slug);

        if (empty($slug)) {
            redirect(lang_base_url());
        }
        get_posts_latest();
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['page'] = $this->page_model->get_page_by_lang($slug, $this->selected_lang->id);

        if (isset($data['page']->id) and $data['page']->id == 32) {
            $this->scholar();
        } else {
            if (!empty($data['page'])) {
                if ($data['page']->visibility == 0) {
                    $this->error_404();
                } else {
                    //check page auth
                    $this->checkPageAuth($data['page']);
                    $data['title'] = $data['page']->title;
                    $data['description'] = $data['page']->description;
                    $data['keywords'] = $data['page']->keywords;

                    $this->load->view('frontend/template/header', $data);
                    $this->load->view('frontend/page', $data);
                    $this->load->view('frontend/template/footer', $data);
                }

            } else {
                $this->post($slug);
            }
        }

    }

    public function search()
    {
        $q = trim($this->input->get('q', true));

        $data['q'] = $q;
        $data['title'] = trans("search") . ': ' . $q;
        $data['description'] = trans("search") . ': ' . $q;
        $data['keywords'] = trans("search") . ', ' . $q;
        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'search', $this->post_model->get_search_post_count($q));
        $data['posts'] = $this->post_model->get_paginated_search_posts($q, $pagination['per_page'], $pagination['offset']);

        $this->load->view('frontend/template/header', $data);
        $this->load->view('frontend/search', $data);
        $this->load->view('frontend/template/footer', $data);
    }

    public function checkPageAuth($page)
    {
        if (!auth_check() && $page->need_auth == 1) {
            $this->session->set_flashdata('error', trans("message_page_auth"));
            redirect(lang_base_url());
        }
    }

    public function post($slug)
    {
        $data['post'] = $this->post_model->get_post($slug);

        //check if post exists
        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data["category"] = $this->category_model->get_category($data['post']->category_id);
            $data["subcategory"] = $this->category_model->get_category($data['post']->subcategory_id);
            $data['post_tags'] = $this->tag_model->get_post_tags($id);
            $data['post_image_count'] = $this->post_file_model->get_post_additional_image_count($id);
            $data['post_file_count'] = $this->post_file_model->get_post_files_count($id);
            $data['post_files'] = $this->post_file_model->get_post_files($id);
            $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
            $data['post_user'] = $this->auth_model->get_user($data['post']->user_id);
            $data['comments'] = $this->comment_model->get_comments($id, 5);
            $data['vr_comment_limit'] = 5;

            $data['related_posts'] = $this->post_model->get_related_posts($data['post']->category_id, $id);
            $data['previous_post'] = $this->post_model->get_previous_post($id);
            $data['next_post'] = $this->post_model->get_next_post($id);

            $data['is_reading_list'] = $this->reading_list_model->is_post_in_reading_list($id);

            $data['post_type'] = $data['post']->post_type;

            if (!empty($data['post']->feed_id)) {
                $data['feed'] = $this->rss_model->get_feed($data['post']->feed_id);
            }

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $data['og_title'] = $data['post']->title;
            $data['og_description'] = $data['post']->summary;
            $data['og_type'] = "article";
            $data['og_url'] = post_url($data['post']);

            if (!empty($data['post']->image_url)) {
                $data['og_image'] = $data['post']->image_url;
            } else {
                $data['og_image'] = base_url() . $data['post']->image_default;
            }
            $data['og_width'] = "750";
            $data['og_height'] = "500";
            $data['og_creator'] = $data['post_user']->username;
            $data['og_author'] = $data['post_user']->username;
            $data['og_published_time'] = $data['post']->created_at;
            $data['og_modified_time'] = $data['post']->created_at;
            $data['og_tags'] = $data['post_tags'];

            $this->reaction_model->set_voted_reactions_session($id);
            $data["reactions"] = $this->reaction_model->get_reaction($id);
            $data["emoji_lang"] = $this->selected_lang->folder_name;

            // $this->load->view('partials/_header', $data);
            // $this->load->view('post/post', $data);
            // $this->load->view('partials/_footer', $data);

            $this->load->view('frontend/template/header', $data);
            $this->load->view('frontend/post/post', $data);
            $this->load->view('frontend/template/footer', $data);

            //increase post hit
            $this->post_model->increase_post_hit($data['post']);

        }
    }

    public function category($slug)
    {
        $slug = $this->security->xss_clean($slug);

        $data['category'] = $this->category_model->get_category_by_slug($slug);

        //check category exists
        if (empty($data['category'])) {
            redirect(lang_base_url());
        }

        $category_id = $data['category']->id;
        $data['title'] = $data['category']->name;
        $data['description'] = $data['category']->description;
        $data['keywords'] = $data['category']->keywords;

        //category type
        $data['category_type'] = "";
        if ($data['category']->parent_id == 0) {
            $data['category_type'] = "parent";
        } else {
            $data['category_type'] = "sub";
        }

        $count_key = 'posts_count_category' . $data['category']->id;
        $posts_key = 'posts_category' . $data['category']->id;

        //category posts count
        $total_rows = get_cached_data($count_key);
        if (empty($total_rows)) {
            $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id);
            set_cache_data($count_key, $total_rows);
        }
        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'category/' . $slug, $total_rows);
        $data['posts'] = get_cached_data($posts_key . '_page' . $pagination['current_page']);
        if (empty($data['posts'])) {
            $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset']);
            set_cache_data($posts_key . '_page' . $pagination['current_page'], $data['posts']);
        }
        // $this->load->view('partials/_header', $data);
        // $this->load->view('category', $data);
        // $this->load->view('partials/_footer');

        $this->load->view('frontend/template/header', $data);
        if ($category_id == 9) {
            $this->load->view('frontend/category_innovation', $data);
        } else {
            $this->load->view('frontend/category', $data);
        }

        $this->load->view('frontend/template/footer', $data);
    }

    public function events()
    {

        $page = $this->page_model->get_page("post_events");

        $data['title'] = get_page_title($page);
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        $data['post_events'] = $this->postevents_admin_model->get_events(10);

        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $this->checkPageAuth($data['page']);
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            //set paginated
            $pagination = $this->paginate(lang_base_url() . "events", $this->total_post_events_count);
            $data['posts'] = $this->postevents_model->get_paginated_post_events($pagination['per_page'], $pagination['offset']);

            // dd($this->total_post_events_count);

            $this->load->view('frontend/template/header', $data);
            $this->load->view('frontend/event', $data);
            $this->load->view('frontend/template/footer', $data);
        }
    }

    public function contactus()
    {

        $page = $this->page_model->get_page("contactus");

        $data['title'] = get_page_title($page);
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $this->checkPageAuth($data['page']);
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $this->load->view('frontend/template/header', $data);
            $this->load->view('frontend/contactus', $data);
            $this->load->view('frontend/template/footer', $data);
        }
    }

    public function event($slug)
    {
        $slug = $this->security->xss_clean($slug);
        if (empty($slug)) {
            redirect(lang_base_url());
        }
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['post'] = $this->postevents_model->get_post($slug);

        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data['post_events'] = $this->postevents_admin_model->get_events(10);

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $this->load->view('frontend/template/header', $data);
            $this->load->view('frontend/event_detail', $data);
            $this->load->view('frontend/template/footer', $data);
        }
    }

    public function error_404()
    {
        $data['title'] = "Error 404";
        $data['description'] = "Error 404";
        $data['keywords'] = "error,404";

        $this->load->view('frontend/template/header', $data);
        $this->load->view('frontend/template/error_404');
        $this->load->view('frontend/template/footer', $data);
    }

    public function custom()
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";

        $this->load->view('frontend/template/header', $data);
        $this->load->view('econ/custom_page', $data);
        $this->load->view('frontend/template/footer', $data);
    }

    public function teams()
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['teams'] = $this->gallery_model->get_images_by_category(3);

        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";

        $this->load->view('frontend/template/header', $data);
        $this->load->view('frontend/teams', $data);
        $this->load->view('frontend/template/footer', $data);
    }

    public function isp($page = 1)
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['isp'] = $this->reg_model->get_isp($page);

        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";
        $data['segment'] = $this->global_data_segment;
        if (!isset($data['segment'][2])) {
            $data['segment'][2] = 1;
        }

        $this->load->view('frontend/template/header', $data);
        $this->load->view('frontend/isp', $data);
        $this->load->view('frontend/template/footer', $data);
    }

    public function isp_detail($id)
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['data'] = $this->reg_model->get_isp_by_id($id);
        if (empty($data['data'])) {
            $this->error_404();
        } else {

            $data['title'] = "ISP : " . $data['data']->OldRegisterID;
            $data['description'] = "MIND CREDIT";
            $data['keywords'] = "MIND CREDIT";
            $data['segment'] = $this->global_data_segment;
            if (!isset($data['segment'][2])) {
                $data['segment'][2] = 1;
            }

            $this->load->view('frontend/template/header', $data);
            $this->load->view('frontend/isp_detail', $data);
            $this->load->view('frontend/template/footer', $data);
        }

    }

    public function scholar()
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        // $data['page'] = $data;

        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";

        $data['page_2'] = $this->page_model->get_page_by_slug('คุณสมบัติผู้เข้าร่วมโครงการ');
        $data['page_3'] = $this->page_model->get_page_by_slug('รูปแบบเงินสนับสนุนโครงการ');
        $data['page_4'] = $this->page_model->get_page_by_slug('เงื่อนไขการเบิกจ่าย');

        $this->load->view('frontend/template/header', $data);
        $this->load->view('frontend/scholar', $data);
        $this->load->view('frontend/template/footer', $data);
    }
}
