<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomPage_controller extends Home_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function faqs()
    {

        $view_data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $view_data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        $data['post'] = $this->page_model->get_page_by_link('faqs');
        $data['faq_sme'] = $this->faq_model->get_top_faqs_by_lang($this->selected_lang->id,1);
        $data['faq_isp'] = $this->faq_model->get_top_faqs_by_lang($this->selected_lang->id,0);
   
        if (empty($data['post'])) {
            $this->error_404();
        } else {
            if ($data['post']->visibility == 0) {
              $this->error_404();
            } else {
              $data['title'] = $data['post']->title;
              $data['description'] = $data['post']->description;
              $data['keywords'] = $data['post']->keywords;

              $this->load->view('frontend/template/header', $data);
              $this->load->view('frontend/faqs',$data);
              $this->load->view('frontend/template/footer', $data);
           }
        }
    }
}
