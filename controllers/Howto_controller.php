<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Howto_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        check_permission('settings');
    }


    public function howto_setting()
    {
        $data['title'] = trans("howto_setting");
        $data['categories'] = $this->howto_model->get_all_howto();
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/howto/howtos', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_howto_post(){
        $this->form_validation->set_rules('name', trans("howto_name"), 'required|xss_clean|max_length[200]');
        // $this->form_validation->set_rules('color', trans("howto_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->howto_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->howto_model->add_howto()) {
                $last_id = $this->db->insert_id();
                $this->howto_model->update_slug($last_id);
                $this->session->set_flashdata('success_form', trans("howto-settings") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->howto_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_howto($id)
    {
        $data['title'] = trans("update_howto");
        //get category
        $data['category'] = $this->howto_model->get_howto($id);

        if (empty($data['category'])) {
            redirect($this->agent->referrer());
        }

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/howto/update_howto', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Category Post
     */
    public function update_howto_post()
    {
        $this->form_validation->set_rules('name', trans("howto_name"), 'required|xss_clean|max_length[200]');
      // $this->form_validation->set_rules('color', trans("howto_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->howto_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //category id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);
            if ($this->howto_model->update_howto($id)) {

                //update slug
                $this->howto_model->update_slug($id);
                $this->session->set_flashdata('success', trans("howto") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'howto-settings');
                }

            } else {
                $this->session->set_flashdata('form_data', $this->howto_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function delete_howto_post()
    {
        $id = $this->input->post('id', true);
        if ($this->howto_model->delete_howto($id)) {
            $this->session->set_flashdata('success', trans("howto") . " " . trans("msg_suc_deleted"));
            reset_cache_data_on_change();
        } else {
            $this->session->set_flashdata('error', trans("msg_error"));
        }
    }
}
