<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petition_form_controller extends Pdf_Core_Controller
{
    public function __construct()
    {
        parent::__construct('petition_form','petition_form_controller','add_post');
    }
}