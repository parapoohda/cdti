<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        check_permission('gallery');
    }

    /**
     * Gallery
     */
    public function images()
    {
        $data['title'] = trans("gallery");
        $data['images'] = $this->gallery_model->get_all_images();
        $data['categories'] = $this->gallery_category_model->get_all_categories();
        $data['lang_search_column'] = 3;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/gallery', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function links()
    {
        $data['title'] = trans("links");
        $data['images'] = $this->gallery_model->get_images_by_category(1);
        $data['categories'] = $this->gallery_category_model->get_categories();
        $data['lang_search_column'] = 3;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/links', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function banners()
    {
        $data['title'] = trans("banners");
        $data['images'] = $this->gallery_model->get_all_images_by_category(2);
        $data['categories'] = $this->gallery_category_model->get_categories();
        $data['lang_search_column'] = 4;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/banners', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function landingpages()
    {
        $data['title'] = trans("landingpages");
        $data['images'] = $this->gallery_model->get_images_by_category(6);
        $data['categories'] = $this->gallery_category_model->get_categories();
        $data['lang_search_column'] = 3;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/landingpages', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function teams()
    {
        $data['title'] = trans("teams");
        $data['images'] = $this->gallery_model->get_images_by_category(3);
        // dd($data['images']);
        $data['categories'] = $this->gallery_category_model->get_categories();
        $data['lang_search_column'] = 3;
   
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/teams', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function howtos()
    {
        $data['title'] = trans("howto");
        $data['images'] = $this->gallery_model->get_images_by_category(4);
        $data['categories'] = $this->gallery_category_model->get_categories();
        $data['lang_search_column'] = 3;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/howtos', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function update_links_settings($id)
    {
        $data['title'] = trans("update_links");
        //get post
        $data['image'] = $this->gallery_model->get_image($id);

        if (empty($data['image'])) {
            redirect($this->agent->referrer());
        }

        $data['categories'] = $this->gallery_category_model->get_categories_by_lang($data['image']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/update_links', $data);
        $this->load->view('admin/includes/_footer');
    }


    public function update_teams_settings($id)
    {
        $data['title'] = trans("update_teams");
        //get post
        $data['image'] = $this->gallery_model->get_image($id);

        if (empty($data['image'])) {
            redirect($this->agent->referrer());
        }

        $data['categories'] = $this->gallery_category_model->get_categories_by_lang($data['image']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/update_teams', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function update_howtos_settings($id)
    {
        $data['title'] = trans("update_howto");
        //get post
        $data['image'] = $this->gallery_model->get_image($id);

        if (empty($data['image'])) {
            redirect($this->agent->referrer());
        }

        $data['categories'] = $this->gallery_category_model->get_categories_by_lang($data['image']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/update_howtos', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function update_banners_settings($id)
    {
        $data['title'] = trans("update_banners");
        //get post
        $data['image'] = $this->gallery_model->get_image($id);

        if (empty($data['image'])) {
            redirect($this->agent->referrer());
        }

        $data['categories'] = $this->gallery_category_model->get_categories_by_lang($data['image']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/update_banners', $data);
        $this->load->view('admin/includes/_footer');
    }


    public function update_landingpages_settings($id)
    {
        $data['title'] = trans("update_landingpages");
        //get post
        $data['image'] = $this->gallery_model->get_image($id);

        if (empty($data['image'])) {
            redirect($this->agent->referrer());
        }

        $data['categories'] = $this->gallery_category_model->get_categories_by_lang($data['image']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/update_landingpages', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Add Image Post
     */
    public function add_gallery_image_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->gallery_model->add()) {
                $this->session->set_flashdata('success_form', trans("image") . " " . trans("msg_suc_added"));
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }


    /**
     * Update Image
     */
    public function update_gallery_image($id)
    {
        
        $data['title'] = trans("update_image");

        //get post
        $data['image'] = $this->gallery_model->get_image($id);

        if (empty($data['image'])) {
            redirect($this->agent->referrer());
        }

        $data['categories'] = $this->gallery_category_model->get_categories_by_lang($data['image']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/gallery/update', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Image Post
     */
    public function update_gallery_image_post()
    {
        
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);

            if ($this->gallery_model->update($id)) {
                $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_updated"));
                redirect(admin_url() . 'gallery-images');
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }
    public function update_links_settings_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);

            if ($this->gallery_model->update($id)) {
                $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_updated"));
                redirect(admin_url() . 'links-settings');
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_banners_settings_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);

            if ($this->gallery_model->update($id)) {
                $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_updated"));
                redirect(admin_url() . 'banners-settings');
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }


    public function update_landingpages_settings_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);

            if ($this->gallery_model->update($id)) {
                $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_updated"));
                redirect(admin_url() . 'landingpages-settings');
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_teams_settings_post()
    {
        // $redirect_uri = admin_url()."teams-settings?type=".$this->input->post('type', true);
        // echo $redirect_uri;exit;
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            //teams-settings?type=2
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);

            if ($this->gallery_model->update($id)) {
                $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_updated"));
                redirect(admin_url() . 'teams-settings?type='.$this->input->post('type', true));
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_howtos_settings_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'xss_clean|max_length[500]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);

            if ($this->gallery_model->update($id)) {
                $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_updated"));
                redirect(admin_url() . 'howto-settings');
            } else {
                $this->session->set_flashdata('form_data', $this->gallery_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }



    /**
     * Delete Image Post
     */
    public function delete_gallery_image_post()
    {
        $id = $this->input->post('id', true);

        if ($this->gallery_model->delete($id)) {
            $this->session->set_flashdata('success', trans("image") . " " . trans("msg_suc_deleted"));
        } else {
            $this->session->set_flashdata('error', trans("msg_error"));
        }
    }


}
