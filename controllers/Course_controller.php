<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_controller extends Home_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check_permission('pages');
    }

    public function faculty($slug,$faculty,$tabmenu)
    {

        $slug = $this->security->xss_clean($slug);
        if (empty($slug)) {
            redirect(lang_base_url());
        }
        // $view_data['load_css']  = array(
        //     'forms/dropify/dropify.css',
        //     'forms/toggle/switchery.min.css',
        //     'extensions/sweetalert.css',
        //   );
        // $view_data['load_js']  = array(
        //             'forms/dropify/dropify.min.js',
        //             'forms/toggle/bootstrap-checkbox.min.js',
        //             'forms/toggle/switchery.min.js',
        //             'extensions/sweetalert.min.js',
        //             // 'forms/validation/jqBootstrapValidation.js',
        //         );

        $data['page'] = $this->page_model->get_default_page($slug,$this->selected_lang->id);
        
        
        $data['page_main']   = $this->page_model->get_main_page($data['page']->subcategory_id,$this->selected_lang->id);
        $data['fac_posts'] = $this->post_model->get_rss_posts_fac_category_limit($data['page']->main_category_id,5);
        
        // dd($data['page_branch']);exit;

        // dd($data['page']);  exit;

        if ($data['page']->visibility == 0) {
            $this->error_404();
            } else {
            $data['title']       = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords']    = $data['page']->keywords;

            $css = explode('/',$data['page']->link);
            
            // dd($css[3]);
            $data['css_theme']   = base_url().'/cdti_assets/css/campus/'.$css[3].'.css';

            $this->load->view('cdti/layout/header', $data);
            //dd($data);exit;
            $data['header']  = true;
            if($data['page']->subcategory_id){
                $data['page'] = $data['page_main'];
            }else{
                if($tabmenu == 'about'){
                    $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id,$this->selected_lang->id);
                    // dd($data['page_branch']);exit;
                    $pageContent = $data['page']->link;
                }else if($tabmenu == 'branch'){
                    $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id,$this->selected_lang->id);
                    // dd($data['page_branch']);exit;
                    $pageContent = 'cdti/default_page/faculty/branch';
                } 
                
            } 
            //$this->load->view($data['page']->link,$data);    

            $this->load->view('cdti/layout/fac_topbar',['page' => $data['page'] , 'route' => '' ]);
            $this->load->view($pageContent,$data);
            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function branch($slug){
        $slug = $this->security->xss_clean($slug);
        if (empty($slug)) {
            redirect(lang_base_url());
        }

        $data['page'] = $this->page_model->get_default_page($slug,$this->selected_lang->id);
        $data['page_main']   = $this->page_model->get_main_page($data['page']->subcategory_id,$this->selected_lang->id);
        // dd($data['page_main']);
        $data['page_img_all']   = $this->page_file_model->get_page_additional_images($data['page']->id);
        
        $data['page_img'] = null;
        
        if($data['page_img_all']){
            foreach($data['page_img_all'] as $value){
                $data['page_img'][$value->flag][] = $value;
            }
        }
        //  dd($data['page_main']); exit;
        
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title']       = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords']    = $data['page']->keywords;
            $css = explode('/',$data['page']->link);
            
            $data['css_theme']   = base_url().'/cdti_assets/css/campus/'.$data['page_main']->link;
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/fac_sub_topbar',['branch' => $data['page'] , 'route' => 'branch' ]);
            $this->load->view('cdti/default_page/faculty/branch_detail',$data);
            $this->load->view('cdti/layout/footer', $data);
        }
    }
}