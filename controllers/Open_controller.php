﻿<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);

class Open_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['title'] = trans("index");

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function isp()
    {

        $data['title'] = "สมาชิก ISP";
        $data['data']  = $this->reg_model->get_isps();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/open/open_isp', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function sme()
    {

        $data['title'] = "สมาชิก SME/START UP";
        $data['data']  = $this->reg_model->get_smes();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/open/open_sme', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function project()
    {

        $data['title'] = "โครงการ";
        $data['data']  = $this->reg_model->get_project();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/open/open_project', $data);
        $this->load->view('admin/includes/_footer');
    }

}
