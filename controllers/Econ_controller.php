<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Econ_controller extends Home_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('bcrypt');
        $this->post_load_more_count = 6;
    }

    public function index()
    {
        $data['title'] = $this->settings->home_title;
        $data['description'] = $this->settings->site_description;
        $data['keywords'] = $this->settings->keywords;
        $data['home_title'] = $this->settings->home_title;
        $data['visible_posts_count'] = $this->post_load_more_count;
        $data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );
       //event posts
        // $data['post_events'] = get_cached_data('post_events');
        // if (empty($data['post_events'])) {
        //     $data['post_events'] = $this->postevents_admin_model->get_events(10);
        //     set_cache_data('post_events', $data['post_events']);
        // }
        $data['post_events'] = $this->postevents_admin_model->get_events(10);

        $data['featured_posts'] = get_cached_data('featured_posts');
        if (empty($data['featured_posts'])) {
            $data['featured_posts'] = $this->post_model->get_featured_posts();
            set_cache_data('featured_posts', $data['featured_posts']);
        }
        //slider posts
        $data['slider_posts'] = get_cached_data('slider_posts');
        if (empty($data['slider_posts'])) {
            $data['slider_posts'] = $this->post_model->get_slider_posts();
            set_cache_data('slider_posts', $data['slider_posts']);
        }
        //latest posts
        $data['latest_posts'] = get_cached_data('latest_posts');
        if (empty($data['latest_posts'])) {
            $data['latest_posts'] = $this->post_model->get_last_posts($this->selected_lang->id, $this->post_load_more_count, 0);
            set_cache_data('latest_posts', $data['latest_posts']);
        }
        //breaking news
        $data['breaking_news'] = get_cached_data('breaking_news');
        if (empty($data['breaking_news'])) {
            $data['breaking_news'] = $this->post_model->get_breaking_news();
            set_cache_data('breaking_news', $data['breaking_news']);
        }

        $this->load->view('econ/layouts/headers', $data);
        $this->load->view('econ/index',$data);
        $this->load->view('econ/layouts/footers', $data);
    }

    public function category($slug)
    {
        $slug = $this->security->xss_clean($slug);

        $data['category'] = $this->category_model->get_category_by_slug($slug);

        //check category exists
        if (empty($data['category'])) {
            redirect(lang_base_url());
        }

        $category_id = $data['category']->id;
        $data['title'] = $data['category']->name;
        $data['description'] = $data['category']->description;
        $data['keywords'] = $data['category']->keywords;

        //category type
        $data['category_type'] = "";
        if ($data['category']->parent_id == 0) {
            $data['category_type'] = "parent";
        } else {
            $data['category_type'] = "sub";
        }

        $count_key = 'posts_count_category' . $data['category']->id;
        $posts_key = 'posts_category' . $data['category']->id;

        //category posts count
        $total_rows = get_cached_data($count_key);
        if (empty($total_rows)) {
            $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id);
            set_cache_data($count_key, $total_rows);
        }
        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'category/' . $slug, $total_rows);
        $data['posts'] = get_cached_data($posts_key . '_page' . $pagination['current_page']);
        if (empty($data['posts'])) {
            $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset']);
            set_cache_data($posts_key . '_page' . $pagination['current_page'], $data['posts']);
        }
        // $this->load->view('partials/_header', $data);
        // $this->load->view('category', $data);
        // $this->load->view('partials/_footer');
        $this->load->view('econ/layouts/headers', $data);
        $this->load->view('econ/category',$data);
        $this->load->view('econ/layouts/footers', $data);
    }

    public function any($slug)
    {
        $slug = $this->security->xss_clean($slug);
        if (empty($slug)) {
            redirect(lang_base_url());
        }
        get_posts_latest();
        $view_data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $view_data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );
        $data['page'] = $this->page_model->get_page_by_lang($slug, $this->selected_lang->id);
        if (!empty($data['page'])) {
            if ($data['page']->visibility == 0) {
                $this->error_404();
            } else {
                //check page auth
                $this->checkPageAuth($data['page']);
                $data['title'] = $data['page']->title;
                $data['description'] = $data['page']->description;
                $data['keywords'] = $data['page']->keywords;

                $this->load->view('frontend/template/header', $data);
                $this->load->view('frontend/page',$data);
                $this->load->view('frontend/template/footer', $data);
            }

        } else {
            $this->post($slug);
        }
    }

    public function checkPageAuth($page)
    {
        if (!auth_check() && $page->need_auth == 1) {
            $this->session->set_flashdata('error', trans("message_page_auth"));
            redirect(lang_base_url());
        }
    }

    public function post($slug)
    {
        $data['post'] = $this->post_model->get_post($slug);

        //check if post exists
        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data["category"] = $this->category_model->get_category($data['post']->category_id);
            $data["subcategory"] = $this->category_model->get_category($data['post']->subcategory_id);
            $data['post_tags'] = $this->tag_model->get_post_tags($id);
            $data['post_image_count'] = $this->post_file_model->get_post_additional_image_count($id);
            $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
            $data['post_user'] = $this->auth_model->get_user($data['post']->user_id);
            $data['comments'] = $this->comment_model->get_comments($id, 5);
            $data['vr_comment_limit'] = 5;

            $data['related_posts'] = $this->post_model->get_related_posts($data['post']->category_id, $id);
            $data['previous_post'] = $this->post_model->get_previous_post($id);
            $data['next_post'] = $this->post_model->get_next_post($id);

            $data['is_reading_list'] = $this->reading_list_model->is_post_in_reading_list($id);

            $data['post_type'] = $data['post']->post_type;

            if (!empty($data['post']->feed_id)) {
                $data['feed'] = $this->rss_model->get_feed($data['post']->feed_id);
            }

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $data['og_title'] = $data['post']->title;
            $data['og_description'] = $data['post']->summary;
            $data['og_type'] = "article";
            $data['og_url'] = post_url($data['post']);

            if (!empty($data['post']->image_url)) {
                $data['og_image'] = $data['post']->image_url;
            } else {
                $data['og_image'] = base_url() . $data['post']->image_default;
            }
            $data['og_width'] = "750";
            $data['og_height'] = "500";
            $data['og_creator'] = $data['post_user']->username;
            $data['og_author'] = $data['post_user']->username;
            $data['og_published_time'] = $data['post']->created_at;
            $data['og_modified_time'] = $data['post']->created_at;
            $data['og_tags'] = $data['post_tags'];

            $this->reaction_model->set_voted_reactions_session($id);
            $data["reactions"] = $this->reaction_model->get_reaction($id);
            $data["emoji_lang"] = $this->selected_lang->folder_name;

            // $this->load->view('partials/_header', $data);
            // $this->load->view('post/post', $data);
            // $this->load->view('partials/_footer', $data);

            $this->load->view('econ/layouts/headers', $data);
            $this->load->view('econ/post/post',$data);
            $this->load->view('econ/layouts/footers', $data);

            //increase post hit
            $this->post_model->increase_post_hit($data['post']);

        }
    }
    public function events(){

        $page = $this->page_model->get_page("post_events");

        $data['title'] = get_page_title($page);
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        $data['post_events'] = $this->postevents_admin_model->get_events(10);
        $data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        $this->checkPageAuth($data['page']);
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            //set paginated
            $pagination = $this->paginate(lang_base_url() . "events", $this->total_post_events_count);

            $data['posts'] = get_cached_data('post_events_page_' . $pagination['current_page']);
             if (empty($data['posts'])) {
                $data['posts'] = $this->postevents_model->get_paginated_post_events($pagination['per_page'], $pagination['offset']);
                set_cache_data('post_events_page_' . $pagination['current_page'], $data['posts']);
            }

            // dd($this->total_post_events_count);

            $this->load->view('econ/layouts/headers', $data);
            $this->load->view('econ/event',$data);
            $this->load->view('econ/layouts/footers', $data);
        }
    }
    public function contactus(){

        $page = $this->page_model->get_page("contactus");

        $data['title'] = get_page_title($page);
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        $data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        $this->checkPageAuth($data['page']);
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $this->load->view('econ/layouts/headers', $data);
            $this->load->view('econ/contactus',$data);
            $this->load->view('econ/layouts/footers', $data);
        }
    }

    public function event($slug){
        $slug = $this->security->xss_clean($slug);
        if (empty($slug)) {
            redirect(lang_base_url());
        }
        $data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        $data['post'] = $this->postevents_model->get_post($slug);

        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data['post_events'] = $this->postevents_admin_model->get_events(10);

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $this->load->view('econ/layouts/headers', $data);
            $this->load->view('econ/event_detail',$data);
            $this->load->view('econ/layouts/footers', $data);
        }
    }

    public function error_404()
    {
        $data['title'] = "Error 404";
        $data['description'] = "Error 404";
        $data['keywords'] = "error,404";

        $this->load->view('econ/layouts/headers', $data);
        $this->load->view('econ/layouts/error_404');
        $this->load->view('econ/layouts/footers', $data);
    }
}
