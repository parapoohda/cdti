<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_controller extends Home_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('bcrypt');
        $this->post_load_more_count = 6;
    }

    public function index()
    {
        //return;
        //echo 'test';
        // dd(1);

        $data['title'] = 'หน้าแรก';
        $data['description'] = $this->settings->site_description;
        $data['keywords'] = $this->settings->keywords;
        $data['home_title'] = $this->settings->home_title;
        $data['visible_posts_count'] = $this->post_load_more_count;

        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['post_events'] = $this->postevents_admin_model->get_events(10);

        $data['royal_speech'] = $this->page_model->get_default_page('royal_speech', $this->selected_lang->id);

        $data['activity_posts'] = get_cached_data('activity_posts');
        if (empty($data['activity_posts'])) {
            $category = $this->category_model->get_category(14);
            $data['activity_posts'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 6);
            set_cache_data('activity_posts', $data['activity_posts']);
        }

        $data['activity_posts_2'] = get_cached_data('activity_posts_2');
        if (empty($data['activity_posts_2'])) {
            $category = $this->category_model->get_category(39);
            $data['activity_posts_2'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_2', $data['activity_posts_2']);
        }

        $data['activity_posts_3'] = get_cached_data('activity_posts_3');
        if (empty($data['activity_posts_3'])) {
            $category = $this->category_model->get_category(40);

            //echo json_encode($category->name_slug) ;

            $data['activity_posts_3'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);

            //echo json_encode($this->db->last_query()) ;
            //exit;

            set_cache_data('activity_posts_3', $data['activity_posts_3']);
        }

        $data['activity_posts_4'] = get_cached_data('activity_posts_4');
        if (empty($data['activity_posts_4'])) {
            $category = $this->category_model->get_category(41);

            $data['activity_posts_4'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_4', $data['activity_posts_4']);
        }

        $data['activity_posts_5'] = get_cached_data('activity_posts_5');
        if (empty($data['activity_posts_5'])) {
            $category = $this->category_model->get_category(42);

            $data['activity_posts_5'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_5', $data['activity_posts_5']);
        }

        if (empty($data['activity_posts_6'])) {
            $category = $this->category_model->get_category(43);


            $data['activity_posts_6'] = $this->post_model->get_emagazine_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_6', $data['activity_posts_6']);
        }

        //echo $this->visitor_model->get_today();
        //echo json_encode($this->visitor_model->get_last_thirty_entries());

        //$data['visitor']->yesterday = $this->visitor_model->get_yesterday();
        //echo $visitor['yesterday'];
        //$data['visitor']->lastMonth = $this->visitor_model->get_last_thirty_entries();
        //$data['visitor']->total = $this->visitor_model->get_total();
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/index', $data);

        $this->load->view('cdti/layout/footer', $data);
    }


    public function any($slug)
    {

        /*echo 'test';
        exit;
        */
        $slug = $this->security->xss_clean($slug);

        if (empty($slug)) {
            redirect(lang_base_url());
        }

        // get_posts_latest();
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_page_by_lang($slug, $this->selected_lang->id);
        // dd($data['page']);exit;
        if (isset($data['page']->id) and $data['page']->page_type == 'page-default') {
            if ($data['page']->visibility == 0) {
                $this->error_404();
            } elseif ($data['page']->id == 110) {
                $this->general_page($slug);
                // echo 555555;exit;
            } elseif ($data['page']->id == 138) {
                $this->school_page($slug);
                // echo 555555;exit;
            } elseif ($data['page']->id == 139) {
                $this->office_page($slug);
                // echo 555555;exit;
            } else {
                $this->page_default($slug);
            }
        } else if (isset($data['page']->id) and $data['page']->page_type == 'page-oit') {
            $this->page_oit($slug);
        } else {
            if (!empty($data['page'])) {
                if ($data['page']->visibility == 0) {
                    $this->error_404();
                } else {

                    if ($data['page']->page_type == 'fac') {
                        $this->fac_page($slug);
                    } else {
                        $this->checkPageAuth($data['page']);
                        $data['title'] = $data['page']->title;
                        $data['description'] = $data['page']->description;
                        $data['keywords'] = $data['page']->keywords;
                        // dd($data);
                        $this->load->view('cdti/layout/header', $data);
                        $this->load->view('cdti/page', $data);

                        $this->load->view('cdti/layout/footer', $data);
                    }
                }
            } else {
                $this->post($slug);
            }
        }
    }

    public function page_contact_us()
    {
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );

        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );


        $data['title'] = "ที่อยู่ การติดต่อ";
        $data['description'] = "ติดต่อเรา";
        $data['keywords'] = "ที่อยู่";
        $data['category'] = new stdClass();
        $data['category']->slugname  = "ที่อยู่ติดต่อ";
        $this->load->view('cdti/layout/header', $data);
        $slug = "test";
        //$this->load->view($data['page']->topbar, ['page' => $data['page'], 'route' => $data['page']->slug]);
        $this->load->view('cdti/default_page/contact_us/topbar', ['page' => $data['page'], 'route' => '']);
        $this->load->view('cdti/default_page/contact_us/contact_us', $data);

        $this->load->view('cdti/layout/footer', $data);
        //echo "contact us";exit;
    }

    public function page_default($slug)
    {
        //การเปิดเผยข้อมูลสาธารณะ (OIT)
        //set data for oit page
        if ($slug == "ข้อมูลสาธารณะ-(OIT)") {
            $oitObj = $this->oit_model->getJsonId1();
            $json = $oitObj->json;
            $data['oit'] = json_decode($json);
        }

        //echo $slug;exit;
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_page_by_lang($slug, $this->selected_lang->id);

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;

            $this->load->view('cdti/layout/header', $data);
            if ($data['page']->topbar) {
                $this->load->view($data['page']->topbar, ['page' => $data['page'], 'route' => $data['page']->slug]);
            } else {
                $this->load->view('cdti/layout/page_default_topbar', ['page' => $data['page'], 'route' => '']);
            }

            $this->load->view($data['page']->link, $data);

            $this->load->view('cdti/layout/footer', $data);
            //cdti/default_page/register/bachelor/topbar.php
        }
    }
    public function page_oit($slug)
    {
        //echo 'a';
        //exit;
        // echo 555;exit;
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_page_by_lang($slug, $this->selected_lang->id);

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;

            $this->load->view('cdti/layout/header', $data);
            if ($data['page']->topbar) {
                $this->load->view($data['page']->topbar, ['page' => $data['page'], 'route' => $data['page']->slug]);
            } else {
                $this->load->view('cdti/layout/oit_topbar', ['page' => $data['page'], 'route' => '']);
            }

            $this->load->view($data['page']->link, $data);

            $this->load->view('cdti/layout/footer', $data);
            //cdti/default_page/register/bachelor/topbar.php
        }
    }


    public function fac_page($slug)
    {
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        // dd($data['page']);
        $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id, $this->selected_lang->id);
        $data['page_main'] = $this->page_model->get_main_page($data['page']->subcategory_id, $this->selected_lang->id);
        // $data['fac_posts'] = $this->post_model->get_rss_posts_fac_category_limit($data['page']->main_category_id,5);
        $data['fac_posts'] = $this->post_model->get_post_by_tag_limit($data['page']->title, 5);

        // dd($data['page']);

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;

            // $css = explode('/',$data['page']->link);
            // dd($css[3]);
            $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

            $this->load->view('cdti/layout/header', $data);

            if ($data['page']->subcategory_id) {
                $this->load->view('cdti/layout/fac_topbar', ['page' => $data['page_main'], 'route' => '']);
            } else {
                if ($data['page']->category_id == 6 or $data['page']->category_id == 9) {
                    $this->load->view('cdti/layout/facschool_topbar', ['page' => $data['page'], 'route' => '']);
                } else {
                    $this->load->view('cdti/layout/fac_topbar', ['page' => $data['page'], 'route' => '']);
                }
            }
            // $this->load->view("cdti/default_page/fac/business/campus.php",$data);

            $this->load->view("cdti/default_page/faculty/campus.php", $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function general_page($slug)
    {
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        // dd($data['page']);
        $data['page_main'] = $this->page_model->get_main_page($data['page']->subcategory_id, $this->selected_lang->id);

        // dd($data);  exit;

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;

            // $css = explode('/',$data['page']->link);
            // dd($css[3]);
            $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

            $this->load->view('cdti/layout/header', $data);

            if ($data['page']->subcategory_id) {
                $this->load->view('cdti/layout/general_topbar', ['page' => $data['page_main'], 'route' => '']);
            } else {
                $this->load->view('cdti/layout/general_topbar', ['page' => $data['page'], 'route' => '']);
            }

            $this->load->view($data['page']->link, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function office_page($slug)
    {
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        // dd($data['page']);
        $data['page_main'] = $this->page_model->get_main_page($data['page']->subcategory_id, $this->selected_lang->id);

        // dd($data);  exit;

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;

            // $css = explode('/',$data['page']->link);
            // dd($css[3]);
            $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

            $this->load->view('cdti/layout/header', $data);

            if ($data['page']->subcategory_id) {
                $this->load->view('cdti/layout/office_topbar', ['page' => $data['page_main'], 'route' => '']);
            } else {
                $this->load->view('cdti/layout/office_topbar', ['page' => $data['page'], 'route' => '']);
            }

            $this->load->view($data['page']->link, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function school_page($slug)
    {
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        // dd($data['page']);
        $data['page_main'] = $this->page_model->get_main_page($data['page']->subcategory_id, $this->selected_lang->id);

        // dd($data);  exit;

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;

            // $css = explode('/',$data['page']->link);
            // dd($css[3]);
            $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

            $this->load->view('cdti/layout/header', $data);

            if ($data['page']->subcategory_id) {
                $this->load->view('cdti/layout/school_topbar', ['page' => $data['page_main'], 'route' => '']);
            } else {
                $this->load->view('cdti/layout/school_topbar', ['page' => $data['page'], 'route' => '']);
            }

            $this->load->view($data['page']->link, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function general($slug, $route)
    {
        $slug = $this->security->xss_clean($slug);
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['route'] = $route;
        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);

        $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id, $this->selected_lang->id);

        $css = explode('/', $data['page']->link);
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

        //$data['page_view'] = $data['page']->link = $page[$data['page']->id];

        if ($route == 'team') {
            $data['header'] = true;
            $data['teams'] = $this->gallery_model->get_images_by_category_type(3, 4);
            // dd($data['teams']);exit;
            // $data['page']->link = str_replace('.php','_team.php',$data['page']->link);
            $page_view = 'cdti/default_page/general_team.php';
        } else if ($route == 'contact') {
            $data['header'] = true;
            // $data['page']->link = str_replace('.php','_contact.php',$data['page']->link);
            $page_view = 'cdti/default_page/general_contact.php';
            // dd($data['page']);exit;
        } else {
            $this->general_page($slug);
        }

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/general_topbar', $data);
            $this->load->view($page_view, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function personnel($key)
    {
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $slug = 'สำนักงานสถาบันฯ';
        $data = array();
        $posts = $this->post_model->get_personnel_pdf($key);

        $data['file_path'] = false;
        if ($posts) {
            $post = end($posts);
            if ($post) {
                $data['file_path'] = $this->download($post->id);
            }
        }
        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        $data['route'] = 'team';
        $data['description'] = 'description';
        $data['keywords'] = 'keywords';
        $data['title'] = 'title';
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/layout/office_topbar', $data);
        $this->load->view('cdti/default_page/office_team_pdf', $data);

        $this->load->view('cdti/layout/footer', $data);
    }

    public function personnel_administration()
    {
        $this->personnel('administration');
    }

    public function student_affairs()
    {
        $this->personnel('student_affairs');
    }

    public function professional_academic()
    {
        $this->personnel('professional_academic');
    }

    public function office($slug, $route)
    {
        $slug = $this->security->xss_clean($slug);
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['route'] = $route;
        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);

        $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id, $this->selected_lang->id);

        $css = explode('/', $data['page']->link);
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

        //$data['page_view'] = $data['page']->link = $page[$data['page']->id];

        if ($route == 'team') {
            $data['header'] = true;
            $data['teams'] = $this->gallery_model->get_images_by_category_type(3, 4);
            // dd($data['teams']);exit;
            // $data['page']->link = str_replace('.php','_team.php',$data['page']->link);
            $page_view = 'cdti/default_page/office_team.php';
        } else if ($route == 'contact') {
            $data['header'] = true;
            // $data['page']->link = str_replace('.php','_contact.php',$data['page']->link);
            $page_view = 'cdti/default_page/office_contact.php';
            // dd($data['page']);exit;
        } else {
            $this->office_page($slug);
        }

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/office_topbar', $data);
            $this->load->view($page_view, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }
    public function school($slug, $route)
    {
        $slug = $this->security->xss_clean($slug);
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['route'] = $route;
        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);

        $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id, $this->selected_lang->id);

        $css = explode('/', $data['page']->link);
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

        //$data['page_view'] = $data['page']->link = $page[$data['page']->id];

        if ($route == 'team') {
            $data['header'] = true;
            $data['teams'] = $this->gallery_model->get_images_by_category_type(3, 5);
            // dd($data['teams']);exit;
            // $data['page']->link = str_replace('.php','_team.php',$data['page']->link);
            $page_view = 'cdti/default_page/school/team.php';
        } else if ($route == 'branch') {
            $data['header'] = true;
            // $data['page']->link = str_replace('.php','_contact.php',$data['page']->link);
            $page_view = 'cdti/default_page/school/branch.php';
            // dd($data['page']);exit;
        } else if ($route == 'posts') {
            $data['header'] = true;
            $category_id = $data['page']->main_category_id;
            // dd($category_id );exit;
            $data['category_type'] = "sub";

            $count_key = 'posts_count_category' . $category_id;
            $posts_key = 'posts_category' . $category_id;
            $data['category_type'] = "sub";

            $total_rows = $this->post_model->get_post_count_by_tag($data['page']->title);
            $pagination = $this->paginate(lang_base_url() . 'school/' . $slug . '/posts', $total_rows);
            // dd($data['page']->title );exit;
            // $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset']);
            $data['posts'] = $this->post_model->get_paginated_tag_posts($data['page']->title, $pagination['per_page'], $pagination['offset']);

            $data['header'] = true;

            $page_view = 'cdti/default_page/school/news.php';
            // dd($data['page']);exit;
        } else if ($route == 'contact') {
            $data['header'] = true;
            // $data['page']->link = str_replace('.php','_contact.php',$data['page']->link);
            $page_view = 'cdti/default_page/school/contact.php';
            // dd($data['page']);exit;
        } else {
            $this->school_page($slug);
        }

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/school_topbar', $data);
            $this->load->view($page_view, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function fac($slug, $route)
    {
        $slug = $this->security->xss_clean($slug);

        if ($slug == "สภาสถาบัน") {

            exit;
        }
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['route'] = $route;
        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);

        $data['page_branch'] = $this->page_model->get_branch_page($data['page']->main_category_id, $this->selected_lang->id);

        $css = explode('/', $data['page']->link);
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/' . $data['page']->link;

        //$data['page_view'] = $data['page']->link = $page[$data['page']->id];

        if ($route == 'knowus') {
            $category_id = $data['page']->main_category_id;
            $data['posttype'] = "post";
            $data['fac_reward'] = $this->post_model->get_paginated_category_posts($data['posttype'], $category_id, 7, 0, $data['posttype']);
            $data['posttype'] = "fac_gallery";
            $data['fac_gallery'] = $this->post_model->get_paginated_category_posts($data['posttype'], $category_id, 6, 0, $data['posttype']);
            $data['page']->link = str_replace('.php', '_about.php', $data['page']->link);
        } else if ($route == 'branch') {
            $data['header'] = true;
            $page_view = 'cdti/default_page/faculty/campus_branch.php';
            //$data['page']->link = str_replace('.php','_branch.php',$data['page']->link);
        } else if ($route == 'posts') {
            $category_id = $data['page']->main_category_id;
            // dd($category_id );exit;
            $data['category_type'] = "sub";

            $count_key = 'posts_count_category' . $category_id;
            $posts_key = 'posts_category' . $category_id;
            $data['category_type'] = "sub";

            // $total_rows = get_cached_data($count_key);
            // if (empty($total_rows)) {
            //     // $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id);
            //     //$total_rows = $this->post_model->get_post_count_by_tag($data['page']->title);
            //     set_cache_data($count_key, $total_rows);
            // }
            // $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id);
            $total_rows = $this->post_model->get_post_count_by_tag($data['page']->title);
            // dd($data['page']->title);
            $pagination = $this->paginate(lang_base_url() . 'fac/' . $slug . '/posts', $total_rows);

            // $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset']);
            $data['posts'] = $this->post_model->get_paginated_tag_posts($data['page']->title, $pagination['per_page'], $pagination['offset']);

            $data['header'] = true;
            $page_view = 'cdti/default_page/faculty/campus_posts.php';
        } else if ($route == 'team') {
            $data['header'] = true;
            $data['teams'] = $this->gallery_model->get_images_by_fac_category($data['page']->main_category_id);
            // $data['page']->link = str_replace('.php','_team.php',$data['page']->link);
            $page_view = 'cdti/default_page/faculty/campus_team.php';
        } else if ($route == 'reward') {
            $category_id = $data['page']->main_category_id;

            $data['category_type'] = "sub";
            $data['posttype'] = "fac_reward";

            $count_key = 'reward_count_category' . $category_id;
            $posts_key = 'reward_category' . $category_id;
            $data['category_type'] = "sub";

            $total_rows = get_cached_data($count_key);
            if (empty($total_rows)) {
                $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id, $data['posttype']);
                set_cache_data($count_key, $total_rows);
            }
            $pagination = $this->paginate(lang_base_url() . 'fac/' . $slug . '/reward', $total_rows);
            $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset'], $data['posttype']);
            $data['header'] = true;
            $data['page']->link = str_replace('.php', '_reward.php', $data['page']->link);
        } else if ($route == 'gallery') {
            $category_id = $data['page']->main_category_id;
            $data['category_type'] = "sub";
            $data['posttype'] = "fac_gallery";

            $count_key = 'gallery_count_category' . $category_id;
            $posts_key = 'gallery_category' . $category_id;
            $data['category_type'] = "sub";

            $total_rows = get_cached_data($count_key);
            if (empty($total_rows)) {
                $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id, $data['posttype']);
                set_cache_data($count_key, $total_rows);
            }
            $pagination = $this->paginate(lang_base_url() . 'fac/' . $slug . '/gallery', $total_rows);
            $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset'], $data['posttype']);
            $data['header'] = true;
            $data['page']->link = str_replace('.php', '_gallery.php', $data['page']->link);
        } else if ($route == 'contact') {
            $data['header'] = true;
            // $data['page']->link = str_replace('.php','_contact.php',$data['page']->link);
            $page_view = 'cdti/default_page/faculty/campus_contact.php';
            // dd($data['page']);exit;
        } else {
            $this->fac_page($slug);
        }

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $data['page']->title;
            $data['description'] = $data['page']->description;
            $data['keywords'] = $data['page']->keywords;
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/fac_topbar', $data);
            $this->load->view($page_view, $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function fac_reward($slug, $slug2)
    {
        $slug = $this->security->xss_clean($slug);
        $slug2 = $this->security->xss_clean($slug2);
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        $data['post'] = $this->post_model->get_post($slug2);
        if (empty($data['page'])) {
            $this->error_404();
        }
        //check if post exists
        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data["category"] = $this->category_model->get_category($data['post']->category_id);
            $data["subcategory"] = $this->category_model->get_category($data['post']->subcategory_id);
            $data['post_tags'] = $this->tag_model->get_post_tags($id);
            $data['post_image_count'] = $this->post_file_model->get_post_additional_image_count($id);
            $data['post_file_count'] = $this->post_file_model->get_post_files_count($id);
            $data['post_files'] = $this->post_file_model->get_post_files($id);
            $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
            $data['post_user'] = $this->auth_model->get_user($data['post']->user_id);
            $data['comments'] = $this->comment_model->get_comments($id, 5);
            $data['vr_comment_limit'] = 5;

            $data['related_posts'] = $this->post_model->get_related_posts($data['post']->category_id, $id);

            $data['previous_post'] = $this->post_model->get_previous_post($id);
            $data['next_post'] = $this->post_model->get_next_post($id);

            $data['is_reading_list'] = $this->reading_list_model->is_post_in_reading_list($id);

            $data['post_type'] = $data['post']->post_type;

            if (!empty($data['post']->feed_id)) {
                $data['feed'] = $this->rss_model->get_feed($data['post']->feed_id);
            }

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $data['og_title'] = $data['post']->title;
            $data['og_description'] = $data['post']->summary;
            $data['og_type'] = "article";
            $data['og_url'] = post_url($data['post']);

            if (!empty($data['post']->image_url)) {
                $data['og_image'] = $data['post']->image_url;
            } else {
                $data['og_image'] = base_url() . $data['post']->image_default;
            }
            $data['og_width'] = "750";
            $data['og_height'] = "500";
            $data['og_creator'] = $data['post_user']->username;
            $data['og_author'] = $data['post_user']->username;
            $data['og_published_time'] = $data['post']->created_at;
            $data['og_modified_time'] = $data['post']->created_at;
            $data['og_tags'] = $data['post_tags'];

            $this->reaction_model->set_voted_reactions_session($id);
            $data["reactions"] = $this->reaction_model->get_reaction($id);
            $data["emoji_lang"] = $this->selected_lang->folder_name;

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/fac_topbar', ['page' => $data['page'], 'route' => '']);
            $this->load->view('cdti/post/fac_reward', $data);

            $this->load->view('cdti/layout/footer', $data);
            $this->post_model->increase_post_hit($data['post']);
        }
    }

    public function fac_gallery($slug, $slug2)
    {
        $slug = $this->security->xss_clean($slug);
        $slug2 = $this->security->xss_clean($slug2);
        $view_data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $view_data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['page'] = $this->page_model->get_default_page($slug, $this->selected_lang->id);
        $data['post'] = $this->post_model->get_post($slug2);
        if (empty($data['page'])) {
            $this->error_404();
        }
        //check if post exists
        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data["category"] = $this->category_model->get_category($data['post']->category_id);
            $data["subcategory"] = $this->category_model->get_category($data['post']->subcategory_id);
            $data['post_tags'] = $this->tag_model->get_post_tags($id);
            $data['post_image_count'] = $this->post_file_model->get_post_additional_image_count($id);
            $data['post_file_count'] = $this->post_file_model->get_post_files_count($id);
            $data['post_files'] = $this->post_file_model->get_post_files($id);
            $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
            $data['post_user'] = $this->auth_model->get_user($data['post']->user_id);
            $data['comments'] = $this->comment_model->get_comments($id, 5);
            $data['vr_comment_limit'] = 5;

            $data['related_posts'] = $this->post_model->get_related_posts($data['post']->category_id, $id);

            $data['previous_post'] = $this->post_model->get_previous_post($id);
            $data['next_post'] = $this->post_model->get_next_post($id);

            $data['is_reading_list'] = $this->reading_list_model->is_post_in_reading_list($id);

            $data['post_type'] = $data['post']->post_type;

            if (!empty($data['post']->feed_id)) {
                $data['feed'] = $this->rss_model->get_feed($data['post']->feed_id);
            }

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $data['og_title'] = $data['post']->title;
            $data['og_description'] = $data['post']->summary;
            $data['og_type'] = "article";
            $data['og_url'] = post_url($data['post']);

            if (!empty($data['post']->image_url)) {
                $data['og_image'] = $data['post']->image_url;
            } else {
                $data['og_image'] = base_url() . $data['post']->image_default;
            }
            $data['og_width'] = "750";
            $data['og_height'] = "500";
            $data['og_creator'] = $data['post_user']->username;
            $data['og_author'] = $data['post_user']->username;
            $data['og_published_time'] = $data['post']->created_at;
            $data['og_modified_time'] = $data['post']->created_at;
            $data['og_tags'] = $data['post_tags'];

            $this->reaction_model->set_voted_reactions_session($id);
            $data["reactions"] = $this->reaction_model->get_reaction($id);
            $data["emoji_lang"] = $this->selected_lang->folder_name;

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/fac_topbar', ['page' => $data['page'], 'route' => '']);
            $this->load->view('cdti/post/fac_gallery', $data);

            $this->load->view('cdti/layout/footer', $data);
            $this->post_model->increase_post_hit($data['post']);
        }
    }

    public function search()
    {
        $q = trim($this->input->post('q', true));

        $data['q'] = $q;
        $data['title'] = trans("search") . ': ' . $q;
        $data['description'] = trans("search") . ': ' . $q;
        $data['keywords'] = trans("search") . ', ' . $q;
        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'search', $this->post_model->get_search_post_count($q));
        $data['posts'] = $this->post_model->get_paginated_search_posts($q, $pagination['per_page'], $pagination['offset']);


        $this->load->view('cdti/layout/header', $data);
        //$this->load->view('cdti/search', $data);

        $this->load->view('cdti/category', $data);
        $this->load->view('cdti/layout/footer', $data);
    }

    public function checkPageAuth($page)
    {
        if (!auth_check() && $page->need_auth == 1) {
            $this->session->set_flashdata('error', trans("message_page_auth"));
            redirect(lang_base_url());
        }
    }

    public function posts()
    {
        $page = $this->page_model->get_page("posts");

        $data['title'] = 'ข่าวทั้งหมด';
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        //check page auth
        $this->checkPageAuth($data['page']);

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            //set paginated
            $pagination = $this->paginate(lang_base_url() . "posts", $this->total_posts_count);

            $data['posts'] = get_cached_data('posts_page_' . $pagination['current_page']);
            if (empty($data['posts'])) {
                $data['posts'] = $this->post_model->get_paginated_posts($pagination['per_page'], $pagination['offset']);
                set_cache_data('posts_page_' . $pagination['current_page'], $data['posts']);
            }

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/posts', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function featured_posts()
    {
        $page = $this->page_model->get_page("posts");

        $data['title'] = 'ข่าวแนะนำ';
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        //check page auth
        $this->checkPageAuth($data['page']);

        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            //set paginated
            $total_posts_count = $this->post_model->featured_posts_count_by_lang($this->selected_lang->id);
            $pagination = $this->paginate(lang_base_url() . "posts", $total_posts_count);
            $data['posts'] = $this->post_model->featured_posts_paginated_posts($pagination['per_page'], $pagination['offset']);

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/category', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function fix_date($mysql_date)
    {
        $month = array(
            'ม.ค.',
            'ก.พ.',
            'มี.ค.',
            'เม.ย.',
            'พ.ค.',
            'มิ.ย.',
            'ก.ค.',
            'ส.ค.',
            'ก.ย.',
            'ต.ค.',
            'พ.ย.',
            'ธ.ค.'
        );
        $time = strtotime($mysql_date);
        $d = date("d", $time);
        $m = $month[date("m", $time) - 1];
        $y = date("Y", $time) + 543;
        return "$d $m $y";
    }

    public function download($id)
    {
        $files = $this->post_file_model->get_post_files($id);
        $file = end($files);
        if ($file) {
            return base_url() . '/' . $file->file_path;
        } else {
            return false;
        }
    }


    public function public($slug)
    {
        if ($slug == 'emagazine') {
            $slug = $this->security->xss_clean($slug);
            $page = $this->page_model->get_default_page("public/" . $slug, $this->selected_lang->id);
            $data['page'] = $page;

            $data['title'] = $page->title;
            $data['description'] =  $page->description;
            $data['keywords'] =  $page->keywords;

            //set paginated
            $pagination = $this->paginate(lang_base_url() . 'public/' . $slug, $this->post_model->get_post_count_by_category('parent', 43, 'emagazine'));
            $pagination['per_page'] = 12;
            $data['posts'] = $this->post_model->get_paginated_category_posts('parent', 43, $pagination['per_page'], $pagination['offset'], 'emagazine');
            //  dd($data['posts']);
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/public/emagazine', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function document($slug)
    {
        //$slug = "document/" . $this->security->xss_clean($slug);
        $slug = $this->security->xss_clean($slug);
        $page = $this->page_model->get_default_page("document/" . $slug, $this->selected_lang->id);
        $data['page'] = $page;
        // echo $slug;
        // // echo 22;แฟ
        // dd($page);

        // //check page auth
        // $this->checkPageAuth($data['page']);

        if (!isset($data['page']) or $data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $data['title'] = $page->title;
            $data['description'] = get_page_description($page);
            $data['keywords'] = get_page_keywords($page);

            if ($slug === 'คู่มือนักศึกษา') {
                $data['posts'] = $this->post_model->get_pdf('manual');
            } else if ($slug === 'แบบฟอร์มคำร้อง') {
                $data['posts'] = $this->post_model->get_pdf('petition_form');
            }

            foreach ($data['posts'] as $key => $post) {
                $data['posts'][$key]->date = $this->fix_date($post->created_at);
                $data['posts'][$key]->file_path = $this->download($post->id);
            }

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/layout/doc_topbar', $data);
            $this->load->view('cdti/default_page/document', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function event_calendar()
    {
        echo json_encode($this->postevents_admin_model->my_get_event_date(date("Y-m-d"), 10));
    }

    public function event_calendar_date($date)
    {
        echo json_encode($this->postevents_admin_model->my_get_event_date($date, 10));
    }

    public function post_cache($slug)
    {
        $slug = $this->security->xss_clean($slug);
        $this->load->driver('cache', array('adapter' => 'file'));

        //----Check Post Cache -----
        $post_slug = $this->post_model->get_post_slug($slug);

        if ($post_slug) {
            $cache_key = "post_id_cache_" . $post_slug->post_id;
            if ($data = $this->cache->get($cache_key)) {
                // echo "---<pre>";
                // print_r($data);
                // exit;
                $this->load->view('cdti/layout/header', $data);
                $this->load->view('cdti/post/post', $data);

                $this->load->view('cdti/layout/footer', $data);
                //increase post hit
                $this->post_model->increase_post_hit($data['post']);
                return;
            }
        }
        //----Check Post Cache -----


        $slug = $this->security->xss_clean($slug);
        $post_slug = $this->post_model->get_post_slug($slug);
        $data = [];
        if ($post_slug) {
            $cache_key = "post_id_cache_" . $post_slug->post_id;
            if (!$data = $this->cache->get($cache_key)) {
                // echo 'Saving to the cache!<br />';
                // $foo = 'foobarbaz!';
                // $data = $this->post_model->get_post($slug);

                // // Save into the cache for 5 minutes
                // $cache_key = "post_id_cache_".$post_slug->post_id;
                // $this->cache->save($key, $data, 3600);
            }
        }
        // echo "---<pre>";
        // print_r($data);
        // exit;
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/post/post', $data);

        $this->load->view('cdti/layout/footer', $data);

        //increase post hit
        $this->post_model->increase_post_hit($data['post']);

        // echo "---<pre>";
        // print_r($data);
        // exit;



        // echo (substr(md5($slug), 0, 32));
    }
    private function post_cache_view($data)
    {
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/post/post', $data);

        $this->load->view('cdti/layout/footer', $data);
        //increase post hit
        $this->post_model->increase_post_hit($data['post']);
        exit;
    }

    public function post($slug)
    {

        //set_time_limit(0);
        //echo "test page";
        //exit;
        $this->load->driver('cache', array('adapter' => 'file'));
        //----Check Post Cache -----
        $post_slug = $this->post_model->get_post_slug($slug);

        if ($post_slug) {
            $cache_key = "post_id_cache_" . $post_slug->post_id;
            if ($data = $this->cache->get($cache_key)) {
                // echo "---<pre>";
                // print_r($data);
                // exit;
                $this->load->view('cdti/layout/header', $data);
                $this->load->view('cdti/post/post', $data);

                $this->load->view('cdti/layout/footer', $data);
                //increase post hit
                $this->post_model->increase_post_hit($data['post']);
                return;
            }
        }
        // echo "**";
        //----Check Post Cache -----

        $data['post'] = $this->post_model->get_post($slug);

        //check if post exists
        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data["category"] = $this->category_model->get_category($data['post']->category_id);
            $data["subcategory"] = $this->category_model->get_category($data['post']->subcategory_id);
            $data['post_tags'] = $this->tag_model->get_post_tags($id);
            $data['post_image_count'] = $this->post_file_model->get_post_additional_image_count($id);
            $data['post_file_count'] = $this->post_file_model->get_post_files_count($id);
            $data['post_files'] = $this->post_file_model->get_post_files($id);
            $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
            $data['post_user'] = $this->auth_model->get_user($data['post']->user_id);
            $data['comments'] = $this->comment_model->get_comments($id, 5);
            $data['vr_comment_limit'] = 5;

            $data['related_posts'] = $this->post_model->get_related_posts($data['post']->category_id, $id);

            $data['previous_post'] = $this->post_model->get_previous_post($id);
            $data['next_post'] = $this->post_model->get_next_post($id);

            $data['is_reading_list'] = $this->reading_list_model->is_post_in_reading_list($id);

            $data['post_type'] = $data['post']->post_type;

            if (!empty($data['post']->feed_id)) {
                $data['feed'] = $this->rss_model->get_feed($data['post']->feed_id);
            }

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $data['og_title'] = $data['post']->title;
            $data['og_description'] = $data['post']->summary;
            $data['og_type'] = "article";
            $data['og_url'] = post_url($data['post']);

            if (!empty($data['post']->image_url)) {
                $data['og_image'] = $data['post']->image_url;
            } else {
                $data['og_image'] = base_url() . $data['post']->image_default;
            }
            $data['og_width'] = "750";
            $data['og_height'] = "500";
            $data['og_creator'] = $data['post_user']->username;
            $data['og_author'] = $data['post_user']->username;
            $data['og_published_time'] = $data['post']->created_at;
            $data['og_modified_time'] = $data['post']->created_at;
            $data['og_tags'] = $data['post_tags'];

            $this->reaction_model->set_voted_reactions_session($id);
            $data["reactions"] = $this->reaction_model->get_reaction($id);
            $data["emoji_lang"] = $this->selected_lang->folder_name;

            // $this->load->view('partials/_header', $data);
            // $this->load->view('post/post', $data);
            // $this->load->view('partials/_footer', $data);
            if ($id == 687) {
                $cache_key = "post_id_cache_" . $id;
                $this->cache->save($cache_key, $data, 3600);
            }


            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/post/post', $data);

            $this->load->view('cdti/layout/footer', $data);

            //increase post hit
            $this->post_model->increase_post_hit($data['post']);
        }
    }

    public function category($slug)
    {
        $slug = $this->security->xss_clean($slug);

        $data['category'] = $this->category_model->get_category_by_slug($slug);

        //check category exists
        if (empty($data['category'])) {
            redirect(lang_base_url());
        }

        $category_id = $data['category']->id;
        $data['title'] = $data['category']->name;
        $data['description'] = $data['category']->description;
        $data['keywords'] = $data['category']->keywords;

        //category type
        $data['category_type'] = "";
        if ($data['category']->parent_id == 0) {
            $data['category_type'] = "parent";
        } else {
            $data['category_type'] = "sub";
        }

        $count_key = 'posts_count_category' . $data['category']->id;
        $posts_key = 'posts_category' . $data['category']->id;

        //category posts count
        $total_rows = get_cached_data($count_key);
        if (empty($total_rows)) {
            $total_rows = $this->post_model->get_post_count_by_category($data['category_type'], $category_id);
            set_cache_data($count_key, $total_rows);
        }

        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'category/' . $slug, $total_rows);

        // $data['posts'] = get_cached_data($posts_key . '_page' . $pagination['current_page']);
        // if (empty($data['posts'])) {
        //     $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset']);
        //     set_cache_data($posts_key . '_page' . $pagination['current_page'], $data['posts']);
        // }
        // dd($category_id);
        $data['posts'] = $this->post_model->get_paginated_category_posts($data['category_type'], $category_id, $pagination['per_page'], $pagination['offset']);
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/category', $data);

        $this->load->view('cdti/layout/footer', $data);
    }

    public function tag($tag_slug)
    {
        $tag_slug = $this->security->xss_clean($tag_slug);
        $data['tag'] = $this->tag_model->get_tag($tag_slug);
        //echo json_encode($data['tag']) ;
        //exit;

        //check tag exists
        if (empty($data['tag'])) {
            redirect(lang_base_url());
        }

        $data['title'] = $data['tag']->tag;
        $data['description'] = trans("tag") . ': ' . $data['tag']->tag;
        $data['keywords'] = trans("tag") . ', ' . $data['tag']->tag;
        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'tag/' . $tag_slug, $this->post_model->get_post_count_by_tag($tag_slug));
        $pagination['per_page'] = 12;
        $data['posts'] = $this->post_model->get_paginated_tag_posts($tag_slug, $pagination['per_page'], $pagination['offset']);
        // dd($data['posts']);
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/tag', $data);

        $this->load->view('cdti/layout/footer', $data);
    }

    public function news($tag_slug)
    {
        $tag_slug = $this->security->xss_clean($tag_slug);
        // $data['tag'] = $this->tag_model->get_tag($tag_slug);
        $data['category'] = $this->category_model->get_category_by_slug($tag_slug);

        // dd($data['category']);
        //check tag exists
        if (empty($data['category'])) {
            redirect(lang_base_url());
        }
        $data['title'] = $data['category']->name;
        $data['description'] = $data['category']->description;
        $data['keywords'] = $data['category']->keywords;
        //set paginated
        $pagination = $this->paginate(lang_base_url() . 'news/' . $tag_slug, $this->post_model->get_post_count_by_tag($tag_slug));
        $pagination['per_page'] = 12;
        $data['posts'] = $this->post_model->get_paginated_tag_posts($tag_slug, $pagination['per_page'], $pagination['offset']);
        // dd($data['posts']);
        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/category', $data);

        $this->load->view('cdti/layout/footer', $data);
    }

    public function events()
    {

        $page = $this->page_model->get_page("post_events");

        $data['title'] = get_page_title($page);
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        $data['post_events'] = $this->postevents_admin_model->get_events(10);

        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $this->checkPageAuth($data['page']);
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            //set paginated
            $pagination = $this->paginate(lang_base_url() . "events", $this->total_post_events_count);
            $data['posts'] = $this->postevents_model->get_paginated_post_events($pagination['per_page'], $pagination['offset']);

            // dd($this->total_post_events_count);

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/event', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function contactus()
    {

        $page = $this->page_model->get_page("contactus");

        $data['title'] = get_page_title($page);
        $data['description'] = get_page_description($page);
        $data['keywords'] = get_page_keywords($page);
        $data['page'] = $page;

        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $this->checkPageAuth($data['page']);
        if ($data['page']->visibility == 0) {
            $this->error_404();
        } else {
            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/contactus', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function event($slug)
    {
        $slug = $this->security->xss_clean($slug);
        if (empty($slug)) {
            redirect(lang_base_url());
        }
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['post'] = $this->postevents_model->get_post($slug);

        if (empty($data['post'])) {
            $this->error_404();
        } else {
            $id = $data['post']->id;
            if (!auth_check() && $data['post']->need_auth == 1) {
                $this->session->set_flashdata('error', trans("message_post_auth"));
                redirect(lang_base_url());
            }
            $data['post_events'] = $this->postevents_admin_model->get_events(10);

            $data['title'] = $data['post']->title;
            $data['description'] = $data['post']->summary;
            $data['keywords'] = $data['post']->keywords;

            $this->load->view('cdti/layout/header', $data);
            $this->load->view('cdti/event_detail', $data);

            $this->load->view('cdti/layout/footer', $data);
        }
    }

    public function error_404()
    {
        $data['title'] = "Error 404";
        $data['description'] = "Error 404";
        $data['keywords'] = "error,404";

        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/layout/error_404');

        $this->load->view('cdti/layout/footer', $data);
    }

    public function custom()
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";

        $this->load->view('cdti/layout/header', $data);
        $this->load->view('econ/custom_page', $data);

        $this->load->view('cdti/layout/footer', $data);
    }

    public function teams()
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        $data['teams'] = $this->gallery_model->get_images_by_category(3);

        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";

        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/teams', $data);

        $this->load->view('cdti/layout/footer', $data);
    }

    public function scholar()
    {
        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );

        // $data['page'] = $data;

        $data['title'] = "MIND CREDIT";
        $data['description'] = "MIND CREDIT";
        $data['keywords'] = "MIND CREDIT";

        $data['page_2'] = $this->page_model->get_page_by_slug('คุณสมบัติผู้เข้าร่วมโครงการ');
        $data['page_3'] = $this->page_model->get_page_by_slug('รูปแบบเงินสนับสนุนโครงการ');
        $data['page_4'] = $this->page_model->get_page_by_slug('เงื่อนไขการเบิกจ่าย');

        $this->load->view('cdti/layout/header', $data);
        $this->load->view('cdti/scholar', $data);

        $this->load->view('cdti/layout/footer', $data);
    }
}
