<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personnel_administration_controller extends Personnel_Pdf_Core_Controller
{
    public function __construct()
    {
        parent::__construct('administration','personnel_administration_controller','add_post');
    }
}