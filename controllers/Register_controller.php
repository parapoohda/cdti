<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_controller extends Home_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('bcrypt');
        $this->post_load_more_count = 6;
    }
    public function index()
    {
        $view_data['title']   = 'ลงทะเบียน';
        $view_data['description'] = 'ลงทะเบียน SMEs';
        $view_data['keywords'] = $this->settings->keywords;
        $view_data['home_title'] = $this->settings->home_title;
        $view_data['visible_posts_count'] = $this->post_load_more_count;
        $view_data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $view_data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        $view_data['links'] = $this->gallery_model->get_images_by_category(1);

        $this->load->view('frontend/template/header', $view_data);
        $this->load->view('frontend/register',$view_data);
        $this->load->view('frontend/template/footer', $view_data);

    }
    
    public function register_isp()
    {
        $view_data['title']   = 'ลงทะเบียน';
        $view_data['description'] = 'ลงทะเบียน SMEs';
        $view_data['keywords'] = $this->settings->keywords;
        $view_data['home_title'] = $this->settings->home_title;
        $view_data['visible_posts_count'] = $this->post_load_more_count;
        $view_data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $view_data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        $view_data['links'] = $this->gallery_model->get_images_by_category(1);

        $this->load->view('frontend/template/header', $view_data);
        $this->load->view('frontend/register',$view_data);
        $this->load->view('frontend/template/footer', $view_data);

    }

}
