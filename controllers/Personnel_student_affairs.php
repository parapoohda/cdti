<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personnel_student_affairs extends Personnel_Pdf_Core_Controller
{
    public function __construct()
    {
        parent::__construct('student_affairs','personnel_student_affairs','add_post');
    }
}