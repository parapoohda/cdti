<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manual_controller extends Pdf_Core_Controller
{
    public function __construct()
    {
        parent::__construct('manual','manual_controller','add_post');
    }
}