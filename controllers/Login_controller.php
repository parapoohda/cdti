<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_controller extends Home_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('bcrypt');
        $this->post_load_more_count = 6;
    }

    public function index()
    {
        if (auth_check()) {
            redirect(admin_url());
        }
        $data['title'] = $this->settings->home_title;
        $data['description'] = $this->settings->site_description;
        $data['keywords'] = $this->settings->keywords;
        $data['home_title'] = $this->settings->home_title;
        $data['visible_posts_count'] = $this->post_load_more_count;
        $data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        // $this->load->view('frontend/template/header', $data);
        $this->load->view('cdti/login',$data);
        // $this->load->view('frontend/template/footer', $data);
    }

    public function member()
    {
        if (auth_check()) {
            redirect('admin');
        }
        $data['title'] = $this->settings->home_title;
        $data['description'] = $this->settings->site_description;
        $data['keywords'] = $this->settings->keywords;
        $data['home_title'] = $this->settings->home_title;
        $data['visible_posts_count'] = $this->post_load_more_count;
        $data['load_css']  = array(
                                          'forms/dropify/dropify.css',
                                          'forms/toggle/switchery.min.css',
                                          'extensions/sweetalert.css',
                                        );
        $data['load_js']  = array(
                                          'forms/dropify/dropify.min.js',
                                          'forms/toggle/bootstrap-checkbox.min.js',
                                          'forms/toggle/switchery.min.js',
                                          'extensions/sweetalert.min.js',
                                          // 'forms/validation/jqBootstrapValidation.js',
                                        );

        // $this->load->view('frontend/template/header', $data);
        $this->load->view('cdti/login_member',$data);
        // $this->load->view('frontend/template/footer', $data);
    }
}
