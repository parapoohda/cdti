<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Industry_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        check_permission('settings');
    }


    public function industry_setting()
    {
        $data['title'] = trans("industry_setting");
        $data['categories'] = $this->industry_model->get_all_industry();
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/industry/industrys', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_industry_post(){
        $this->form_validation->set_rules('name', trans("mainindustry_name"), 'required|xss_clean|max_length[200]');
        $this->form_validation->set_rules('color', trans("mainindustry_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->industry_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->industry_model->add_industry()) {
                $last_id = $this->db->insert_id();
                $this->industry_model->update_slug($last_id);
                $this->session->set_flashdata('success_form', trans("category") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->industry_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

      public function update_industry($id)
    { ;
        $data['title'] = trans("update_mainindustry");
        //get category
        $data['category'] = $this->industry_model->get_industry($id);

        if (empty($data['category'])) {
            redirect($this->agent->referrer());
        }

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/industry/update_industry', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Category Post
     */
    public function update_industry_post()
    {
        $this->form_validation->set_rules('name', trans("mainindustry_name"), 'required|xss_clean|max_length[200]');
        $this->form_validation->set_rules('color', trans("mainindustry_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->industry_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //category id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);
            if ($this->industry_model->update_industry($id)) {

                //update slug
                $this->industry_model->update_slug($id);
                $this->session->set_flashdata('success', trans("mainindustry") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'industry-settings');
                }

            } else {
                $this->session->set_flashdata('form_data', $this->industry_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function delete_industry_post()
    {

        $id = $this->input->post('id', true);
        //check subcategories
        if (count($this->industry_model->get_subindustry_by_parent_id($id)) > 0) {
            $this->session->set_flashdata('error', trans("msg_delete_subcategories"));
        }

        if ($this->industry_model->delete_industry($id)) {
            $this->session->set_flashdata('success', trans("mainindustry") . " " . trans("msg_suc_deleted"));
            reset_cache_data_on_change();
        } else {
            $this->session->set_flashdata('error', trans("msg_error"));
        }
    }

    public function subcategories()
    {

        $data['title'] = trans("subindustry");
        $data['categories'] = $this->industry_model->get_all_subindustry();
        $data['top_categories'] = $this->industry_model->get_top_industry_by_lang($this->selected_lang->id);
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/industry/subindustry', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_subindustry_post()
    {

        $this->form_validation->set_rules('name', trans("subindustry"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->industry_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->industry_model->add_subindustry()) {
                $this->session->set_flashdata('success_form', trans("subindustry") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->industry_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_subindustry($id)
    {

        $data['title'] = trans("update_subindustry");
        //get category
        $data['category'] = $this->industry_model->get_industry($id);

        if (empty($data['category'])) {
            redirect($this->agent->referrer());
        }

        $data['top_categories'] = $this->industry_model->get_top_industry_by_lang($data['category']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/industry/update_subindustry', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Subcategory Post
     */
    public function update_subindustry_post()
    {

        $this->form_validation->set_rules('name', trans("subindustry"), 'required|xss_clean|max_length[200]');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->industry_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //category id
            $id = $this->input->post('id', true);
            if ($this->industry_model->update_subindustry($id)) {
                $this->session->set_flashdata('success', trans("subindustry") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                redirect(admin_url() . 'subindustry-settings');
            } else {
                $this->session->set_flashdata('form_data', $this->industry_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }



}
