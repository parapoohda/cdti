<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        check_permission('settings');
    }


    public function participants_setting()
    {
        $data['title'] = trans("participants_setting");
        $data['categories'] = $this->participants_model->get_all_participants();
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/participants/participants', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_participants_post(){
        $this->form_validation->set_rules('name', trans("participants_name"), 'required|xss_clean|max_length[200]');
        // $this->form_validation->set_rules('color', trans("participants_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->participants_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->participants_model->add_participants()) {
                $last_id = $this->db->insert_id();
                $this->participants_model->update_slug($last_id);
                $this->session->set_flashdata('success_form', trans("participants-settings") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->participants_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_participants($id)
    {
        $data['title'] = trans("update_participants");
        //get category
        $data['category'] = $this->participants_model->get_participants($id);

        if (empty($data['category'])) {
            redirect($this->agent->referrer());
        }

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/participants/update_participants', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Category Post
     */
    public function update_participants_post()
    {
        $this->form_validation->set_rules('name', trans("participants_name"), 'required|xss_clean|max_length[200]');
      // $this->form_validation->set_rules('color', trans("participants_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->participants_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //category id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);
            if ($this->participants_model->update_participants($id)) {

                //update slug
                $this->participants_model->update_slug($id);
                $this->session->set_flashdata('success', trans("participants") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'participants-settings');
                }

            } else {
                $this->session->set_flashdata('form_data', $this->participants_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function delete_participants_post()
    {
        $id = $this->input->post('id', true);
        if ($this->participants_model->delete_participants($id)) {
            $this->session->set_flashdata('success', trans("participants") . " " . trans("msg_suc_deleted"));
            reset_cache_data_on_change();
        } else {
            $this->session->set_flashdata('error', trans("msg_error"));
        }
    }
}
