<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announce_controller extends Admin_Core_Controller
{

    public function __construct()
    {
        parent::__construct();
        check_permission('settings');
    }


    public function announce_setting()
    {
        $data['title'] = trans("announce_setting");
        $data['categories'] = $this->announce_model->get_all_announce();
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/announce/announce', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_announce_post(){
        $this->form_validation->set_rules('name', trans("announce_name"), 'required|xss_clean|max_length[200]');
        // $this->form_validation->set_rules('color', trans("announce_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors_form', validation_errors());
            $this->session->set_flashdata('form_data', $this->announce_model->input_values());
            redirect($this->agent->referrer());
        } else {
            if ($this->announce_model->add_announce()) {
                $last_id = $this->db->insert_id();
                $this->announce_model->update_slug($last_id);
                $this->session->set_flashdata('success_form', trans("announce-settings") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->announce_model->input_values());
                $this->session->set_flashdata('error_form', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_announce($id)
    {
        $data['title'] = trans("update_announce");
        //get category
        $data['category'] = $this->announce_model->get_announce($id);

        if (empty($data['category'])) {
            redirect($this->agent->referrer());
        }

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/announce/update_announce', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Category Post
     */
    public function update_announce_post()
    {
        $this->form_validation->set_rules('name', trans("announce_name"), 'required|xss_clean|max_length[200]');
      // $this->form_validation->set_rules('color', trans("announce_color"), 'required|xss_clean|max_length[200]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->announce_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //category id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);
            if ($this->announce_model->update_announce($id)) {

                //update slug
                $this->announce_model->update_slug($id);
                $this->session->set_flashdata('success', trans("announce") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'announce-settings');
                }

            } else {
                $this->session->set_flashdata('form_data', $this->announce_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function delete_announce_post()
    {
        $id = $this->input->post('id', true);
        if ($this->announce_model->delete_announce($id)) {
            $this->session->set_flashdata('success', trans("announce") . " " . trans("msg_suc_deleted"));
            reset_cache_data_on_change();
        } else {
            $this->session->set_flashdata('error', trans("msg_error"));
        }
    }
}
