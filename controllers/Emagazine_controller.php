<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Emagazine_controller extends Pdf_Core_Controller
{
    private $type = 'emagazine';
    private $emagazinePath = '';
    private $update_title = '';
    private $add_title = '';
    public function __construct()
    {
        parent::__construct($this->type, 'emagazine_controller', 'add_post');
        $this->update_title = 'update_' . $this->type;
        $this->add_title = 'add_' . $this->type;
    }

    public function posts()
    {
        check_permission('add_manual');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . $this->type;
        $data['detail'] = admin_url() . $this->type;
        $data['list_type'] = $this->type;
        $data['title'] = trans($this->type);
        $data['type'] = $this->type;

        $data['top_categories'] = $this->category_model->get_top_categories();

        //get paginated posts
        $pagination = $this->paginate(admin_url() . $this->type, $this->post_pdf_model->get_paginated_posts_count($this->type, $this->type,0));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($this->type, $pagination['per_page'], $pagination['offset'], $data['list_type'],0);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/emagazine/posts', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function add_post()
    {
        $this->check_permission();
        $data['title'] = trans($this->type);
        $data['add_title'] = trans($this->add_title);
        $data['type']  = $this->type;
        $data['top_categories'] = $this->category_model->get_top_categories();
        $data['controller'] = 'Emagazine_controller';
        //dd($data);
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/emagazine/add_post', $data);
        $this->load->view('admin/includes/_footer');
    }

    // Delete Directory
    public function delete_directory($folderName)
    {
        $this->load->helper('file'); // Load codeigniter file helper

        $dir_path  = 'emagazine/' . $folderName;  // For check folder exists
        $del_path  = './emagazine/' . $folderName . '/'; // For Delete folder

        if (is_dir($dir_path)) {
            delete_files($del_path, true); // Delete files into the folder
            rmdir($del_path); // Delete the folder

            echo 'Deleted Directory';
            return true;
        }
        echo 'can not Delete Directory';
        return false;
    }

    public function do_upload()
    {
        $config['upload_path']          = './emagazine/';
        $config['allowed_types']        = 'zip|pdf';
        $config['max_size']             = 3504800;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('eMagazineFile')) {

            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $fileExt = $uploadData['file_ext'];
            // log_message('error',"test :: $fileExt");
            $this->emagazinePath = $filename;
            if ($fileExt == '.zip') {
                ## Extract the zip file ---- start
                $fileDir = "./emagazine/" . $filename;
                $zip = new ZipArchive;
                $res = $zip->open($fileDir);
                if ($res === TRUE) {

                    // Unzip path
                    $extractpath = "./emagazine/";

                    // Extract file
                    $zip->extractTo($extractpath);
                    $zip->close();

                    //delete file 
                    unlink($fileDir);


                    echo 'msg Upload & Extract successfully.';
                    return true;
                } else {
                    echo 'msg Failed to extract.';
                }
                ## ---- end
            }else{
                return true;
            }
        }
        return false;
    }


    public function add_post_post()
    {
        $this->check_permission();
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');
        $this->form_validation->set_rules('summary', trans("summary"), 'xss_clean|max_length[5000]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');
        $this->form_validation->set_rules('optional_url', trans("optional_url"), 'xss_clean|max_length[1000]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
            redirect($this->agent->referrer());
        } else {
            $post_type = $this->type;
            //unzip file
            if (!$this->do_upload()) {
                $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
                $this->session->set_flashdata('error', 'upload ผิดพลาด');
                redirect($this->agent->referrer());
            } else {
                //if post added
                if ($this->post_pdf_model->add_post($post_type)) {
                    //last id
                    $last_id = $this->db->insert_id();
                    //update slug
                    $this->post_pdf_model->update_slug($last_id);
                    //insert post tags
                    $this->tag_model->add_post_tags($last_id);
                    //insert file name
                    $this->post_file_model->add_post_files($last_id);

                    //send post
                    $send_post_to_subscribes = $this->input->post('send_post_to_subscribes', true);
                    if ($send_post_to_subscribes) {
                        $this->send_to_all_subscribers($last_id);
                    }
                    $this->session->set_flashdata('success', "นิตยสาร" . $this->emagazinePath . " " . trans("msg_suc_added"));
                    reset_cache_data_on_change();
                    redirect($this->agent->referrer());
                } else {
                    $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
                    $this->session->set_flashdata('error', trans("msg_error"));
                    redirect($this->agent->referrer());
                }
            }
        }
    }

    //update post    
    public function update_post($id)
    {

        $this->check_permission();
        $data['type'] = trans($this->type);
        $data['title'] = trans($this->update_title);
        $data['controller'] = "Emagazine_controller";
        //get post
        $data['post'] = $this->post_pdf_model->get_post($id);

        if (empty($data['post'])) {
            redirect($this->agent->referrer());
        }
        //combine post tags
        $tags = "";
        $count = 0;
        $tags_array = $this->tag_model->get_post_tags($id);
        foreach ($tags_array as $item) {
            if ($count > 0) {
                $tags .= ",";
            }
            $tags .= $item->tag;
            $count++;
        }

        $data['tags'] = $tags;
        $data['type'] = $data['post']->post_type;

        $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
        $data['categories'] = $this->category_model->get_top_categories_by_lang($data['post']->lang_id);
        $data['subcategories'] = $this->category_model->get_subcategories_by_parent_id($data['post']->category_id);
        $data['categories'] = $this->category_model->get_top_categories();

        $data['users'] = $this->auth_model->get_active_users();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/emagazine/update_post', $data);
        $this->load->view('admin/includes/_footer');
    }
}
