<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor_controller extends API_Controller
{
    
    public function index()
    {
    }

    
    public function get_test(){
        
        $milliseconds1 = round(microtime(true) * 1000);
        //echo $this->db->last_query();    
        echo '<link rel="shortcut icon" href="#" />';
        echo "test";
        $milliseconds2 = round(microtime(true) * 1000);

        echo $microsecond2-$microsecond1;
    }

    public function get_today(){
        $milliseconds1 = round(microtime(true) * 1000);

        echo '<link rel="shortcut icon" href="#" />';
        
        echo $this->visitor_model->get_today();
        echo "|";

        //echo $this->db->last_query();
        $milliseconds2 = round(microtime(true) * 1000);
        echo $microsecond2-$microsecond1;
    }
    
    public function get_yesterday(){
        $milliseconds1 = round(microtime(true) * 1000);

        echo '<link rel="shortcut icon" href="#" />';
        
        echo $this->visitor_model->get_yesterday();
        echo "|";

        //echo $this->db->last_query();
        $milliseconds2 = round(microtime(true) * 1000);
        echo $microsecond2-$microsecond1;

    }
    
    public function get_last_thirty_entries(){
        $milliseconds1 = round(microtime(true) * 1000);
        echo '<link rel="shortcut icon" href="#" />';
        echo $this->visitor_model->get_last_thirty_entries();
        echo "|";

        //echo $this->db->last_query();
        $milliseconds2 = round(microtime(true) * 1000);
        echo $microsecond2-$microsecond1;

    }
    
    public function get_total(){
        $milliseconds1 = round(microtime(true) * 1000);
        echo '<link rel="shortcut icon" href="#" />';
        
        echo $this->visitor_model->get_total();
        echo $this->db->last_query();
        echo "|";

        $milliseconds2 = round(microtime(true) * 1000);
        echo $microsecond2-$microsecond1;
    }
    public function get_visitor(){
        $this->load->model('visitor_model');
        //echo '<link rel="shortcut icon" href="#" />';

        //$this->benchmark->mark('today_start');
        $visitor['today'] = $this->visitor_model->get_today();
        //$this->benchmark->mark('yesterday_start');

        //$stringTime = "query today :"+($time_end - $time_start)+". ";
        $visitor['yesterday'] = $this->visitor_model->get_yesterday();
        //$this->benchmark->mark('lastMonth_start');

        $visitor['lastMonth'] = $this->visitor_model->get_last_thirty_entries();
        //$this->benchmark->mark('total_start');

        $visitor['total'] = $this->visitor_model->get_total();
        //$this->benchmark->mark('total_end');
        echo json_encode($visitor);
        

        
        /*
        echo $this->benchmark->elapsed_time('today_start', 'yesterday_start');
        echo " ";
        echo $this->benchmark->elapsed_time('yesterday_start', 'lastMonth_start');
        echo " ";

        echo $this->benchmark->elapsed_time('lastMonth_start', 'total_start');
        echo " ";

        echo $this->benchmark->elapsed_time('total_start', 'total_end');
        echo " ";
        $this->benchmark->mark('echo_end');
        
        echo "test";

        echo $this->benchmark->elapsed_time('total_end', 'echo_end');
    */
    }
    
    public function add_visitor(){
        $this->load->model('visitor_model');
        
        //echo $this->visitor_model->addCount() ;

        if($_SESSION["last_visit"] != date('Y-m-d')){
            
        //echo "22222234";
            $this->visitor_model->addCount();
            //echo "test1";

            $_SESSION["last_visit"] =date('Y-m-d');
        }
        echo "success";

    } 
}