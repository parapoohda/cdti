<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_controller extends Admin_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_permission('pages');
    }


    /**
     * Add Page
     */
    public function add_page()
    {
        $data['title'] = trans("add_page");
        $data['menu_links'] = $this->navigation_model->get_menu_links_for_admin();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/page/add', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Add Page Post
     */
    public function add_page_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->page_model->input_values());
            redirect($this->agent->referrer());
        } else {

            if (!$this->page_model->check_page_name()) {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_page_slug_error"));
                redirect($this->agent->referrer());
                exit();
            }

            if ($this->page_model->add()) {
                $this->session->set_flashdata('success', trans("page") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }


      /**
     * Add Page
     */
    public function add_page_fac()
    {
        $data['title'] = trans("add_page");
        $data['menu_links'] = $this->navigation_model->get_menu_links_for_admin();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/page/add_fac', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Add Page Post
     */
    public function add_page_post_fac()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->page_model->input_values());
            redirect($this->agent->referrer());
        } else {

            if (!$this->page_model->check_page_name()) {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_page_slug_error"));
                redirect($this->agent->referrer());
                exit();
            }

            if ($this->page_model->add_fac()) {
                $this->session->set_flashdata('success', trans("page") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    /**
     * Pages
     */
    public function pages()
    {
        
        $data['title'] = trans("pages");
        $data['pages'] = $this->page_model->get_all_pages();
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/page/pages', $data);
        $this->load->view('admin/includes/_footer');
    }
    public function pages_fac()
    {
        $data['title'] = trans("pages");
        $data['main'] = (isset($_GET['sub']))?false:true;

        $data['pages'] = $this->page_model->get_all_pages_fac($data['main']);
        $data['lang_search_column'] = 2;

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/page/pages_fac', $data);
        
        
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Page
     */
    public function update_page($id)
    {
        $data['title'] = trans("update_page");

        //find page
        $data['page'] = $this->page_model->get_page_by_id_new($id);
        // dd($data['page']);exit;
        $data['page']->sflag  = "";
        //page not found
        if (empty($data['page'])) {
            redirect($this->agent->referrer());
        }
        $data['menu_links'] = $this->navigation_model->get_menu_links_by_lang($data['page']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        if($data['page']->page_type == 'fac'){
            if($data['page']->main_category_id){
                $this->load->view('admin/page/update_fac_main', $data);
            }else{
                $this->load->view('admin/page/update_fac', $data);
            }
        }else{
           $this->load->view('admin/page/update', $data);
        }
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Update Page Tab
     */
    public function update_page_tab($id,$tab)
    {
        $data['title'] = trans("update_page");

        //find page
        $data['page'] = $this->page_model->get_page_by_id_new($id);
        
        //page not found
        if (empty($data['page'])) {
            redirect($this->agent->referrer());
        }
        $data['menu_links'] = $this->navigation_model->get_menu_links_by_lang($data['page']->lang_id);

        $this->load->view('admin/includes/_header', $data);
        if($data['page']->page_type == 'fac'){
            
            $data['page']->sflag = $tab;
            if($tab == "intro"){
                $data['page']->flag = 'branch_description';
                $data['page']->content = $data['page']->branch_description;
            }else if($tab == "highlight"){
                $data['page']->flag = 'branch_highlight';
                $data['page']->content = $data['page']->branch_highlight;
            }else if($tab == "qualification"){
                $data['page']->flag = 'branch_qualification';
                $data['page']->content = $data['page']->branch_qualification;
            }else if($tab == "structure"){
                $data['page']->flag = 'branch_structure';
                $data['page']->content = $data['page']->branch_structure;
            }else if($tab == "study"){
                $data['page']->flag = 'branch_study';
                
                $data['page']->content = $data['page']->branch_study;
            }else if($tab == "jobs"){
                $data['page']->flag = 'branch_jobs';
                $data['page']->content = $data['page']->branch_jobs;
            }else if($tab == "about"){
                $data['page']->flag = 'page_content';
                $data['page']->content = $data['page']->page_content;
            }
            
            // $this->load->view('admin/page/update_fac_intro', $data);
            if($tab == "about"){
                $this->load->view('admin/page/update_fac_about', $data);
            }else{
                $this->load->view('admin/page/update_fac_intro', $data);
            }
            
           
        }
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Page Post
     */
    public function update_page_post()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->page_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //get id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);

            if (!$this->page_model->check_page_name()) {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_page_slug_error"));
                redirect($this->agent->referrer());
                exit();
            }
            
            if ($this->page_model->update($id)) {
                $this->session->set_flashdata('success', trans("page") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'pages');
                }
            } else {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_page_brach()
    {
        //validate inputs
        // echo 1;exit;
        // $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');
        $id = $this->input->post('id', true);
        $field = $this->input->post('field', true);
        
        if ($id) {
            //get id
            
            $redirect_url = $this->input->post('redirect_url', true);
            
            if (!$this->page_model->check_page_name()) {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_page_slug_error"));
                redirect($this->agent->referrer());
                exit();
            }

            if ($this->page_model->update_fac_branch($id,$field)) {

                $this->page_file_model->add_page_additional_images($id,$field);

                $this->session->set_flashdata('success', trans("page") . " " . trans("msg_suc_updated"));
                // reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'pages-fac');
                }
            } else {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }

    public function update_page_post_fac()
    {
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->page_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //get id
            $id = $this->input->post('id', true);
            $redirect_url = $this->input->post('redirect_url', true);

            if (!$this->page_model->check_page_name()) {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_page_slug_error"));
                redirect($this->agent->referrer());
                exit();
            }

            if ($this->page_model->update_fac($id)) {

                $this->page_file_model->add_page_additional_images($id);

                $this->session->set_flashdata('success', trans("page") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
                if (!empty($redirect_url)) {
                    redirect($redirect_url);
                } else {
                    redirect(admin_url() . 'pages-fac');
                }
            } else {
                $this->session->set_flashdata('form_data', $this->page_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }


    /**
     * Delete Page Post
     */
    public function delete_page_post()
    {

        $id = $this->input->post('id', true);

        $page = $this->page_model->get_page_by_id_new($id);

        if (empty($page)) {
            redirect($this->agent->referrer());
        }

        //check if page custom or not
        if ($page->is_custom == 0) {
            $lang = $this->language_model->get_language($page->lang_id);
            if (!empty($lang)) {
                $this->session->set_flashdata('error', trans("msg_page_delete"));
            }
        } else {
            if ($this->page_model->delete($id)) {
                $this->session->set_flashdata('success', trans("page") . " " . trans("msg_suc_deleted"));
                reset_cache_data_on_change();
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }
    }

      /**
     * Delete Additional Image
     */
    public function delete_page_additional_image()
    {
        $file_id = $this->input->post('file_id', true);
        $this->page_file_model->delete_page_additional_image($file_id);
    }

    /**
     * Delete Post Main Image
     */
    public function delete_page_main_image()
    {
        $page_id = $this->input->post('page_id', true);
        $this->page_file_model->delete_page_main_image($page_id);
    }


}
