<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personnel_professional_academic extends Personnel_Pdf_Core_Controller
{
    public function __construct()
    {
        parent::__construct('professional_academic','personnel_professional_academic','add_post');
    }
}