<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_controller extends Home_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->post_load_more_count = 6;
    }
    
    public function index()
    {

        $this->load->view('en/cdti/layout/header', $data);
        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
    //bachelor degree
    public function business_administration()
    {
        
        $data['link_to_own_page'] = "English/Business_admin";
        $data['title'] = "Bachelor of Business Adminstration";
        $this->load->view('en/cdti/layout/header', $data);
        $this->load->view('en/cdti/default_page/business_admin', $data);

        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
    
    public function techology()
    {
        
        $data['link_to_own_page'] = "English/Industry_tech";
        $data['title'] = "Technology";
        //$data['page'] = $this->page_model->get_default_page($slug, 1);
        
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/orenge.css';
        
        $this->load->view('en/cdti/layout/header', $data);
        $this->load->view('en/cdti/default_page/industry_tech', $data);

        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
    
    
    public function digital_tech()
    {
        
        $data['link_to_own_page'] = "English/Digital_tech";
        $data['title'] = "Digital Technology";
        //$data['page'] = $this->page_model->get_default_page($slug, 1);
        
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/digital.css';
        
        $this->load->view('en/cdti/layout/header', $data);
        $this->load->view('en/cdti/default_page/digital_tech', $data);

        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
    
    
    
    public function high_vocational()
    {
        
        $data['link_to_own_page'] = "English/high-vocational";
        $data['title'] = "High Vocational School";
        //$data['page'] = $this->page_model->get_default_page($slug, 1);
        
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/purple.css';
        
        $this->load->view('en/cdti/layout/header', $data);
        $this->load->view('en/cdti/default_page/high_vocational', $data);

        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
    
    public function vocational()
    {
        
        $data['link_to_own_page'] = "English/vocational";
        $data['title'] = " Vocational School";
        //$data['page'] = $this->page_model->get_default_page($slug, 1);
        
        $data['css_theme'] = base_url() . '/cdti_assets/css/campus/yellow.css';
        
        $this->load->view('en/cdti/layout/header', $data);
        $this->load->view('en/cdti/default_page/vocational', $data);

        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
}