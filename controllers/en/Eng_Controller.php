<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eng_controller extends Home_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->post_load_more_count = 6;
    }
    
    public function index()
    {
        $data['title'] = 'หน้าแรก';
        $data['description'] = $this->settings->site_description;
        $data['keywords'] = $this->settings->keywords;
        $data['home_title'] = $this->settings->home_title;
        $data['visible_posts_count'] = $this->post_load_more_count;

        $data['load_css'] = array(
            'forms/dropify/dropify.css',
            'forms/toggle/switchery.min.css',
            'extensions/sweetalert.css',
        );
        $data['load_js'] = array(
            'forms/dropify/dropify.min.js',
            'forms/toggle/bootstrap-checkbox.min.js',
            'forms/toggle/switchery.min.js',
            'extensions/sweetalert.min.js',
            // 'forms/validation/jqBootstrapValidation.js',
        );
        $data['post_events'] = $this->postevents_admin_model->get_events(10);

        $data['royal_speech'] = $this->page_model->get_default_page('royal_speech', $this->selected_lang->id);

        $data['activity_posts'] = get_cached_data('activity_posts');
        if (empty($data['activity_posts'])) {
            $category = $this->category_model->get_category(14);
            $data['activity_posts'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 6);
            set_cache_data('activity_posts', $data['activity_posts']);
        }

        $data['activity_posts_2'] = get_cached_data('activity_posts_2');
        if (empty($data['activity_posts_2'])) {
            $category = $this->category_model->get_category(39);
            $data['activity_posts_2'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_2', $data['activity_posts_2']);
        }

        $data['activity_posts_3'] = get_cached_data('activity_posts_3');
        if (empty($data['activity_posts_3'])) {
            $category = $this->category_model->get_category(40);

            //echo json_encode($category->name_slug) ;

            $data['activity_posts_3'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);

            //echo json_encode($this->db->last_query()) ;
            //exit;

            set_cache_data('activity_posts_3', $data['activity_posts_3']);
        }

        $data['activity_posts_4'] = get_cached_data('activity_posts_4');
        if (empty($data['activity_posts_4'])) {
            $category = $this->category_model->get_category(41);

            $data['activity_posts_4'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_4', $data['activity_posts_4']);
        }

        $data['activity_posts_5'] = get_cached_data('activity_posts_5');
        if (empty($data['activity_posts_5'])) {
            $category = $this->category_model->get_category(42);

            $data['activity_posts_5'] = $this->post_model->get_post_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_5', $data['activity_posts_5']);
        }

        if (empty($data['activity_posts_6'])) {
            $category = $this->category_model->get_category(43);


            $data['activity_posts_6'] = $this->post_model->get_emagazine_by_tag_limit($category->name_slug, 3);
            set_cache_data('activity_posts_6', $data['activity_posts_6']);
        }

        $this->load->view('cdti/layout/header', $data);
        $this->load->view('en/cdti/index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
}