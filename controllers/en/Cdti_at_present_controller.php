<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdti_at_present_controller extends Home_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->post_load_more_count = 6;
    }
    
    public function index()
    {
        
        $data['link_to_own_page'] = "English/CDTI_at_present";
        $data['title'] = "CDTI at Present";
        $this->load->view('en/cdti/layout/header', $data);
        
        //$this->load->view('cdti/layout/fac_topbar',['page' => $data['page'] , 'route' => '' ]);
        $this->load->view('en/cdti/default_page/cdti_at_present', $data);
        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
    
    public function history()
    {
        

        $data['link_to_own_page'] = "English/History";
        $data['title'] = "History";
        $this->load->view('en/cdti/layout/header', $data);
        $this->load->view('en/cdti/default_page/history', $data);

        //$this->load->view('index', $data);
        $this->load->view('en/cdti/layout/footer', $data);
    }
}