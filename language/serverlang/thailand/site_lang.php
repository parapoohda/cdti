<?php defined("BASEPATH") OR exit("No direct script access allowed");

$lang["enroll"] = "สมัครเรียน";
$lang["enroll_close"] = "ปิดรับสมัคร";
$lang["gallery_maincategories"] = "หมวดหมู่แกลเลอรี่หลัก";
$lang["position"] = "ตำแหน่ง";
$lang["fac_category"] = "หมวดหมู่ระดับ";
$lang["fac_subcategory"] = "หมวดหมู่คณะ";
$lang["add"] = "เพิ่ม";
$lang["edit"] = "แก้ไข";
$lang["details"] = "รายละเอียด";
$lang["fac_reward"]  = "ผลงานและรางวัล (คณะสาขา)";
$lang["fac_gallery"] = "บรรยากาศการเรียน (คณะสาขา)";
$lang["fac_bursary"] = "ทุนการศึกษา";


$lang["Jan"] = "ม.ค.";
$lang["Feb"] = "ก.พ.";
$lang["Mar"] = "มี.ค.";
$lang["Apr"] = "เม.ย.";
$lang["May"] = "พ.ค.";
$lang["Jun"] = "มิ.ย.";
$lang["Jul"] = "ก.ค.";
$lang["Aug"] = "ส.ค.";
$lang["Sep"] = "ก.ย.";
$lang["Oct"] = "ต.ค.";
$lang["Nov"] = "พ.ย.";
$lang["Dec"] = "ธ.ค.";
$lang["level"] = "ชั้น";



$lang["add_participants"] = "สร้างคุณสมบัติผู้เข้าร่วมโครงการ";
$lang["participants_name"] = "ชื่อคุณสมบัติผู้เข้าร่วมโครงการ";
$lang["participants_detail"] = "รายละเอียดคุณสมบัติผู้เข้าร่วมโครงการ";
$lang["participants"] = "คุณสมบัติผู้เข้าร่วมโครงการ";

$lang["add_announce"] = "สร้างประกาศ";
$lang["announce_date"] = "วันที่ประกาศ";
$lang["announce_name"] = "ชื่อประกาศ";
$lang["announce_detail"] = "รายละเอียดประกาศ";
$lang["announce"] = "ประกาศ(ตัววิ่ง)";

$lang["select_pdf"] = "เลือกเอกสาร";
$lang["file"] = "เอกสาร";
$lang["add_file"] = "เพิ่มเอกสาร";
$lang["additional_file"] = "เอกสารประกอบ";
$lang["industry"] = "สาขาอุตสาหกรรม";
$lang["mainindustry"] = "สาขาอุตสาหกรรมหลัก";
$lang["subindustry"] = "สาขาอุตสาหกรรมย่อย";
$lang["industry_setting"] = "ตั้งค่าสาขาอุตสาหกรรมหลัก";
$lang["subindustry_setting"] = "ตั้งค่าสาขาอุตสาหกรรมย่อย";
$lang["add_mainindustry"] = "เพิ่มสาขาอุตสาหกรรมหลัก";
$lang["add_subindustry"] = "เพิ่มสาขาอุตสาหกรรมย่อย";
$lang["update_mainindustry"] = "แก้ไขสาขาอุตสาหกรรมหลัก";
$lang["update_subindustry"] = "แก้ไขสาขาอุตสาหกรรมย่อย";
$lang["mainindustry_name"] = "ชื่อสาขาอุตสาหกรรมหลัก";
$lang["mainindustry_color"] = "สีสาขาอุตสาหกรรมหลัก";
$lang["active_on_menu"] = "สถานะเปิด/ปิด";
$lang["confirm_mainindustry"] = "ยืนยันลบ สาขาอุตสาหกรรมหลัก";
$lang["confirm_subindustry"] = "ยืนยันลบ สาขาอุตสาหกรรมย่อย";
$lang["register_date"] = "วันลงทะเบียน";
$lang["proposal_date"] = "วันยื่นโครงการ";
$lang["isp"] = "ISP";
$lang["sme"] = "SME/STARTUP";
$lang["type_faq"] = "ประเภทของ";
$lang["faq"] = "FAQ";
$lang["faq_setting"] = "ตั้งค่า FAQ";
$lang["add_faq"] = "จัดการ FAQ";
$lang["faq_name"] = "คำถาม";
$lang["faq_detail"] = "คำตอบ";
$lang["faq_color"] = "สี FAQ";
$lang["confirm_faq"] = "ยืนยันลบ FAQ";
$lang["update_faq"] = "แก้ไข FAQ";
$lang["teams"] = "บุคลากร";
$lang["add_teams"] = "การจัดการบุคลากร";
$lang["update_teams"] = "แก้ไขบุคลากร";
$lang["banners"] = "แบนเนอร์";
$lang["add_banners"] = "จัดการแบนเนอร์";
$lang["update_banners"] = "แก้ไขแบนเนอร์";

$lang["landingpages"] = "Landing Pages";
$lang["add_landingpages"] = "จัดการ Landing Pages";
$lang["update_landingpages"] = "แก้ไข Landing Pages";

$lang["home_links"] = "ฝึกปฏิบัติงานกับสถานประกอบการชั้นนำ";
$lang["links"] = "สถานประกอบการชั้นนำ";
$lang["add_links"] = "จัดการสถานประกอบการชั้นนำ";
$lang["update_links"] = "แก้ไขสถานประกอบการชั้นนำ";
$lang["type_howto"] = "ประเภทของ";
$lang["howto"] = "เงื่อนไขการขอทุน";
$lang["howto_setting"] = "ตั้งค่า ขอทุนต้องทำอย่างไร";
$lang["add_howto"] = "จัดการเงื่อนไขการขอทุน";
$lang["howto_name"] = "หัวข้อ";
$lang["howto_detail"] = "รายละเอียด";
$lang["howto_color"] = "สี ขอทุนต้องทำอย่างไร";
$lang["confirm_howto"] = "ยืนยันลบ ขอทุนต้องทำอย่างไร";
$lang["update_howto"] = "แก้ไข ขอทุนต้องทำอย่างไร";
$lang["all"] = "ทั้งหมด";
$lang["all_posts"] = "ข่าวทั้งหมด";
$lang["add_page"] = "เพิ่มหน้าเพจ";
$lang["add_post"] = "เพิ่มเนื้อหา";
$lang["add_post_events"] = "เพิ่มปฏิทินกิจกรรม";
$lang["add_video"] = "เพิ่มวีดีทัศน์ ";
$lang["add_widget"] = "เพิ่ม วิดเจ็ต";
$lang["add_poll"] = "เพิ่มแบบสำรวจ";
$lang["add_category"] = "เพิ่ม";
$lang["add_subcategory"] = "เพิ่ม หมวดหมูย่อย";
$lang["add_audio"] = "เพิ่มเสียง";
$lang["audio_file"] = "ไฟล์เสียง";
$lang["add_link"] = "เพิ่มลิงค์เมนู";
$lang["add_oit"] = "เพิ่มข้อมูลสาธารณะ";
$lang["update_oit"] = "แก้ไขข้อมูลสาธารณะ";
$lang["oit"] = "ข้อมูลสาธารณะ";
$lang["add_language"] = "เพิ่มภาษา";
$lang["add_image"] = "เพิ่ม";
$lang["add_reading_list"] = "Add to Reading List";
$lang["ad_spaces"] = "พื้นที่โฆษณา";
$lang["admin"] = "ผู้ดูแลระบบ";
$lang["admin_panel"] = "แผงควบคุม";
$lang["about_me"] = "About Me";
$lang["audio"] = "เสียง";
$lang["audio_name"] = "ชื่อไฟล์เสียง";
$lang["audios"] = "เสียง";
$lang["auto_update"] = "อัปเดตอัตโนมัติ";
$lang["all_users_can_vote"] = "ผู้ใช้ทุกคนสามารถลงคะแนน";
$lang["address"] = "ที่อยู่";
$lang["app_name"] = "Application Name";
$lang["app_id"] = "App ID";
$lang["app_secret"] = "App Secret";
$lang["avatar"] = "Avatar";
$lang["add_featured"] = "แสดงในโพสแนะนำ";
$lang["add_breaking"] = "Add to Breaking";
$lang["add_slider"] = "Add to Slider";
$lang["add_recommended"] = "Add to Recommended";
$lang["active"] = "เปิดการทำงาน";
$lang["additional_images"] = "รูปภาพเพิ่มเติม";
$lang["approve"] = "Approve";
$lang["add_image_url"] = "เพิ่ม Url รูปภาพ";
$lang["back"] = "Back";
$lang["breadcrumb_home"] = "Home";
$lang["breadcrumb_videos"] = "Videos";
$lang["btn_send"] = "Send";
$lang["btn_submit"] = "Submit";
$lang["btn_reply"] = "Reply";
$lang["btn_delete"] = "ลบ";
$lang["btn_like"] = "Like";
$lang["btn_goto_home"] = "Go Back to the Homepage";
$lang["breaking_news"] = "Breaking News";
$lang["banned"] = "Banned";
$lang["ban_user"] = "Ban User";
$lang["block_color"] = "สีของส่วนหัวและบล็อกส่วนหัว";
$lang["breaking"] = "Breaking";
$lang["category"] = "หมวดหมู่ข่าว";
$lang["change_avatar"] = "Change Avatar";
$lang["change_password"] = "Change Password";
$lang["create_account"] = "Create an Account";
$lang["comment"] = "คิดเห็น";
$lang["comments"] = "คิดเห็น";
$lang["custom"] = "กำหนดเอง";
$lang["close"] = "Close";
$lang["contact_messages"] = "ข้อความติดต่อ";
$lang["categories"] = "หมวดหมู่";
$lang["category_name"] = "ชื่อหมวดหมู่";
$lang["category_block_style"] = "รูปแบบหมวดหมู่";
$lang["configurations"] = "Configurations";
$lang["color"] = "Color";
$lang["client_id"] = "Client ID";
$lang["client_secret"] = "Client Secret";
$lang["contact"] = "Contact";
$lang["contact_text"] = "ข้อความติดต่อ";
$lang["comment_system"] = "Comment System";
$lang["change_favicon"] = "Change favicon";
$lang["change_user_role"] = "Change User Role";
$lang["change_logo"] = "Change logo";
$lang["copyright"] = "Copyright";
$lang["contact_settings"] = "การตั้งค่าการติดต่อ";
$lang["content"] = "เนื้อหา";
$lang["confirm_category"] = "คุณแน่ใจหรือว่าต้องการลบหมวดหมู่นี้";
$lang["confirm_image"] = "คุณแน่ใจหรือว่าต้องการลบภาพนี้?";
$lang["confirm_page"] = "คุณแน่ใจหรือว่าต้องการลบหน้านี้?";
$lang["confirm_poll"] = "คุณแน่ใจหรือว่าต้องการลบแบบสำรวจนี้";
$lang["confirm_post"] = "คุณแน่ใจหรือว่าต้องการลบข่าวนี้?";
$lang["confirm_widget"] = "คุณแน่ใจหรือว่าต้องการลบวิดเจ็ตนี้?";
$lang["confirm_comment"] = "คุณแน่ใจหรือว่าต้องการลบความคิดเห็นนี้?";
$lang["confirm_message"] = "คุณแน่ใจหรือว่าต้องการลบข้อความนี้?";
$lang["confirm_email"] = "คุณแน่ใจหรือว่าต้องการลบอีเมลนี้?";
$lang["confirm_user"] = "คุณแน่ใจหรือว่าต้องการลบผู้ใช้รายนี้?";
$lang["confirm_ban"] = "คุณแน่ใจหรือไม่ว่าต้องการแบนผู้ใช้นี้?";
$lang["confirm_remove_ban"] = "คุณแน่ใจหรือไม่ว่าต้องการลบการห้ามสำหรับผู้ใช้รายนี้?";
$lang["confirm_link"] = "คุณแน่ใจหรือว่าต้องการลบลิงค์นี้?";
$lang["confirm_rss"] = "คุณแน่ใจหรือว่าต้องการลบฟีดนี้?";
$lang["confirm_language"] = "คุณแน่ใจหรือว่าต้องการลบภาษานี้?";
$lang["confirm_posts"] = "คุณแน่ใจหรือว่าต้องการลบข่าวที่เลือก?";
$lang["confirm_comments"] = "คุณแน่ใจหรือว่าต้องการลบความคิดเห็นที่เลือก?";
$lang["cookies_warning"] = "Cookies Warning";
$lang["date"] = "วันที่";
$lang["date_added"] = "วันที่สร้าง";
$lang["delete"] = "ลบ";
$lang["delete_reading_list"] = "Remove from Reading List";
$lang["default"] = "ค่าเริ่มต้น";
$lang["disable"] = "ปิดการทำงาน";
$lang["days_remaining"] = "วันเวลาที่เหลือ";
$lang["date_publish"] = "Date Published";
$lang["download_button"] = "ปุ่มดาวน์โหลด";
$lang["drafts"] = "ข่าวที่เป็นแบบร่าง";
$lang["description"] = "คำอธิบาย";
$lang["default_language"] = "ภาษาเริ่มต้น";
$lang["dont_add_menu"] = "ไม่เพิ่มในเมนู";
$lang["email"] = "อีเมล";
$lang["email_reset_password"] = "Your password has been successfully reset! Your new password:";
$lang["edit"] = "แก้ไข";
$lang["enable"] = "เปิดการทำงาน";
$lang["email_settings"] = "การตั้งค่าอีเมล";
$lang["edit_phrases"] = "Edit Phrases";
$lang["example"] = "Example";
$lang["featured_posts"] = "โพสต์แนะนำ";
$lang["facebook_comments"] = "ความเห็นของ Facebook";
$lang["form_username"] = "Username";
$lang["form_email"] = "Email";
$lang["form_password"] = "รหัสผ่าน";
$lang["forgot_password"] = "Forgot Password?";
$lang["form_old_password"] = "Old Password";
$lang["form_confirm_password"] = "Confirm Password";
$lang["footer_random_posts"] = "Random Posts";
$lang["footer_follow"] = "Social Media";
$lang["footer_newsletter"] = "Subscribe here to get interesting stuff and updates!";
$lang["font_options"] = "ตัวเลือกแบบอักษร";
$lang["fonts"] = "Fonts";
$lang["feed_name"] = "Feed Name";
$lang["feed_url"] = "Feed URL";
$lang["filter"] = "กรอง";
$lang["file_manager"] = "การจัดการไฟล์";
$lang["feed"] = "Feed";
$lang["folder_name"] = "Folder Name";
$lang["footer_about_section"] = "Footer About Section";
$lang["facebook_comments_code"] = "Facebook Comments Plugin Code";
$lang["featured"] = "Featured";
$lang["featured_order"] = "Featured Order";
$lang["favicon"] = "Favicon";
$lang["footer"] = "เมนูด้านล่าง";
$lang["gallery"] = "แกลอรี่";
$lang["gallery_categories"] = "หมวดหมู่แกลเลอรี่";
$lang["general_settings"] = "การตั้งค่าทั่วไป";
$lang["gmail_smtp"] = "Gmail SMTP";
$lang["google_analytics"] = "Google Analytics";
$lang["google_analytics_code"] = "Google Analytics Code";
$lang["google_recaptcha"] = "Google reCAPTCHA";
$lang["site_key"] = "Site Key";
$lang["secret_key"] = "Secret Key";
$lang["get_video_from_url"] = "เลือกวีดีทัศน์  จาก Url";
$lang["get_video"] = "ตกลง";
$lang["generate"] = "Generate";
$lang["hide"] = "ซ่อน";
$lang["home"] = "หน้าหลัก";
$lang["home_title"] = "ชื่อโฮมเพจ";
$lang["head_code"] = "HTML Head Code";
$lang["id"] = "รหัส";
$lang["images"] = "รูปภาพ";
$lang["image_description"] = "คำอธิบายรูปภาพ";
$lang["index"] = "Index";
$lang["import_rss_feed"] = "Import RSS Feed";
$lang["invalid_url"] = "Invalid URL!";
$lang["image"] = "รูปภาพ";
$lang["inactive"] = "ปิดการทำงาน";
$lang["image_for_video"] = "รูปภาพสำหรับวิดีโอ";
$lang["keywords"] = "คำค้นหา";
$lang["last_comments"] = "ความคิดเห็นล่าสุด";
$lang["last_comments_exp"] = "คุณสามารถดูความคิดเห็นล่าสุดได้ที่นี่";
$lang["last_contact_messages"] = "ข้อความติดต่อล่าสุด";
$lang["last_contact_messages_exp"] = "คุณสามารถดูข้อความติดต่อล่าสุดได้ที่นี่";
$lang["latest_users"] = "ผู้ใช้งานล่าสุด";
$lang["latest_users_exp"] = "คุณสามารถดูผู้ใช้ที่ลงทะเบียนล่าสุดได้ที่นี่";
$lang["latest_posts"] = "Latest Posts";
$lang["label"] = "Label";
$lang["label_video"] = "วีดีทัศน์ ";
$lang["leave_message"] = "Send a Message";
$lang["load_more"] = "Load More";
$lang["view_more"] = "ดูเพิ่มเติม";
$lang["login"] = "Login";
$lang["logout"] = "Logout";
$lang["login_with_social"] = "Login with social account";
$lang["login_error"] = "Wrong username or password!";
$lang["leave_reply"] = "Leave a Reply";
$lang["language"] = "ภาษา";
$lang["languages"] = "ภาษา";
$lang["language_settings"] = "ตั้งค่าภาษา";
$lang["language_name"] = "ชื่อภาษา";
$lang["language_code"] = "รหัสภาษา";
$lang["logo"] = "Logo";
$lang["logo_footer"] = "Logo Footer";
$lang["location"] = "ตำแหน่งที่แสดงผล";
$lang["link"] = "ลิงค์";
$lang["left_to_right"] = "จากซ้ายไปขวา";
$lang["main_navigation"] = "MAIN NAVIGATION";
$lang["message"] = "ข้อความ";
$lang["message_email_unique_error"] = "The email has already been taken.";
$lang["message_newsletter_success"] = "Your email address has been successfully added!";
$lang["message_newsletter_error"] = "Your email address is already registered!";
$lang["message_contact_success"] = "Your message has been successfully sent!";
$lang["message_contact_error"] = "There was a problem sending your message!";
$lang["message_register_error"] = "There was a problem during registration. Please try again!";
$lang["message_profile_success"] = "Your profile has been successfully updated!";
$lang["message_comment_delete"] = "Are you sure you want to delete this comment?";
$lang["message_invalid_email"] = "Invalid email address!";
$lang["message_page_auth"] = "You must be logged in to view this page!";
$lang["message_post_auth"] = "You must be logged in to view this post!";
$lang["message_change_password_success"] = "Your password has been successfully changed!";
$lang["message_change_password_error"] = "There was a problem changing your password!";
$lang["message_ban_error"] = "Your account has been banned!";
$lang["msg_suc_added"] = "successfully added!";
$lang["msg_suc_updated"] = "successfully updated!";
$lang["msg_suc_deleted"] = "successfully deleted!";
$lang["msg_error"] = "An error occurred please try again!";
$lang["msg_slug_used"] = "The slug you entered is being used by another user!";
$lang["msg_email_sent"] = "Email successfully sent!";
$lang["msg_role_changed"] = "User role successfully changed!";
$lang["msg_user_banned"] = "User successfully banned!";
$lang["msg_ban_removed"] = "User ban successfully removed!";
$lang["msg_delete_subcategories"] = "Please delete subcategories belonging to this category first!";
$lang["msg_delete_posts"] = "Please delete posts belonging to this category first!";
$lang["msg_delete_images"] = "Please delete images belonging to this category first!";
$lang["msg_add_slider"] = "Post added to slider!";
$lang["msg_remove_slider"] = "Post removed from slider!";
$lang["msg_add_featured"] = "Post added to featured posts!";
$lang["msg_remove_featured"] = "Post removed from featured posts!";
$lang["msg_add_breaking"] = "Post added to breaking news!";
$lang["msg_remove_breaking"] = "Post removed from breaking news!";
$lang["msg_add_recommended"] = "Post added to recommended posts!";
$lang["msg_remove_recommended"] = "Post removed from recommended posts!";
$lang["msg_post_approved"] = "Post approved!";
$lang["msg_page_delete"] = "Default pages cannot be deleted!";
$lang["msg_language_delete"] = "Default language cannot be deleted!";
$lang["msg_widget_delete"] = "Default widgets cannot be deleted!";
$lang["msg_published"] = "Post successfully published!";
$lang["msg_recaptcha"] = "Please confirm that you are not a robot!";
$lang["msg_cron_sitemap"] = "With this URL you can automatically update your sitemap.";
$lang["msg_cron_feed"] = "With this URL you can automatically update your feeds.";
$lang["msg_page_slug_error"] = "Invalid page slug!";
$lang["msg_lang_shortform_error"] = "The language short form is used by a page. Please edit or delete the page from the pages section!";
$lang["multilingual_system"] = "Multilingual System";
$lang["more_info"] = "More info";
$lang["meta_tag"] = "Meta Tag";
$lang["mail_protocol"] = "Mail Protocol";
$lang["mail"] = "Mail";
$lang["mail_title"] = "Mail Title";
$lang["mail_host"] = "Mail Host";
$lang["mail_port"] = "Mail Port";
$lang["mail_username"] = "อีเมลชื่อผู้ใช้";
$lang["mail_password"] = "รหัสผ่านจดหมาย";
$lang["musician"] = "ชื่อนักดนตรี";
$lang["menu_limit"] = "จำกัดเมนู ";
$lang["main_menu"] = "เมนูหลัก";
$lang["main_image"] = "รูปหลัก";
$lang["name"] = "ชื่อ";
$lang["nav_home"] = "Home";
$lang["nav_login"] = "Login";
$lang["nav_register"] = "Register";
$lang["nav_reset_password"] = "Reset Password";
$lang["next_article"] = "Next Article";
$lang["next_video"] = "Next Video";
$lang["no"] = "ไม่";
$lang["newsletter"] = "จดหมายข่าว";
$lang["navigation"] = "จัดการเมนู";
$lang["number_of_posts_import"] = "จำนวนข่าวที่จะนำเข้า";
$lang["or"] = "or";
$lang["options"] = "ตัวเลือก";
$lang["online"] = "online";
$lang["option_1"] = "ตัวเลือก 1";
$lang["option_2"] = "ตัวเลือก 2";
$lang["option_3"] = "ตัวเลือก 3";
$lang["option_4"] = "ตัวเลือก 4";
$lang["option_5"] = "ตัวเลือก 5";
$lang["option_6"] = "ตัวเลือก 6";
$lang["option_7"] = "ตัวเลือก 7";
$lang["option_8"] = "ตัวเลือก 8";
$lang["option_9"] = "ตัวเลือก 9";
$lang["option_10"] = "ตัวเลือก 10";
$lang["optional"] = "Optional";
$lang["order"] = "ลำดับ";
$lang["order_1"] = "ลำดับ";
$lang["optional_url"] = "URL ตัวเลือก";
$lang["optional_url_name"] = "Post Optional Url Button Name";
$lang["only_registered"] = "Only Registered";
$lang["page"] = "หน้าเพจ";
$lang["pages"] = "หน้าเพจ";
$lang["page_type"] = "ประเภทหน้าเพจ";
$lang["page_not_found"] = "Page not found";
$lang["page_not_found_sub"] = "The page you are looking for doesn't exist.";
$lang["panel"] = "Panel";
$lang["previous_article"] = "Previous Article";
$lang["previous_video"] = "Previous Video";
$lang["placeholder_username"] = "Username";
$lang["placeholder_password"] = "Password";
$lang["placeholder_search"] = "Search...";
$lang["placeholder_email"] = "Email Address";
$lang["placeholder_message"] = "Message";
$lang["placeholder_comment"] = "Comment...";
$lang["placeholder_old_password"] = "Old Password";
$lang["placeholder_confirm_password"] = "Confirm Password";
$lang["placeholder_about_me"] = "About Me";
$lang["post"] = "ข่าว";
$lang["posts"] = "จัดการข่าว";
$lang["post_events"] = "ปฏิทินกิจกรรม";
$lang["pending_posts"] = "ข่าวที่รอการอนุมัติ";
$lang["post_tags"] = "Tags:";
$lang["popular_posts"] = "Popular Posts";
$lang["polls"] = "แบบสำรวจ";
$lang["poll"] = "แบบสำรวจ";
$lang["profile"] = "Profile";
$lang["preferences"] = "Preferences";
$lang["parent_category"] = "Parent Category";
$lang["publish"] = "รูปแบบข่าว";
$lang["post_type"] = "ประเภทข่าว";
$lang["parent_link"] = "Parent Link";
$lang["post_details"] = "รายละเอียดข่าว";
$lang["play_list"] = " Playlist ไฟล์เสียง";
$lang["play_list_empty"] = "Playlist ว่างเปล่า";
$lang["pagination_number_posts"] = "Number of Posts Per Page (Pagination)";
$lang["phone"] = "เบอร์โทรติดต่อ";
$lang["priority"] = "ลำดับความสำคัญ";
$lang["priority_exp"] = "ลำดับความสำคัญของ URL เฉพาะที่เกี่ยวข้องกับหน้าอื่น ๆ ในเว็บไซต์เดียวกัน";
$lang["priority_none"] = "ลำดับความสำคัญจากการคำนวณโดยอัตโนมัติ";
$lang["post_list_style"] = "รูปแบบข่าว";
$lang["phrases"] = "Phrases";
$lang["question"] = "คำถาม";
$lang["remember_me"] = "Remember Me";
$lang["register"] = "Register";
$lang["register_with_social"] = "Register with social account";
$lang["register_message"] = "By continuing to register, you are accepting the";
$lang["reset_password_success"] = "Your password has been reset! We have e-mailed your password.";
$lang["reset_password_error"] = "We can't find a user with that e-mail address!";
$lang["random_posts"] = "Random Posts";
$lang["reset_password"] = "Reset Password";
$lang["related_posts"] = "Related Posts";
$lang["related_videos"] = "Related Videos";
$lang["reading_list"] = "Reading List";
$lang["recommended_posts"] = "Recommended Posts";
$lang["rss_feeds"] = "RSS Feeds";
$lang["read_more_button_text"] = "ปุ่มอ่านข้อความเพิ่มเติม";
$lang["registered_users_can_vote"] = "เฉพาะผู้ใช้ที่ลงทะเบียนแล้วสามารถลงคะแนนได้";
$lang["role"] = "บทบาท";
$lang["remove_ban"] = "Remove Ban";
$lang["registered_emails"] = "Registered Emails";
$lang["registration_system"] = "Registration System";
$lang["rss"] = "RSS";
$lang["remove_featured"] = "Remove from Featured";
$lang["remove_breaking"] = "Remove from Breaking";
$lang["remove_recommended"] = "Remove from Recommended";
$lang["remove_slider"] = "Remove from Slider";
$lang["recommended"] = "Recommended";
$lang["right_to_left"] = "จากขวาไปซ้าย";
$lang["save_changes"] = "บันทึกการเปลี่ยนแปลง";
$lang["subtitle_change_password"] = "You can change your password!";
$lang["subtitle_reset_password"] = "Your new password will be sent to your email address!";
$lang["social_accounts"] = "Social Accounts";
$lang["search_noresult"] = "No results found.";
$lang["share"] = "Share";
$lang["search"] = "ค้นหา";
$lang["slider_posts"] = "Slider Posts";
$lang["slug"] = "Slug";
$lang["show"] = "แสดงผล";
$lang["short_form"] = "อักษรย่อภาษา";
$lang["select"] = "เลือก";
$lang["select_an_option"] = "ตัวเลือก";
$lang["select_category"] = "เลือกหมวดหมู่";
$lang["seo_tools"] = "เครื่องมือ Seo";
$lang["seo_options"] = "Seo options";
$lang["social_login_configuration"] = "การกำหนดค่าเข้าสู่ระบบ Social";
$lang["settings"] = "ตั้งค่า";
$lang["show_at_homepage"] = "แสดงบนหน้าเพจหลัก";
$lang["show_cookies_warning"] = "Show Cookies Warning";
$lang["slug_exp"] = "หากคุณเว้นว่างไว้มันจะถูกสร้างขึ้นโดยอัตโนมัติ";
$lang["subcategory"] = "หมวดหมูย่อย";
$lang["subcategories"] = "หมวดหมูย่อย";
$lang["select_image"] = "เลือกรูป";
$lang["select_audio"] = "Select Audio";
$lang["select_video"] = "Select Video";
$lang["show_only_registered"] = "แสดงเฉพาะผู้ใช้ที่ลงทะเบียน";
$lang["show_title"] = "แสดงชื่อเรื่อง";
$lang["show_breadcrumb"] = "แสดง breadcrumb";
$lang["show_right_column"] = "แสดงคอลัมน์ขวา";
$lang["status"] = "สถานะ";
$lang["summary"] = "สาระสำคัญ";
$lang["show_read_more_button"] = "แสดงปุ่มอ่านเพิ่มเติม";
$lang["save_draft"] = "บันทึกเป็นแบบร่าง";
$lang["smtp"] = "SMTP";
$lang["social_media_settings"] = "การตั้งค่าโซเชียลมีเดีย";
$lang["shared"] = "Shared";
$lang["show_featured_section"] = "Show Featured Section";
$lang["show_latest_posts_homepage"] = "Show Latest Posts on Homepage";
$lang["show_news_ticker"] = "Show News Ticker";
$lang["show_post_author"] = "Show Post Author";
$lang["show_post_dates"] = "Show Post Date";
$lang["show_post_view_counts"] = "Show Post View Count";
$lang["site_title"] = "ชื่อเว็บไซต์";
$lang["site_description"] = "รายละเอียดเว็บไซต์";
$lang["send_email_registered"] = "Send Email to Registered Emails";
$lang["subject"] = "เรื่อง";
$lang["send_email"] = "Send Email";
$lang["save"] = "Save";
$lang["site_color"] = "สีของเว็บไซต์";
$lang["sitemap"] = "แผนผังเว็บไซต์";
$lang["scheduled_posts"] = "ข่าวที่ตั้งเวลาแสดงผล";
$lang["scheduled_post"] = "ตั้งเวลาแสดงผล";
$lang["select_file"] = "เลือกไฟล์";
$lang["show_on_menu"] = "แสดงที่เว็บไซต์";
$lang["slider_order"] = "Slider Order";
$lang["slider"] = "Slider";
$lang["settings_language"] = "การตั้งค่าภาษา";
$lang["tag"] = "Tag";
$lang["text_list_empty"] = "Your reading list is empty.";
$lang["total_vote"] = "Total Vote:";
$lang["tags"] = "Tags";
$lang["type_tag"] = "Type tag and hit enter";
$lang["title"] = "หัวข้อ";
$lang["top_menu"] = "Top Menu";
$lang["text_direction"] = "ทิศทางข้อความ";
$lang["type"] = "Type";
$lang["phrase"] = "Phrase";
$lang["update_profile"] = "Update Profile";
$lang["user"] = "ผู้ใช้งาน";
$lang["users"] = "การจัดการผู้ใช้งาน";
$lang["username"] = "ชื่อผู้ใช้งาน";
$lang["user_agreement"] = "User Agreement";
$lang["update_category"] = "Update Category";
$lang["update_subcategory"] = "Update Subcategory";
$lang["update_image"] = "แก้ไข";
$lang["update_link"] = "Update Menu Link";
$lang["update_rss_feed"] = "Update RSS Feed";
$lang["upload"] = "อัปโหลด";
$lang["update"] = "Update";
$lang["update_widget"] = "Update Widget";
$lang["upload_video"] = "อัปโหลดวีดีทัศน์ ";
$lang["update_video"] = "แก้ไขวีดีทัศน์ ";
$lang["update_page"] = "แก้ไขหน้าเพจ";
$lang["update_poll"] = "แก้ไขแบบสำรวจ";
$lang["update_post"] = "แก้ไขข่าว";
$lang["update_language"] = "Update Language";
$lang["update_sitemap"] = "Update Sitemap";
$lang["url"] = "Url";
$lang["view_all"] = "ดูทั้งหมด";
$lang["view_all_posts"] = "ดูข่าวทั้งหมด";
$lang["view_options"] = "View Options";
$lang["view_results"] = "View Results";
$lang["view_site"] = "View Site";
$lang["video"] = "วีดีทัศน์ ";
$lang["videos"] = "Videos";
$lang["vote"] = "Vote";
$lang["voted_message"] = "You already voted this poll before.";
$lang["visual_settings"] = "ตั้งค่า Visual";
$lang["view_result"] = "View Results";
//$lang["visibility"] = "สถานะการมองเห็น";
$lang["visibility"] = "แสดงที่เว็บไซต์";
$lang["video_embed_code"] = "รหัสฝังวิดีโอ";
$lang["video_thumbnails"] = "รูปภาพของวิดีโอ";
$lang["video_url"] = "Url วีดีทัศน์  ";
$lang["video_name"] = "ชื่อวีดีทัศน์ ";
$lang["video_file"] = "Video File";
$lang["vote_permission"] = "อนุญาตให้ลงคะแนน";
$lang["widget"] = "วิดเจ็ต";
$lang["widgets"] = "วิดเจ็ต";
$lang["wrong_password_error"] = "Wrong old password!";
$lang["yes"] = "ใช่";
$lang["facebook"] = "Facebook";
$lang["twitter"] = "Twitter";
$lang["google"] = "Google";
$lang["January"] = "มกราคม";
$lang["February"] = "กุมภาพันธ์";
$lang["March"] = "มีนาคม";
$lang["April"] = "เมษายน";
$lang["May"] = "พฤษภาคม";
$lang["June"] = "มิถุนายน";
$lang["July"] = "กรกฎาคม";
$lang["August"] = "สิงหาคม";
$lang["September"] = "กันยายน";
$lang["October"] = "ตุลาคม";
$lang["November"] = "พฤศจิกายน";
$lang["December"] = "ธันวาคม";
$lang["select_ad_spaces"] = "เลือกพื้นที่โฆษณา";
$lang["ad_space"] = "พื้นที่โฆษณา";
$lang["header_top_ad_space"] = "Header";
$lang["index_top_ad_space"] = "Index (Top)";
$lang["index_bottom_ad_space"] = "Index (Bottom)";
$lang["post_top_ad_space"] = "Post Details (Top)";
$lang["post_bottom_ad_space"] = "Post Details (Bottom)";
$lang["posts_top_ad_space"] = "Posts (Top)";
$lang["posts_bottom_ad_space"] = "Posts (Bottom)";
$lang["category_top_ad_space"] = "Category (Top)";
$lang["category_bottom_ad_space"] = "Category (Bottom)";
$lang["tag_top_ad_space"] = "Tag (Top)";
$lang["tag_bottom_ad_space"] = "Tag (Bottom)";
$lang["search_top_ad_space"] = "Search (Top)";
$lang["search_bottom_ad_space"] = "Search (Bottom)";
$lang["profile_top_ad_space"] = "Profile &#40;Top&#41;";
$lang["profile_bottom_ad_space"] = "Profile &#40;Bottom&#41;";
$lang["reading_list_top_ad_space"] = "Reading List (Top)";
$lang["reading_list_bottom_ad_space"] = "Reading List (Bottom)";
$lang["sidebar_top_ad_space"] = "Sidebar (Top)";
$lang["sidebar_bottom_ad_space"] = "Sidebar (Bottom)";
$lang["banner"] = "Banner";
$lang["paste_ad_code"] = "รหัสโฆษณา";
$lang["upload_your_banner"] = "สร้างรหัสโฆษณา";
$lang["paste_ad_url"] = "Url โฆษณา";
$lang["primary_font"] = "แบบอักษรหลัก (หลัก)";
$lang["secondary_font"] = "แบบอักษรรอง (ชื่อเรื่อง)";
$lang["tertiary_font"] = "แบบอักษรลำดับที่สาม (ข่าว & ข้อความในหน้าเพจ)";
$lang["download_sitemap"] = "Download Sitemap";
$lang["frequency"] = "Frequency";
$lang["frequency_exp"] = "ค่านี้ระบุว่าเนื้อหาใน URL ที่เฉพาะเจาะจงมีแนวโน้มที่จะเปลี่ยนแปลงบ่อยเพียงใด";
$lang["none"] = "ไม่มี";
$lang["always"] = "Always";
$lang["hourly"] = "Hourly";
$lang["daily"] = "Daily";
$lang["weekly"] = "Weekly";
$lang["monthly"] = "Monthly";
$lang["yearly"] = "Yearly";
$lang["never"] = "Never";
$lang["last_modification"] = "การปรับเปลี่ยนครั้งล่าสุด";
$lang["last_modification_exp"] = "เวลาที่แก้ไข URL ครั้งล่าสุด";
$lang["server_response"] = "การตอบสนองของเซิร์ฟเวอร์";
$lang["form_validation_required"] = "The {field} field is required.";
$lang["form_validation_min_length"] = "The {field} field must be at least {param} characters in length.";
$lang["form_validation_max_length"] = "The {field} field cannot exceed {param} characters in length.";
$lang["form_validation_matches"] = "The {field} field does not match the {param} field.";
$lang["form_validation_is_unique"] = "The {field} field must contain a unique value.";
$lang["emoji_reactions"] = "Emoji Reactions";
$lang["whats_your_reaction"] = "What's Your Reaction?";
$lang["like"] = "Like";
$lang["dislike"] = "Dislike";
$lang["love"] = "Love";
$lang["funny"] = "Funny";
$lang["angry"] = "Angry";
$lang["sad"] = "Sad";
$lang["wow"] = "Wow";
$lang["admin_panel_link"] = "Admin Panel Link";
$lang["number_of_links_in_menu"] = "จำนวนลิงค์ที่ปรากฏในเมนู";
$lang["no_records_found"] = "ไม่พบรายการ";
$lang["reset"] = "Reset";
$lang["send_email_subscribers"] = "ส่งอีเมล์ไปยังสมาชิก";
$lang["subscribe"] = "Subscribe";
$lang["subscribers"] = "สมาชิก";
$lang["logo_email"] = "Logo Email";
$lang["preview"] = "Preview";
$lang["hit"] = "Hit";
$lang["post_owner"] = "เพิ่มข่าวโดย";
$lang["send_contact_to_mail"] = "ส่งข้อความติดต่อไปยังที่อยู่อีเมล";
$lang["contact_messages_will_send"] = "ข้อความติดต่อจะถูกส่งไปยังอีเมลนี้";
$lang["msg_updated"] = "Changes successfully saved!";
$lang["msg_user_added"] = "User successfully added!";
$lang["contact_message"] = "ข้อความติดต่อ";
$lang["dont_want_receive_emails"] = "Don't want receive these emails?";
$lang["unsubscribe"] = "Unsubscribe";
$lang["unsubscribe_successful"] = "Unsubscribe Successful!";
$lang["send_post_to_subscribes"] = "ส่งข่าวไปยังสมาชิกทั้งหมด (E-mail)";
$lang["msg_unsubscribe"] = "You will no longer receive emails from us!";
$lang["invalid_feed_url"] = "Invalid feed URL!";
$lang["cache_system"] = "ระบบแคช";
$lang["cache_refresh_time"] = "Cache Refresh Time (Minute)";
$lang["cache_refresh_time_exp"] = "After this time, your cache files will be refreshed.";
$lang["refresh_cache_database_changes"] = "Refresh Cache Files When Database Changes";
$lang["reset_cache"] = "Reset Cache";
$lang["msg_reset_cache"] = "All cache files have been deleted!";
$lang["choose_post_format"] = "Choose a Post Format";
$lang["article"] = "Article";
$lang["article_post_exp"] = "Add an article with images and embed videos.";
$lang["video_post_exp"] = "Upload a video or embed video from Youtube or Vimeo.";
$lang["audio_post_exp"] = "Upload your audios and create your playlist.";
$lang["add_user"] = "เพิ่มผู้ใช้งาน";
$lang["administrators"] = "ผู้ดูแลระบบ";
$lang["msg_username_unique_error"] = "The username has already been taken.";
$lang["add_posts_as_draft"] = "เพิ่มข่าวเป็นแบบร่าง";
$lang["roles_permissions"] = "บริหารจัดการสิทธิ์";
$lang["permissions"] = "สิทธิ์การใช้งาน";
$lang["all_permissions"] = "ใช้งานเมนูได้ทั้งหมด";
$lang["manage_all_posts"] = "จัดการข่าวทั้งหมด";
$lang["edit_role"] = "แก้ไขสิทธิ์การใช้งาน";
$lang["role_name"] = "ชื่อบทบาท";
$lang["msg_not_authorized"] = "You are not authorized to perform this operation!";
$lang["show_all_files"] = "Show all Files";
$lang["show_only_own_files"] = "Show Only Users Own Files";
$lang["approve_added_user_posts"] = "Approve Added User Posts";
$lang["approve_updated_user_posts"] = "Approve Updated User Posts";
$lang["this_week"] = "This Week";
$lang["this_month"] = "This Month";
$lang["this_year"] = "This Year";
$lang["timezone"] = "Timezone";
$lang["msg_send_confirmation_email"] = "A confirmation email has been sent to your email address for activation. Please confirm your account.";
$lang["msg_confirmation_email"] = "Please confirm your email address by clicking the button below.";
$lang["msg_confirmed"] = "Your email address has been successfully confirmed!";
$lang["msg_confirmed_required"] = "Please verify your email address!";
$lang["confirmed"] = "Confirmed";
$lang["unconfirmed"] = "Unconfirmed";
$lang["confirm_your_email"] = "Confirm Your Email";
$lang["email_verification"] = "การยืนยันอีเมล";
$lang["resend_activation_email"] = "Resend Activation Email";
$lang["please_select_option"] = "Please select an option!";
$lang["just_now"] = "Just Now";
$lang["ago"] = "ago";
$lang["minute"] = "minute";
$lang["minutes"] = "minutes";
$lang["hour"] = "hour";
$lang["hours"] = "hours";
$lang["day"] = "day";
$lang["days"] = "days";
$lang["month"] = "month";
$lang["months"] = "months";
$lang["year"] = "year";
$lang["years"] = "years";
$lang["follow"] = "Follow";
$lang["unfollow"] = "Unfollow";
$lang["following"] = "Following";
$lang["followers"] = "Followers";
$lang["member_since"] = "Member since";
$lang["email_template"] = "รูปแบบอีเมล";

$lang["files"] = "จัดการเอกสาร";

$lang["manual"] = "จัดการคู่มือ";
$lang["add_manual"] = "เพิ่มคู่มือ";
$lang["update_manual"] = "แก้ไขคู่มือ";

$lang["petition_form"] = "จัดการแบบฟอร์มคำร้อง";
$lang["add_petition_form"] = "เพิ่มแบบฟอร์มคำร้อง";
$lang["update_petition_form"] = "แก้ไขแบบฟอร์มคำร้อง";

$lang["administration"] = "ฝ่ายบริหาร";
$lang["student_affairs"] = "กิจการนักเรียน";
$lang["professional_academic"] = "วิชาการวิชาชีพ";

$lang["update_administration"] = "แก้ไขบุคลากรฝ่ายบริหาร";
$lang["update_student_affairs"] = "แก้ไขบุคลากรฝ่ายกิจการนักเรียนและนักศึกษา";
$lang["update_professional_academic"] = "แก้ไขบุคลากรฝ่ายวิชาการวิชาชีพ";

$lang["emagazine"] = "จัดการนิตยสาร";
$lang["add_emagazine"] = "เพิ่มนิตยสาร";
$lang["update_emagazine"] = "แก้ไขนิตยสาร";
?>
