<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id'            => $this->input->post('lang_id', true),
            'title'              => $this->input->post('title', true),
            'description'        => $this->input->post('description', true),
            'description2'       => $this->input->post('description2', true),
            'category_id'        => $this->input->post('category_id', true),
            'fac_category_id'    => $this->input->post('fac_category_id', true),
            'fac_subcategory_id' => $this->input->post('fac_subcategory_id', true),
            'gallery_order'      => $this->input->post('gallery_order', true),
            'gallery_level'      => $this->input->post('gallery_level', true),
            'path_big'           => $this->input->post('path_big', true),
            'path_small'         => $this->input->post('path_small', true)
        );

       if($this->input->post('type', true)){
          $data['type'] = $this->input->post('type', true);
       }

        return $data;
    }

    //add image
    public function add()
    {
        $data = $this->input_values();



        $file2 = $_FILES['file2'];
        if (!empty($file2['name'])) {
            $data["path_small"] = $this->upload_model->gallery_small_image_upload($file2);
        }else{
            $data['path_small'] = "";
        }

        $file = $_FILES['file'];
        if (!empty($file['name'])) {
            $data["path_big"] = $this->upload_model->gallery_big_image_upload($file);
            if($data["path_small"] == ""){
               $data["path_small"] = $this->upload_model->gallery_small_image_upload($file);
            }
        } else {
            $data['path_big'] = "";
            $data['path_small'] = "";
        }

        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('gallery', $data);
    }

    //get gallery images
    public function get_images()
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->join('gallery_main_categories', 'gallery_main_categories.id = gallery_categories.main_category_id','LEFT');
        $this->db->select('gallery.* , gallery_categories.name as category_name, gallery_main_categories.name as main_category_name,gallery_categories.main_category_id');
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->order_by('gallery.id', 'DESC');
        $query = $this->db->get('gallery');
        return $query->result();
    }

    //get all gallery images
    public function get_all_images()
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->join('gallery_main_categories', 'gallery_main_categories.id = gallery_categories.main_category_id','LEFT');
        $this->db->select('gallery.* , gallery_categories.name as category_name, gallery_main_categories.name as main_category_name,gallery_categories.main_category_id');
        $this->db->where('gallery.category_id > 5');
        $this->db->order_by('gallery.id', 'DESC');
        $query = $this->db->get('gallery');
        return $query->result();
    }

    //get gallery images by category
    public function get_images_by_category($category_id)
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->join('gallery_main_categories', 'gallery_main_categories.id = gallery_categories.main_category_id','LEFT');
        $this->db->select('gallery.* , gallery_categories.name as category_name, gallery_main_categories.name as main_category_name,gallery_categories.main_category_id');
        $this->db->select('(SELECT c.name FROM categories c WHERE c.id = gallery.fac_category_id) as fac_category_name');
        $this->db->select('(SELECT c.name FROM categories c WHERE c.id = gallery.fac_subcategory_id) as fac_subcategory_name');
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->where('category_id', $category_id);

        if(isset($_GET['type'])){
              $this->db->where('type',$_GET['type']);
        }

        $this->db->order_by('gallery.gallery_level', 'ASC');
        $this->db->order_by('gallery.gallery_order', 'ASC');

        $query = $this->db->get('gallery');
        return $query->result();
    }
    //get gallery images by category
    public function get_images_by_category_type($category_id,$type)
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->join('gallery_main_categories', 'gallery_main_categories.id = gallery_categories.main_category_id','LEFT');
        $this->db->select('gallery.* , gallery_categories.name as category_name, gallery_main_categories.name as main_category_name,gallery_categories.main_category_id');
        $this->db->select('(SELECT c.name FROM categories c WHERE c.id = gallery.fac_category_id) as fac_category_name');
        $this->db->select('(SELECT c.name FROM categories c WHERE c.id = gallery.fac_subcategory_id) as fac_subcategory_name');
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->where('category_id', $category_id);
        $this->db->where('type', $type); 
        $this->db->order_by('gallery.gallery_level,gallery.gallery_order', 'ASC');
        $this->db->order_by('gallery.id', 'DESC');
        $query = $this->db->get('gallery');
        return $query->result();
    }

    public function get_images_by_fac_category($category_id)
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->join('gallery_main_categories', 'gallery_main_categories.id = gallery_categories.main_category_id','LEFT');
        $this->db->select('gallery.* , gallery_categories.name as category_name, gallery_main_categories.name as main_category_name,gallery_categories.main_category_id');
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->where('fac_subcategory_id', $category_id);
        $this->db->order_by('gallery.gallery_level,gallery.gallery_order', 'ASC');
        $query = $this->db->get('gallery');
        return $query->result();
    }
    public function get_all_images_by_category($category_id)
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->join('gallery_main_categories', 'gallery_main_categories.id = gallery_categories.main_category_id','LEFT');
        $this->db->select('gallery.* , gallery_categories.name as category_name, gallery_main_categories.name as main_category_name,gallery_categories.main_category_id');
        // $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->where('category_id', $category_id);
        $this->db->order_by('gallery.id', 'DESC');
        $query = $this->db->get('gallery');
        return $query->result();
    }
    //get gallery images by category
    public function get_images_by_category_random($category_id,$limit=3)
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->select('gallery.* , gallery_categories.name as category_name');
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->where('category_id', $category_id);
        $this->db->order_by('gallery.id','RANDOM');
        $this->db->limit($limit);
        $query = $this->db->get('gallery');
        return $query->result();
    }

    //get gallery images by category
    public function get_images_by_category_limit($category_id,$limit=3)
    {
        $this->db->join('gallery_categories', 'gallery.category_id = gallery_categories.id');
        $this->db->select('gallery.* , gallery_categories.name as category_name');
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $this->db->where('category_id', $category_id);
        $this->db->order_by('gallery.gallery_order','ASC');
        $query = $this->db->get('gallery');
        return $query->result();
    }

    //get category image count
    public function get_category_image_count($category_id)
    {
        $this->db->where('category_id', $category_id);
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $query = $this->db->get('gallery');
        return $query->num_rows();
    }
    public function get_maincategory_image_count($category_id)
    {
        $this->db->where('main_category_id', $category_id);
        $this->db->where('gallery.lang_id', $this->selected_lang->id);
        $query = $this->db->get('gallery');
        return $query->num_rows();
    }

    //get image
    public function get_image($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('gallery');
        return $query->row();
    }

    //update image
    public function update($id)
    {
        $data = $this->input_values();

        $file2 = $_FILES['file2'];
        if (!empty($file2['name'])) {
            $image = $this->get_image($id);
            delete_image_from_server($image->path_small);
            $path_small = 'xx';
            $data["path_small"] = $this->upload_model->gallery_small_image_upload($file2);
        }else{
            $path_small = "";
        }

        $file = $_FILES['file'];
        if (!empty($file['name'])) {

            $image = $this->get_image($id);
            delete_image_from_server($image->path_big);

            if($path_small == ""){
                delete_image_from_server($image->path_small);
            }

            $data["path_big"] = $this->upload_model->gallery_big_image_upload($file);
            if($path_small == ""){
                $data["path_small"] = $this->upload_model->gallery_small_image_upload($file);
            }

            $this->db->where('id', $id);
            return $this->db->update('gallery', $data);
        }

        $this->db->where('id', $id);
        return $this->db->update('gallery', $data);
    }


    //delete image
    public function delete($id)
    {
        $image = $this->get_image($id);

        if (!empty($image)) {
            //delete image
            delete_image_from_server($image->path_big);
            delete_image_from_server($image->path_small);

            $this->db->where('id', $id);
            return $this->db->delete('gallery');
        } else {
            return false;
        }

    }
}
