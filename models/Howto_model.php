<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class howto_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'name' => $this->input->post('name', true),
            'name_slug' => $this->input->post('name_slug', true),
            'parent_id' => $this->input->post('parent_id', true),
            'description' => $this->input->post('description', false),
            'keywords' => $this->input->post('keywords', true),
            'color' => $this->input->post('color', true),
            'howto_order' => $this->input->post('howto_order', true),
            'show_at_homepage' => $this->input->post('show_at_homepage', true),
            'show_on_menu' => $this->input->post('show_on_menu', true),
            'block_type' => $this->input->post('block_type', true),
        );
        return $data;
    }

    //add howto
    public function add_howto()
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('howto', $data);
    }

    //add subhowto
    public function add_subhowto()
    {
        $data = $this->input_values();

        $howto = helper_get_howto($data["parent_id"]);

        if ($howto) {
            $data["color"] = $howto->color;
        } else {
            $data["color"] = "#0a0a0a";
        }

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('howto', $data);
    }

    //update slug
    public function update_slug($id)
    {
        $howto = $this->get_howto($id);

        if (empty($howto->name_slug) || $howto->name_slug == "-") {
            $data = array(
                'name_slug' => $howto->id
            );
            $this->db->where('id', $id);
            $this->db->update('howto', $data);
        } else {
            if ($this->check_is_slug_unique($howto->name_slug, $id) == true) {
                $data = array(
                    'name_slug' => $howto->name_slug . "-" . $howto->id
                );

                $this->db->where('id', $id);
                $this->db->update('howto', $data);
            }
        }
    }

    //check slug
    public function check_is_slug_unique($slug, $id)
    {
        $this->db->where('howto.name_slug', $slug);
        $this->db->where('howto.id !=', $id);
        $query = $this->db->get('howto');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    //get howto
    public function get_howto($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('howto');
        return $query->row();
    }

    //get howto by slug
    public function get_howto_by_slug($slug)
    {
        $this->db->where('name_slug', $slug);
        $query = $this->db->get('howto');
        return $query->row();
    }

    //check howto slug
    public function check_howto_slug($slug, $id)
    {
        $this->db->where('name_slug', $slug);
        $this->db->where('id !=', $id);
        $query = $this->db->get('howto');
        return $query->row();
    }

    //get all top howto
    public function get_all_howto()
    {
        $this->db->where('parent_id', 0);
        $this->db->order_by('howto_order');
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get howto
    public function get_howtos()
    {
        $this->db->order_by('howto_order');
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get top howto
    public function get_top_howto()
    {
        $this->db->where('howto.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('howto_order');
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get top howto
    public function get_top_howto_by_lang($lang_id)
    {
        $this->db->where('howto.lang_id', $lang_id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('howto_order');
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get subhowto
    public function get_subhowto()
    {
        $this->db->where('howto.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id !=', 0);
        $this->db->order_by('howto_order');
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get all top howto
    public function get_all_top_howto()
    {
        $this->db->where('parent_id', 0);
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get all subhowto
    public function get_all_subhowto()
    {
        $this->db->where('parent_id !=', 0);
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get subhowto by id
    public function get_subhowto_by_parent_id($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get sitemap howto
    public function get_sitemap_howto()
    {
        $this->db->order_by('howto_order');
        $query = $this->db->get('howto');
        return $query->result();
    }

    //get howto count
    public function get_howto_count()
    {
        $query = $this->db->get('howto');
        return $query->num_rows();
    }

    //update howto
    public function update_howto($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $howto = helper_get_howto($id);
        //check if parent
        if ($howto->parent_id == 0) {
            $this->update_subhowto_color($id, $data["color"]);
        } else {
            $howto = helper_get_howto($data["parent_id"]);
            if ($howto) {
                $data["color"] = $howto->color;
            } else {
                $data["color"] = "#0a0a0a";
            }
        }

        $this->db->where('id', $id);
        return $this->db->update('howto', $data);
    }

    //update subhowto
    public function update_subhowto($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $this->db->where('id', $id);
        return $this->db->update('howto', $data);
    }

    //update subhowto color
    public function update_subhowto_color($parent_id, $color)
    {
        $howto = $this->get_subhowto_by_parent_id($parent_id);
        if (!empty($howto)) {
            foreach ($howto as $item) {
                $data = array(
                    'color' => $color,
                );
                $this->db->where('parent_id', $parent_id);
                return $this->db->update('howto', $data);
            }
        }
    }

    //delete howto
    public function delete_howto($id)
    {
        $howto = $this->get_howto($id);

        if (!empty($howto)) {
            $this->db->where('id', $id);
            return $this->db->delete('howto');
        } else {
            return false;
        }
    }

}
