<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class participants_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'name' => $this->input->post('name', true),
            'name_slug' => $this->input->post('name_slug', true),
            'parent_id' => $this->input->post('parent_id', true),
            'description' => $this->input->post('description', false),
            'keywords' => $this->input->post('keywords', true),
            'color' => $this->input->post('color', true),
            'participants_order' => $this->input->post('participants_order', true),
            'show_at_homepage' => $this->input->post('show_at_homepage', true),
            'show_on_menu' => $this->input->post('show_on_menu', true),
            'block_type' => $this->input->post('block_type', true),
        );
        return $data;
    }

    //add participants
    public function add_participants()
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $file = $_FILES['file'];
        if (!empty($file['name'])) {
            $data["path_default"]   = $this->upload_model->post_default_image_upload($file);
        } else {
            $data['path_default'] = "";
        }

        $data['created_at'] = date('Y-m-d H:i:s');
        return $this->db->insert('participants', $data);
    }

    //add subparticipants
    public function add_subparticipants()
    {
        $data = $this->input_values();

        $participants = helper_get_participants($data["parent_id"]);

        if ($participants) {
            $data["color"] = $participants->color;
        } else {
            $data["color"] = "#0a0a0a";
        }

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('participants', $data);
    }

    //update slug
    public function update_slug($id)
    {
        $participants = $this->get_participants($id);

        if (empty($participants->name_slug) || $participants->name_slug == "-") {

            $data = array(
                'name_slug' => $participants->id
            );
            $this->db->where('id', $id);
            $this->db->update('participants', $data);
        } else {
            if ($this->check_is_slug_unique($participants->name_slug, $id) == true) {
                $data = array(
                    'name_slug' => $participants->name_slug . "-" . $participants->id
                );

                $this->db->where('id', $id);
                $this->db->update('participants', $data);
            }
        }
    }

    //check slug
    public function check_is_slug_unique($slug, $id)
    {
        $this->db->where('participants.name_slug', $slug);
        $this->db->where('participants.id !=', $id);
        $query = $this->db->get('participants');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    //get participants
    public function get_participants($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('participants');
        return $query->row();
    }

    //get participants by slug
    public function get_participants_by_slug($slug)
    {
        $this->db->where('name_slug', $slug);
        $query = $this->db->get('participants');
        return $query->row();
    }

    //check participants slug
    public function check_participants_slug($slug, $id)
    {
        $this->db->where('name_slug', $slug);
        $this->db->where('id !=', $id);
        $query = $this->db->get('participants');
        return $query->row();
    }

    //get all top participants
    public function get_all_participants()
    {
        $this->db->where('parent_id', 0);
        $this->db->order_by('participants_order');
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get participants
    public function get_participantss()
    {
        $this->db->order_by('participants_order');
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get top participants
    public function get_top_participants()
    {
        $this->db->where('participants.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('participants_order');
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get top participants
    public function get_top_participants_by_lang($lang_id)
    {
        $this->db->where('participants.lang_id', $lang_id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('participants_order');
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get subparticipants
    public function get_subparticipants()
    {
        $this->db->where('participants.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id !=', 0);
        $this->db->order_by('participants_order');
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get all top participants
    public function get_all_top_participants()
    {
        $this->db->where('parent_id', 0);
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get all subparticipants
    public function get_all_subparticipants()
    {
        $this->db->where('parent_id !=', 0);
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get subparticipants by id
    public function get_subparticipants_by_parent_id($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get sitemap participants
    public function get_sitemap_participants()
    {
        $this->db->order_by('participants_order');
        $query = $this->db->get('participants');
        return $query->result();
    }

    //get participants count
    public function get_participants_count()
    {
        $query = $this->db->get('participants');
        return $query->num_rows();
    }

    //update participants
    public function update_participants($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $file = $_FILES['file'];
        if (!empty($file['name'])) {
            $data["path_default"]   = $this->upload_model->post_default_image_upload($file);
        }
 
        $participants = helper_get_participants($id);
        //check if parent
        if ($participants->parent_id == 0) {
            $this->update_subparticipants_color($id, $data["color"]);
        } else {
            $participants = helper_get_participants($data["parent_id"]);
            if ($participants) {
                $data["color"] = $participants->color;
            } else {
                $data["color"] = "#0a0a0a";
            }
        }

        $this->db->where('id', $id);
        return $this->db->update('participants', $data);
    }

    //update subparticipants
    public function update_subparticipants($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $this->db->where('id', $id);
        return $this->db->update('participants', $data);
    }

    //update subparticipants color
    public function update_subparticipants_color($parent_id, $color)
    {
        $participants = $this->get_subparticipants_by_parent_id($parent_id);
        if (!empty($participants)) {
            foreach ($participants as $item) {
                $data = array(
                    'color' => $color,
                );
                $this->db->where('parent_id', $parent_id);
                return $this->db->update('participants', $data);
            }
        }
    }

    //delete participants
    public function delete_participants($id)
    {
        $participants = $this->get_participants($id);

        if (!empty($participants)) {
            $this->db->where('id', $id);
            return $this->db->delete('participants');
        } else {
            return false;
        }
    }

}
