<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oit_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'link' => $this->input->post('link', true),
            'parent_index' => $this->input->post('parent_index', true),
        );
        return $data;
    }
    public function getJsonId1()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('oit');
        return $query->row();
    }

    public function delete($dataArray){
        $dataArray = array(
            'index' => $this->input->post('index'),
            'parent_index' => $this->input->post('parent_index'),
        );
        $index= $dataArray['index'];
        //return $index;
        $parent_index = $dataArray['parent_index'];

        //$a = new stdClass();
        //$a -> b ="test";
        //unset($a['b']);
        $newJsonObject = $this->deleteDataToJson($parent_index,$index);
        $jsonUpdate = json_encode($newJsonObject);
        //$oit->oit = [$data];
        
        //return   $jsonUpdate;
        //return $jsonUpdate;
        $updateObj['id'] = 1;
        
        $updateObj = array();
        $updateObj['json'] = $jsonUpdate;
        
        
        $this->db->where('id', 1);
        return $this->db->update('oit', $updateObj);
    }

    public function update(){
        $dataArray = array(
            'title' => $this->input->post('title'),
            'link' => $this->input->post('link'),
            'index_hidden_edit' => $this->input->post('index_hidden_edit'),
            'parent_index_hidden_edit' => $this->input->post('parent_index_hidden_edit'),
            'parent_index_edit' => $this->input->post('parent_index_edit')
        );
        

        //return $dataArray['parent_index_hidden_edit'];
       // return $dataArray['parent_index'];
       // return $dataArray['parent_index_hidden_edit']===$dataArray['parent_index'];
        //return $dataArray['title'];
        //return $dataArray['index_hidden'];
        $updateData = new stdClass();
        $updateData->title =  $dataArray['title'];
        $updateData->link =  $dataArray['link'];
        if (empty($updateData->link)||$updateData->link == null) {
            //sreturn 12346;

            $pdf_id = $this->input->post('selected_pdf_id_edit');
            if($pdf_id!=null){
                
                $pdf = $this->file_model->get_pdf($pdf_id);
                //return base_url().$pdf->file_path;
                $updateData->link = base_url().$pdf->file_path;
            }else{
                $updateData->link = "#";
            }
        }
        //return json_encode($dataArray['title']);
        if($dataArray['parent_index_hidden_edit']===$dataArray['parent_index_edit']){
            //$data = new stdClass();
            //return 12;
            //$oit = new stdClass();
            
            $newJsonObject = $this->updateDataToJsonSameParent($updateData,$dataArray);
            $jsonUpdate = json_encode($newJsonObject);
            //$oit->oit = [$data];
            
            //return $jsonUpdate;
            $updateObj['id'] = 1;
            
            $updateObj = array();
            $updateObj['json'] = $jsonUpdate;
            
        
            $this->db->where('id', 1);
            return $this->db->update('oit', $updateObj);
        }else{
            //return 21;
            $this->db->where('id', 1);
            $this->db->update('oit', $updateObj);
             
            $jsonUpdate = json_encode($this->updateDataToJsonChangeParent($updateData,$dataArray));
            //return $jsonUpdate;
            $updateObj['json'] = $jsonUpdate;
            
            $this->db->where('id', 1);
            return $this->db->update('oit', $updateObj);
        }
        
       
       
    }
    public function updateDataToJsonChangeParent($data,$allData){
        //return "test";
        $updateObj = $this->getJsonId1();
        $json = $updateObj->json;
        $obj = json_decode($json);
        $data->children = null;
        $old_index = $allData['index_hidden_edit'];
        $old_parent_indexes = explode("/",$allData['parent_index_hidden_edit']);
        //return "fsddsgfdfgfgddfg";
        //return $allData['parent_index_hidden_edit'];
        $parentIndexes = explode("/",$allData['parent_index_edit']);
        $beforeData = new stdClass();
        
        switch (count($old_parent_indexes)) {
            case 0:
                
                $beforeData = $obj->oit[$old_index];
                array_splice($obj->oit,$old_index,1 );
                //array_push($obj->oit[],$data);
                //return 0;
                break;
                
            case 1:
                //return $parentIndexes[0] === "0";
                if($old_parent_indexes[0] === ""){
                    $beforeData = $obj->oit[$old_index];
                    array_splice($obj->oit,$old_index,1 );
                    
                    //return $oit_index;
                    //return 1;
                }else{
                    //return $obj->oit[$old_parent_indexes[0]];
                    $beforeData = $obj->oit[$old_parent_indexes[0]]->children[$old_index];
                    
                    array_splice($obj->oit[$old_parent_indexes[0]]->children,$old_index,1  );
                    //return $obj->oit[$old_parent_indexes[0]]->children;
                }
                break;
            case 2:
                $beforeData = $obj->oit[$old_parent_indexes[0]]->children[$old_parent_indexes[1]]->children[$old_index];
                
                array_splice($obj->oit[$old_parent_indexes[0]]->children[$old_parent_indexes[1]]->children,$old_index,1 );

                //return 4;
                break;
            case 3:
                $beforeData = $obj->oit[$old_parent_indexes[0]]->children[$old_parent_indexes[1]]->children[$old_parent_indexes[2]]->children[$old_index];
                
                array_splice($obj->oit[$old_parent_indexes[0]]->children[$old_parent_indexes[1]]->children[$old_parent_indexes[2]]->children,$old_index,1); //return 5;
                break;
            default:
                throw new \Exception("not implement","not implement");
        }
        $data->children = $beforeData->children;
        //return $data->children;
        //return $beforeData;
        switch (count($parentIndexes)) {
            case 0:
                //no one should come here
                $obj->oit[]= $data;
                //return 0;

                break;
            case 1:
                //return $obj->oit[$indexOfthisNode];

                if($parentIndexes[0] === ""){
                    $obj->oit[]  = $data;
                    //return   $obj->oit[$indexOfthisNode];
                    //return 1;

                }else{
                    if($obj->oit[$parentIndexes[0]]->children == null){
                        $obj->oit[$parentIndexes[0]]->children = [];
                    }
                    //return $obj->oit;
                    $obj->oit[$parentIndexes[0]]->children[] = $data;
                    //return $obj->oit[$parentIndexes[0]];
                    //return 2;
                }
                break;
            case 2:
                if($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children == null){
                    $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children = [];
                }
                $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[] = $data;
                //return 4;
                break;
            case 3:
                if($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children == null){
                    $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children = [];
                }
                $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children[] = $data;
                //return 5;
                break;
            default:
                throw new \Exception("not implement","not implement");
        }
        return $obj;
    }
    public function updateDataToJsonSameParent($data,$allData){
        //return $data->link;
        $updateObj = $this->getJsonId1();
        $json = $updateObj->json;
        $obj = json_decode($json);
        //$data->children = null;
        
        //return  $indexOfthisNode;
        //return  $obj->oit;
        //return $data;
        $indexOfthisNode = $allData['index_hidden_edit'];
        
        $data -> children = $obj->oit[$indexOfthisNode] ->children;
        //return $allData;
        //return $indexOfthisNode;
        //parent indexes
        $parentIndexes = explode("/",$allData[parent_index_edit]);
        //return $parentIndexes;
        //return count($parentIndexes);
        switch (count($parentIndexes)) {
            case 0:
                //no one should come here
                $obj->oit[$indexOfthisNode]= $data;
                break;
            case 1:
                //return $obj->oit[$indexOfthisNode];

                if($parentIndexes[0] === ""){
                    //return 11;
                    //return $obj->oit[$indexOfthisNode] ;

                    $data -> children = $obj->oit[$indexOfthisNode] ->children;
                    //return $data;
                    
                    $obj->oit[$indexOfthisNode]  = $data;
                    //return   $obj->oit[$indexOfthisNode];
                }else{
                    //return 22;

                    $data -> children =$obj->oit[$parentIndexes[0]]->children[$indexOfthisNode]  ->children;
                    //return $data;

                    if($obj->oit[$parentIndexes[0]]->children == null){
                        return 1;
                        $obj->oit[$parentIndexes[0]]->children = [];
                    }
                    //return $data;
                    $obj->oit[$parentIndexes[0]]->children[$indexOfthisNode] = $data;
                    //return 2;
                }
                break;
            case 2:
                //return 3;

                $data -> children = $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$indexOfthisNode]  ->children;
                //return $data;

                if($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children == null){
                    return 2;

                    $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children = [];
                }
                $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$indexOfthisNode] = $data;
                //return 4;
                break;
            case 3:
                //return 4;
                
                $data -> children = $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children[$indexOfthisNode]  ->children;
                //return $data;
                
                if($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children == null){
                    return 3;

                    $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children = [];
                }
                $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children[$indexOfthisNode] = $data;
                //return 5;
                break;
            default:
                throw new \Exception("not implement","not implement");
        }
        //return  4;
        return $obj;
    }
    //add link
    public function add_link()
    {
        $data = $this->input_values();

        //slug for title
        /*if (empty($data["slug"])) {
            $data["slug"] = str_slug($data["title"]);
        }*/

        //print_r($data["title"]);
        if (empty($data['link'])) {
            $pdf_id = $this->input->post('selected_pdf_id_add');
            //return $pdf_id;
            if($pdf_id!=null){
                $pdf = $this->file_model->get_pdf($pdf_id);
                //return base_url().$pdf->file_path;
                $data['link'] = base_url().$pdf->file_path;
            }else{
                $data['link'] = "#";
            }
        }
        //addDataToJsonTest(0,$data)
        //print_r($data["link"]);

        //print_r($data["parent_id"]);
        //$l["json"] = '{"oit":[{"id":1,"title":"ข้อมูลพื้นฐาน","link":"","children":null}]}';

        //$data['created_at'] = date('Y-m-d H:i:s');

        $insertData = new stdClass();
        $insertData->title =  $data['title'];
        $insertData->link =  $data['link'];
        $newJsonObject = $this->addDataToJson($data["parent_index"],$insertData);
        //return $newJsonObject;
        $jsonUpdate = json_encode($newJsonObject);
        //return $jsonUpdate;
        //throw new \Exception(count($newJsonObject->oit));
        $updateObj = array();
        $updateObj['json'] = $jsonUpdate;
        $updateObj['id'] = 1;
        
        $this->db->where('id', 1);
        return $this->db->update('oit', $updateObj);
        //return $this->db->insert('oit',$updateObj );
    }

    public function find($ids){
        /*
        $updateObj = getJsonId1();
        $json = $updateObj->json;

        $obj = json_decode($json);
        $array = $obj->oit;
        $ids = explode("/",parentId);

        if($ids.count>=1){
            
        }*/
    }
    public function addDataToJson($parentIndexes,$data){
        $updateObj = $this->getJsonId1();
        $json = $updateObj->json;
        $obj = json_decode($json);
        $data->children = null;
        $parentIndexes = explode("/",$parentIndexes);
        //return count($parentIndexes);
        switch (count($parentIndexes)) {
            case 0:
                array_push($obj->oit[],$data);
                //return 0;
                break;
            case 1:
                //return $parentIndexes[0] === "0";
                if($parentIndexes[0] === ""){
                    $obj->oit[] = $data;
                    //return 1;
                }else{
                    if($obj->oit[$parentIndexes[0]]->children == null){
                        $obj->oit[$parentIndexes[0]]->children = [];
                    }
                    $obj->oit[$parentIndexes[0]]->children[] = $data;
                    //return 2;
                }
                break;
            case 2:
                
                if($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children == null){
                    $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children = [];
                }
                $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[] = $data;
                //return 4;
                break;
            case 3:
                
                if($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children == null){
                    $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children = [];
                }
                $obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children[] = $data;
                //return 5;
                break;
            default:
                throw new \Exception("not implement","not implement");
        }
        return $obj;
    }
    public function deleteDataToJson($parentIndexes,$index){
        $updateObj = $this->getJsonId1();
        $json = $updateObj->json;
        $obj = json_decode($json);
        $parentIndexes = explode("/",$parentIndexes);
        //return count($parentIndexes);
        switch (count($parentIndexes)) {
            case 0:
                //array_push($obj->oit[],$data);
                //return 0;
                break;
            case 1:
                //return $parentIndexes[0] === "0";
                if($parentIndexes[0] === ""){
                    array_splice($obj->oit,$index,1 );
                    ///return 1;
                }else{
                    array_splice($obj->oit[$parentIndexes[0]]->children,$index,1  );
                    //return 2;
                }
                break;
            case 2:
                array_splice($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children,$index,1 );
                //return 4;
                break;
            case 3:
                array_splice($obj->oit[$parentIndexes[0]]->children[$parentIndexes[1]]->children[$parentIndexes[2]]->children,$index,1);
                //return 5;
                break;
            default:
                throw new \Exception("not implement","not implement");
        }
        //return "aaaa";
        return $obj;
    }
    /*
    public function addDataToJson($parentIds,$data){
        

        $updateObj = $this->getJsonId1();
        $json = $updateObj->json;

        $obj = json_decode($json);

        $array = &$obj->oit;
        if($parentIds == null|| $parentIds.count == 0){
            $array[] = $data;
            return $obj;
        }else{

            $ids = explode("/",$parentIds);
            $arrayToFind = $array;
            
            foreach($ids as $id){
                if($arrayToFind == null){
                    throw new \Exception("can't this id",$id,"which come from",$ids,"in obj",$array);
                    break;
                }
                $parent = $this->findById($id,$arrayToFind);
                $arrayToFind = $parent->children;
            }
            if ($parent->children == null){
                $parent->children = [];
            }
            $parent->children[] = $data;
        }
            return 2;
            return $obj;
    }
    public function &findById($id,&$arrayToFind)
    {
        foreach($arrayToFind as &$element){
            if($element->id == $id){
                return $element;
            }
        }
    }*/

}