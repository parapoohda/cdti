<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'name' => $this->input->post('name', true),
            'name_slug' => $this->input->post('name_slug', true),
            'parent_id' => $this->input->post('parent_id', true),
            'description' => $this->input->post('description', true),
            'keywords' => $this->input->post('keywords', true),
            'color' => $this->input->post('color', true),
            'faq_order' => $this->input->post('faq_order', true),
            'show_at_homepage' => $this->input->post('show_at_homepage', true),
            'show_on_menu' => $this->input->post('show_on_menu', true),
            'block_type' => $this->input->post('block_type', true),
        );
        return $data;
    }

    //add faq
    public function add_faq()
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('faqs', $data);
    }

    //add subfaq
    public function add_subfaq()
    {
        $data = $this->input_values();

        $faq = helper_get_faq($data["parent_id"]);

        if ($faq) {
            $data["color"] = $faq->color;
        } else {
            $data["color"] = "#0a0a0a";
        }

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('faqs', $data);
    }

    //update slug
    public function update_slug($id)
    {
        $faq = $this->get_faq($id);

        if (empty($faq->name_slug) || $faq->name_slug == "-") {
            $data = array(
                'name_slug' => $faq->id
            );
            $this->db->where('id', $id);
            $this->db->update('faqs', $data);
        } else {
            if ($this->check_is_slug_unique($faq->name_slug, $id) == true) {
                $data = array(
                    'name_slug' => $faq->name_slug . "-" . $faq->id
                );

                $this->db->where('id', $id);
                $this->db->update('faqs', $data);
            }
        }
    }

    //check slug
    public function check_is_slug_unique($slug, $id)
    {
        $this->db->where('faqs.name_slug', $slug);
        $this->db->where('faqs.id !=', $id);
        $query = $this->db->get('faqs');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    //get faq
    public function get_faq($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('faqs');
        return $query->row();
    }

    //get faq by slug
    public function get_faq_by_slug($slug)
    {
        $this->db->where('name_slug', $slug);
        $query = $this->db->get('faqs');
        return $query->row();
    }

    //check faq slug
    public function check_faq_slug($slug, $id)
    {
        $this->db->where('name_slug', $slug);
        $this->db->where('id !=', $id);
        $query = $this->db->get('faqs');
        return $query->row();
    }

    //get all top faqs
    public function get_all_faqs()
    {
        $this->db->where('parent_id', 0);
        $this->db->order_by('faq_order');
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get faqs
    public function get_faqs()
    {
        $this->db->order_by('faq_order');
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get top faqs
    public function get_top_faqs()
    {
        $this->db->where('faqs.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('faq_order');
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get top faqs
    public function get_top_faqs_by_lang($lang_id,$type)
    {
          $this->db->where('faqs.lang_id', $lang_id);
          $this->db->where('parent_id', 0);

          if($type == 1){
              $this->db->where('show_at_homepage', 1);
          }else{
              $this->db->where('show_at_homepage', 0);
          }

          $this->db->order_by('faq_order');
          $query = $this->db->get('faqs');
          return $query->result();
    }

    //get subfaqs
    public function get_subfaqs()
    {
        $this->db->where('faqs.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id !=', 0);
        $this->db->order_by('faq_order');
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get all top faqs
    public function get_all_top_faqs()
    {
        $this->db->where('parent_id', 0);
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get all subfaqs
    public function get_all_subfaqs()
    {
        $this->db->where('parent_id !=', 0);
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get subfaqs by id
    public function get_subfaqs_by_parent_id($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get sitemap faqs
    public function get_sitemap_faqs()
    {
        $this->db->order_by('faq_order');
        $query = $this->db->get('faqs');
        return $query->result();
    }

    //get faq count
    public function get_faq_count()
    {
        $query = $this->db->get('faqs');
        return $query->num_rows();
    }

    //update faq
    public function update_faq($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $faq = helper_get_faq($id);
        //check if parent
        if ($faq->parent_id == 0) {
            $this->update_subfaqs_color($id, $data["color"]);
        } else {
            $faq = helper_get_faq($data["parent_id"]);
            if ($faq) {
                $data["color"] = $faq->color;
            } else {
                $data["color"] = "#0a0a0a";
            }
        }

        $this->db->where('id', $id);
        return $this->db->update('faqs', $data);
    }

    //update subfaq
    public function update_subfaq($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $this->db->where('id', $id);
        return $this->db->update('faqs', $data);
    }

    //update subfaq color
    public function update_subfaqs_color($parent_id, $color)
    {
        $faqs = $this->get_subfaqs_by_parent_id($parent_id);
        if (!empty($faqs)) {
            foreach ($faqs as $item) {
                $data = array(
                    'color' => $color,
                );
                $this->db->where('parent_id', $parent_id);
                return $this->db->update('faqs', $data);
            }
        }
    }

    //delete faq
    public function delete_faq($id)
    {
        $faq = $this->get_faq($id);

        if (!empty($faq)) {
            $this->db->where('id', $id);
            return $this->db->delete('faqs');
        } else {
            return false;
        }
    }

}
