<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'title' => $this->input->post('title', true),
            'slug' => $this->input->post('slug', true),
            'description' => $this->input->post('description', true),
            'keywords' => $this->input->post('keywords', true),
            'page_content' => $this->input->post('page_content', false),
            'page_order' => $this->input->post('page_order', true),
            'parent_id' => $this->input->post('parent_id', true),
            'visibility' => $this->input->post('visibility', true),
            'title_active' => $this->input->post('title_active', true),
            'breadcrumb_active' => $this->input->post('breadcrumb_active', true),
            'right_column_active' => $this->input->post('right_column_active', true),
            'need_auth' => $this->input->post('need_auth', true),
            'location' => $this->input->post('location', true),
            'is_econ' => 0,
            'page_type' => ($this->input->post('page_type', true))?$this->input->post('page_type', true):"page",
        );
        return $data;
    }
    //input values
    public function input_values_fac()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'title' => $this->input->post('title', true),
            'slug' => $this->input->post('slug', true),
            'description' => $this->input->post('description', true),
            'keywords' => $this->input->post('keywords', true),
            'page_content' => $this->input->post('page_content', false),
            'page_order' => $this->input->post('page_order', true),
            'parent_id' => $this->input->post('parent_id', true),
            'visibility' => $this->input->post('visibility', true),
            'title_active' => $this->input->post('title_active', true),
            'breadcrumb_active' => $this->input->post('breadcrumb_active', true),
            'right_column_active' => $this->input->post('right_column_active', true),
            'need_auth' => $this->input->post('need_auth', true),
            'location' => $this->input->post('location', true),
            'category_id'    => $this->input->post('category_id', true),
            'is_custom' => 0,
            'link'      => $this->input->post('link', true),
            'page_type' => ($this->input->post('page_type', true))?$this->input->post('page_type', true):"page",

            'branch_title_en' => $this->input->post('branch_title_en', true),
            'branch_description' => $this->input->post('branch_description', true),
            'branch_highlight' => $this->input->post('branch_highlight', true),
            'branch_qualification' => $this->input->post('branch_qualification', true),
            'branch_structure' => $this->input->post('branch_structure', true),
            'branch_study' => $this->input->post('branch_study', true),
            'branch_jobs' => $this->input->post('branch_jobs', true),
        );

        if (empty($this->input->post('image_url', true))):
            //add page image

            $image = $this->file_model->get_image($this->input->post('post_image_id', true));
            
            if (!empty($image)) {
                $data["branch_cover_image"] = $image->image_big;
            }
        endif;

        if($this->input->post('sub', true) == 1){
            $data['subcategory_id'] = $this->input->post('subcategory_id', true);
        }else{
            $data['main_category_id'] = $this->input->post('main_category_id', true);
        }
        // dd($data);exit;
        return $data;
    }

    //add page
    public function add()
    {
        $data = $this->page_model->input_values();

        if (empty($data["slug"])) {
            //slug for title
            $data["slug"] = str_slug($data["title"]);

            if (empty($data["slug"])) {
                $data["slug"] = "page-" . uniqid();
            }
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('pages', $data);
    }

    public function add_fac()
    {
        $data = $this->page_model->input_values_fac();

        if (empty($data["slug"])) {
            //slug for title
            $data["slug"] = str_slug($data["title"]);

            if (empty($data["slug"])) {
                $data["slug"] = "page-" . uniqid();
            }
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('pages', $data);
    }

    //update page
    public function update($id)
    {
        //set values
        $data = $this->page_model->input_values();
        if (empty($data["slug"])) {
            $data["slug"] = str_slug($data["title"]);
            if (empty($data["slug"])) {
                $data["slug"] = "page-" . uniqid();
            }
        }
        $page = $this->get_page_by_id_new($id);
        if (!empty($page)) {
            $this->db->where('id', $id);
            return $this->db->update('pages', $data);
        }
        return false;
    }

    public function update_fac($id)
    {
        //set values
        $data = $this->page_model->input_values_fac();
        
        if (empty($data["slug"])) {
            $data["slug"] = str_slug($data["title"]);
            if (empty($data["slug"])) {
                $data["slug"] = "page-" . uniqid();
            }
        }
        $page = $this->get_page_by_id_new($id);
        if (!empty($page)) {
            $this->db->where('id', $id);
            return $this->db->update('pages', $data);
        }
        return false;
    }

    public function update_fac_branch($id,$field)
    {
        //set values
        $data[$field] = $this->input->post('content_text', true);
        
        if (($this->input->post('post_image_id', true))):
            //add page image

            $image = $this->file_model->get_image($this->input->post('post_image_id', true));
            // dd($image);
            if (!empty($image)) {
                $data["branch_cover_image"] = $image->image_default;
            }
        endif;

        
        
        $page = $this->get_page_by_id_new($id);
        if (!empty($page)) {
            $this->db->where('id', $id);
            return $this->db->update('pages', $data);
        }
        return false;
    }

    //get all pages
    public function get_all_pages()
    {
        $this->db->order_by('page_order');
        $this->db->where('page_type','page');
        $this->db->or_where('page_type','page-default');
        $query = $this->db->get('pages');
        return $query->result();
    }
    //get all pages
    public function get_all_pages_fac($type='main',$category_id = "")
    {
        $this->db->order_by('page_order');
        $this->db->join('categories', 'pages.category_id = categories.id');
        $this->db->select('pages.*, categories.name as category_name,(SELECT c.name FROM categories c WHERE c.id = pages.subcategory_id) as subcategory_name');

        $this->db->where('page_type','fac');
        if($type == 'main'){
          $this->db->where('main_category_id > 0');
        }else{
          $this->db->where('subcategory_id > 0');
        }
        if($category_id != ""){
            $this->db->where('category_id',$category_id);
        }
        $this->db->where('visibility', 1);
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get pages
    public function get_pages()
    {
        $this->db->order_by('page_order');
        $this->db->where('is_econ',0);
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get pages
    public function get_pages_by_lang($lang_id)
    {
        $this->db->where('pages.lang_id', $lang_id);
        $this->db->order_by('page_order');
        // $this->db->where('is_econ',0);
        $this->db->where('visibility', 1);
        $query = $this->db->get('pages');
        return $query->result();
    }
    public function get_page_by_slug($lang_id)
    {
        $this->db->where('pages.slug', $lang_id);
        $this->db->order_by('page_order');
        $this->db->where('is_econ',0);
        // $this->db->where('visibility', 1);
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get subpages
    public function get_subpages($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $this->db->where('visibility', 1);
        $this->db->where('is_econ',0);
        $this->db->order_by('page_order');
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get pages sitemap
    public function get_pages_sitemap()
    {
        $this->db->where('page_type', "page");
        $this->db->order_by('pages.id');
        $this->db->where('is_econ',0);
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get top menu pages
    public function get_top_menu_pages()
    {
        $this->db->where('pages.lang_id', $this->selected_lang->id);
        $this->db->order_by('page_order');
        $this->db->where('is_econ',0);
        $this->db->where('location', 'top');
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get main menu pages
    public function get_main_menu_pages()
    {
        $this->db->where('pages.lang_id', $this->selected_lang->id);
        $this->db->order_by('page_order');
        $this->db->where('is_econ',0);
        $this->db->where('location', 'main');
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get footer pages
    public function get_footer_pages()
    {
        $this->db->where('pages.lang_id', $this->selected_lang->id);
        $this->db->order_by('page_order');
        $this->db->where('is_econ',0);
        $this->db->where('location', 'footer');
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get page
    public function get_page($slug)
    {
        $this->db->where('slug', $slug);
        $query = $this->db->get('pages');
        return $query->row();
    }

    //get page by lang
    public function get_page_by_lang($slug, $lang_id)
    {
        $this->db->where('lang_id', $lang_id);
        $this->db->where('slug', $slug);
        $this->db->where('is_econ',0);
        $query = $this->db->get('pages');
        return $query->row();
    }

    //get page by lang
    public function get_default_page($slug, $lang_id)
    {
        $this->db->where('lang_id', $lang_id);
        $this->db->where('slug', $slug);
        $query = $this->db->get('pages');
        return $query->row();
    }

    public function get_main_page($main_category_id, $lang_id)
    {
      $this->db->where('lang_id', $lang_id);
      $this->db->where('main_category_id', $main_category_id);
      $this->db->where('visibility', 1);
      $query = $this->db->get('pages');
      return $query->row();
    }

    public function get_branch_page($subcategory_id, $lang_id)
    {
        $this->db->where('lang_id', $lang_id);
        $this->db->where('subcategory_id', $subcategory_id);
        $this->db->where('visibility', 1);
        $query = $this->db->get('pages');
        return $query->result();
    }

    public function get_page_category($category_id, $lang_id)
    {
        $this->db->where('lang_id', $lang_id);
        $this->db->where('category_id', $category_id);
        $this->db->where('visibility', 1);
        $this->db->order_by('page_order');
        $query = $this->db->get('pages');
        return $query->result();
    }

    //get page by id
    public function get_page_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->where('is_econ',0);
        $query = $this->db->get('pages');
        return $query->row();
    }


    public function get_page_by_id_new($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('pages');
        return $query->row();
    }

    //get page by link
    public function get_page_by_link($link)
    {
        $this->db->where('link', $link);
        $this->db->where('is_econ',0);
        $query = $this->db->get('pages');
        return $query->row();
    }

    //check page name
    public function check_page_name()
    {
        $title = $this->input->post('title', true);
        $slug = $this->input->post('slug', true);

        if (empty($slug)) {
            //slug for title
            $slug = str_slug($title);
        }

        $languages = $this->language_model->get_languages();
        if (!empty($languages)) {
            foreach ($languages as $language) {
                if ($language->short_form == trim($slug)) {
                    return false;
                }
            }
        }
        return true;
    }

    //delete page
    public function delete($id)
    {
        $page = $this->get_page_by_id_new($id);
        if (!empty($page)) {

            $this->db->where('id', $id);
            return $this->db->delete('pages');
        }
        return false;
    }
}
