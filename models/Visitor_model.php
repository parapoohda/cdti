<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Visitor_model extends CI_Model {
        public function __construct()
        {
            parent::__construct();
            // $this->db = $this->load->database('default3',true);
        }
public function addCount(){
        $today =date('Y-m-d');
       //return $today;
       //return 1;
        $query = $this->db->select('id')->select('count')->order_by('id',"desc")->limit(1)->where('id',strtotime($today))->get('visitor');
        //return $query;
        $dataToday = $query->row();
        //return $dataToday==null;
        if($dataToday == null){
                //return 1;
                //return 111111111;
                //insertCount();
                $data = array(
                        'count'=>1,
                        'id'=>strtotime($today)
                );
                $this->db->insert('visitor',$data);
                //return 111111111;
        }else{
                //return 2;

                ////updateCount();
                //return $dataToday;

                $count = $dataToday ->count;
                $count++;
                //return $count;
                $dataToday ->count = $count;
                $this->db->where('id',strtotime($today))->update('visitor',$dataToday);
                
                //return 222222;
        }
}

public function get_today()
{
        $query = $this->db->select('*')->order_by('id',"desc")->limit(1)->get('visitor');
        $dataToday = $query->row();
        return $dataToday->count;
        //$query = $this->db->get('entries', 30);
        //$this->db->order_by('title', 'DESC');
        //return $query->result();
}
public function get_yesterday()
{
        $query = $this->db->select('*')->order_by('id',"desc")->limit(2)->get('visitor');
        $dataToday = $query->row(1);
        return $dataToday->count;
        //$query = $this->db->get('entries', 30);
        //$this->db->order_by('title', 'DESC');
        //return $query->result();
}
public function get_last_thirty_entries()
{
                
                
                $query = $this->db->select('*')->order_by('id',"desc")->limit(30)->get('visitor');
                $dataLast30Days = $query->result();
                $count=0;
                foreach($dataLast30Days as $data){
                        /*$dataDate = strtotime(($data->date+" 00:00:00.0"));
                        return $dataDate;


                        $date30DayAgo = time()-(30*60*60*24);
                        return $date30DayAgo;
                        */
                        
                        //$dataDate =date('Y-m-d');
                        
                        $dataDate = $data->id;
                        $date30DayAgo = strtotime(date('Y-m-d', strtotime('-30 days')));
                        //return $date30DayAgo>$dataDate;
                        //return $date30DayAgo;
                        if($date30DayAgo<$dataDate){
                                $count += $data->count;

                        }
                }
                return $count;

                
                /*$count =0;
                for($i = 0; $i<30;$i++){
                        $count += $query->row(i)->count;
                }
                */
                //$query = $this->db->get('entries', 30);
                //$this->db->order_by('title', 'DESC');
                //return $query->result();
}
public function get_total()
{
                
                //21-03
                $query = $this->db->select('*')->order_by('id',"desc")->get('visitor');
                $dataLast30Days = $query->result();
                $count=0;
                foreach($dataLast30Days as $data){
                                $count += $data->count;
                }
                return $count;
}
public function get_every_month_visitor_every_five_year($quinquennialCount){

        date_default_timezone_set('UTC');
        //$get = $this->db->select('*')->limit(1);
        $txt ='';
        $month = array();
        for($i = 0;$i<61;$i++){
                $month_since_first_month = ($quinquennialCount-1)*60+$i; 
                //$txt=$txt.' '.$i.':'.strtotime('+'.$month_since_first_month.' month',date('2021-03-01'));
                if($month_since_first_month == 0){
                        $month[$i]=strtotime(date('2021-03-01'));
                }else{
                       //$txt=$txt.' '.strtotime('first day of +'.$month_since_first_month.' month',date('2021-03-01'));
                        $month[$i]=strtotime('+'.$month_since_first_month.' month', strtotime('2021-03-01'));
                        
                }
        }
        //return '';
        $data = array();
        for($j=0;$j<60;$j++){
                $array = array( 'id >' => $month[$j], 'id <' => $month[$j+1]);
                
                $query = $this->db->select('*')->select_sum('count')->where($array)->limit(1)->get('visitor');
                $data[$j] = $query->row();
        }
        return $data;
}
/*public function get()
{
        $this->db->where('Date', );
        $query = $this->db->get('oit');
        return $query->row();
        //$query = $this->db->where('date', );
        //return $query->result();
}
public function get_last_thirty_entries()
{
        
        $query = $this->db->get('visitor')->order_by('title', 'DESC');
        return $query->result();
        //$query = $this->db->get('entries', 30);
        //$this->db->order_by('title', 'DESC');
        //return $query->result();
}*/
/*
public function insert_entry()
{
        $this->title    = $_POST['title']; // please read the below note
        $this->content  = $_POST['content'];
        $this->date     = time();

        $this->db->insert('entries', $this);
}

public function update_entry()
{
        $this->title    = $_POST['title'];
        $this->content  = $_POST['content'];
        $this->date     = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
}*/

}