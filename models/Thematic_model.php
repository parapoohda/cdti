<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thematic_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // $this->db = $this->load->database('default3',true);
    }


    public function get_member()
    {
        $this->db->where('trash_status',0);
        $this->db->order_by('mcode','DESC');
        $this->db->select('member.*');
        $this->db->select('(SELECT setting_industry.name FROM setting_industry WHERE setting_industry.id = member.industry) as industry_name');
        $query = $this->db->get('member');
        return $query->result();
    }
    public function get_project()
    {
        $this->db->where('trash_status',0);
        $this->db->order_by('pcode','DESC');

        $this->db->select('proposal.*');
        $this->db->select('(SELECT member.username FROM member WHERE member.id = proposal.member_id) as username');
        $this->db->select('(SELECT member.name FROM member WHERE member.id = proposal.member_id) as name');
        $this->db->select('(SELECT setting_industry.name FROM setting_industry WHERE setting_industry.id = proposal.industry_main) as industry_name');
        $query = $this->db->get('proposal');
        return $query->result();
    }

}
