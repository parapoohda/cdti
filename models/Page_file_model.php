<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_file_model extends CI_Model
{
    //add post additional images
    public function add_page_additional_images($post_id,$field = "")
    {
        $image_ids = $this->input->post('additional_post_image_id', true);
        $field = ($field)?$field:"";
        if (!empty($image_ids)) {
            foreach ($image_ids as $image_id) {
                $image = $this->file_model->get_image($image_id);

                if (!empty($image)) {
                    $item = array(
                        'page_id' => $post_id,
                        'image_big' => $image->image_big,
                        'image_default' => $image->image_default,
                        'flag' => $field,
                    );

                    if (!empty($item["image_default"])) {
                        $this->db->insert('pages_images', $item);
                    }
                }
            }
        }
    }

    //delete additional image
    public function delete_page_additional_image($file_id)
    {
        $image = $this->get_page_additional_image($file_id);

        if (!empty($image)) {
            $this->db->where('id', $file_id);
            $this->db->delete('pages_images');
        }
    }

    //delete additional images
    public function delete_post_additional_images($post_id)
    {
        $images = $this->get_post_additional_images($post_id);

        if (!empty($images)):

            foreach ($images as $image) {
                $this->db->where('id', $image->id);
                $this->db->delete('pages_images');
            }

        endif;
    }



    //get post additional images
    public function get_page_additional_images($page_id,$flag = "")
    {
        if($flag){
            $this->db->where('flag', $flag);
        }
        $this->db->where('page_id', $page_id);
        $this->db->order_by('id', 'asc');
        $query = $this->db->get('pages_images');
        return $query->result();
    }

    //get post additional image
    public function get_page_additional_image($file_id)
    {
        $this->db->where('id', $file_id);
        $query = $this->db->get('pages_images');
        return $query->row();
    }

    //get post additional image count
    public function get_page_additional_image_count($post_id)
    {
        $this->db->where('post_id', $post_id);
        $query = $this->db->get('pages_images');
        return $query->num_rows();
    }

    public function get_page_files_count($post_id)
    {
        $this->db->where('page_id', $post_id);
        $query = $this->db->get('page_files');
        return $query->num_rows();
    }

    

    public function add_page_files($post_id)
    {
        $file_ids = $this->input->post('page_file_id', true);
        if (!empty($file_ids)) {
            foreach ($file_ids as $file_id) {
                $file = $this->file_model->get_pdf($file_id);

                if (!empty($file)) {
                    $item = array(
                        'post_id' => $post_id,
                        'files_id' => $file->id,
                    );
                    $this->db->insert('page_files', $item);
                }
            }
        }
    }

    public function delete_post_files($post_id, $files_id)
    {
        $this->db->where('post_id', $post_id);
        $this->db->where('files_id', $files_id);
        $this->db->delete('post_files');
    }

    //delete post video
    public function delete_post_video($post_id)
    {
        $post = $this->post_admin_model->get_post($post_id);

        if (!empty($post)) {
            $data = array(
                'video_path' => ""
            );

            $this->db->where('id', $post_id);
            return $this->db->update('posts', $data);
        }
    }

    //delete page main image
    public function delete_page_main_image($page_id)
    {
        $page = $this->page_model->get_page_by_id($page_id);

        if (!empty($page)) {
            $data = array(
                'branch_cover_image' => "",
            );

            $this->db->where('id', $page_id);
            $this->db->update('pages', $data);
        }
    }

}
