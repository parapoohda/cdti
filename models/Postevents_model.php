<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Postevents_model extends CI_Model
{

    public function set_filter_query()
    {
      $this->db->join('users', 'post_events.user_id = users.id');
      // $this->db->join('categories', 'post_events.category_id = categories.id');
      $this->db->select('post_events.*, users.username as username, users.slug as user_slug');
      $this->db->where('post_events.edate_event > CURRENT_TIMESTAMP()');
      $this->db->where('post_events.visibility', 1);
      // $this->db->where('post_events.is_econ', 1);
      $this->db->where('post_events.status', 1);
      $this->db->order_by('post_events.edate_event','DESC');
        // $this->db->where('post_events.lang_id', $this->selected_lang->id);
    }

    //get post
    public function get_post($slug)
    {
        $this->set_filter_query();
        $this->db->where('post_events.title_slug', $slug);
        $query = $this->db->get('post_events');
        return $query->row();
    }

    //get breaking news
    public function get_breaking_news()
    {
        $this->set_filter_query();
        $this->db->where('is_breaking', 1);
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit(20);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get slider post_events
    public function get_slider_post_events()
    {
        $this->set_filter_query();
        $this->db->where('is_slider', 1);
        $this->db->order_by('slider_order');
        $this->db->limit(20);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get featured post_events
    public function get_featured_post_events()
    {
        $this->set_filter_query();
        $this->db->where('is_featured', 1);
        $this->db->order_by('featured_order');
        $this->db->limit(5);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get last post_events
    public function get_last_post_events($lang_id, $limit, $skip)
    {
        $this->db->join('users', 'post_events.user_id = users.id');
        $this->db->join('categories', 'post_events.category_id = categories.id');
        $this->db->select('post_events.*, categories.name as category_name, categories.color as category_color, users.username as username, users.slug as user_slug');
        $this->db->where('post_events.created_at <= CURRENT_TIMESTAMP()');
        $this->db->where('post_events.visibility', 1);
        $this->db->where('post_events.status', 1);
        $this->db->where('post_events.lang_id', $lang_id);
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit($limit, $skip);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get post count
    public function get_post_count()
    {
        $this->set_filter_query();
        $query = $this->db->get('post_events');
        return $query->num_rows();
    }

    //get post count by lang
    public function get_post_count_by_lang($lang_id)
    {
        $this->db->join('users', 'post_events.user_id = users.id');
        // $this->db->join('categories', 'post_events.category_id = categories.id');
        $this->db->select('post_events.*, users.username as username, users.slug as user_slug');
        // $this->db->where('post_events.created_at <= CURRENT_TIMESTAMP()');
        $this->db->where('post_events.visibility', 1);
        $this->db->where('post_events.status', 1);
        // $this->db->where('post_events.lang_id', $lang_id);
        $query = $this->db->get('post_events');

        return $query->num_rows();
    }

    //get all post_events
    public function get_paginated_post_events($per_page, $offset)
    {
        $this->set_filter_query();
        $this->db->order_by('post_events.sdate_event', 'ASC');
        $this->db->limit($per_page, $offset);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get popular post_events
    public function get_popular_post_events($day_count)
    {
        $sql = "SELECT post_events.id, post_events.title, post_events.title_slug, post_events.post_type, post_events.image_small, post_events.image_url, users.slug AS user_slug, users.username AS username, post_events.created_at, hit_counts.count AS hit FROM post_events
                INNER JOIN (SELECT COUNT(post_hits.post_id) AS count, post_hits.post_id FROM post_hits WHERE post_hits.created_at > DATE_SUB(NOW(), INTERVAL ? DAY) GROUP BY post_hits.post_id) AS hit_counts ON hit_counts.post_id = post_events.id
                INNER JOIN users ON users.id = post_events.user_id
                INNER JOIN categories ON categories.id = post_events.category_id
                WHERE post_events.created_at <= CURRENT_TIMESTAMP() AND post_events.status = 1 AND post_events.lang_id = ? ORDER BY hit_counts.count DESC LIMIT 5";
        $query = $this->db->query($sql, array($day_count, $this->selected_lang->id));
        return $query->result();
    }

    //get recommended post_events
    public function get_recommended_post_events()
    {
        $this->set_filter_query();
        $this->db->where('is_recommended', 1);
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit(5);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get random post_events
    public function get_random_post_events($limit)
    {
        $this->set_filter_query();
        $this->db->order_by('rand()');
        $this->db->limit($limit);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get related post_events
    public function get_related_post_events($category_id, $post_id)
    {
        $this->set_filter_query();
        $this->db->where('post_events.id !=', $post_id);
        $this->db->where('post_events.category_id', $category_id);
        $this->db->order_by('rand()');
        $this->db->limit(3);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get post_events by user
    public function get_paginated_user_post_events($user_id, $per_page, $offset)
    {
        $this->set_filter_query();
        $this->db->where('post_events.user_id', $user_id);
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit($per_page, $offset);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get post count by user
    public function get_post_count_by_user($user_id)
    {
        $this->set_filter_query();
        $this->db->where('post_events.user_id', $user_id);
        $query = $this->db->get('post_events');
        return $query->num_rows();
    }

    //get search post_events
    public function get_paginated_search_post_events($q, $per_page, $offset)
    {
        $this->set_filter_query();
        $this->db->group_start();
        $this->db->like('post_events.title', $q);
        $this->db->or_like('post_events.content', $q);
        $this->db->or_like('post_events.summary', $q);
        $this->db->group_end();
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit($per_page, $offset);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get search post count
    public function get_search_post_count($q)
    {
        $this->set_filter_query();
        $this->db->group_start();
        $this->db->like('post_events.title', $q);
        $this->db->or_like('post_events.content', $q);
        $this->db->or_like('post_events.summary', $q);
        $this->db->group_end();
        $query = $this->db->get('post_events');
        return $query->num_rows();
    }

    //get latest post_events by category
    public function get_latest_post_events_by_category($category, $count)
    {
        $this->set_filter_query();
        if ($category->parent_id == 0) {
            $this->db->where('category_id', $category->id);
        } else {
            $this->db->where('subcategory_id', $category->id);
        }
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit($count);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get post_events by topcategory
    public function get_post_events_by_category($category_id)
    {
        $this->set_filter_query();
        $this->db->where('category_id', $category_id);
        $this->db->order_by('post_events.created_at', 'DESC');
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get paginated category post_events
    public function get_paginated_category_post_events($type, $category_id, $per_page, $offset)
    {
        if ($type == "parent") {
            $this->set_filter_query();
            $this->db->where('category_id', $category_id);
            $this->db->order_by('post_events.created_at', 'DESC');
            $this->db->limit($per_page, $offset);
            $query = $this->db->get('post_events');
            return $query->result();
        } else {
            $this->set_filter_query();
            $this->db->where('subcategory_id', $category_id);
            $this->db->order_by('post_events.created_at', 'DESC');
            $this->db->limit($per_page, $offset);
            $query = $this->db->get('post_events');
            return $query->result();
        }

    }

    //get post count by category
    public function get_post_count_by_category($type, $category_id)
    {
        if ($type == "parent") {
            $this->set_filter_query();
            $this->db->where('post_events.category_id', $category_id);
            $query = $this->db->get('post_events');
            return $query->num_rows();
        } else {
            $this->set_filter_query();
            $this->db->where('post_events.subcategory_id', $category_id);
            $query = $this->db->get('post_events');
            return $query->num_rows();
        }
    }

    //get rss latest post_events
    public function get_rss_latest_post_events($limit)
    {
        $this->set_filter_query();
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get rss post_events by category
    public function get_rss_post_events_by_category($category)
    {
        $this->set_filter_query();
        if ($category->parent_id == 0) {
            $this->db->where('category_id', $category->id);
        } else {
            $this->db->where('subcategory_id', $category->id);
        }
        $this->db->order_by('post_events.created_at', 'DESC');
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get post_events by tag
    public function get_paginated_tag_post_events($tag_slug, $per_page, $offset)
    {
        $this->set_filter_query();
        $this->db->join('tags', 'post_events.id = tags.post_id');
        $this->db->where('tags.tag_slug', $tag_slug);
        $this->db->order_by('post_events.created_at', 'DESC');
        $this->db->limit($per_page, $offset);
        $query = $this->db->get('post_events');
        return $query->result();
    }

    //get post count by tag
    public function get_post_count_by_tag($tag_slug)
    {
        $this->set_filter_query();
        $this->db->join('tags', 'post_events.id = tags.post_id');
        $this->db->where('tags.tag_slug', $tag_slug);
        $query = $this->db->get('post_events');
        return $query->num_rows();
    }

    //get previous post
    public function get_previous_post($id)
    {
        $sql = "SELECT * FROM post_events WHERE post_events.created_at <= CURRENT_TIMESTAMP() AND post_events.visibility=1 AND post_events.status=1 AND post_events.id < ? AND post_events.lang_id= ? ORDER BY post_events.created_at DESC LIMIT 1";
        $query = $this->db->query($sql, array($id, $this->selected_lang->id));
        return $query->row();
    }

    //get next post
    public function get_next_post($id)
    {
        $sql = "SELECT * FROM post_events WHERE post_events.created_at <= CURRENT_TIMESTAMP() AND post_events.visibility=1 AND post_events.status=1 AND post_events.id > ? AND post_events.lang_id= ? ORDER BY post_events.created_at DESC LIMIT 1";
        $query = $this->db->query($sql, array($id, $this->selected_lang->id));
        return $query->row();
    }

    //increase post hit
    public function increase_post_hit($post)
    {
        if (!empty($post)):
            if (!isset($_COOKIE['var_post_' . $post->id])):
                //increase hit
                helper_setcookie('var_post_' . $post->id, '1');
                $data = array(
                    'hit' => $post->hit + 1
                );

                $this->db->where('id', $post->id);
                $this->db->update('post_events', $data);

                $data = array(
                    'post_id' => $post->id,
                );
                $this->db->insert('post_hits', $data);
            endif;
        endif;
    }

}
