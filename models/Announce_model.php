<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announce_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'name' => $this->input->post('name', true),
            'name_slug' => $this->input->post('name_slug', true),
            'parent_id' => $this->input->post('parent_id', true),
            'description' => $this->input->post('description', false),
            'keywords' => $this->input->post('keywords', true),
            'color' => $this->input->post('color', true),
            'announce_order' => $this->input->post('announce_order', true),
            'show_at_homepage' => $this->input->post('show_at_homepage', true),
            'show_on_menu' => $this->input->post('show_on_menu', true),
            'block_type' => $this->input->post('block_type', true),
            'startDate' => $this->input->post('startDate', true),
            'endDate' => $this->input->post('endDate', true),
        );
        return $data;
    }

    //add announce
    public function add_announce()
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('announce', $data);
    }

    //add subannounce
    public function add_subannounce()
    {
        $data = $this->input_values();

        $announce = helper_get_announce($data["parent_id"]);

        if ($announce) {
            $data["color"] = $announce->color;
        } else {
            $data["color"] = "#0a0a0a";
        }

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('announce', $data);
    }

    //update slug
    public function update_slug($id)
    {
        $announce = $this->get_announce($id);

        if (empty($announce->name_slug) || $announce->name_slug == "-") {
            $data = array(
                'name_slug' => $announce->id
            );
            $this->db->where('id', $id);
            $this->db->update('announce', $data);
        } else {
            if ($this->check_is_slug_unique($announce->name_slug, $id) == true) {
                $data = array(
                    'name_slug' => $announce->name_slug . "-" . $announce->id
                );

                $this->db->where('id', $id);
                $this->db->update('announce', $data);
            }
        }
    }

    //check slug
    public function check_is_slug_unique($slug, $id)
    {
        $this->db->where('announce.name_slug', $slug);
        $this->db->where('announce.id !=', $id);
        $query = $this->db->get('announce');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    //get announce
    public function get_announce($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('announce');
        return $query->row();
    }

    //get announce by slug
    public function get_announce_by_slug($slug)
    {
        $this->db->where('name_slug', $slug);
        $query = $this->db->get('announce');
        return $query->row();
    }

    //check announce slug
    public function check_announce_slug($slug, $id)
    {
        $this->db->where('name_slug', $slug);
        $this->db->where('id !=', $id);
        $query = $this->db->get('announce');
        return $query->row();
    }

    //get all top announce
    public function get_all_announce()
    {
        $this->db->where('parent_id', 0);
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get announce
    public function get_announces()
    {
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }
    //get announce
    public function get_announce_active()
    {
        $date = date("Y-m-d H:i:s");
        $this->db->where("startDate <= '{$date}'");
        $this->db->where("endDate >= '{$date}'");
        $this->db->where('show_on_menu',1);
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get top announce
    public function get_top_announce()
    {
        $this->db->where('announce.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get top announce
    public function get_top_announce_by_lang($lang_id)
    {
        $this->db->where('announce.lang_id', $lang_id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get subannounce
    public function get_subannounce()
    {
        $this->db->where('announce.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id !=', 0);
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get all top announce
    public function get_all_top_announce()
    {
        $this->db->where('parent_id', 0);
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get all subannounce
    public function get_all_subannounce()
    {
        $this->db->where('parent_id !=', 0);
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get subannounce by id
    public function get_subannounce_by_parent_id($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get sitemap announce
    public function get_sitemap_announce()
    {
        $this->db->order_by('announce_order');
        $query = $this->db->get('announce');
        return $query->result();
    }

    //get announce count
    public function get_announce_count()
    {
        $query = $this->db->get('announce');
        return $query->num_rows();
    }

    //update announce
    public function update_announce($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $announce = helper_get_announce($id);
        //check if parent
        if ($announce->parent_id == 0) {
            $this->update_subannounce_color($id, $data["color"]);
        } else {
            $announce = helper_get_announce($data["parent_id"]);
            if ($announce) {
                $data["color"] = $announce->color;
            } else {
                $data["color"] = "#0a0a0a";
            }
        }

        $this->db->where('id', $id);
        return $this->db->update('announce', $data);
    }

    //update subannounce
    public function update_subannounce($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $this->db->where('id', $id);
        return $this->db->update('announce', $data);
    }

    //update subannounce color
    public function update_subannounce_color($parent_id, $color)
    {
        $announce = $this->get_subannounce_by_parent_id($parent_id);
        if (!empty($announce)) {
            foreach ($announce as $item) {
                $data = array(
                    'color' => $color,
                );
                $this->db->where('parent_id', $parent_id);
                return $this->db->update('announce', $data);
            }
        }
    }

    //delete announce
    public function delete_announce($id)
    {
        $announce = $this->get_announce($id);

        if (!empty($announce)) {
            $this->db->where('id', $id);
            return $this->db->delete('announce');
        } else {
            return false;
        }
    }

}
