<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Industry_model extends CI_Model
{
    //input values
    public function input_values()
    {
        $data = array(
            'lang_id' => $this->input->post('lang_id', true),
            'name' => $this->input->post('name', true),
            'name_slug' => $this->input->post('name_slug', true),
            'parent_id' => $this->input->post('parent_id', true),
            'description' => $this->input->post('description', true),
            'keywords' => $this->input->post('keywords', true),
            'color' => $this->input->post('color', true),
            'industry_order' => $this->input->post('industry_order', true),
            'show_at_homepage' => $this->input->post('show_at_homepage', true),
            'show_on_menu' => $this->input->post('show_on_menu', true),
            'block_type' => $this->input->post('block_type', true),
            'startDate' => $this->input->post('startDate', true),
            'startDate2' => $this->input->post('startDate2', true),
            'endDate' => $this->input->post('endDate', true),
            'endDate2' => $this->input->post('endDate2', true),
        );
        return $data;
    }

    //add industry
    public function add_industry()
    {
        $data = $this->input_values();
        if (empty($data["name_slug"])) {            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');
        return $this->db->insert('industry', $data);
    }

    //add subindustry
    public function add_subindustry()
    {
        $data = $this->input_values();

        $industry = helper_get_industry($data["parent_id"]);

        if ($industry) {
            $data["color"] = $industry->color;
            $data["startDate"] = $industry->startDate;
            $data["startDate2"] = $industry->startDate2;
            $data["endDate"] = $industry->endDate;
            $data["endDate2"] = $industry->endDate2;
            $data["color"] = $industry->color;
        } else {
            $data["color"] = "#0a0a0a";
            $data["startDate"] = "";
            $data["startDate2"] = "";
            $data["endDate"] = "";
            $data["endDate2"] = "";
        }

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert('industry', $data);
    }

    //update slug
    public function update_slug($id)
    {
        $industry = $this->get_industry($id);

        if (empty($industry->name_slug) || $industry->name_slug == "-") {
            $data = array(
                'name_slug' => $industry->id
            );
            $this->db->where('id', $id);
            $this->db->update('industry', $data);
        } else {
            if ($this->check_is_slug_unique($industry->name_slug, $id) == true) {
                $data = array(
                    'name_slug' => $industry->name_slug . "-" . $industry->id
                );

                $this->db->where('id', $id);
                $this->db->update('industry', $data);
            }
        }
    }

    //check slug
    public function check_is_slug_unique($slug, $id)
    {
        $this->db->where('industry.name_slug', $slug);
        $this->db->where('industry.id !=', $id);
        $query = $this->db->get('industry');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    //get industry
    public function get_industry($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('industry');
        return $query->row();
    }

    //get industry by slug
    public function get_industry_by_slug($slug)
    {
        $this->db->where('name_slug', $slug);
        $query = $this->db->get('industry');
        return $query->row();
    }

    //check industry slug
    public function check_industry_slug($slug, $id)
    {
        $this->db->where('name_slug', $slug);
        $this->db->where('id !=', $id);
        $query = $this->db->get('industry');
        return $query->row();
    }

    //get all top industry
    public function get_all_industry()
    {
        $this->db->where('parent_id', 0);
        $this->db->order_by('industry_order');
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get industry
    public function get_industrys()
    {
        $this->db->order_by('industry_order');
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get top industry
    public function get_top_industry()
    {
        $this->db->where('industry.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('industry_order');
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get top industry
    public function get_top_industry_by_lang($lang_id)
    {
        $this->db->where('industry.lang_id', $lang_id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('industry_order');
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get subindustry
    public function get_subindustry()
    {
        $this->db->where('industry.lang_id', $this->selected_lang->id);
        $this->db->where('parent_id !=', 0);
        $this->db->order_by('industry_order');
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get all top industry
    public function get_all_top_industry()
    {
        $this->db->where('parent_id', 0);
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get all subindustry
    public function get_all_subindustry()
    {
        $this->db->where('parent_id !=', 0);
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get subindustry by id
    public function get_subindustry_by_parent_id($parent_id)
    {
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get sitemap industry
    public function get_sitemap_industry()
    {
        $this->db->order_by('industry_order');
        $query = $this->db->get('industry');
        return $query->result();
    }

    //get industry count
    public function get_industry_count()
    {
        $query = $this->db->get('industry');
        return $query->num_rows();
    }

    //update industry
    public function update_industry($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $industry = helper_get_industry($id);
        //check if parent
        if ($industry->parent_id == 0) {
            $this->update_subindustry_color($id, $data);
        } else {
            $industry = helper_get_industry($data["parent_id"]);
            if ($industry) {
                $data["color"] = $industry->color;
            } else {
                $data["color"] = "#0a0a0a";
            }
        }

        $this->db->where('id', $id);
        return $this->db->update('industry', $data);
    }

    //update subindustry
    public function update_subindustry($id)
    {
        $data = $this->input_values();

        if (empty($data["name_slug"])) {
            //slug for title
            $data["name_slug"] = str_slug(trim($data["name"]));
        } else {
            $data["name_slug"] = str_slug(trim($data["name_slug"]));
        }

        $this->db->where('id', $id);
        return $this->db->update('industry', $data);
    }

    //update subindustry color
    public function update_subindustry_color($parent_id, $data)
    {
        $industry = $this->get_subindustry_by_parent_id($parent_id);
        if (!empty($industry)) {
            foreach ($industry as $item) {
                $data = array(
                    'color' => $data['color'],
                    'startDate' => $data['startDate'],
                    'endDate' => $data['endDate'],
                    'startDate2' => $data['startDate2'],
                    'endDate2' => $data['endDate2']
                );
                $this->db->where('parent_id', $parent_id);
                return $this->db->update('industry', $data);
            }
        }
    }

    //delete industry
    public function delete_industry($id)
    {
        $industry = $this->get_industry($id);

        if (!empty($industry)) {
            $this->db->where('id', $id);
            return $this->db->delete('industry');
        } else {
            return false;
        }
    }

}
