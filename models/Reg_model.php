<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reg_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // $this->db = $this->load->database('default2',true);
    }

    public function get_isp($page)
    {
        $this->db->where('deleted',0);
        $this->db->order_by('OldRegisterID','DESC');
        $query = $this->db->get('tb_nia_isp',30,$page);
        return $query->result();
    }
    public function get_isps()
    {
        $this->db->where('deleted',0);
        $this->db->order_by('OldRegisterID','DESC');
        $query = $this->db->get('tb_nia_isp');
        return $query->result();
    }
    public function get_smes()
    {
        $this->db->where('deleted',0);
        $this->db->order_by('Gen_ID','DESC');
        $query = $this->db->get('tb_nia_smes');
        return $query->result();
    }
    public function get_project()
    {
        $this->db->order_by('Project_Code','DESC');
        $query = $this->db->get('tb_project');
        return $query->result();
    }

    public function get_isp_by_id($page)
    {
        $this->db->where('OldRegisterID',$page);
        $this->db->where('deleted',0);
        $this->db->order_by('OldRegisterID','DESC');
        $query = $this->db->get('tb_nia_isp');
        return $query->row();
    }
}
