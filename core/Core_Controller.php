<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Core_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //settings
        $global_data['general_settings'] = $this->settings_model->get_general_settings();
        $this->general_settings = $global_data['general_settings'];

        //set timezone
        date_default_timezone_set($this->general_settings->timezone);
        //lang base url
        $global_data['lang_base_url'] = base_url();
        //languages
        $global_data['languages'] = $this->language_model->get_active_languages();
        //site lang
        $global_data['site_lang'] = $this->language_model->get_language($this->general_settings->site_lang);
        if (empty($global_data['site_lang'])) {
            $global_data['site_lang'] = $this->language_model->get_language('1');
        }
        $global_data['selected_lang'] = $global_data['site_lang'];

        //set language
        $lang_segment = $this->uri->segment(1);
        foreach ($global_data['languages'] as $lang) {
            if ($lang_segment == $lang->short_form) {
                if ($this->general_settings->multilingual_system == 1):
                    $global_data['selected_lang'] = $lang;
                    $global_data['lang_base_url'] = base_url() . $lang->short_form . "/";
                else:
                    redirect(base_url());
                endif;
            }
        }

        $this->selected_lang = $global_data['selected_lang'];
        $this->lang_base_url = $global_data['lang_base_url'];
        if (!file_exists(APPPATH . "language/" . $this->selected_lang->folder_name)) {
            echo "Language folder doesn't exists!";
            exit();
        }
        $this->config->set_item('language', $global_data['selected_lang']->folder_name);
        $this->lang->load("site_lang", $global_data['selected_lang']->folder_name);

        $global_data['rtl'] = false;
        if ($global_data['selected_lang']->text_direction == "rtl") {
            $global_data['rtl'] = true;
        }
        $this->rtl = $global_data['rtl'];
        $this->language_id = $global_data['selected_lang']->id;

        //set lang base url
        if ($this->general_settings->site_lang == $this->language_id) {
            $global_data['lang_base_url'] = base_url();
        } else {
            $global_data['lang_base_url'] = base_url() . $global_data['selected_lang']->short_form . "/";
        }
        $this->lang_base_url = $global_data['lang_base_url'];

        //check remember
        $this->auth_model->check_remember();

        //check auth
        $this->auth_check = auth_check();
        if ($this->auth_check) {
            $this->auth_user = user();
        }

        $global_data['vsettings'] = $this->visual_settings_model->get_settings();
        $global_data['settings'] = $this->settings_model->get_settings($global_data['selected_lang']->id);
        $this->settings = $global_data['settings'];

        //get site primary font
        $this->config->load('fonts');
        $global_data['primary_font'] = $this->general_settings->primary_font;
        $global_data['primary_font_family'] = $this->config->item($global_data['primary_font'] . '_font_family');
        $global_data['primary_font_url'] = $this->config->item($global_data['primary_font'] . '_font_url');

        //get site secondary font
        $global_data['secondary_font'] = $this->general_settings->secondary_font;
        $global_data['secondary_font_family'] = $this->config->item($global_data['secondary_font'] . '_font_family');
        $global_data['secondary_font_url'] = $this->config->item($global_data['secondary_font'] . '_font_url');

        //get site tertiary font
        $global_data['tertiary_font'] = $this->general_settings->tertiary_font;
        $global_data['tertiary_font_family'] = $this->config->item($global_data['tertiary_font'] . '_font_family');
        $global_data['tertiary_font_url'] = $this->config->item($global_data['tertiary_font'] . '_font_url');

        //bg images
        $global_data["img_bg_mid"] = base_url() . "assets/img/img_bg_mid.jpg";
        $global_data["img_bg_sm"] = base_url() . "assets/img/img_bg_sm.jpg";
        $global_data["img_bg_sl"] = base_url() . "assets/img/img_bg_sl.jpg";
        $global_data["img_bg_lg"] = base_url() . "assets/img/img_bg_lg.jpg";
        $global_data["img_bg_sm_footer"] = base_url() . "assets/img/img_bg_sm_footer.jpg";
        $global_data["segment"] = $this->uri->segment_array();


        $global_data["fac_categories"]    = $this->category_model->get_fac_categories();
        $global_data["fac_subcategories"] = $this->category_model->get_fac_subcategories();

        //update last seen
        $this->global_data_segment = $global_data["segment"];
        $this->auth_model->update_last_seen();

        $this->load->vars($global_data);
    }

}

class Home_Core_Controller extends Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$_SESSION["last_visit"] =0;
        /*if(isset($_SESSION["last_visit"] )&&$_SESSION["last_visit"] != date('Y-m-d')){
            
            //$this->visitor_model->addCount();
            //echo "test1";

            $this->visitor_model->addCount() ;
            $_SESSION["last_visit"] =date('Y-m-d');
        }else{
            //echo "test2";

            //echo json_encode($this->visitor_model->addCount()) ;
            if(!isset($_SESSION["last_visit"] )){
            $this->visitor_model->addCount();

            $_SESSION["last_visit"] =date('Y-m-d');

            }
        } */
        //categories
        $this->categories = get_cached_data('categories');
        if (empty($this->categories)) {
            $this->categories = $this->category_model->get_categories();
            set_cache_data('categories', $this->categories);
        }

        //categories
        $this->pages = get_cached_data('pages');
        if (empty($this->pages)) {
            $this->pages = $this->page_model->get_all_pages();
            set_cache_data('pages', $this->pages);
        }


        $this->random_posts = $this->post_model->get_random_posts(3);
        //popular posts
        //slow
/*
        $this->popular_posts_week = get_cached_data('popular_posts_week');
        if (empty($this->popular_posts_week)) {
            $this->popular_posts_week = $this->post_model->get_popular_posts(7);
            set_cache_data('popular_posts_week', $this->popular_posts_week);
        }
        $this->popular_posts_month = get_cached_data('popular_posts_month');
        if (empty($this->popular_posts_month)) {
            $this->popular_posts_month = $this->post_model->get_popular_posts(30);
            set_cache_data('popular_posts_month', $this->popular_posts_week);
        }
        $this->popular_posts_year = get_cached_data('popular_posts_year');
        if (empty($this->popular_posts_year)) {
            $this->popular_posts_year = $this->post_model->get_popular_posts(365);
            set_cache_data('popular_posts_year', $this->popular_posts_year);
        }
        //recommended posts
        // $this->recommended_posts = get_cached_data('recommended_posts');
        // if (empty($this->recommended_posts)) {
        //     $this->recommended_posts = $this->post_model->get_recommended_posts();
        //     set_cache_data('recommended_posts', $this->recommended_posts);
        // }

        $this->recommended_posts = $this->post_model->get_recommended_posts();
        //category posts
        $this->category_posts = get_cached_data('category_posts');
        if (empty($this->category_posts)) {
            $this->category_posts = $this->generate_posts_array_by_category();
            set_cache_data('category_posts', $this->category_posts);
        }
        //widgets
        $this->widgets = get_cached_data('widgets');
        if (empty($this->widgets)) {
            $this->widgets = $this->widget_model->get_widgets();
            set_cache_data('widgets', $this->widgets);
        }
        //random tags
        $this->random_tags = get_cached_data('random_tags');
        if (empty($this->random_tags)) {
            $this->random_tags = $this->tag_model->get_random_tags();
            set_cache_data('random_tags', $this->random_tags);
        }*/
        
        //slow
        
        $global_data['polls'] = $this->poll_model->get_polls();







        $this->total_posts_count = $this->post_model->get_post_count_by_lang($this->selected_lang->id);
        $this->post_events = $this->postevents_admin_model->get_events(10);

        $this->menu_links = $this->navigation_model->get_menu_links();


        //Social Login
        $global_data['fac_link'] = $this->config->item('fac_link');
        $global_data['fb_login_state'] = 0;
        $global_data['google_login_state'] = 0;
        $global_data['googleapi'] = 'AIzaSyDKeFCp-UmWZhZqT7D9ukelQb1cV9I22tc';

        if (!empty($this->general_settings->facebook_app_id)) {
            $global_data['fb_login_state'] = 1;
        }
        if (!empty($this->general_settings->google_client_id)) {
            $global_data['google_login_state'] = 1;
        }
        //recaptcha status
        $global_data['recaptcha_status'] = true;
        if (empty($this->general_settings->recaptcha_site_key) || empty($this->general_settings->recaptcha_secret_key)) {
            $global_data['recaptcha_status'] = false;
        }
        $this->recaptcha_status = $global_data['recaptcha_status'];

        //fposts
        $global_data['featured_posts'] = get_cached_data('featured_posts'); // โพสแนะนำ
        // if (empty($global_data['featured_posts'])) {
            $global_data['featured_posts'] = $this->post_model->get_featured_posts();
            set_cache_data('featured_posts', $global_data['featured_posts']);
        // }

        //featured_posts
        $global_data['latest_posts'] = get_cached_data('latest_posts'); // ข่าวประชาสัมพัมธ์ ข่าวทั้งหมด
        if (empty($global_data['latest_posts'])) {
            $global_data['latest_posts'] = $this->post_model->get_rss_latest_posts(5);
            set_cache_data('latest_posts', $global_data['latest_posts']);
        }

        //featured_posts
        $global_data['channel_posts'] = get_cached_data('channel_posts'); // cdti channel
        if (empty($global_data['channel_posts'])) {
            $global_data['channel_posts'] = $this->post_model->get_rss_posts_by_category_limit(13,6);
            set_cache_data('channel_posts', $global_data['channel_posts']);
        }


        $main_category_ID1 = $this->gallery_category_model->get_bymain_categories(1);
        foreach ($main_category_ID1 as $key => $value) {
            $rs = $this->gallery_model->get_all_images_by_category($value->id);
            if($rs){
                 $image = $rs[0];
                 $global_data['main_category1_cover'][$value->id] = $image;
            }
        }



        $global_data['other_posts'] = []; /// ข่าวที่เกี่ยวข้อง
        $global_data['event_calendar'] = $this->postevents_admin_model->get_events(10);
        $global_data['today_visit'] = $this->visitor_model->get_today();
        $global_data['yesterday_visit'] = $this->visitor_model->get_yesterday();
        $global_data['last_30_days_visit'] = $this->visitor_model->get_last_thirty_entries();
        $global_data['all_visit'] = $this->visitor_model->get_total();

        $global_data['categories']  =  $this->category_model->get_all_categories();
        $global_data['tags_show']  =  $this->category_model->get_tags_categories();
        $global_data['banners'] = $this->gallery_model->get_images_by_category(2);
        $global_data['teams_type3'] = $this->gallery_model->get_images_by_category_type(3,3);
        $global_data['teams_type2'] = $this->gallery_model->get_images_by_category_type(3,2);
        // dd($global_data['teams_type2']);
        $global_data['links']   = $this->gallery_model->get_all_images_by_category(1);
        $this->load->vars($global_data);
    }

    
    
    //generate posts array by category
    public function generate_posts_array_by_category()
    {
        $array_posts = array();
        if (!empty($this->categories)) {
            foreach ($this->categories as $category) {
                $array_posts['category_' . $category->id] = get_latest_posts_by_category($category, 8);
            }
        }
        return $array_posts;
    }

    //verify recaptcha
    public function recaptcha_verify_request()
    {
        if (!$this->recaptcha_status) {
            return true;
        }

        $this->load->library('recaptcha');
        $recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) && $response['success'] === true) {
                return true;
            }
        }
        return false;
    }

    public function paginate($url, $total_rows)
    {
        $per_page = $this->general_settings->pagination_per_page;
        //initialize pagination
        $page = $this->security->xss_clean($this->input->get('page'));
        if (empty($page)) {
            $page = 0;
        }
        if ($page != 0) {
            $page = $page - 1;
        }
        $config['num_links'] = 4;
        $config['base_url'] = $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['reuse_query_string'] = true;
        $config['attributes'] = array('class' => 'page-link');

        $config['full_tag_open'] = '<ul class="pagination d-inline-flex mx-auto">';
        $config['full_tag_close'] = '</ul><!--pagination-->';

        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $this->pagination->initialize($config);
        return array('per_page' => $per_page, 'offset' => $page * $per_page, 'current_page' => $page + 1);
    }

}

class Admin_Core_Controller extends Core_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!auth_check()) {
            redirect(admin_url().'login');
        }
        // echo 22;exit;
        cls_category();

        $global_data['images'] = $this->file_model->get_images(48);
        //$global_data['audios'] = $this->file_model->get_audios(48);
        //$global_data['videos'] = $this->file_model->get_videos(48);
        $global_data['pdfs']   = $this->file_model->get_pdfs(48);
        $global_data['post_type_config']   = $this->config->item('post_type_config');
        $global_data['is_show']   = $this->config->item('is_show');

        $categoriesx = $this->category_model->get_all_categories_tag();
        $datax = [];
        foreach ($categoriesx as $key => $value) {
            $datax[] = $value->name_slug;
        }
        $global_data['categories_data'] = $datax;

        $this->load->vars($global_data);
    }

    public function paginate($url, $total_rows)
    {
        //initialize pagination
        $page = $this->security->xss_clean($this->input->get('page'));
        $per_page = $this->input->get('show', true);
        if (empty($page)) {
            $page = 0;
        }

        if ($page != 0) {
            $page = $page - 1;
        }

        if (empty($per_page)) {
            $per_page = 15;
        }

        $config['num_links'] = 4;
        $config['base_url'] = $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['reuse_query_string'] = true;
        $this->pagination->initialize($config);

        return array('per_page' => $per_page, 'offset' => $page * $per_page);
    }
}


class Pdf_Core_Controller extends Admin_Core_Controller
{
    private $permission_key;
    private $type;
    private $add_title;
    private $update_title;
    private $controller;
    private $admin_model;

    public function __construct($type , $controller , $permission_key)
    {
        parent::__construct();
        get_posts_latest();
        if ($this->general_settings->email_verification == 1 && user()->email_status == 0) {
            $this->session->set_flashdata('error', trans("msg_confirmed_required"));
            redirect(lang_base_url() . "settings");
        }
        $this->type = $type;
        $this->controller = $controller;
        $this->permission_key = $permission_key;
        $this->add_title = 'add_'.$this->type;
        $this->update_title = 'update_'.$this->type;
    }

    public function check_permission()
    {
        check_permission($this->permission_key);
    }

    /**
     * Add Post
     */
    public function add_post()
    {
        $this->check_permission();
        $data['title'] = trans($this->type);
        $data['add_title'] = trans($this->add_title);
        $data['type']  = $this->type;
        $data['top_categories'] = $this->category_model->get_top_categories();
        $data['controller'] = $this->controller;
        // dd($data);
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/pdf/add_post', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Add Audio
     */
    public function add_audio()
    {
        $this->check_permission();
        $data['title'] = trans("add_audio");
        $data['top_categories'] = $this->category_model->get_top_categories();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/add_audio', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Add Video
     */
    public function add_video()
    {
        $this->check_permission();
        $data['title'] = trans("add_video");


        $data['top_categories'] = $this->category_model->get_top_categories();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/add_video', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Add Post Post
     */
    public function add_post_post()
    {
        $this->check_permission();
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');
        $this->form_validation->set_rules('summary', trans("summary"), 'xss_clean|max_length[5000]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');
        $this->form_validation->set_rules('optional_url', trans("optional_url"), 'xss_clean|max_length[1000]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
            redirect($this->agent->referrer());
        } else {
            $post_type = $this->type;
            //if post added
            if ($this->post_pdf_model->add_post($post_type)) {
                //last id
                $last_id = $this->db->insert_id();
                //update slug
                $this->post_pdf_model->update_slug($last_id);
                //insert post tags
                $this->tag_model->add_post_tags($last_id);

                $this->post_file_model->add_post_files($last_id);

                //send post
                $send_post_to_subscribes = $this->input->post('send_post_to_subscribes', true);
                if ($send_post_to_subscribes) {
                    $this->send_to_all_subscribers($last_id);
                }
                $this->session->set_flashdata('success', trans("post") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }


    /**
     * Send to All  Subscribers
     */
    public function send_to_all_subscribers($post_id)
    {
        $post = $this->post_pdf_model->get_post($post_id);
        if ($this->selected_lang->id == $post->lang_id) {
            $link = base_url() . $post->title_slug;
        } else {
            $lang = get_language($post->lang_id);
            $link = base_url() . $lang->short_form . "/" . $post->title_slug;
        }
        if (!empty($post)) {
            $subject = $post->title;
            $message = "<a href='" . $link . "'><img src='" . base_url() . $post->image_default . "' alt='' style='max-width: 100%; height: auto;'></a><br><br>" . $post->content;
            $this->load->model("email_model");
            $data['subscribers'] = $this->newsletter_model->get_subscribers();
            foreach ($data['subscribers'] as $item) {
                $this->email_model->send_email($item->email, $subject, $message, true, $link);
            }
        }
    }


    /**
     * Posts
     */
    public function posts()
    {
        check_permission('add_manual');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . $this->type;
        $data['detail'] = admin_url() . $this->type;
        $data['list_type'] = $this->type;
        $data['title'] = trans($this->type);
        $data['type'] = $this->type;
        
        $data['top_categories'] = $this->category_model->get_top_categories();

        //get paginated posts
        $pagination = $this->paginate(admin_url() . $this->type, $this->post_pdf_model->get_paginated_posts_count($this->type,$this->type));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($this->type,$pagination['per_page'], $pagination['offset'], $this->type,$data['list_type']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/pdf/posts', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function posts_econ()
    {
        $this->check_permission();
        $data['title'] = trans('posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "posts";
        $data['list_type'] = "posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'posts', $this->post_pdf_model->get_paginated_posts_econ_count('posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts_econ($pagination['per_page'], $pagination['offset'], 'posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Slider Posts
     */
    public function slider_posts()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('slider_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "slider-posts";
        $data['list_type'] = "slider_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'slider-posts', $this->post_pdf_model->get_paginated_posts_count('slider_posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'slider_posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Featured Posts
     */
    public function featured_posts()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('featured_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "featured-posts";
        $data['list_type'] = "featured_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'featured-posts', $this->post_pdf_model->get_paginated_posts_count('featured_posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'featured_posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Breaking news
     */
    public function breaking_news()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('breaking_news');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "breaking-news";
        $data['list_type'] = "breaking_news";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'breaking-news', $this->post_pdf_model->get_paginated_posts_count('breaking_news'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'breaking_news');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Recommended Posts
     */
    public function recommended_posts()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('recommended_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "recommended-posts";
        $data['list_type'] = "recommended_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'recommended-posts', $this->post_pdf_model->get_paginated_posts_count('recommended_posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'recommended_posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Pending Posts
     */
    public function pending_posts()
    {
        $this->check_permission();
        $data['title'] = trans('pending_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "pending-posts";
        $data['list_type'] = "pending_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'pending-posts', $this->post_pdf_model->get_paginated_pending_posts_count());
        $data['posts'] = $this->post_pdf_model->get_paginated_pending_posts($pagination['per_page'], $pagination['offset']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/pending_posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Scheduled Posts
     */
    public function scheduled_posts()
    {
        $this->check_permission();
        $data['title'] = trans('scheduled_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "scheduled-posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'scheduled-posts', $this->post_pdf_model->get_paginated_scheduled_posts_count());
        $data['posts'] = $this->post_pdf_model->get_paginated_scheduled_posts($pagination['per_page'], $pagination['offset']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/scheduled_posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Drafts
     */
    public function drafts()
    {
        $this->check_permission();
        $data['title'] = trans('drafts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "drafts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'drafts', $this->post_pdf_model->get_paginated_drafts_count());
        $data['posts'] = $this->post_pdf_model->get_paginated_drafts($pagination['per_page'], $pagination['offset']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/drafts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Post
     */
    public function update_post($id)
    {
        $this->check_permission();
        $data['type'] = trans($this->type);
        $data['title'] = trans($this->update_title);
        $data['controller'] = $this->controller;

        //get post
        $data['post'] = $this->post_pdf_model->get_post($id);

        if (empty($data['post'])) {
            redirect($this->agent->referrer());
        }
        //combine post tags
        $tags = "";
        $count = 0;
        $tags_array = $this->tag_model->get_post_tags($id);
        foreach ($tags_array as $item) {
            if ($count > 0) {
                $tags .= ",";
            }
            $tags .= $item->tag;
            $count++;
        }

        $data['tags'] = $tags;
        $data['type'] = $data['post']->post_type;

        $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
        $data['categories'] = $this->category_model->get_top_categories_by_lang($data['post']->lang_id);
        $data['subcategories'] = $this->category_model->get_subcategories_by_parent_id($data['post']->category_id);
        $data['categories'] = $this->category_model->get_top_categories();

        $data['users'] = $this->auth_model->get_active_users();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/pdf/update_post', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Post Post
     */
    public function update_post_post()
    {
        $this->check_permission();
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');
        $this->form_validation->set_rules('summary', trans("summary"), 'xss_clean|max_length[5000]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');
        $this->form_validation->set_rules('optional_url', trans("optional_url"), 'xss_clean|max_length[1000]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //post id
            $post_id = $this->input->post('id', true);
            $post_type = $this->type;

            if ($this->post_pdf_model->update_post($post_id, $post_type)) {

                //update slug
                $this->post_pdf_model->update_slug($post_id);

                //update post tags
                $this->tag_model->update_post_tags($post_id);

                $this->post_file_model->add_post_files($post_id);

                $this->session->set_flashdata('success', trans("post") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
            } else {
                $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }

        redirect($this->agent->referrer());
    }

    /**
     * Post Options Post
     */
    public function post_options_post()
    {
        $option = $this->input->post('option', true);
        $id = $this->input->post('id', true);

        $data["post"] = $this->post_pdf_model->get_post($id);

        //check if exists
        if (empty($data['post'])) {
            redirect($this->agent->referrer());
        }

        //if option add remove from slider
        if ($option == 'add-remove-from-slider') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_slider($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_slider"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_slider"));
            }
        } elseif ($option == 'add-remove-from-featured') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_featured($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_featured"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_featured"));
            }
        } elseif ($option == 'add-remove-from-breaking') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_breaking($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_breaking"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_breaking"));
            }
        } elseif ($option == 'add-remove-from-recommended') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_recommended($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_recommended"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_recommended"));
            }
        } elseif ($option == 'approve') {
            check_permission('manage_all_posts');
            if ($this->post_pdf_model->approve_post($id)) {
                $this->session->set_flashdata('success', trans("msg_post_approved"));
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        } elseif ($option == 'publish') {
            $this->check_permission();
            if ($this->post_pdf_model->publish_post($id)) {
                $this->session->set_flashdata('success', trans("msg_published"));
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        } elseif ($option == 'publish_draft') {
            $this->check_permission();
            if ($this->post_pdf_model->publish_draft($id)) {
                $this->session->set_flashdata('success', trans("msg_published"));
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }
        reset_cache_data_on_change();
        redirect($this->agent->referrer());
    }

    /**
     * Delete Post
     */
    public function delete_post()
    {
        $this->check_permission();
        $id = $this->input->post('id', true);
        $data["post"] = $this->post_pdf_model->get_post($id);
        //check if exists
        if (empty($data['post'])) {
            $this->session->set_flashdata('error', trans("msg_error"));
        } else {
            if ($this->post_pdf_model->delete_post($id)) {
                //delete post tags
                $this->tag_model->delete_post_tags($id);
                $this->session->set_flashdata('success', trans("post") . " " . trans("msg_suc_deleted"));
                reset_cache_data_on_change();
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }
    }

    /**
     * Delete Selected Posts
     */
    public function delete_selected_posts()
    {
        $this->check_permission();
        $post_ids = $this->input->post('post_ids', true);
        $this->post_pdf_model->delete_multi_posts($post_ids);
        reset_cache_data_on_change();
    }

    /**
     * Save Featured Post Order
     */
    public function featured_posts_order_post()
    {
        check_permission('manage_all_posts');
        $post_id = $this->input->post('id', true);
        $order = $this->input->post('featured_order', true);
        $this->post_pdf_model->save_featured_post_order($post_id, $order);
        reset_cache_data_on_change();
        redirect($this->agent->referrer());
    }


    /**
     * Save Home Slider Post Order
     */
    public function home_slider_posts_order_post()
    {
        check_permission('manage_all_posts');
        $post_id = $this->input->post('id', true);
        $order = $this->input->post('slider_order', true);
        $this->post_pdf_model->save_home_slider_post_order($post_id, $order);
        reset_cache_data_on_change();
        redirect($this->agent->referrer());
    }


    /**
     * Get Video from URL
     */
    public function get_video_from_url()
    {
        $url = $this->input->post('url', true);

        $this->load->library('video_url_parser');
        echo $this->video_url_parser->get_url_embed($url);

    }


    /**
     * Get Video Thumbnail
     */
    public function get_video_thumbnail()
    {
        $url = $this->input->post('url', true);

        echo $this->file_model->get_video_thumbnail($url);
    }


    /**
     * Delete Additional Image
     */
    public function delete_post_additional_image()
    {
        $file_id = $this->input->post('file_id', true);
        $this->post_file_model->delete_post_additional_image($file_id);
    }

    public function delete_post_files()
    {
        $post_id = $this->input->post('post_id', true);
        $file_id = $this->input->post('file_id', true);
        $this->post_file_model->delete_post_files($post_id,$file_id);
    }


    /**
     * Delete Audio
     */
    public function delete_post_audio()
    {
        $post_id = $this->input->post('post_id', true);
        $audio_id = $this->input->post('audio_id', true);
        $this->post_file_model->delete_post_audio($post_id, $audio_id);
    }

    /**
     * Delete Video
     */
    public function delete_post_video()
    {
        $post_id = $this->input->post('post_id', true);
        $this->post_file_model->delete_post_video($post_id);
    }

    /**
     * Delete Post Main Image
     */
    public function delete_post_main_image()
    {
        $post_id = $this->input->post('post_id', true);
        $this->post_file_model->delete_post_main_image($post_id);
    }


    public function set_pagination_per_page($count)
    {
        $_SESSION['pagination_per_page'] = $count;
        redirect($this->agent->referrer());
    }
}


class Personnel_Pdf_Core_Controller extends Admin_Core_Controller
{
    private $permission_key;
    private $type;
    private $add_title;
    private $update_title;
    private $controller;
    private $admin_model;

    public function __construct($type , $controller , $permission_key)
    {
        parent::__construct();
        get_posts_latest();
        if ($this->general_settings->email_verification == 1 && user()->email_status == 0) {
            $this->session->set_flashdata('error', trans("msg_confirmed_required"));
            redirect(lang_base_url() . "settings");
        }
        $this->type = $type;
        $this->controller = $controller;
        $this->permission_key = $permission_key;
        $this->add_title = 'add_'.$this->type;
        $this->update_title = 'update_'.$this->type;
    }

    public function check_permission()
    {
        check_permission($this->permission_key);
    }

    /**
     * Add Post
     */
    public function add_post()
    {
        $this->check_permission();
        $data['title'] = trans($this->type);
        $data['add_title'] = trans($this->add_title);
        $data['type']  = $this->type;
        $data['top_categories'] = $this->category_model->get_top_categories();
        $data['controller'] = $this->controller;
        // dd($data);
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/pdf/add_post', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Add Audio
     */
    public function add_audio()
    {
        $this->check_permission();
        $data['title'] = trans("add_audio");
        $data['top_categories'] = $this->category_model->get_top_categories();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/add_audio', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Add Video
     */
    public function add_video()
    {
        $this->check_permission();
        $data['title'] = trans("add_video");


        $data['top_categories'] = $this->category_model->get_top_categories();

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/add_video', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Add Post Post
     */
    public function add_post_post()
    {
        $this->check_permission();
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');
        $this->form_validation->set_rules('summary', trans("summary"), 'xss_clean|max_length[5000]');
        $this->form_validation->set_rules('category_id', trans("category"), 'required');
        $this->form_validation->set_rules('optional_url', trans("optional_url"), 'xss_clean|max_length[1000]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
            redirect($this->agent->referrer());
        } else {
            $post_type = $this->type;
            //if post added
            if ($this->post_pdf_model->add_post($post_type)) {
                //last id
                $last_id = $this->db->insert_id();
                //update slug
                $this->post_pdf_model->update_slug($last_id);
                //insert post tags
                $this->tag_model->add_post_tags($last_id);

                $this->post_file_model->add_post_files($last_id);

                //send post
                $send_post_to_subscribes = $this->input->post('send_post_to_subscribes', true);
                if ($send_post_to_subscribes) {
                    $this->send_to_all_subscribers($last_id);
                }
                $this->session->set_flashdata('success', trans("post") . " " . trans("msg_suc_added"));
                reset_cache_data_on_change();
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
                redirect($this->agent->referrer());
            }
        }
    }


    /**
     * Send to All  Subscribers
     */
    public function send_to_all_subscribers($post_id)
    {
        $post = $this->post_pdf_model->get_post($post_id);
        if ($this->selected_lang->id == $post->lang_id) {
            $link = base_url() . $post->title_slug;
        } else {
            $lang = get_language($post->lang_id);
            $link = base_url() . $lang->short_form . "/" . $post->title_slug;
        }
        if (!empty($post)) {
            $subject = $post->title;
            $message = "<a href='" . $link . "'><img src='" . base_url() . $post->image_default . "' alt='' style='max-width: 100%; height: auto;'></a><br><br>" . $post->content;
            $this->load->model("email_model");
            $data['subscribers'] = $this->newsletter_model->get_subscribers();
            foreach ($data['subscribers'] as $item) {
                $this->email_model->send_email($item->email, $subject, $message, true, $link);
            }
        }
    }


    /**
     * Posts
     */
    public function posts()
    {
        check_permission('add_manual');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . $this->type;
        $data['detail'] = admin_url() . $this->type;
        $data['list_type'] = $this->type;
        $data['title'] = trans($this->type);
        $data['type'] = $this->type;
        
        $data['top_categories'] = $this->category_model->get_top_categories();

        //get paginated posts
        $pagination = $this->paginate(admin_url() . $this->type, $this->post_pdf_model->get_paginated_posts_count($this->type,$this->type));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($this->type,$pagination['per_page'], $pagination['offset'], $this->type,$data['list_type']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/pdf/posts', $data);
        $this->load->view('admin/includes/_footer');
    }

    public function posts_econ()
    {
        $this->check_permission();
        $data['title'] = trans('posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "posts";
        $data['list_type'] = "posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'posts', $this->post_pdf_model->get_paginated_posts_econ_count('posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts_econ($pagination['per_page'], $pagination['offset'], 'posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Slider Posts
     */
    public function slider_posts()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('slider_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "slider-posts";
        $data['list_type'] = "slider_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'slider-posts', $this->post_pdf_model->get_paginated_posts_count('slider_posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'slider_posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Featured Posts
     */
    public function featured_posts()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('featured_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "featured-posts";
        $data['list_type'] = "featured_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'featured-posts', $this->post_pdf_model->get_paginated_posts_count('featured_posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'featured_posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Breaking news
     */
    public function breaking_news()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('breaking_news');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "breaking-news";
        $data['list_type'] = "breaking_news";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'breaking-news', $this->post_pdf_model->get_paginated_posts_count('breaking_news'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'breaking_news');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }

    /**
     * Recommended Posts
     */
    public function recommended_posts()
    {
        check_permission('manage_all_posts');
        $data['title'] = trans('recommended_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "recommended-posts";
        $data['list_type'] = "recommended_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'recommended-posts', $this->post_pdf_model->get_paginated_posts_count('recommended_posts'));
        $data['posts'] = $this->post_pdf_model->get_paginated_posts($pagination['per_page'], $pagination['offset'], 'recommended_posts');

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Pending Posts
     */
    public function pending_posts()
    {
        $this->check_permission();
        $data['title'] = trans('pending_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "pending-posts";
        $data['list_type'] = "pending_posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'pending-posts', $this->post_pdf_model->get_paginated_pending_posts_count());
        $data['posts'] = $this->post_pdf_model->get_paginated_pending_posts($pagination['per_page'], $pagination['offset']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/pending_posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Scheduled Posts
     */
    public function scheduled_posts()
    {
        $this->check_permission();
        $data['title'] = trans('scheduled_posts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "scheduled-posts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'scheduled-posts', $this->post_pdf_model->get_paginated_scheduled_posts_count());
        $data['posts'] = $this->post_pdf_model->get_paginated_scheduled_posts($pagination['per_page'], $pagination['offset']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/scheduled_posts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Drafts
     */
    public function drafts()
    {
        $this->check_permission();
        $data['title'] = trans('drafts');
        $data['authors'] = $this->auth_model->get_all_users();
        $data['form_action'] = admin_url() . "drafts";

        //get paginated posts
        $pagination = $this->paginate(admin_url() . 'drafts', $this->post_pdf_model->get_paginated_drafts_count());
        $data['posts'] = $this->post_pdf_model->get_paginated_drafts($pagination['per_page'], $pagination['offset']);

        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/post/drafts', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Post
     */
    public function update_post($id)
    {
        $this->check_permission();
        $data['type'] = trans($this->type);
        $data['title'] = trans($this->update_title);
        $data['controller'] = $this->controller;

        //get post
        $data['post'] = $this->post_pdf_model->get_post($id);

        if (empty($data['post'])) {
            redirect($this->agent->referrer());
        }
        //combine post tags
        $tags = "";
        $count = 0;
        $tags_array = $this->tag_model->get_post_tags($id);
        foreach ($tags_array as $item) {
            if ($count > 0) {
                $tags .= ",";
            }
            $tags .= $item->tag;
            $count++;
        }

        $data['tags'] = $tags;
        $data['type'] = $data['post']->post_type;

        $data['post_images'] = $this->post_file_model->get_post_additional_images($id);
        $data['categories'] = $this->category_model->get_top_categories_by_lang($data['post']->lang_id);
        $data['subcategories'] = $this->category_model->get_subcategories_by_parent_id($data['post']->category_id);

        $data['categories'] = $this->category_model->get_top_categories();

        $data['users'] = $this->auth_model->get_active_users();
        $this->load->view('admin/includes/_header', $data);
        $this->load->view('admin/personnel/update_post', $data);
        $this->load->view('admin/includes/_footer');
    }


    /**
     * Update Post Post
     */
    public function update_post_post()
    {
        $this->check_permission();
        //validate inputs
        $this->form_validation->set_rules('title', trans("title"), 'required|xss_clean|max_length[500]');
        //$this->form_validation->set_rules('summary', trans("summary"), 'xss_clean|max_length[5000]');
        //$this->form_validation->set_rules('category_id', trans("category"), 'required');
        //$this->form_validation->set_rules('optional_url', trans("optional_url"), 'xss_clean|max_length[1000]');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('errors', validation_errors());
            $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
            redirect($this->agent->referrer());
        } else {
            //post id
            $post_id = $this->input->post('id', true);
            $post_type = $this->type;

            if ($this->post_pdf_model->update_post($post_id, $post_type)) {

                //update slug
                $this->post_pdf_model->update_slug($post_id);

                //update post tags
                $this->tag_model->update_post_tags($post_id);

                $this->post_file_model->add_post_files($post_id);

                $this->session->set_flashdata('success', trans("post") . " " . trans("msg_suc_updated"));
                reset_cache_data_on_change();
            } else {
                $this->session->set_flashdata('form_data', $this->post_pdf_model->input_values());
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }

        redirect($this->agent->referrer());
    }

    /**
     * Post Options Post
     */
    public function post_options_post()
    {
        $option = $this->input->post('option', true);
        $id = $this->input->post('id', true);

        $data["post"] = $this->post_pdf_model->get_post($id);

        //check if exists
        if (empty($data['post'])) {
            redirect($this->agent->referrer());
        }

        //if option add remove from slider
        if ($option == 'add-remove-from-slider') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_slider($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_slider"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_slider"));
            }
        } elseif ($option == 'add-remove-from-featured') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_featured($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_featured"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_featured"));
            }
        } elseif ($option == 'add-remove-from-breaking') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_breaking($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_breaking"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_breaking"));
            }
        } elseif ($option == 'add-remove-from-recommended') {
            check_permission('manage_all_posts');
            $result = $this->post_pdf_model->post_add_remove_recommended($id);
            if ($result == "removed") {
                $this->session->set_flashdata('success', trans("msg_remove_recommended"));
            }
            if ($result == "added") {
                $this->session->set_flashdata('success', trans("msg_add_recommended"));
            }
        } elseif ($option == 'approve') {
            check_permission('manage_all_posts');
            if ($this->post_pdf_model->approve_post($id)) {
                $this->session->set_flashdata('success', trans("msg_post_approved"));
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        } elseif ($option == 'publish') {
            $this->check_permission();
            if ($this->post_pdf_model->publish_post($id)) {
                $this->session->set_flashdata('success', trans("msg_published"));
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        } elseif ($option == 'publish_draft') {
            $this->check_permission();
            if ($this->post_pdf_model->publish_draft($id)) {
                $this->session->set_flashdata('success', trans("msg_published"));
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }
        reset_cache_data_on_change();
        redirect($this->agent->referrer());
    }

    /**
     * Delete Post
     */
    public function delete_post()
    {
        $this->check_permission();
        $id = $this->input->post('id', true);
        $data["post"] = $this->post_pdf_model->get_post($id);
        //check if exists
        if (empty($data['post'])) {
            $this->session->set_flashdata('error', trans("msg_error"));
        } else {
            if ($this->post_pdf_model->delete_post($id)) {
                //delete post tags
                $this->tag_model->delete_post_tags($id);
                $this->session->set_flashdata('success', trans("post") . " " . trans("msg_suc_deleted"));
                reset_cache_data_on_change();
            } else {
                $this->session->set_flashdata('error', trans("msg_error"));
            }
        }
    }

    /**
     * Delete Selected Posts
     */
    public function delete_selected_posts()
    {
        $this->check_permission();
        $post_ids = $this->input->post('post_ids', true);
        $this->post_pdf_model->delete_multi_posts($post_ids);
        reset_cache_data_on_change();
    }

    /**
     * Save Featured Post Order
     */
    public function featured_posts_order_post()
    {
        check_permission('manage_all_posts');
        $post_id = $this->input->post('id', true);
        $order = $this->input->post('featured_order', true);
        $this->post_pdf_model->save_featured_post_order($post_id, $order);
        reset_cache_data_on_change();
        redirect($this->agent->referrer());
    }


    /**
     * Save Home Slider Post Order
     */
    public function home_slider_posts_order_post()
    {
        check_permission('manage_all_posts');
        $post_id = $this->input->post('id', true);
        $order = $this->input->post('slider_order', true);
        $this->post_pdf_model->save_home_slider_post_order($post_id, $order);
        reset_cache_data_on_change();
        redirect($this->agent->referrer());
    }


    /**
     * Get Video from URL
     */
    public function get_video_from_url()
    {
        $url = $this->input->post('url', true);

        $this->load->library('video_url_parser');
        echo $this->video_url_parser->get_url_embed($url);

    }


    /**
     * Get Video Thumbnail
     */
    public function get_video_thumbnail()
    {
        $url = $this->input->post('url', true);

        echo $this->file_model->get_video_thumbnail($url);
    }


    /**
     * Delete Additional Image
     */
    public function delete_post_additional_image()
    {
        $file_id = $this->input->post('file_id', true);
        $this->post_file_model->delete_post_additional_image($file_id);
    }

    public function delete_post_files()
    {
        $post_id = $this->input->post('post_id', true);
        $file_id = $this->input->post('file_id', true);
        $this->post_file_model->delete_post_files($post_id,$file_id);
    }


    /**
     * Delete Audio
     */
    public function delete_post_audio()
    {
        $post_id = $this->input->post('post_id', true);
        $audio_id = $this->input->post('audio_id', true);
        $this->post_file_model->delete_post_audio($post_id, $audio_id);
    }

    /**
     * Delete Video
     */
    public function delete_post_video()
    {
        $post_id = $this->input->post('post_id', true);
        $this->post_file_model->delete_post_video($post_id);
    }

    /**
     * Delete Post Main Image
     */
    public function delete_post_main_image()
    {
        $post_id = $this->input->post('post_id', true);
        $this->post_file_model->delete_post_main_image($post_id);
    }


    public function set_pagination_per_page($count)
    {
        $_SESSION['pagination_per_page'] = $count;
        redirect($this->agent->referrer());
    }
}
class API_Controller extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
        }
    }